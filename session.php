<?php

header('Access-Control-Allow-Origin: *');

    // configure the JSON to use in the session.
    $json = '
        {
            "webAppId": "webapp-id-example",
            "allowedOrigins": ["*"],
            "urlSchemeDetails": {
                "host": "172.31.250.52",
                "port": "8443",
                "secure": true
            },
            "voice":
            {
                "username": "anon",
                "displayName": "anon",
                "domain": "172.31.250.52",
                "inboundCallingEnabled": true
            },

             "aed":
             {
                   "accessibleSessionIdRegex":".*",
                   "maxMessageAndUploadSize":"5000",
                   "dataAllowance":"5000"
            },  "additionalAttributes":{
                "AED2.metadata":{
                    "role":"consumer",
                    "name":"bob",
                    "permissions" : {
                        "viewable" : ["default", "Blue", "Magenta", "slider", "Overlay"],
                        "interactive" : ["default", "Blue", "Magenta", "Overlay"]
                    }
                    
                },
                "AED2.allowedTopic":".*"
            }
        }
    ';

    // configure the curl options
    $ch = curl_init("http://172.31.250.52:8080/gateway/sessions/session");
    curl_setopt($ch,CURLOPT_POST, true);
    curl_setopt($ch,CURLOPT_POSTFIELDS, $json);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);	
    curl_setopt($ch, CURLOPT_HTTPHEADER, [         
        'Content-Type: application/json',
        'Content-Length: ' . strlen($json)
    ]);

    // execute HTTP POST & close the connection
    $response = curl_exec($ch);
    curl_close($ch);

    // decode the JSON and pick out the session token
    $decodedJson = json_decode($response);
    $id = $decodedJson->{'sessionid'};

    // echo the ID we've retrieved
    echo $id;

?>
