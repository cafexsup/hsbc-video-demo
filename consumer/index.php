<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://172.31.250.52:8443/assistserver/sdk/web/agent/css/assist-console.css">
    <link rel="stylesheet" href="https://172.31.250.52:8443/assistserver/sdk/web/shared/css/shared-window.css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <style type="text/css">

        #quality {
            height: 25px;
            width: 35px;
            background-repeat: no-repeat;
            background-size: cover;
            top: 10px;
            left: 10px;
            position: absolute;
        }

        #quality.call-quality-good {
            background-color: green;
        }

        #quality.call-quality-moderate {
            background-color: yellow;
        }

        #quality.call-quality-poor {
            background-color: red;
        }

        #quality.no-call {
            display: none;
        }

        #local {
            width: 160px;
            height: 120px;
        }

        #remote {
            width: 320px;
            height: 240px;
        }

        #share {
            width: 640px;
            height: 480px;
        }

        #local, #remote, #share {
            
            border: 1px solid grey;
            margin: 5px;
            border-radius: 5px;
        }

    </style>
</head>
<body>

    <div class="container">

        <div class="col-md-12">

        <img src="../hsbc-logo-large.png" width="80px" class="pull-left clearfix">
        <h1 class="text-center">Consumer Console</h1>
        <br/>
            <div class="col-md-8">

            <!-- remote screen share -->
            <div id="share"></div>

            </div>

                <div class="col-md-4">
            
            <!-- remote video view -->
            <div id="remote"></div>

            <!-- local video preview -->
            <div id="local"></div>

            <!-- indicates the quality of the call -->
            <div id="quality"></div>
            <br/>
            <button id="request-share" class="btn ">Request screen share</button>

            <h3>Annotation Control</h3>
            <form class="annotation-control">
                <input type="color" class="color" value="#ff0000">
                <input type="text" class="opacity" value="0.5">
                <input type="text" class="width" value="2">
                <button type="submit">Update</button>
                <button id="clear">Clear</button>
            </form>

            <div id="push" style="display:none;">
            <h3>Push</h3>
            <form class="push" >
                <input type="text" class="url" placeholder="URL"><br>
                <input type="radio" name="type" value="document" checked>Document<br>
                <input type="radio" name="type" value="content">Content<br>
                <input type="radio" name="type" value="link">Link<br>
                <button type="submit">Push</button>
            </form>
            </div>

            <h3>Mode</h3>
            <form class="mode">
                <input type="radio" name="mode" value="control" checked>Control<br>
                <input type="radio" name="mode" value="draw">Draw<br>
                <input type="radio" name="mode" value="spotlight">Spotlight<br>
            </form>
            </div>
            </div>
    </div>

    <!-- libraries needed for Assist SDK -->
    <script src="https://172.31.250.52:8443/assistserver/sdk/web/shared/js/thirdparty/i18next-1.7.4.min.js"></script>
    <script src="https://172.31.250.52:8443/gateway/adapter.js"></script>
    <script src="https://172.31.250.52:8443/gateway/fusion-client-sdk.js"></script>
    <script src="https://172.31.250.52:8443/assistserver/sdk/web/shared/js/assist-aed.js"></script>
    <script src="https://172.31.250.52:8443/assistserver/sdk/web/shared/js/shared-windows.js"></script>
    <script src="https://172.31.250.52:8443/assistserver/sdk/web/agent/js/assist-console.js"></script>
    
    <script src="https://172.31.250.52:8443/assistserver/sdk/web/agent/js/assist-console-callmanager.js"></script>

    <!-- load jQuery - helpful for DOM manipulation -->
    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>

    <!-- control -->
    <script>

         //params

      $.urlParam = function(name){
          var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
          if (results==null){
             return null;
          }
          else{
             return results[1] || 0;
          }
      }

      var sid = $.urlParam('sid'); // name


      // hook up the hangup button
        $('#hangup').click(function() {
            // if we're on a call - end it!

            $('#remote').html("");

            var call = UC.phone.getCalls()[0];
            if (call) {
                call.end();

            }
            else { alert('Not on a call');}
        });

        function makeCall() {

            UC.phone.setPreviewElement(document.getElementById('local'));
                    
            var call = UC.phone.createCall('sip:'+sid+'@172.31.250.116');
            // var call = UC.phone.createCall('sip:888@172.31.250.116');

            call.onRemoteMediaStream = function() {
                call.setVideoElement(document.getElementById('remote'));
            };

  
            call.dial(true, true);

            AssistAgentSDK.startSupport({correlationId: 'anon', sessionToken: window.globalVar,  url: "https://172.31.250.52:8443" });
            AssistAgentSDK.requestScreenShare();


        };

        console.log("make call")

        UC.onInitialised = makeCall;



        $.post('session.php', function (sessionID) {


            var sessionId = sessionID;  

            UC.start(sessionId);

            window.globalVar = sessionId;

        var AgentModule = function (sessionId) {
            var 
                // native DOM elements
                remote,
                local,
                share,
                quality,

                // jQuery elements for easier event binding
                $requestShare,
                $annotationControl,
                $clear,
                $push,
                $assistMode;

            // ready & init the library
            cacheDom();
            attachEventListeners();
            bindAgentSdkCallbacks();
            linkUi();
            init();

            // caches the DOM elements we will need
            function cacheDom() {
                // extract native DOM elements - AgentSDK works
                // with DOM elements directly
                remote = $('#remote')[0];
                local = $('#local')[0];
                share = $('#share')[0];
                quality = $('#quality')[0];

                // cache as jQuery objects for easier UI event handling
                $requestShare = $('#request-share');
                $annotationControl = $('.annotation-control');
                $clear = $('#clear');
                $push = $('.push');
                $assistMode = $('[name=mode]');
            }

            // binds events in the DOM to their required actions
            // E.g. - start screen sharing, push a document etc.
            function attachEventListeners() {
                // bind to configure the screen
                // $requestShare.click(function (event) {
                //     event.preventDefault();
                //     AssistAgentSDK.requestScreenShare();
                // });


                // configure the annotation
                $annotationControl.submit(function (event) {
                    // prevent normal form submission behaviour
                    event.preventDefault();

                    // extract the vars
                    var $this = $(this);
                    var color = $this.find('.color').val();
                    var opacity = $this.find('.opacity').val();
                    var width = $this.find('.width').val();

                    // update the annotation draw style
                    AssistAgentSDK.setAgentDrawStyle(color, opacity, width);
                });

                $clear.click(function () {
                    AssistAgentSDK.clearSelected();
                });

                // configure document push
                $push.submit(function (event) {
                    // prevent the normal form submission behaviour
                    event.preventDefault();

                    var $this = $(this);
                    var url = $this.find('.url').val();
                    var type = $this.find('[name=type]:checked').val();

                    var method, callback;
                    switch(type) {
                        case 'content':
                            AssistAgentSDK.pushContent(url);
                            break;
                        case 'document':
                            AssistAgentSDK.pushDocument(url);
                            break;
                        case 'link':
                            AssistAgentSDK.pushLink(url);
                            break;
                    }
                });

                // respond to changes in the share type
                $assistMode.change(function () {
                    var $this = $(this);
                    var mode = $this.val();
                    
                    // determine which mode to switch in to
                    switch(mode) {
                        case 'control': 
                            AssistAgentSDK.controlSelected();
                            break;
                        case 'draw':
                            AssistAgentSDK.drawSelected();
                            break;
                        case 'spotlight':
                            AssistAgentSDK.spotlightSelected();
                            break;
                    }
                });
            }

            // set the AssistAgentSDK callbacks
            function bindAgentSdkCallbacks() {

                AssistAgentSDK.setCallEndedCallback(function () {
                    console.log('setCallEndedCallback');

                });

                AssistAgentSDK.setConnectionEstablishedCallback(function () {
                    console.log('setConnectionEstablishedCallback');
                });

                AssistAgentSDK.setConnectionLostCallback(function () {
                    console.log('setConnectionLostCallback');
                });

                AssistAgentSDK.setConnectionReestablishedCallback(function () {
                    console.log('setConnectionReestablishedCallback');
                });

                AssistAgentSDK.setConnectionRetryCallback(function () {
                    console.log('setConnectionRetryCallback');
                });

                AssistAgentSDK.setConsumerJoinedCallback(function () {
                    console.log('setConsumerJoinedCallback');

                 

                    if (AssistAgentSDK.consumerIsOnAssistPages) {
                            $('#push').fadeIn(2000);
                        }

                       
                });

                AssistAgentSDK.setConsumerLeftCallback(function () {
                    console.log('setConsumerLeftCallback');
                            $('#push').fadeOut(2000);
                });

                AssistAgentSDK.setFormCallBack(function (form) {
                    console.log('setFormCallBack');
                });

                AssistAgentSDK.setRemoteViewCallBack(function (x, y) {
                    console.log('setRemoteViewCallBack');
                    console.log('x: ' + x + ', y: ' + y);
                });

                AssistAgentSDK.setScreenShareActiveCallback(function(active) {
                    console.log('setScreenShareActiveCallback active: ' + active);
                     if (AssistAgentSDK.consumerIsOnAssistPages) {
                            $('#push').fadeIn(2000);
                        }
                });

                AssistAgentSDK.setScreenShareRejectedCallback(function () {
                    console.log('setScreenShareRejectedCallback');
                });

                AssistAgentSDK.setSnapshotCallBack(function (snapshot) {
                    console.log('setSnapshotCallBack');

                    // open the snapshot image
                    window.open(snapshot);
                });    

            }

            //links UI elements to their Agent SDK outlets
            function linkUi() {
                // screen share
                AssistAgentSDK.setRemoteView(share);

                // video calling elements
                // CallManager.setRemoteVideoElement(remote);
                // CallManager.setLocalVideoElement(local);
                CallManager.setCallQualityIndicator(quality);
            }


            // initialises using the Agent SDK CallManager
            // function init() {
            //     // now register the agent
            //     var gatewayUrl = 'https://172.31.250.52:8443';
            //     CallManager.init({
            //         sessionToken: sessionId,
            //         autoanswer: false,
            //         username: 'agent1',
            //         password: 'none',
            //         agentName: 'Agent',
            //         agentPictureUrl: gatewayUrl + '/assistserver/img/avatar.png',
            //         // url: gatewayUrl
            //         url: 'https://172.31.250.52:8443'

            //     });
            // }



        }(sessionId);




           });



    </script>
</body>
</html>