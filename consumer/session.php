<?php

    // configure the JSON to use in the session.

   $json = '{
            "webAppId": "webapp-id-example",
            "allowedOrigins": [],
            "urlSchemeDetails": {
                "secure": "true",
                "host": "172.31.250.52",
                "port": "8443"
            },
            "voice": {
                "username": "consumer1",
                 "domain": "172.31.250.52"
             },

             "aed":
             {
                   "accessibleSessionIdRegex":".*",
                   "maxMessageAndUploadSize":"5000",
                   "dataAllowance":"5000"
            },  "additionalAttributes":{
                "AED2.metadata":{
                    "role":"consumer",
                    "name":"bob",
                    "permissions" : {
                        "viewable" : ["default", "Blue", "Magenta", "slider", "Overlay"],
                        "interactive" : ["default", "Blue", "Magenta", "Overlay"]
                    }
                    
                },
                "AED2.allowedTopic":".*"
            }
            
        }';

	 // configure the curl options
    $ch = curl_init("http://172.31.250.52:8080/gateway/sessions/session");
    curl_setopt($ch,CURLOPT_POST, true);
    curl_setopt($ch,CURLOPT_POSTFIELDS, $json);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
    curl_setopt($ch, CURLOPT_HTTPHEADER, [         
        'Content-Type: application/json',
        'Content-Length: ' . strlen($json)
    ]);

    // execute HTTP POST & close the connection
    $response = curl_exec($ch);
	
	if(curl_errno($ch))
		{
			echo 'error:' . curl_error($ch);
		}

     curl_close($ch);

    // decode the JSON and pick out the session token
    $decodedJson = json_decode($response);
    $id = $decodedJson->{'sessionid'};
   

    // echo the ID we've retrieved
    echo $id; 

?>