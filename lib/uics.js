/* Theme Pack 3.2.1+ DO NOT modify */
if(!CS){
   var CS = {};
}
CS.v3 = {};
CS.v3.Base = function(){};
CS.v3.Base.prototype = {
   parsePX: function(amount){
      var parsed = parseInt(amount,10);
      var round = Math.round(parsed);
      return round;
   },
   getStyleValue: function(el,style){
      var obj = $(el);
      var rc = null;
      if(obj){
         if(style == 'left' || style == 'right' || style == 'top' || style == 'bottom' || style == 'width' || style == 'height'){
            var sty = obj.getStyle(style);
            rc = this.parsePX(sty) || null;
         }
      }
      return rc;
   },
   hideIECover: function(el){
      if(Prototype.Browser.IE){
         var ieCover = Element.select(el,"iframe[class='ie6-cover']");
         if(ieCover){
            var i = ieCover.length;
            while(i--){
               Element.remove(ieCover[i]);
            }
         }
      }
   },
   getCustomAttribute: function(el,name){ // get Custom Attribute
      var replace = new RegExp(name);
      this.classNames = $w(el.className);// replace
      this.matchedClasses = this.classNames.findAll(function(n){
         return n.startsWith(name);
      });
      var attributes = [];
      this.matchedClasses.each(function(value,index){
         this.attribute = this.matchedClasses[index].replace(replace,'').strip();
         attributes.push(this.attribute);
      }.bind(this));
      var rc;
      if(attributes.length === 0){
         rc = null;
      } else {
         rc = attributes;
      }
      return rc;
   },
   getCA: function(el,attribute,replace){ // get Custom Attribute
      this.c = el.className + ' ';
      this.att = [];
      this.c.scan(attribute,
         function(match){
            this.att.push(match[0]);
         }.bind(this));
      var aStrip = [];
      var EMPTY = new String('');
      this.att.each(function(value,index){
         this.a = this.att[index];
         this.a = this.a.replace(replace,EMPTY);
         this.a = this.a.strip();
         aStrip.push(this.a);
      }.bind(this));
      return aStrip;
   },
   getBlindDuration: function(el){
      var duration = 0.2;
      var height = this.getStyleValue(el,'height');
      if(height >= 150){ duration = 0.3;}
      if(height > 200){ duration = 0.5;}
      return duration;
   },
   navStatus: function(collection,item){
      var selectedItem;
      var CSNAVON = 'csNavOn';// TODO try String
      var obj;
      var i = collection.length;
      while(i--){
         obj = collection[i];
         // remove status
         if(Element.hasClassName(obj,CSNAVON)){
            Element.removeClassName(obj,CSNAVON);
            selectedItem = i;
         }
      }
      // add status
      Element.addClassName(item,CSNAVON);
      return selectedItem;
   }
};
CS.v3.getSelected = Class.create(CS.v3.Base.prototype, {
   initialize: function(item,styleRule){
      this.selectedIndex = -1;
      this.isFirst = false;
      this.isLast = false;
      this.collectionWrap = Element.up(item);
      var ar = Element.select(this.collectionWrap,styleRule);
      this.collection = ar;
      this.first = ar.first();
      this.last = ar.last();
      var FLAG = new String('csSelected-temp');// only used to find this field in the list.
      Element.addClassName(item,FLAG);
      var i = ar.length;
      var sz = i-1;
      var obj;
      while(i--){
         obj = ar[i];
         if(Element.hasClassName(obj,FLAG)){
            this.selectedIndex = i;
            if(i===0){
               this.isFirst = true;
            }
            if(i===sz){
               this.isLast = true;
            }
         }
      }
      Element.removeClassName(item,FLAG);
   }
});
CS.v3.AjaxNav = Class.create(CS.v3.Base.prototype, {
   initialize: function(el,updateTarget,param){
      this.el = el;
      if(param.bind){
         this.bindEvent = true;
      } else {
         this.bindEvent = false;
      }
      if(!param.param){
         param.param = '';
      }
      var tag = el.tagName;
      switch(tag){
         case 'A':
            this.url = el.href;
            break;
         case 'FORM':
            this.url = el.action;
            break;
         default:
            // NOOP
            break;
      }
      var QUEST = new String('?');
      var AMP = new String('&');//NOTE: consider escaping this as '&amp;' for the URL
      // check for "?"
      //this.url.scan(/\?+/,function(){this.query = 'true'}.bind(this));
      this.query = 'false';
      if(this.url.indexOf(QUEST) >=0){
         this.query = 'true';
      }
      // TODO use CS.v3.UrlFunctions.urlAppender() to simplify this function
      var sb = new StringBuffer();
      sb.append(this.url);
      if(this.query == 'false'){
         sb.append(QUEST);
      } else {
         sb.append(AMP);
      }
      sb.append('ajax=true');
      sb.append(AMP);
      sb.append(param.param);
      var rc = sb.toString(); sb=null;
      this.mainUrl = CS.v3.UrlFunctions.urlAppenderCacheBuster(rc);
      var locale = CS.v3.I18N.initialize(el);
      var httpHeaders = ['Accept-Language',locale];
      var i18nurl = CS.v3.UrlFunctions.urlAppenderLocale(this.mainUrl,locale);
      var myurl = CS.v3.UrlFunctions.urlAppenderCacheBuster(i18nurl);
      new Ajax.Request(myurl, {
         method: 'get',
         requestHeaders: httpHeaders,
         onCreate: function(){
               this.processing(updateTarget);
            }.bind(this),
         onComplete: function(request){
               this.loader(request,updateTarget);
            }.bind(this)
      });
   },
   processing: function(updateTarget){
      updateTarget.innerHTML = '';//request.responseText;
      Element.addClassName(updateTarget,'ajax-spinner');
   },
   loader: function(request,updateTarget){
      Element.removeClassName(updateTarget,'ajax-spinner');
      updateTarget.innerHTML = request.responseText;
      // Add events to snippet
      if(this.bindEvent){
         new CS.v3.Config(updateTarget,{bind:'true', set:'true'});
      }
   }
});
CS.v3.Components = {};
CS.v3.Config = {};
CS.v3.Config = Class.create(CS.v3.Base.prototype, {
   initialize: function(el,options){
      var obj;
      var ar = CS.v3.Components;//NOTE:  this is not a standard array
      for (var i in ar){
         obj = ar[i];
			if(obj instanceof Function){
            this.comp = obj;
            this.comp(el,options);
         }
      }
   }
});
document.observe("dom:loaded", function(){
   var body = $$('body');
   new CS.v3.Config(body[0],{bind:'true', set:'true'});
});
//CS.v3.Components.hideIEFocus = function(el,options){ new CS.v3.Config.hideIEFocus(el,options);};
CS.v3.Config.hideIEFocus = Class.create(CS.v3.Base.prototype, {
   initialize: function(el,options){
      if(Prototype.Browser.IE){
         this.ieMenuItems = Element.select(el,"div.csTabs li a");
         this.bind();
      }
   },
   bind: function(){
      var ar = this.ieMenuItems;
      var el;
      var FLAG = new String('csTabs-hideIEFocus-init');
      var i = ar.length;
      var b;
      while(i--){
         el = ar[i];
         b = Element.hasClassName(el,FLAG);// only change once!
         if(!b){
            Element.addClassName(el,FLAG);
            Event.observe(el,'focus', function(event){
               var target = $(Event.element(event));
               target.hideFocus = true;
            });
         }
      }
   }
});
// GET SETTINGS
CS.v3.getCompNode = Class.create(CS.v3.Base.prototype, {
   initialize: function(target){
      this.getClass = this.getClasses(target);
      this.compNode = '';
      if(this.getClass){
         this.compNode = Element.up(target,this.getClass);
      }
   },
   getClasses: function(target){
      this.classNames = $w(target.className);
      var ar = this.classNames;
      var name;
      var obj;
      var i = ar.length;
      var cached = CS.v3.componentTargets;
      while(i--){
         name = ar[i];
         obj = cached[name];
         if(obj){
            this.getComponentName = obj.component;
         }
      }
      return this.getComponentName;
   }
});
CS.v3.ComponentAttributes = Class.create(CS.v3.Base.prototype, {
   initialize: function(target){
      // all DOM
      this.readToShow = Element.readAttribute(target,'data-csnodestoshow');
      this.showContent = $$(this.readToShow);
      this.readToHide = Element.readAttribute(target,'data-csnodestohide');
      this.hideContent = $$(this.readToHide);
      this.readToToggle = Element.readAttribute(target,'data-csnodestotoggle');
      this.toggleContent = $$(this.readToToggle);
      // up DOM
      this.readToHideUp = Element.readAttribute(target,'data-csnodetohideup');
      if(this.readToHideUp){
         this.hideContentUp = Element.up(target,this.readToHideUp);
      }
      this.readToRemoveUp = Element.readAttribute(target,'data-csnodetoremoveup');
      if(this.readToRemoveUp){
         this.removeContentUp = Element.up(target,this.readToRemoveUp);
      }
   },
   log: function(s,e){
      CS.v3.Logger.log("ComponentAttributes:" + s,e);
   }
});
CS.v3.ComponentAttributesSelectNone = Class.create(CS.v3.Base.prototype, {
   // Replacement CS.v3.ComponentAttributes specifically for select elements when the 'none' (empty string value) option is selected
   // In this case, we hide all of the 'to show' elements for all other options and determine the element to show by concatenating
   // the 'to hide' class names to use in the selector 
   initialize: function(ar, idx){
      // all DOM
      this.showContent = '';
      this.hideContent = new Array();
      this.toggleContent = [];
      for (var ix = 0; ix < ar.length; ix++) {
         if (ix != idx) {
            var target = CS.v3.castToEl(ar[ix]);
            this.showContent += Element.readAttribute(target,'data-csnodestohide');
            
            this.readToShow = Element.readAttribute(target,'data-csnodestoshow');
            this.hideContent = this.hideContent.concat($$(this.readToShow));
         }
      }
      this.showContent = $$(this.showContent);
   },
   log: function(s,e){
      CS.v3.Logger.log("ComponentAttributesSelectNone:" + s,e);
   }
});
CS.v3.Components.addOver = function(section,options){
   new CS.v3.Config.addOver(section,options);
};
CS.v3.Config.addOver = Class.create(CS.v3.Base.prototype, {
   initialize: function(section){
      var mouseOver = $$('.csOver');
      new CS.v3.addEvents(mouseOver,'over');
   }
});
/*** ADD EVENTS ***/
CS.v3.Components.addEvents = function(section,options){
   var version3;
   if(Element.match(section,'body.csV-3')){
      version3 = $$('.csV-3');
   } else {
      version3 = Element.select(section,'.csV-3');
   }
   new CS.v3.addEvents(version3,'click');
};
CS.v3.addEvents = Class.create(CS.v3.Base.prototype, {
   initialize: function(sections,eventType){
      this.observeNodes = sections;
      switch(eventType){
         case 'click':
            this._bindClick();
            break;
         case 'over':
            this._bindOver();
            break;
         default:
            // NOOP
            break;
      }
      this._bindFocus();
   },
   _bindFocus: function(){
      var ar = this.observeNodes;
      var node;
      var i = ar.length;
      while(i--){
         node = ar[i];
         Event.observe(node,'focus', function(event){ this._handleFocus(event,'.csFocus'); }.bind(this));
         Event.observe(node,'blur', function(event){ this._handleFocus(event,'.csFocus'); }.bind(this));
      }
   },
   _bindClick: function(){
      var ar = this.observeNodes;
      var node;
      var i = ar.length;
      while(i--){
         node = ar[i];
         Event.observe(node,'click', function(event){
            var target = $(Event.element(event));
            new CS.v3.CloseNavsX(target,event);
            this._handle(event,'.csClick');
         }.bind(this));
         //Note: Required for IE Fixed dropdown, and expandable panel keyboard control
         Event.observe(node,'keypress', function(event){ this._handle(event,'.csClick'); }.bind(this));
      }
   },
   _bindOver: function(){
      var ar = this.observeNodes;
      var node;
      var i = ar.length;
      while(i--){
         node = ar[i];
         Event.observe(node,'mouseover', function(event){ this._handle(event,'.csOver'); }.bind(this));
         Event.observe(node,'mouseout', function(event){ this._handle(event,'.csOver'); }.bind(this));
         Event.observe(node,'click', function(event){ this._handle(event,'.csOver'); }.bind(this));
         Event.observe(node,'keydown', function(event){
            //if(event.keyCode===Event.KEY_RETURN){
               this._handle(event,'.csOver');
            //}
         }.bind(this));
      }
   },
   _handle: function(event,eventClass){
		//this.log('_handle:init');
      var target = $(Event.element(event));
      /* TODO consider using 'hasClassName(eventClass) */
      var b = false;
      try{
         if(Element.match(target,eventClass)){
            b = true;
         } else {
            var elements = new CS.v3.getCompNode(target,event);
            var obj = elements.compNode;
            if(obj){
               if(Element.match(obj,eventClass)){
                  b = true;
               }
            }
         }
      }
      catch(e){
         this.log('_handle:',e);
      }

      // IE7 bug - disabled elements still cause a click event to be fired to a listening parent container,
      // so for elements that support disabled, don't propagate the event if they are truly disabled.
      if (target.tagName == 'INPUT' && target.disabled) {
         b = false;
      }
      if(b){
         this._getClasses(target,event);
      }
   },
   _handleFocus: function(event,eventClass){
      var target = $(Event.element(event));
      this._getClassesFocus(target,event);
   },
   _getClasses: function(target,event){
      var classNames = $w(target.className);
      var name;
      var obj;
      var cached = CS.v3.componentTargets;
      var i = classNames.length;
      while(i--){
         name = classNames[i];
         obj = cached[name];
         if(obj){
            obj.fire(target,event);
         }
      }
   },
   _getClassesFocus: function(target,event){
      var classNames = $w(target.className);
      var name;
      var obj;
      var cached = CS.v3.componentTargetsFocus;
      var i = classNames.length;
      while(i--){
         name = classNames[i];
         obj = cached[name];
         if(obj){
            obj.fire(target,event);
         }
      }
   },
   log: function(s,e){
      CS.v3.Logger.log('addEvents:' + s,e);
   }
});
// GET TARGETS & WRAPS Focus
CS.v3.componentTargetsFocus = {};
CS.v3.componentTargetsFocus = {
   'csHintToggle': {
      component:'',
      fire: function(target,event){
         var typ = event.type;
         switch(typ){
            case 'focus':
               event.preventDefault(event);
               var m = new CS.v3.HintToggle(target,event);
               CS.v3.componentTargetsFocus['csHintToggle'].test = window.setTimeout(function (m) {
                  CS.v3.HintVis.show(m);
               }.curry(m), 375);
               break;
            case 'blur':
               window.clearTimeout(CS.v3.componentTargetsFocus['csHintToggle'].test);
               event.preventDefault(event);
               CS.v3.HintVis.blur(new CS.v3.HintToggle(target,event));
               break;
            default:
               // NOOP
               break;
         }
      }
   }
};

// GET TARGETS & WRAPS
CS.v3.componentTargets = {};
CS.v3.componentTargets = {
   'csToggle': {
      component:'',
      fire: function(target,event){
         var typ = event.type;
         var key = event.keyCode;
         if(typ==='click' || key===Event.KEY_RETURN){
            event.preventDefault(event);
            var attr = new CS.v3.ComponentAttributes(target);
            var targetWrap = target.up(0,'.csIcon');
            new CS.v3.ToggleDisplay(target,attr);
            target.toggleClassName('csToggleShow');
            if(targetWrap){
               targetWrap.toggleClassName('csIconShow');
            }
         }
      }
   },
   'csTabToggleNBA': {
      component:'',
      fire: function(target,event){
         var typ = event.type;
         var key = event.keyCode;
         if(typ==='click' || key===Event.KEY_RETURN){
            event.preventDefault(event);
            var cached = CS.v3.componentTargets;
            var obj = cached['csToggle'];
            obj.fire(target,event);
            // Tabs
            new CS.v3.TabsNBA(target);
         }
      }
   },
   'csCloseNBA': {
      component:'',
      fire: function(target,event){
         var typ = event.type;
         var key = event.keyCode;
         if(typ==='click' || key===Event.KEY_RETURN){
            event.preventDefault(event);
            this.wrapNBA = Element.up(target,'.csPanelNextBestAction');
            var obj = Element.down(this.wrapNBA,'.csNBAModule');
            obj.toggleClassName('csHide');
            var active = CS.v3.castToEl('csActiveNBA');
            CS.v3.focus(active);
            active.id = '';
         }
      }
   },
   'csNavToggle': {
      component:'.csNav-y,.csNav-x',
      fire: function(target,event){
         var key = event.keyCode;
         var m;
         switch(key){
            case Event.KEY_RETURN:
               m = new CS.v3.MenuToggle(target,event);
               event.preventDefault(event);
               CS.v3.MenuVis.effect(m,event);
               break;
            case Event.KEY_DOWN:
               m = new CS.v3.MenuToggle(target,event);
               event.preventDefault(event);
               CS.v3.MenuVis.show(m,event);
               break;
            case Event.KEY_TAB:
               m = new CS.v3.MenuToggle(target,event);
               CS.v3.MenuVis.hide(m,event);
               break;
            default:
               // NOOP
               break;
         }
         var typ = event.type;
         switch(typ){
            case 'click':
               m = new CS.v3.MenuToggle(target,event);
               event.preventDefault(event);
               CS.v3.MenuVis.effect(m,event);
               break;
            case 'mouseover':
               m = new CS.v3.MenuToggle(target,event);
               event.preventDefault(event);
               CS.v3.MenuVis.effect(m,event);
               break;
            default:
               // NOOP
               break;
         }
      }
   },
   'csNavItem-1': {
      component:'.csNav-y,.csNav-x',
      fire: function(target,event){
         //var typ = event.type;
         var key = event.keyCode;
         var m;
         var t;
         //if(typ==='click'){Event.stop(event);}
         switch(key){
            case Event.KEY_UP:
               m = new CS.v3.MenuItem(target,event);
               event.preventDefault(event);
               if(m.togglePrev){
                  CS.v3.focus(m.togglePrevTarget);
               }
               break;
            case Event.KEY_DOWN:
               m = new CS.v3.MenuItem(target,event);
               event.preventDefault(event);
               if(m.toggleNext){
                  CS.v3.focus(m.toggleNextTarget);
               }
               break;
            case Event.KEY_ESC:
               target = CS.v3.castToEl('csActiveNavXToggle');
               event.preventDefault(event);
               if(target){
                  CS.v3.focus(target);
                  t = new CS.v3.MenuToggle(target,event);
                  CS.v3.MenuVis.hide(t,event);
               }
               break;
            case Event.KEY_TAB:
               target = CS.v3.castToEl('csActiveNavXToggle');
               if(target){
                  CS.v3.focus(target);
                  t = new CS.v3.MenuToggle(target,event);
                  if(Element.hasClassName(t.wrap,'csMenuContextual')){
                     event.preventDefault(event);
                     CS.v3.MenuVis.hide(t,event);
                  }
               }
               break;
            default:
               // NOOP
               break;
         }
      }
   },
   'csPanelFirstItem': {
      component:'.csNav-y,.csNav-x',
      fire: function(target,event){
         var key = event.keyCode;
         if((key===Event.KEY_TAB) && (event.shiftKey)){
            //event.preventDefault(event);
            Event.stop(event);
         }
      }
   },
   'csPanelLastItem': {
      component:'.csNav-y,.csNav-x',
      fire: function(target,event){
         var key = event.keyCode;
         if((key===Event.KEY_TAB) && (!event.shiftKey)){
            //event.preventDefault(event);
            Event.stop(event);
         }
      }
   },
   'csShowAll': {
      component:'',
      fire: function(target,event){
         var typ = event.type;
         var key = event.keyCode;
         if(typ==='click' || key===Event.KEY_RETURN){
            event.preventDefault(event);
            var rows;
            var menus;
            var panels;
            if(Element.hasClassName(target,'csPanelsAll')){
               panels = {};
               panels.readToShow = Element.readAttribute(target,'data-csnodestoshow');
               panels.showContent = $$(panels.readToShow + ' .csPanelToggle ');
               CS.v3.PanelVis.showAll(panels);
            }
            if(Element.hasClassName(target,'csListTreesAll')){
               menus = {};
               menus.readToShow = Element.readAttribute(target,'data-csnodestoshow');
               menus.showContent = $$(menus.readToShow + ' .csPanelTreeList  a.csNavToggle');
               CS.v3.MenuVis.showAll(menus,event);
            }
            if(Element.hasClassName(target,'csTogglesRowsAll')){
               rows = {};
               rows.readToShow = Element.readAttribute(target,'data-csnodestoshow');
               rows.showContent = $$(rows.readToShow + ' .csPanelTable span.csRowToggle  a.csToggle');
               rows.hideContent = [];
               rows.toggleContent = [];
               CS.v3.TogglesVis.showAll(rows);
            }
            if(Element.hasClassName(target,'csTogglesDataAll')){
               rows = {};
               rows.readToShow = Element.readAttribute(target,'data-csnodestoshow');
               rows.showContent = $$(rows.readToShow + ' .csPanelTable span.csDataToggle  a.csShowMoreLessToggle');
               rows.hideContent = [];
               rows.toggleContent = [];
               CS.v3.TogglesVis.showAll(rows);
            }
         }
      }
   },
   'csHideAll': {
      component:'',
      fire: function(target,event){
         var typ = event.type;
         var key = event.keyCode;
         if(typ==='click' || key===Event.KEY_RETURN){
            event.preventDefault(event);
            var panels;
            var menus;
            var rows;
            if(Element.hasClassName(target,'csPanelsAll')){
               panels = {};
               panels.readToHide = Element.readAttribute(target,'data-csnodestohide');
               panels.hideContent = $$(panels.readToHide + ' .csPanelToggle');
               CS.v3.PanelVis.hideAll(panels);
            }
            if(Element.hasClassName(target,'csListTreesAll')){
               menus = {};
               menus.readToHide = Element.readAttribute(target,'data-csnodestohide');
               menus.hideContent = $$(menus.readToHide + ' .csPanelTreeList  a.csNavToggle');
               CS.v3.MenuVis.hideAll(menus,event);
            }
            if(Element.hasClassName(target,'csTogglesRowsAll')){
               rows = {};
               rows.readToHide = Element.readAttribute(target,'data-csnodestohide');
               rows.showContent = [];
               rows.hideContent = $$(rows.readToHide + ' .csPanelTable span.csRowToggle  a.csToggle');
               rows.toggleContent = [];
               CS.v3.TogglesVis.hideAll(rows);
            }
            if(Element.hasClassName(target,'csTogglesDataAll')){
               rows = {};
               rows.readToHide = Element.readAttribute(target,'data-csnodestohide');
               rows.showContent = [];
               rows.hideContent = $$(rows.readToHide + ' .csPanelTable span.csDataToggle  a.csShowMoreLessToggle');
               rows.toggleContent = [];
               CS.v3.TogglesVis.hideAll(rows);
            }
         }
      }
   },
   'csHidePanels': {
      component:'',
      fire: function(target,event,section){
         event.preventDefault(event);
         if(!Element.hasClassName(target,'csPanelsAll')){
            var attr = new CS.v3.ComponentAttributes(target);
            CS.v3.PanelVis.hideAll(attr);
         } else {
            return;
         }
      }
   },
   'csShowPanels': {
      component:'',
      fire: function(target,event){
         event.preventDefault(event);
         if(!Element.hasClassName(target,'csPanelsAll')){
            var attr = new CS.v3.ComponentAttributes(target);
            CS.v3.PanelVis.showAll(attr);
         } else {
            return;
         }
      }
   },
   'csPanelToggle': {
      component:'.csPanelHeading',
      fire: function(target,event){
         var typ = event.type;
         var key = event.keyCode;
         if(typ==='click' || key===Event.KEY_RETURN){
            event.preventDefault(event);
            var m = new CS.v3.Panel(target);
            CS.v3.PanelVis.toggle(m);
         }
      }
   },
   'csPanelToggleText': {
      component:'.csPanelHeading',
      fire: function(target,event){
         var typ = event.type;
         var key = event.keyCode;
         if(typ==='click' || key===Event.KEY_RETURN){
            event.preventDefault(event);
            var m = new CS.v3.Panel(target);
            CS.v3.PanelVis.toggle(m);
         }
      }
   },
   'csTabToggle': {
      component:'.csTabs-x,.csTabs-y',
      fire: function(target,event){
         var typ = event.type;
         var key = event.keyCode;
         if(typ==='click' || key===Event.KEY_RETURN){
            event.preventDefault(event);
            new CS.v3.Tabs(target);
         }
      }
   },
   'csHintToggle': {
      component:'',
      fire: function(target,event){
         var typ = event.type;
         var key = event.keyCode;
         var m;
         switch(typ){
            case 'click':
               /* prevent anchor default */
               event.preventDefault(event);
               break;
            case 'mouseover':
               m = new CS.v3.HintToggle(target,event);
               event.preventDefault(event);
               CS.v3.HintVis.show(m);
               break;
            case 'mouseout':
               if(!Element.hasClassName(target,'cskeyFocus')){
                  m = new CS.v3.HintToggle(target,event);
                  event.preventDefault(event);
                  CS.v3.HintVis.hide(m);
               }
               break;
            default:
               // NOOP
               break;
         }
         if(key===Event.KEY_RETURN){
            event.preventDefault(event);
            m = new CS.v3.HintToggle(target,event);
            CS.v3.HintVis.show(m);
         }
      }
   },
   'csOverlayToggle': {
      component:'',
      fire: function(target,event){
         var typ = event.type;
         var key = event.keyCode;
         if(typ==='click' || key===Event.KEY_RETURN){
            event.preventDefault(event);
            var rc = true;
            if (target.hasClassName('ukpwsClientValidateFirst')) {
               rc = CS.v3.validateFormsSubmit(event);
            }
            if (rc) {
               var attr = new CS.v3.ComponentAttributes(target);
               new CS.v3.PanelOverlay(target,event,attr);
            }
         }
      }
   },
   'csOverlayClose': {
      component:'',
      fire: function(target,event,section){
         var typ = event.type;
         var key = event.keyCode;
         if((typ==='click') || (key===Event.KEY_RETURN)){
            new CS.v3.PanelPopClose(target,event);
         }
      }
   },
   'csAddEntryRow': {
      component:'',
      fire: function(target,event){
         var key = event.keyCode;
         if((event.type==='click') || (key===Event.KEY_RETURN)){
            event.preventDefault(event);
            new CS.v3.addAddEntryContent(target);
         }
      }
   },
   'csRemove': {
      component:'',
      fire: function(target,event){
         //new CS.v3.cancelAddEntryContent(target);
         var attr = new CS.v3.ComponentAttributes(target);
         Element.remove(attr.removeContentUp);
      }
   },
   'csCancelEntryRow': {
      component:'',
      fire: function(target,event){
         new CS.v3.cancelAddEntryContent(target);
         var attr = new CS.v3.ComponentAttributes(target);
         Element.remove(attr.removeContentUp);
      }
   },
   'csOverlayToggleSaveData': {
      component:'',
      fire: function(target,event){
         if(event.type==='click' || event.keyCode===Event.KEY_RETURN){
            event.preventDefault(event);
            var attr = new CS.v3.ComponentAttributes(target);
            new CS.v3.addAddEntryRow(target);
         }
      }
   },
   'csShowMoreLessToggle': {
      component:'',
      fire: function(target,event){
         if(event.type==='click' || event.keyCode===Event.KEY_RETURN){
            event.preventDefault(event);
            new CS.v3.tableShowMoreLess(target);
         }
      }
   },
   'csToggleText': {
      component:'',
      fire: function(target,event){
         var showMoreLessToggleTarget = CS.v3.getParentElementWithClass(target,'csShowMoreLessToggle');
         new CS.v3.tableShowMoreLess(showMoreLessToggleTarget);
      }
   }
};
/*** Scroll Detect ***/
CS.v3.Components.ScrollDetect = function(section,options){
   new CS.v3.ScrollDetect(section,options);
};
CS.v3.ScrollDetect = Class.create(CS.v3.Base.prototype, {
   initialize: function(section){
      var ar = $$('.csScroll');
      this._bindScroll(ar);
   },
   _bindScroll: function(ar){
      var node;
      var i = ar.length;
      while(i--){
         node = ar[i];
         Event.observe(node,'scroll', function(event){
            var target = Event.element(event);
            CS.v3.MenuVis.hideActiveNavXToggle(target,event);
            CS.v3.contextualMenu.style.display = 'none';
            CS.v3.ContainerHint.style.display = 'none';
         });
      }
   }
});
/*** Bubble Cancel ***/
CS.v3.Components.eventBubbleCancels = function(el,options){
   new CS.v3.Config.eventBubbleCancels(el,options);
};
CS.v3.Config.eventBubbleCancels = Class.create(CS.v3.Base.prototype, {
   initialize: function(el,options){
      this.clickCancels = Element.select(el,"div.csMenuLogOn div.csPanelScroll");
      this.bind();
   },
   bind: function(){
      var ar = this.clickCancels;
      var node;
      var i = ar.length;
      while(i--){
         node = ar[i];
         Event.observe(node,'click', function(event){
            //var target = $(Event.element(event));
            Event.stop(event);
         }.bind(this));
         Event.observe(node,'keydown', function(event){
            var target = Event.element(event);
            var key = event.keyCode;
            if(key===Event.KEY_ESC){
               CS.v3.MenuVis.hideActiveNavXToggle(target,event);
               //event.preventDefault(event);
            }
         }.bind(this));
      }
   }
});
/*** Effects ***/
CS.v3.ToggleDisplayEffect = Class.create(CS.v3.Base.prototype, {
   initialize: function(target,toggle,toggleTarget,wrap,toggleClass,defaultState){
      this.target = target;
      this.toggle = toggle;
      this.wrap = wrap;
      this.toggleTarget = toggleTarget;
      this.toggleClass = toggleClass;
      // simple toggle
      if(!Element.hasClassName(this.wrap,'csBlind') && !Element.hasClassName(this.wrap,'csOpacity')){
         this.toggleTarget.toggleClassName(this.toggleClass);
      }
      // blind up and down
      if(Element.hasClassName(this.wrap,'csBlind') || Element.hasClassName(this.wrap,'csAccordian')){
         if(defaultState == 'hidden'){
            if(Element.hasClassName(this.toggle,this.toggleClass)){
               this._blindUp();
            } else {
               this._blindDown();
            }
         } else {
            if(Element.hasClassName(this.toggle,this.toggleClass)){
               this._blindDown();
            } else {
               this._blindUp();
            }
         }
      }
      // opacity
      if(Element.hasClassName(this.wrap,'csOpacity')){
         if(defaultState == 'hidden'){
            if(Element.hasClassName(this.toggle,this.toggleClass)){
               this._opacityHide();
            } else {
               this._opacityShow();
            }
         } else {
            if(Element.hasClassName(this.toggle,this.toggleClass)){
               this._opacityShow();
            } else {
               this._opacityHide();
            }
         }
      }
   },
   _blindDown: function(){
      this.height = this.toggleTarget.getDimensions();
      this.height = this.height.height;
      this.time = CS.v3.Base.prototype.getBlindDuration(this.toggleTarget);
      new Effect.BlindDown(this.toggleTarget, {duration: this.time, queue: {position: 'front', scope: 'hiddenLi'},
         beforeStart: function(){
            Element.addClassName(this.target,'csTargetShow');
            this.toggleTarget.style.display = 'none';
            this.toggleTarget.style.height = 'auto';
            this.toggle.toggleClassName(this.toggleClass);
         }.bind(this),
         afterFinish: function(){
            //ie 6 only items disappear without this
            this.toggleTarget.style.height = '';
            this.toggleTarget.style.display = 'block';
            //ie 6 only
            CS.v3.MenuState = null;
         }.bind(this)
      });
   },
   _blindUp: function(){
      this.height = this.toggleTarget.getDimensions();
      this.height = this.height.height;
      this.time = CS.v3.Base.prototype.getBlindDuration(this.toggleTarget);
      new Effect.BlindUp(this.toggleTarget, {duration: this.time, queue: {position: 'end', scope: 'shownLi'},
         //new
         beforeStart: function(){
            Element.removeClassName(this.target,'csTargetShow');
            this.toggleTarget.style.height = this.height;
            this.toggleTarget.style.overflow = 'hidden';
         }.bind(this),
         //new
         afterFinish: function(){
            this.toggle.toggleClassName(this.toggleClass);
            CS.v3.MenuState = null;
         }.bind(this)
      });
   },
   _opacityShow: function(){
      new Effect.Opacity(this.toggleTarget, {from: 0.0, to: 1.0, duration:0.4,
         beforeStart: function(){
            Element.addClassName(this.target,'csTargetShow');
            this.toggle.toggleClassName(this.toggleClass);
         }.bind(this)
      });
   },
   _opacityHide: function(){
      new Effect.Opacity(this.toggleTarget, {from: 1.0, to: 0.0, duration:0.4,
         afterFinish: function(){
            Element.removeClassName(this.target,'csTargetShow');
            this.toggle.toggleClassName(this.toggleClass);
         }.bind(this)
      });
   }
});
// New 3.0
CS.v3.ToggleDisplay = Class.create(CS.v3.Base.prototype, {
   initialize: function(toggle,attr,section){
      /* effect */
      var rc;
      var CSBLIND = 'csBlind';
      var CSOPACITY = 'csOpacity';
      if(!Element.hasClassName(toggle,CSOPACITY) && !Element.hasClassName(toggle,CSBLIND)){
         rc = 'show/hide';
      }
      if(Element.hasClassName(toggle,CSBLIND)){
         rc = 'blind';
      }
      if(Element.hasClassName(toggle,CSOPACITY)){
         rc = 'opacity';
      }
      this.effect = rc;
      this._toggleCollection(attr);
   },
   _toggleCollection: function(attr){
      // All DOM
      var arShow = attr.showContent;
      var arHide = attr.hideContent;
      var arToggle = attr.toggleContent;
      var el;
      var showFunction = function(){
         CS.v3.showFunction(el);
      };
      var hideFunction = function(){
         CS.v3.hideFunction(el);
      };
      var i = arHide.length;
      while(i--){
         el = arHide[i];
         this._hideAll(el,hideFunction);
      }
      i = arShow.length;
      while(i--){
         el = arShow[i];
         this._showAll(el,showFunction);
      }
      i = arToggle.length;
      var CSHIDE = 'csHide';// TODO try String
      while(i--){
         el = arToggle[i];
         if(Element.hasClassName(el,CSHIDE)){
            this._showAll(el,showFunction);
         } else {
            this._hideAll(el,hideFunction);
         }
      }
      // Up DOM
      var obj = attr.hideContentUp;
      if(obj){
         CS.v3.hideFunction(obj);
      }
   },
   _showAll: function(el,toggleFunction){
      switch(this.effect){
         case 'show/hide':
            toggleFunction();
            break;
         case 'opacity':
            CS.v3.ToggleEffect._opacityShow(el,toggleFunction);
            break;
         case 'blind':
            CS.v3.ToggleEffect._blindDown(el,toggleFunction);
            break;
         default:
            this.log('_showAll:No Effect Defined');
            break;
      }
   },
   _hideAll: function(el,toggleFunction){
      switch(this.effect){
         case 'show/hide':
            toggleFunction();
            break;
         case 'opacity':
            CS.v3.ToggleEffect._opacityHide(el,toggleFunction);
            break;
         case 'blind':
            CS.v3.ToggleEffect._blindUp(el,toggleFunction);
            break;
         default:
            this.log('_hideAll:No Effect Defined');
            break;
      }
   },
   log: function(s,e){
      CS.v3.Logger.log("ToggleDisplay:" + s,e);
   }
});
CS.v3.ToggleEffect = {
   _opacityShow: function(toggleTarget,toggleFunction){
      new Effect.Opacity(toggleTarget, {from: 0.0, to: 1.0, duration:0.4,
         beforeStart: function(){
            toggleFunction();
            //this.toggle.toggleClassName(this.toggleClass);
         }.bind(this)
      });
   },
   _opacityHide: function(toggleTarget,toggleFunction){
      new Effect.Opacity(toggleTarget, {from: 1.0, to: 0.0, duration:0.4,
         afterFinish: function(){
            toggleFunction();
         }.bind(this)
      });
   },
   _blindDown: function(toggleTarget,toggleFunction){
      this.height = toggleTarget.getDimensions();
      this.height = this.height.height;
      this.time = CS.v3.Base.prototype.getBlindDuration(toggleTarget);
      new Effect.BlindDown(toggleTarget, {duration: this.time, queue: {position: 'front', scope: 'hiddenLi'},
         beforeStart: function(){
            toggleTarget.style.display = 'none';
            toggleTarget.style.height = 'auto';
            //this.toggle.toggleClassName(this.toggleClass);
         }.bind(this),
         afterFinish: function(){
            //ie 6 only items disappear without this
            toggleTarget.style.height = '';
            toggleTarget.style.display = 'block';
            //ie 6 only
            CS.v3.MenuState = null;
         }.bind(this)
      });
   },
   _blindUp: function(toggleTarget,toggleFunction){
      this.height = toggleTarget.getDimensions();
      this.height = this.height.height;
      this.time = CS.v3.Base.prototype.getBlindDuration(toggleTarget);
      new Effect.BlindUp(toggleTarget, {duration: this.time, queue: {position: 'end', scope: 'shownLi'},
         beforeStart: function(){
            toggleTarget.style.height = this.height;
            toggleTarget.style.overflow = 'hidden';
         }.bind(this),
         afterFinish: function(){
            //this.toggle.toggleClassName(this.toggleClass);
            CS.v3.MenuState = null;
         }.bind(this)
      });
   }
};
CS.v3.styleZIndex = {
   active: function(target,compNode){
      var CSZINDEX = 'csZindex';// TODO try String
      if(compNode){
         Element.addClassName(compNode,CSZINDEX);
      }
      //apply to context
      var getContextNodeAttr = Element.readAttribute(target,'data-csoverlaycontextnode');
      if(getContextNodeAttr){
         var getContextNode = Element.up(target,getContextNodeAttr);
         if(getContextNode){
            Element.addClassName(getContextNode,CSZINDEX);
         }
      }
   },
   inactive: function(target,compNode){
      var CSZINDEX = 'csZindex';// TODO try String
      if(compNode){
         Element.removeClassName(compNode,CSZINDEX);
      }
      //apply to context
      var getContextNodeAttr = Element.readAttribute(target,'data-csoverlaycontextnode');
      if(getContextNodeAttr){
         var getContextNode = Element.up(target,getContextNodeAttr);
         if(getContextNode){
            Element.removeClassName(getContextNode,CSZINDEX);
         }
      }
   },
   log: function(s,e){
      CS.v3.Logger.log("styleZIndex:" + s,e);
   }
};
/* Reverses zindex layers*/
CS.v3.Components.zIndex = function(section,options){
   new CS.v3.zIndex(section,options);
};
CS.v3.zIndex = Class.create(CS.v3.Base.prototype, {
   initialize: function(el){
		//this.log('initialize:init');
      this.tables = $$('table.csTable');
      this._addRowIndex();
		//this.log('initialize:exit');
   },
   _addRowIndex: function(){
      this.tables.reverse();
      var tds;
      var td;
      var field;
      var label;
      var i;
      this.tables.each(function(table,n1arg){
         var n1 = n1arg + 100;
         this.trs = Element.select(table,['tr','.csRow']);
         this.trs = this.trs.reverse();
         this.trs.each(function(tr,num){
            this.z = num + 100;
            tds = Element.select(tr,'td');
            i = tds.length;
            while(i--){// was able to convert this loop as order is not significant!
               td = tds[i];
               field = Element.down(td,'DIV.csFields');
               label = Element.down(td,'DIV.csLabel');
               if(field){
                  field.setStyle({zIndex: this.z});
               }
               if(label){
                  label.setStyle({zIndex: this.z});
               }
            }
         }.bind(this));
      }.bind(this));
   },
   log: function(s,e){
      CS.v3.Logger.log("zIndex:" + s,e);
   }
});
CS.v3.hideFunction = function(el){
   var CSHIDE = 'csHide';// TODO try String
   if(!Element.hasClassName(el,CSHIDE)){// dedupe
      Element.addClassName(el,CSHIDE);
   }
};
CS.v3.showFunction = function(el){
   var CSHIDE = 'csHide';// TODO try String
   if(Element.hasClassName(el,CSHIDE)){
      Element.removeClassName(el,CSHIDE);
   }
};
CS.v3.fireOnchange = function(el){
   CS.v3.fireUserEvent(el,'change');
};
CS.v3.fireOnclick = function(el){
   CS.v3.fireUserEvent(el,'click');
};
CS.v3.fireUserEvent = function(el,str){
   try{
      if(document.dispatchEvent){ // W3C
         var oEvent = document.createEvent("MouseEvents");
         oEvent.initMouseEvent(str, true, true,window, 1, 1, 1, 1, 1, false, false, false, false, 0, el);
         el.dispatchEvent( oEvent );
      } else {
         if(document.fireEvent){ // IE
            var s = 'on' + str;
            el.fireEvent(s);
         }
      }
   }
   catch(e){
      CS.v3.Logger.log('fireUserEvent:str=' + str,e);
   }
};
CS.v3.submitForm = function(frmEl){
   try{
      var ar = CS.v3.Placeholder.getAllElements(frmEl);
      if(ar){
         var obj;
         var i = ar.length;
         while(i--){
            obj = ar[i];
            if(CS.v3.Placeholder.isSupportedElement(obj)){
               CS.v3.Placeholder.erasePlaceholder(obj);
            }
         }
      }
      // Prevent double submission
      if (!CS.v3.I18N.getCustomAttributeSubmitted(frmEl)){
         frmEl.submit();
         UKPWSPrivateUtils.setFormsSubmittedState('true',frmEl);
      }
   }
   catch(e){
      CS.v3.Logger.log('submitForm:error',e);
   }
};
CS.v3.up = function(el,arg){
   var rc;
   if(el){
      rc = Element.up(el,arg);
   }
/*
   el = $(el);
   if(arg.length == 1){
      rc = $(el.parentNode);
   } else if(Object.isNumber(arg)){
      // return the nth parent , 0 is direct
      rc = el;
      do {
         rc = $(rc.parentNode);
      } while (rc && --arg >= 0);
      rc = $(rc);
   } else {
      var tagName;
      if(/^[^\.\*\s]+$/.test(arg)){
         tagName = arg.toUpperCase();
         rc = CS.v3.getParentElement(el,arg);
      } else {
         if(/^\.[^\.\*\s]+$/.test(arg)){
            rc = CS.v3.getParentElementWithClass(el,arg.substring(1));
         } else {
            if(/^[^\.\*\s]+\.[^\.\*\s]+$/.test(arg)){
               var tagAndClass = CS.v3.I18N.split(arg,'.');
               var tmp = el;
               tagName = tagAndClass[0].toUpperCase();
               do {
                  tmp = $(Element.up(tmp,tagName));
               } while (tmp && !Element.hasClassName(tmp,[tagAndClass[1]]));
               rc = tmp;
            }
         }
      }
      if(Object.isNumber(idx) && idx >0){
         rc = Element.up(rc, arg, idx-1);
      }
   }
*/
   return rc;
};
CS.v3.down = function(el,arg){
   var rc;
   if(el){
      rc = Element.down(el,arg);
   }
   return rc;
};
CS.v3.select = function(el,arg){
   var rc;
   if(el){
      rc = Element.select(el,arg);
   }
   return rc;
};
CS.v3.getParentElement = function(el,tag){
   var parent = el;
   var mytag = tag.toUpperCase();
   do {
      parent = parent.parentNode;
   } while (parent && (mytag !== parent.tagName ));
   if(parent){
      return $(parent);
   }
};
CS.v3.getParentElementWithClass = function(el,cls){
/* var rc = Element.up(el,cls); return rc; */
   var rc;
   var parent = el;
   if(cls){
      if(cls.startsWith('.')){
         cls = cls.substring(1);
      }
      var exit = false;
      var tag;
      do {
         parent = parent.parentNode;
         tag = parent.tagName;
         if(tag==='HTML'){
            exit = true;
         }
      } while (!exit && parent && (!Element.hasClassName($(parent),cls)) );
      if(parent){
         var obj = $(parent);
         tag = obj.tagName;
         if(tag!=='HTML'){
            rc = obj;
         }
      }
   }
   return rc;
};
CS.v3.getParentElementWithClass_TEST = function(el,cls){
   var parent = el;
   if(cls){
      if(cls.startsWith('.')){
         cls = cls.substring(1);
      }
      var re = new RegExp("(^|\\s)" + cls + "(\\s|$)");
      var classes;
      do {
         parent = parent.parentNode;
         classes = parent.className?parent.classname:null;
      } while (parent && !(classes && classes.length>0 && (classes==cls || re.test(classes))));
   }
   if(parent){
      return $(parent);
   }
};
CS.v3.focus = function(el){
   var rc;
   if(el){
      try{
         // NOTE: this prevents MSIE error generated when focusing some elements.
         var obj=$(el);
         if(obj.type==='hidden' || obj.style.display=='none' || obj.disabled===true || obj.style.visibility=='hidden'){
            // NOOP
            CS.v3.Logger.log('focus:Error while attempting to focus element:' + obj);
         } else {
            obj.focus();
            rc = obj;
         }
      }
      catch(e){
         var id = Element.identify(el);
         var tag = el.tagName;
         var msg = 'id=' + id + '|tag=' + tag;
         CS.v3.Logger.log('focus:' + msg, e);
      }
   }
   return rc;
};
CS.v3.castToEl = function(id){
   var rc;
   if(id){
      rc = $(id);
   }
   return rc;
};
CS.v3.insertHiddenInput = function(el,nam,val,cssClass){
   if(el){
      var sb = new StringBuffer();
      sb.append('<input type="hidden" name="');
      sb.append(nam);
      sb.append('" value="');
      sb.append(val);
      var b = CS.v3.I18N.isEmpty(cssClass);
      if(!b){
         sb.append('" class="');
         sb.append(cssClass);
      }
      sb.append('" />');
      var htm = sb.toString(); sb=null;
      Element.insert(el,{bottom:htm});
   }
};
CS.v3.HTML = {
   errorBox: function(){
      var sb = new StringBuffer();
      sb.append('<div class="csPanel csPanelError">');
      sb.append('<div class="csPanelTopLeft"><div class="csPanelTopRight">');
      sb.append('<div class="csPanelContent"><div class="csPanelScroll">');
      sb.append('<div class="csErrorMsg"></div></div></div></div></div>');
      sb.append('<div class="csPanelBottomLeft"><div class="csPanelBottomRight"></div></div>');
      sb.append('<div class="csChevron csChevronLeft"></div></div>');
      var rc = sb.toString(); sb=null;
      return rc;
   },
   errorBoxCount: function(el,count){
      if (count == 1) {
         var msg = CS.v3.I18N.findProperty(el,'val-error-header-count');   
      } else {
         var msg = CS.v3.I18N.findProperty(el,'val-errors-header-count');
      }      
      var sb = new StringBuffer();
      sb.append('<div class="csPanel csFormStatus csPanelStatus csPanelErrors csBoxHide">');
      sb.append('<div class="csPanelTopLeft"><div class="csPanelTopRight">');
      sb.append('<div class="csPanelContent"><div class="csPanelScroll">');
      sb.append('<div class="csErrorMsg"><div class="csErrorMsgHeading"><h3>');
      sb.append(msg);

      sb.append('</h3><div class="ukpwsErrorMsgToggle"><a href="#" class="ukpwsErrorMsgToggle"><span class="csHideForSR" alt="Toggle display of error messages">Toggle display of error messages</span></a></div></div><div class="ukpwsErrorMsgList"></div></div></div></div></div></div>');
      sb.append('<div class="csPanelBottomLeft"><div class="csPanelBottomRight"></div></div>');
      sb.append('<div class="csChevron csChevronLeft"></div></div>');
      var rc = sb.toString(); sb=null;
      return rc;
   },
   // Template string for a field level error message, id is used to identify the corresponding field to allow click to move focus to that field
   // which can happen for all links with the 'ukpwsErrorMsgLink' CSS class 
   errorBoxMessageTemplate: new Template('<li><a href="#" class="ukpwsErrorMsgLink" data-fieldid="#{id}">#{msg}</a></li>'),
   // Generate the list of error messages, based on the provided field level messages.
   errorBoxMessages: function(bx,fieldErrorMessages){
      // Add a unordered list to the error message box that has already been generated and added to the page 
      var msgList = bx.down('div.ukpwsErrorMsgList'); 
      var uList = new Element('ul');
      msgList.insert({'bottom': uList});
      fieldErrorMessages.each(function(errMsg) {
         uList.insert({'bottom': CS.v3.HTML.errorBoxMessageTemplate.evaluate(
            {
               id:   errMsg.readAttribute('data-fieldid'),
               msg:  errMsg.innerHTML.stripTags()
            })
         });
      });
   },
   box: function(el){ /* TODO can find no references to this, if used we should use I18N for the 'X'  close */
      var sb = new StringBuffer();
      sb.append('<div id="box" class="pop hide"><div class="box box-em">');
      sb.append('<div class="heading"><div class="col-right"><a class="control close" title="close" href="#"><span>X</span></a></div>');
      sb.append('<h3 class="toggle"></h3></div><div class="chevron"></div>');
      sb.append('<div class="content">Text</div></div></div><div id="fader" class="hide"></div>');
      var rc = sb.toString(); sb=null;
      return rc;
   },
   ieCover: function(el){
      var rc = '<div class="ie6-cover"><iframe tabindex="-1" title="iframe for fixing ie bug" src="" scrolling="no" frameborder="0" border="0" framespacing="0" name=""></iframe></div>';
      return rc;
   }
};
/* MOVED FROM i18n.js */
function StringBuffer(){
   this.buffer = [];
}
StringBuffer.prototype.append = function append(string){
   this.buffer.push(string);
   return this;
};
StringBuffer.prototype.toString = function toString(){
   return this.buffer.join("");
};
if(!CS.I18N){
   CS.I18N = {
      I18NValues: [],
      I18NDefaultLocale: 'default',
      findHash: function(key,locale){
         var b = this.isEmpty(locale);
         if(b){
            locale = this.I18NDefaultLocale;
         }
         var rc = key + '|' + locale;
         return rc;
      },
      isEmpty: function(x){
         var rc = false;
         if(x=='undefined' || x==null || x==undefined || x==''){
            rc = true;
         }
         return rc;
      },
      addProperty: function(key,locale,val){
         var hash = this.findHash(key,locale);
         this.I18NValues[hash] = val;
      }
   };
}
CS.v3.Logger = {
   log: function(s,e){
      var sb = new StringBuffer();
      sb.append('<li>v3:');
      sb.append(s);
      if(e!==undefined){
         sb.append('<em>');
         if(e instanceof Error){
            sb.append('|name=');
            sb.append(e.name);
            sb.append('|message=');
            sb.append(e.message);
            sb.append('|');
         } else {
            sb.append('e=' + e);
         }
         sb.append('</em>');
      }
      sb.append('</li>');
      var msg = sb.toString(); sb=null;
      var b = false;//this.isFirebug();
      if(b){
         console.info(s);
      } else {
         var el = CS.v3.castToEl("log");
         if(el){
            el.innerHTML += msg;
         }
      }
   },
   isFirebug: function(){
      var rc = false;
      if(window.console){
         var x = window.console.firebug;
         if(x){
            rc = true;
         }
      }
      return rc;
   }
};
CS.v3.I18N = {
  initialize: function(el){
   var rc = this.initializeLocale(el,'');
   if (typeof this.hasInitializedProperties === 'undefined') {
      this.initializeProperties();
      this.hasInitializedProperties = true;
   }
   return rc;
  },
  initializeLocale: function(el,locale){
   if(this.isEmpty(locale)){
      locale = this.walkUpTreeForLocale(el);
      if(this.isEmpty(locale)){
         locale = CS.I18N.I18NDefaultLocale;
      }
      this.setLocale(el,locale);
   }
   return locale;
  },
  isEmpty: function(x){
   return CS.I18N.isEmpty(x);
  },
  isEmptyOrPlaceholderEl: function(el){
   var val = this.getValueUnmasked(el,true);
   var rc = this.isEmpty(val);
   return rc;
  },
  isEmptyOrBeginsWithMask: function(val,mask){
   var rc = false;
   var b = (this.isEmpty(val)) || (val == mask) || (val.startsWith(mask));
   if(b){
      rc = true;
   }
   return rc;
  },
  getTagName: function(el){
   // NOTE: while this is useful for NPE avoidance, it comes at a performance cost, avoid if possible!
   var rc;
   if(el){
      rc = el.tagName;//testing  ('' + this.toUpper(el.tagName);
   }
   return rc;
  },
  arrayHelper: function(ar,val){
   var rc;
   if(ar!=null){
      var x = ar[val];
      if(!this.isEmpty(x)){
         rc = x;
      }
   } else {
      rc = '';
   }
   return rc;
  },
  walkUpTreeForLocale: function(el){
   var locale=this.getLocale(el);
   var b = this.isEmpty(locale);
   if(b){
      //var startEl = el;
      var exit = false;
      var tag;
      var ar = [];
      //ar.push(el);
      while(this.isEmpty(locale) && exit===false){
         if(el){
            ar.push(el);
            el = Element.up(el);
            locale = this.getLocale(el);
            tag = el.tagName;
            if(tag==='HTML'){
               exit = true;
            }
         } else {
            exit = true;
         }
      }
      // add the language to the first element to simplify future searches.
      b = this.isEmpty(locale);
      if(b){
         locale = CS.I18N.I18NDefaultLocale;
      }
      //this.setLocale(startEl,locale);
      // set the locale back down the tree to save future walks
      var i = ar.length;
      var obj;
      while(i--){
         obj = ar[i];
         this.setLocale(obj,locale);
      }
   }
   return locale;
  },
  getLocale: function(el){
   var locale;
   if(el){
      locale = Element.readAttribute(el,'lang');
      if(this.isEmpty(locale)){
         locale = Element.readAttribute(el,'xml:lang');
      }
   }
   return locale;
  },
  setLocale: function(el,locale){
   this.updateAttribute(el,'lang',locale);
   //this.updateAttribute(el,'xml:lang',locale);
  },
  writeLocale: function(locale){
   var sb = new StringBuffer();
   sb.append(' lang="');
   sb.append(locale);
   //sb.append('" xml:lang="');
   //sb.append(locale);
   sb.append('"');
   var rc = sb.toString(); sb=null;
   return rc;
  },
  getYear_numeric: function(el,val){
   var x = this.convertNumber(el,val);
   var rc = this.zeroPad(el,x,4);
   return rc;
  },
  getNumbers: function(el){
   var rc = this.findProperty(el,'numbers');
   return rc;
  },
  getCharacters: function(el){
   var rc = this.findProperty(el,'characters');
   return rc;
  },
  getNumber: function(el,val){
   var rc = 0;
   var ar = this.getNumbers(el);
   var size = ar.length;
   var i = parseInt(val,10);
   if(i<size){
      rc = ar[i];
   }
   return rc;
  },
  getEmailLocal: function(el){
   var rc = this.findProperty(el,'email_local');// NOTE: this is prior to and including the '@', the rest is a DOMAIN and has  more stingent rules!
   return rc;
  },
  getSymbols: function(el){
   var rc = this.findProperty(el,'symbols');
   return rc;
  },
  getSymbol: function(el,val){
   var ar = this.getSymbols(el);
   var size = ar.length;
   var i = parseInt(val,10);
   if(i<size){
      rc = ar[i];
   }
   return rc;
  },
  getSymbol_thousands: function(el){
   return this.getSymbol(el,0);
  },
  getSymbol_decimal: function(el){
   return this.getSymbol(el,1);
  },
  getSymbol_time: function(el){
   return this.getSymbol(el,2);
  },
  getUnit: function(el,val){
   var rc = this.findProperty(el,'units_long');
   return rc;
  },
  getUnit_days: function(el){
   return this.getUnit(el,0);
  },
  getUnit_hours: function(el){
   return this.getUnit(el,1);
  },
  getUnit_minutes: function(el){
   return this.getUnit(el,2);
  },
  getUnit_seconds: function(el){
   return this.getUnit(el,3);
  },
  getUnit_milliseconds: function(el){
   return this.getUnit(el,4);
  },
  getCalculator: function(el){
   var rc = this.getNumbers(el);
   return rc;
  },
  calcOffset: function(symbol,days,hours,minutes,seconds,mms){
   var millis = mms;
   millis += 86400000*days;
   millis +=   360000*hours;
   millis +=    60000*minutes;
   millis +=     1000*seconds;
   if(symbol=='-'){
      millis = millis * -1;
   }
   return millis;
  },
  setDateObj:function(yr,mo,da){
   var rc = new Date(yr,mo-1,da);
   return rc;
  },
  createDate: function(val){
   var rc;
   if(this.isEmpty(val)){
      rc = new Date();
   } else {
      rc = new Date(val);
   }
   return rc;
  },
  createTime: function(val){
   var dt = this.createDate(val);
   var rc = this.getTime(dt);
   return rc;
  },
  getTime: function(dt){
   var rc;
   if(this.isEmpty(dt)){
      rc = '';
   } else {
      rc = dt.getTime();
   }
   return rc;
  },
  offsetDate: function(offset,isUTC){
   var rc;
   if(offset==0){
      rc = this.createDate();
   } else {
      var tm;
      if(isUTC){
         tm = this.getUTC();
      } else {
         tm = this.createTime();
      }
      var longDate = tm + offset;
      rc = this.createDate(longDate);
   }
   return rc;
  },
  localeSort: function(string1, string2){
   return string1.toString().localeCompare(string2.toString());
  },
  timeDiff: function(dateNew, dateOld){
   var rc;
   var b1 = this.isEmpty(dateOld);
   var b2 = this.isEmpty(dateNew);
   if(!b1 && !b2){
      rc = dateNew - dateOld;
   } else {
      rc = 0;
   }
   return rc;
  },
  timeDiffPattern: function(el,millis, pattern){
   var days = 0;
   var hours = 0;
   var minutes = 0;
   var seconds = 0;
   if(millis>86400000){
      days = Math.floor(millis / 86400000);
      millis = millis - (days * 86400000);
   }
   if(millis>3600000){
      hours = Math.floor(millis / 3600000);
      millis = millis - (hours * 3600000);
   }
   if(millis>60000){
      minutes = Math.floor(millis / 60000);
      millis = millis - (minutes * 60000);
   }
   if(millis>1000){
      seconds = Math.floor(millis / 1000);
      millis = millis - (seconds * 1000);
   }
   var ampm = false;
   var tz = null;
   var rc = this.pattern_replace(el,pattern, tz, -1, -1, days, hours, minutes, seconds, millis, ampm);
   return rc;
  },
  timeDiffText: function(el,millis){
   var days = 0;
   var hours = 0;
   var minutes = 0;
   var seconds = 0;
   if(millis>86400000){
      days = Math.floor(millis / 86400000);
      millis = millis - (days * 86400000);
   }
   if(millis>3600000){
      hours = Math.floor(millis / 3600000);
      millis = millis - (hours * 3600000);
   }
   if(millis>60000){
      minutes = Math.floor(millis / 60000);
      millis = millis - (minutes * 60000);
   }
   if(millis>1000){
      seconds = Math.floor(millis / 1000);
      millis = millis - (seconds * 1000);
   }
   var sb = new StringBuffer();
   if(days > 0){
      sb.append( days );
      sb.append( this.getUnit_days(el) );
   }
   if(hours > 0){
      sb.append( this.zeroPad(el,hours, 2) );
      sb.append( this.getUnit_hours(el) );
    }
   if(minutes > 0){
      sb.append( this.zeroPad(el,minutes,2) );
      sb.append( this.getUnit_minutes(el) );
   }
   if(seconds > 0){
      sb.append( this.zeroPad(el,seconds,2) );
      sb.append( this.getUnit_seconds(el) );
   }
   if(millis > 0){
      sb.append(millis);
      //sb.append( this.getUnit_milliseconds(el) );
   }
   var rc = sb.toString(); sb=null;
   return rc;
   },
  zeroPad: function(el,x,len){
   var rc = new String(x + '');
   var zero = this.getNumber(el,0);
   var zlen = this.getLength(zero);
   if(zlen>0){// NOTE: this is a Sanity check for the I18N framework, we do not want to iterate forever if EMTPY!
      while(this.getLength(rc) < len){
         rc = new String(zero + rc);
      }
   }
   return rc;
  },
  zeroTrim: function(el,x,len){
   /* NOTE: this is currently intentionally designed to only trim ONE leading zero, it will likely be enhanced in the future */
   var rc = new String(x + '');
   var zero = this.getNumber(el,0);
   var zlen = this.getLength(zero);
   var c;
   var b;
   if(zlen>0){// NOTE: this is a Sanity check for the I18N framework, we do not want to iterate forever if EMTPY!
      c = rc.substring(0,1);
      b = (c==zero);
      if(b){
         rc = rc.substring(1,rc.length);
      }
   }
   return rc;
  },
  convertNumber: function(el,x){
   var rc = new String('');
   var b = this.isEmpty(x);
   if(!b){
      rc = x;
      var repVal;
      var i=10;
      while(i--){
         repVal = this.getNumber(el,i);
         rc = this.replaceString(rc,i,repVal);
      }
   }
   return rc;
  },
  unconvertNumber: function(el,x){
   var rc;
   var b = this.isEmpty(x);
   if(!b){
      rc = x;
      var repVal;
      var i=10;
      while(i--){
         repVal = this.getNumber(el,i);
         rc = this.replaceString(rc,repVal,i);
      }
   } else {
      rc = '';
   }
   return rc;
  },
  repChar:function(x,oldChar,newChar){
   var rc = x + '';
   rc = rc.gsub(oldChar,newChar);
   return rc;
  },
  pattern_obj: function(el,pattern,dt){
   var rc;
   if(dt){
      var yr = dt.getFullYear();
      var mo = dt.getMonth() + 1;
      var da = dt.getDate();
      var hr = dt.getHours();
      var mi = dt.getMinutes();
      var se = dt.getSeconds();
      var ms = dt.getMilliseconds();
      var tz = dt.getTimezoneOffset();
      var ampm = hr>12;
      rc = this.pattern_replace(el,pattern,tz,yr,mo,da,hr,mi,se,ms,ampm);
   } else {
      rc = '';
   }
   return rc;
  },
  pattern_cal: function(el,pattern,dt){
	//this.log('pattern_cal:init|pattern=' + pattern + '|dt=' + dt);
   var rc;
   if(dt){
      var yr = dt.getFullYear();
      var mo = dt.getMonth() + 1;
      var da = dt.getDate();
      var tz = dt.getTimezoneOffset();
      var ampm = false;
      rc = this.pattern_replace(el,pattern,tz,yr,mo,da,-1,-1,-1,-1,ampm);
   } else {
      rc = '';
   }
	//this.log('pattern_cal:exit|rc=' + rc);
   return rc;
  },
  pattern_parse: function(el,pattern){
   var rc = [];
   var size = this.getLength(pattern);
   var str;
   var lastc;
   var c;
   for(var i=0;i<size;i++){
      c = pattern.charAt(i);//this.getCharAt(mypattern,i);
      if((c!=lastc) && (lastc!='')){
         rc.push(str);
         str = c;
      } else {
         str +=c;
      }
      lastc = c;
   }
   var b = this.isEmpty(str);
   if(!b){
      rc.push(str);
   }
   return rc;
  },
  pattern_replace: function(el,pattern,isTZ,isYR,isMO,isDA,isHR,isMN,isSE,isMS,ampm){
	//this.log('pattern_cal:replace|pattern=' + pattern);
   var ar1 = this.pattern_parse(el,pattern);
   var EMPTY = new String('');
   var rc = EMPTY;
   var i = this.getLength(pattern);
   if(i>0){
      var str;
      var obj;
      var tmp;
      var d;
      var ar2 = new Array(i);
      while(i--){
         str = ar1[i];
         switch(str){
            case "SSS":
               if(isMS>-1){
                  tmp = this.convertNumber(el,isMS);
                  obj = this.zeroPad(el,tmp,3);
               }
               break;
            case "ss":
               if(isSE>-1){
                  obj = this.getArrayValue(el,'ss',isSE);
               }
               break;
            case "mm":
               if(isMN>-1){
                  obj = this.getArrayValue(el,'mm',isMN);
               }
               break;
            case "hh":
               if(isHR>-1){
                  if(isHR>12){
                     obj = this.getArrayValue(el,'HH',isHR-12);
                  } else {
                     obj = this.getArrayValue(el,'HH',isHR);
                  }
               }
               break;
            case "HH":
               if(isHR>-1){
                  obj = this.getArrayValue(el,'HH',isHR);
               }
               break;
            case "aa":
               if(isHR>-1){
                  if(isHR>12){
                     ampm=1;
                  }
                  obj = this.getArrayValue(el,'AA',ampm);
               }
               break;
            case "AA":
               if(isHR>-1){
                  if(isHR>12){
                     ampm=1;
                     tmp = this.getArrayValue(el,'AA',ampm);
                     obj = this.toUpper(tmp);
                  }
               }
               break;
            case "dd": // pass thru
            case "DD":
               if(isDA>-1){
                  obj = this.getArrayValue(el,'DD',isDA-1);
               }
               break;
            case "MMMM": // pass thru
            case "MMM": // pass thru
            case "MM": // pass thru
               if(isMO>-1){
                  obj = this.getArrayValue(el,str,isMO-1);
               }
               break;
            case "yyyy": // pass thru
            case "YYYY":   obj = this.getArrayYearValue(el,'YYYY',isYR);
               break;
            case "yy": // pass thru
            case "YY":     obj = this.getArrayYearValue(el,'YY',isYR);
               break;
            case "y": // pass thru
            case "Y":      obj = this.getArrayYearValue(el,'Y',isYR);
               break;
            case "GG":     obj = this.getArrayValue(el,'GG',1);// TODO implement 'BC' (0)
               break;
            case "ZZZZ":   obj = this.convertNumber(el,isTZ);
               break;
            case "EEEE":
               if(isYR > -1 && isMO > -1 && isDA > -1){
                  d = this.setDateObj(isYR,isMO,isDA);
                  tmp = d.getDay();
                  obj = this.getArrayValue(el,'DDDD',tmp);
               }
               break;
            case "EEE":
               if(isYR > -1 && isMO > -1 && isDA > -1){
                  d = this.setDateObj(isYR,isMO,isDA);
                  tmp = d.getDay();
                  obj = this.getArrayValue(el,'DDD',tmp);
               }
               break;
            case "WW":
               if(isYR > -1 && isMO > -1 && isDA > -1){
                  d = this.setDateObj(isYR,isMO,isDA);
                  obj = this.getWeek(el,d);
               }
               break;
            case "JJJJ":
               if(isYR > -1 && isMO > -1 && isDA > -1){
                  d = this.setDateObj(isYR,isMO,isDA);
                  obj = this.getJulian(d);
               }
               break;
            case "JJJ":
               if(isYR > -1 && isMO > -1 && isDA > -1){
                  d = this.setDateObj(isYR,isMO,isDA);
                  obj = this.getJulianThisYear(d);
               }
               break;
            case ' ':
               obj = str;
               break;
            case ':':
               if(isHR>-1 && isMN>-1){
                  obj = str;
               }
               break;
            case '.':
               if(isMS>-1){
                  obj = str;
               }
               break;
            case '-':// passthru
            case '/':// passthru
            case ',':
               if(isYR > -1 && isMO > -1 && isDA > -1){
                  obj = str;
               }
               break;
            case "d": // pass thru
            case "D": // pass thru
               if(isDA>-1){
                  tmp = this.getArrayValue(el,'DD',isDA-1);
                  obj = this.zeroTrim(el,tmp,1);
               }
               break;
            case "M":
               if(isMO>-1){
                  tmp = this.getArrayValue(el,'MM',isMO-1);
                  obj = this.zeroTrim(el,tmp,1);
               }
               break;
            default:
               obj = EMPTY;
               break;
         }
         var b = this.isEmpty(obj);
         if(!b){
            ar2[i] = obj;
            ar1[i] = EMPTY;
         }
      }
      var x;
      var sb = new StringBuffer();
      var size = this.getLength(ar2);
      for(var j=0;j<size;j++){
         x = ar2[j];
         sb.append(x);
      }
      rc = sb.toString(); sb=null;
   }
   return rc;
  },
   calculateTickInterval: function(pattern){
      var rc = 0;
      var hrs = pattern.indexOf('hh') + pattern.indexOf('HH');
      var min = pattern.indexOf('mm');// NOTE: 'MM' = months!
      var sec = pattern.indexOf('ss') + pattern.indexOf('SS');
      if(sec>0){
         rc = 1;
      } else {
         if(min>0){
            rc = 10;//seconds
         } else {
            if(hrs>0){
               rc = 60;//seconds
            }
         }
      }
      return rc;
   },
   deriveLocales: function(el){
      var locale=this.getLocale(el);
      var DASH = new String('-');
      var SCORE = new String('_');
      var b = this.isEmpty(locale);
      if(b){
         locale = CS.I18N.I18NDefaultLocale;
      }
      var clean = locale.replace(DASH,SCORE);
      var ar = this.split(clean,SCORE);
      var language = ar[0];
      var country = ar[1];
      var variant = ar[2];
      var loc0 = new String('');
      b = this.isEmpty(language);
      if(!b){
         var loc1 = language;
         var b2 = this.isEmpty(country);
         if(!b2){
            var loc2 = loc1 + SCORE + country;
            var b3 = this.isEmpty(variant);
            if(!b3){
               var loc3 = loc2 + SCORE + variant;
               rc = [loc3,loc2,loc1,loc0];
            } else {
               rc = [loc2,loc1,loc0];
            }
         } else {
            rc = [loc1,loc0];
         }
      } else {
         rc = [loc0];
      }
      return rc;
   },
   getDaysInMonth: function(mo,yr){
      var fullyear = parseInt(yr,10);
      if(fullyear<1){
         var dt = this.createDate();
         fullyear = dt.getFullYear();
      }
      var leap = this._isLeapYear(fullyear);
      var feb = 28;
      if(leap){
         feb = 29;
      }
      var ar = [0,31,feb,31,30,31,30,31,31,30,31,30,31];
      var rc = ar[mo];
      return rc;
   },
   getJulian: function(dt){
      var rc = Math.floor((dt / 86400000) - (dt.getTimezoneOffset()/1440) + 2440587.5);
      return rc;
   },
   getJulianThisYear:function(dt){
      var target = this.getJulian(dt);
      var year = dt.getFullYear();
      var jan1 = this.setDateObj(year,1,1);//jan 1st
      var jan1j = this.getJulian(jan1);
      var rc = jan1j - target;
      return rc;
   },
   _isLeapYear: function(yr){
      var rc = !(yr%4) && (yr%100) || !(yr%400);
      return rc;
   },
   parseDateField: function(el,pattern,dt_now){
		//this.log('parseDateField:init|el=' + el + '|pattern=' + pattern);
      var val = this.getValueUnmasked(el,true);
      var rc = this.parseDateFieldVal(el,val,pattern,dt_now);
		//this.log('parseDateField:exit|rc=' + rc);
      return rc;
   },
   parseDateFieldVal: function(el,val,pattern,dt_now){
		//this.log('parseDateFieldVal:pattern=' + pattern + '|val='+ val);
      if(this.isEmpty(dt_now)){
         dt_now = this.createDate();
      }
      var now_yy = dt_now.getFullYear();
      var now_mm = dt_now.getMonth() + 1;
      var now_dd = dt_now.getDate();
      var yy = this.parseYear(el,val,pattern,now_yy);
      var mm = this.parseMonth(el,val,pattern,now_mm);
      var dd = this.parseDate(el,val,pattern,now_dd);
      //var now_hrs = dt_now.getHours();
      //var now_mins = dt_now.getMinutes();
      //var now_secs = dt_now.getSeconds();
      //var hour = this.parseDate(el,val,pattern,now_hrs);
      //var mins = this.parseDate(el,val,pattern,now_min);
      //var secs = this.parseDate(el,val,pattern,now_sec);
      var rc = this.setDateObj(yy,mm,dd);
		//this.log('parseDateFieldVal:pattern=' + pattern + '|val=' + val + '|rc=' + rc);
      return rc;
   },
   parseDateFieldValString: function(el,val,pattern){
      var EMPTY = new String('');
      var defyy = this.getShortPattern(pattern,'Y');
      var defmm = this.getShortPattern(pattern,'M');
      var defdd = this.getShortPattern(pattern,'D');
      var yy = this.parseYear(el,val,pattern,defyy);
      var mm = this.parseMonth(el,val,pattern,defmm);
      var dd = this.parseDate(el,val,pattern,defdd);
      var tz = null;
      var ampm = false;
      var rc = this.pattern_replace(el,pattern,tz,yy,mm,dd,-1,-1,-1,-1,ampm);
      return rc;
   },
   parseYear: function(el,str,pattern,def){
      var rc = this.parseCommon(el,str,pattern,'Y',def);
      return rc;
   },
   parseMonth: function(el,str,pattern,def){
		//this.log('parseMonth:init');
      var rc = this.parseCommon(el,str,pattern,'M',def);
		//this.log('parseMonth:exit|rc=' + rc + '|str=' + str + '|pattern=' + pattern);
      return rc;
   },
   parseDate: function(el,str,pattern,def){
      var rc = this.parseCommon(el,str,pattern,'D',def);
      return rc;
   },
   parseHour: function(el,str,pattern,def){
      var rc = this.parseCommon(el,str,pattern,'h',def);
      return rc;
   },
   parseMinute: function(el,str,pattern,def){
      var rc = this.parseCommon(el,str,pattern,'m',def);
      return rc;
   },
   parseSecond: function(el,str,pattern,def){
      var rc = this.parseCommon(el,str,pattern,'s',def);
      return rc;
   },
   parseCommon: function(el,str,pattern,abbr,def){
		//this.log('parseCommon:init|pattern=' + pattern + '|str=' + str + '|abbr=' + abbr);
      var shortpattern = this.getShortPattern(pattern,abbr);
      var val = this.extractPatternValue(el,str,pattern,shortpattern,def);
      var rc;
      if(abbr=='Y'){
         rc = this.getArrayYearValue(el,shortpattern,val);
      } else {
         rc = val;
      }
		//this.log('parseCommon:exit|rc=' + rc);
      return rc;
   },
   getValueUnmasked: function(el,inputValue){
      // extends ability of getValue by also returning the 'placeholder' mask as EMPTY;
      var val = this.getValue(el,inputValue);
      var rc = val;
      var b = this.isEmpty(val);
      if(!b){
         var mask = this.getPlaceholder(el);
         b = this.isEmpty(mask);
         if(!b){
            b = this.isEmptyOrBeginsWithMask(val,mask);
            if(b){
               rc = this.replaceString(val,mask,'');
            }
         }
      }
      return rc;
   },
   getValue: function(el,inputValue){
      // TODO  must be updated for HTML5 input types
      // NOTE: inputValue (when true) will effect the returncode from 'SELECT' items (the inputvalue in the HTML vs. the displayed text on the page), might also be useful for BUTTON and PASSWORD in the future
		//this.log('getValue:init|el=' + el);
      var rc;
      if(el){
         var tag = el.tagName;
         switch(tag){
            case 'INPUT':
               var typ = el.type;
               switch(typ){
                  case 'checkbox':
                     var checked = this.isChecked(el);
                     if(checked){
                        rc = el.value;
                     }
                     break;
                  case 'radio':
                     rc = this.getRadioSelectedText(el);
                     break;
                  default:
                     rc = el.value;
                     break;
               }
               break;
            case 'SELECT':
               rc = this.getDropdownSelected(el,inputValue);
               break;
            case 'OPTION':
               rc = this.getOptionSelected(el,inputValue);
               break;
            case 'TEXTAREA':
               rc = el.value;
               break;
            default:
               rc = this.getInnerText(el);
               break;
         }
      } else {
         rc = '';
      }
		//this.log('getValue:exit|rc=' + rc);
      return rc;
   },
   setValue: function(el,str){
      // TODO  must be updated for HTML5 input types
		//this.log('setValue:init|el=' + el);
      if(el){
         var tag = el.tagName;
         switch(tag){
            case 'INPUT':
               var typ = el.type;
               if(typ==='radio' || typ==='checkbox'){
                  this.setRadioSelectedText(el,str);
               } else {
                  el.value = str;
               }
               break;
            case 'SELECT':
               this.setDropdownSelectedText(el,str);
               break;
            case 'TEXTAREA':
               el.value = str;
               break;
            case 'BUTTON':
               el.value = str;
               break;
            default:
               CS.v3.updateFix(el,str);
               break;
         }
      }
		//this.log('setValue:exit');
   },
   getType: function(el){
      // NOTE: while this is useful for NPE avoidance, it comes at a performance cost, avoid if possible!
      var rc='';
      if(el){
         //rc = this.toUpper(Element.readAttribute(el,'type'));
         rc = this.toUpper(el.type);
      }
      return rc;
   },
   getDropdownIsMultiple: function(el){
      var rc = false;
      if(el){
         var selMulti = Element.readAttribute(el,'multiple');
         rc = !this.isEmpty(selMulti);// NOTE: return 'true' for non empty,  could look for match on 'multiple'
      }
		//this.log('getDropdownIsMultiple:rc=' + rc);
      return rc;
   },
   getDropdownSingleOption: function(el){
      var rc;
      if(el){
         var selected = el.selectedIndex;
         if (selected != -1) {
            rc = $(el.options[selected]);
         }
      }
      return rc;
   },
   getDropdownSelected: function(el,inputValue){
      var rc;
      if(el){
         var isMulti = this.getDropdownIsMultiple(el);
         if(!isMulti){
            var option = this.getDropdownSingleOption(el);
            rc = this.getOptionSelected(option,inputValue);
         } else {
            var delim = new String('|');//config?
            var sb = new StringBuffer();
            var ar = el.options;
            var i = ar.length;
            var obj;
            var str;
            var selval;
            while(i--){
               obj = $(ar[i]);
               selval = Element.readAttribute(obj,'selected');
               if(!this.isEmpty(selval)){
                  str = this.getOptionSelected(obj,inputValue);
                  sb.append(str);
                  sb.append(delim);
               }
            }
         }
      }
      return rc;
   },
   getDropdownSelectedValue: function(el){
      var rc = this.getDropdownSelected(el,true);
      return rc;
   },
   getDropdownSelectedText: function(el){
      var rc = this.getDropdownSelected(el,false);
      return rc;
   },
   getOptionSelected: function(el,inputValue){
      var rc;
      if(el){
         if(inputValue){
            rc = el.value;
         } else {
            rc = el.text;
         }
      } else {
         rc = '';
      }
      return rc;
   },
   getOptionSelectedValue: function(el){
      var rc = this.getOptionSelected(el,true);
      return rc;
   },
   getOptionSelectedText: function(el){
      var rc = this.getOptionSelected(el,false);
      return rc;
   },
   setDropdownSelectedText: function(el,str){
      if(el){
         var ar = el.options;
         var i = ar.length;
         var obj;
         var txt;
         while(i--){
            obj = $(ar[i]);
            txt = obj.text;
            if(txt==str){
               el.selectedIndex = i;
               break;
            }
         }
      }
   },
   setRadioSelectedText: function(el,str){
      // TODO if there is ever a requirement
   },
   getRadioSelectedText: function(el){
      var rc;
      if(el){
         var nam = Element.readAttribute(el,'name');
         var formEl = CS.v3.getParentElement(el,'FORM');
         var str = 'INPUT';//'INPUT[name=' + nam + ']';
         var ar = Element.select(formEl,str);
         var i = ar.length;
         var inp;
         var objnam;
         var chk;
         while(i--){
            inp = ar[i];
            objnam = Element.readAttribute(inp,'name');
            if(objnam==nam){
               chk = this.isChecked(inp);
               if(chk){
                  rc = inp.value;// NOTE: do not use getValue() as it is recursive with this!
                  break;//test
               }
            }
         }
      } else {
         rc = '';
      }
      return rc;
   },
   isChecked: function(el){
      var rc = false;
      var b = CS.v3.I18N.isEmpty(el);
      if(!b){
         rc = el.checked;
      }
      return rc;
   },
   setDataAttribute: function(el,nam,val){
      this.updateAttribute(el,nam,val);
   },
   getDataAttribute: function(el,val,def){
      var rc;
      if(el){
         rc = Element.readAttribute(el,val);
      }
      if(this.isEmpty(rc)){
         rc = def;
      }
      return rc;
   },
   updateAttribute: function(el,nam,val){
      if(el){
         if(Element.hasAttribute(el,nam)){
            el.removeAttribute(nam);
         }
         if(!this.isEmpty(val)){
            Element.writeAttribute(el,nam,val);
         }
      }
   },
   copyAttribute: function(fmEl,toEl,nam,def){
      var val = this.getDataAttribute(fmEl,nam,def);
      this.updateAttribute(toEl,nam,val);
   },
   setCustomAttributeDateTime: function(el,val){
      this.setDataAttribute(el,'data-cssort',val);
   },
   getCustomAttributeDateTime: function(el,def){
      var rc = this.getDataAttribute(el,'data-cssort',def);
      return rc;
   },
   setCustomAttributePattern: function(el,val){
      this.setDataAttribute(el,'data-cspattern',val);
   },
   getCustomAttributePattern: function(el,def){
      var rc = this.getDataAttribute(el,'data-cspattern',def);
      return rc;
   },
   getCustomAttributeBaseYear: function(el){
      var def = this.findProperty(el,'baseyear');//0
      var rc = this.getDataAttribute(el,'data-csbaseyear',def);
      return rc;
   },
   getUTC: function(){
      var dt = this.createDate();
      var now = this.getTime(dt);
      var min = dt.getTimezoneOffset();
      var offset = this.calcOffset('',0,0,min,0,0);
      var utc = now + offset;
      //var x = this.getClientTZOffset();
      //var diff = now-utc;
      return utc;
   },
   getClientTZOffset: function(){
      var dt = this.createDate();
      var min = dt.getTimezoneOffset();
      var rc = this.calcOffset('',0,0,min,0,0);
      return rc;
   },
   getCustomAttributeOffset: function(el,def,pattern){
      var rc = 0;
      var str = Element.readAttribute(el,'data-csoffset');
      if(this.isEmpty(str)){
         //str = this.getClientTZOffset();
         //this.setCustomAttributeOffset(str);
         rc = def;
      } else {
         var b = this.isEmpty(pattern);
         if(!b){
            var p = this.toLower(pattern);
            var x = this.extractPatternValue(el,str,p,'x','');
            var d = this.extractPatternValue(el,str,p,'dd',0);
            var h = this.extractPatternValue(el,str,p,'hh',0);
            var m = this.extractPatternValue(el,str,p,'mm',0);
            var s = this.extractPatternValue(el,str,p,'ss',0);
            rc = this.calcOffset(x,d,h,m,s,0);
				//this.log('getCustomAttributeOffset:mmsoffset=' + p + '|h=' + h + '|m=' + m + '|s=' + s + '|x=' + x + '|rc=' + rc);
         } else {
            var min = parseInt(str,10);
            rc = this.calcOffset('',0,0,min,0,0);
         }
      }
      return rc;
   },
   extractPatternValue: function(el,val,str,pattern,def){
		//this.log('extractPatternValue:init|val=' + val);
      var rc;
      var bval = this.isEmpty(val);
      if(!bval){
         var bstr = this.isEmpty(str);
         if(!bstr){
            var bpat = this.isEmpty(pattern);
            if(!bpat){
               var mystr = new String(str);
               var idx = mystr.indexOf(pattern);
               if(idx>-1){
                  var size;
                  if((pattern=='MMM') || (pattern=='MMMM')){
                     if(idx>1){
                        idx = idx-1;//setback for some delims
                     }
                     size = this.getLength(mystr) - idx;
                  } else {
                     size = this.getLength(pattern);
                  }
						//this.log('extractPatternValue:pattern=' + pattern + '|val=' + val + '|str=' + str + '|idx=' + idx + '|size=' + size);
                  var pat = mystr.substring(idx,idx+size);
                  var bp = this.isEmpty(pat);
                  if(!bp){
                     try{
                        /* NOTE: in future use el and getArray  for I18N externalization! */
                        var delims = this.findProperty(el,'date_delimiters');
                        if(delims){
                           var x = val.substring(idx,idx+size);
                           while(this.containsDelimiters(x,delims)){
                              idx++;
                              x = val.substring(idx,idx+size);
                           }
                        }
                     }
                     catch(e){
                        this.log('extractPatternValue:',e);
                     }
							//this.log('extractPatternValue:x=' + x + '|c=' + c);
                     rc = this.matchArrayValue(el,pattern,x);// test
                  }
               }
            }
         }
      }
      if(this.isEmpty(rc)){
         rc = def;
      }
		//this.log('extractPatternValue:exit|rc=' + rc);
      return rc;
   },
   containsDelimiters: function(str,ar){
		//this.log('containsDelimiters:init|str=' + str);
      var rc = false;
      var i = ar.length;
      var obj;
      while(i--){
         obj = ar[i];
         if(str.indexOf(obj)>-1){
            rc = true;
            break;
         }
      }
		//this.log('containsDelimiters:exit|rc=' + rc);
      return rc;
   },
   getShortPattern: function(str,pattern){
      var rc;
      var bstr = this.isEmpty(str);
      if(!bstr){
         var bpat = this.isEmpty(pattern);
         if(!bpat){
            try{
               var mystr = new String(str);
               var idx_start=mystr.indexOf(pattern);
               var size = this.getLength(mystr);
               var idx_stop = idx_start;
               var x;
               for(var i=idx_start-1;i<=size;i++){
                  x = mystr.indexOf(pattern,i);
                  if(x>=idx_start){
                     idx_stop = i;
                  }
               }
            }
            catch(e){
               this.log('getShortPattern:str=' + str,e);
            }
            var part = mystr.substring(idx_start,idx_stop+1);
            rc = new String(part);
         }
      } else {
         rc = '';
      }
      return rc;
   },
   setCustomAttributeOffset: function(el,val){
      this.setDataAttribute(el,'data-csoffset',val);
   },
   getWeek: function(el,dt){
      var d=this.createDate(dt.valueOf());
      var dayNr=(d.getDay()+6)%7;
      d.setDate(d.getDate()-dayNr+3);
      var year=d.getFullYear();
      var jan4=this.setDateObj(year,1,4);//jan4
      var dayDiff=(d-jan4)/86400000;
      var weekNum=(1+Math.ceil(dayDiff/7) - 4);// -4 for bug fix?
      var rc=this.convertNumber(el,weekNum);
      return rc;
   },
   matchArrayValue: function(el,pattern,val){
		//this.log('matchArrayValue:init|pattern=' + pattern + '|val=' + val);
      var rc;
      var ar = this.findArray(el,pattern);
      if(ar==null){
         rc = val;//usually for YYYY or YY
      } else {
         var pval = this.getLength(val);
         var plen = this.getLength(pattern);
         if(pval<plen){
            val = this.zeroPad(el,val,plen);
         }
         var i = this.getLength(ar);
         var lc = this.toLower(val);
         var item;
         var ilc;
         while(i--){
            item = ar[i];
            ilc = this.toLower(item);
            if(ilc==lc){
               rc = i + 1;
               break;
            }
            // TODO consider adding a 'possibilities' array and use it if only 1, else best match.
         }
      }
		//this.log('matchArrayValue:exit|rc=' + rc);
      return rc;
   },
   toLower: function(val){
      var rc;
      if(val){
         rc = val.toLowerCase();
      } else {
         rc = '';
      }
      return rc;
   },
   toUpper: function(val){
      var rc;
      if(val){
         rc = val.toUpperCase();
      } else {
         rc = '';
      }
      return rc;
   },
   getLength: function(obj){
      var rc = 0;
      var b = this.isEmpty(obj);
      if(!b){
			//var isArray = CS.v3.isArray(obj);
         //if(isArray){
         // rc  = obj.length;
         //} else {
            var typ = typeof(obj);
            rc = obj.length;
            if(typ !== 'string'){
               if(typ !== 'object'){
                  if(typ === 'number'){
                     var str = new String(obj);
                     rc = str.length;
							//this.log('getLength():got a Number:' + obj + '|type=' + typ);
                  } else {
                     // future work?
                     this.log('getLength():got an unknown:' + obj + '|type=' + typ);
                  }
               }
            }
         //}
      }
      return rc;
   },
   findArray: function(el,pattern){
      var rc = null;
      var key = null;
      if(pattern=='MMMM'){ key='months_long'; } else {
         if(pattern=='MMM'){ key='months_short'; } else {
            if((pattern=='MM')||(pattern=='M')){ key='months_numeric'; } else {
               if(pattern=='DDDD'){ key='days_long'; } else {
                  if(pattern=='DDD'){ key='days_short'; } else {
                     if((pattern=='DD')||(pattern=='D')){ key='days_numeric'; } else {
                        if((pattern=='HH') || (pattern=='hh')){ key='hours_numeric'; } else {
                           if(pattern=='mm'){ key='minutes_numeric'; } else {
                              if((pattern=='SS') || (pattern=='ss')){ key='seconds_numeric'; } else {
                                 if((pattern=='AA') || (pattern=='aa')){ key='time_am_pm'; } else {
                                    if((pattern=='GG') || (pattern=='gg')){ key='eras'; } else {
                                       if((pattern=='YYYY') || (pattern=='YY') || (pattern=='Y') ){ key = null; } else {
                                          if((pattern=='-') || (pattern=='+')){ key='numeric'; } else {
                                             this.log('findArray:bad pattern:' + pattern);
                                          }
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }
      if(key!=null){
         rc = this.findProperty(el,key);
      }
      return rc;
   },
   getArrayYearValue: function(el,pattern,val){
		//this.log('getArrayYearValue:enter:val=[' + val + ']');
      if(val == 0){
         var todayVal = this.createDate();
         val = todayVal.getFullYear();
      }
      var x = this.getCustomAttributeBaseYear(el);
      var baseyear = parseInt(x,10);
      //var str = this.rightString(val,2);
      var i = parseInt(val,10);
      if(i < baseyear){
         val = 100 + i;
      }
      var patstr = new String(pattern);
      var size = this.getLength(patstr);
      var str = this.rightString(val,size);
      var rc = this.convertNumber(el,str);
		//this.log('getArrayYearValue:exit:rc=[' + rc + ']');
      return rc;
   },
   getArrayValue: function(el,pattern,val){
		//this.log('getArrayValue:init|pattern=' + pattern + '|val=' + val);
      var ar = this.findArray(el,pattern);
      var rc = this.arrayHelper(ar,val);
		//this.log('getArrayValue:exit|rc=' + rc);
      return rc;
   },
   equalDates: function(dt1,dt2){
      var rc = false;
      var yr1 = dt1.getFullYear();
      var yr2 = dt2.getFullYear();
      if(yr1==yr2){
         var mo1 = dt1.getMonth();
         var mo2 = dt2.getMonth();
         if(mo1==mo2){
            var da1 = dt1.getDate();
            var da2 = dt2.getDate();
            if(da1==da2){
               rc = true;
            }
         }
      }
      return rc;
   },
   addError: function(el){
      if(el){
         var CSERROR = 'csError';
         if(!Element.hasClassName(el,CSERROR)){
            Element.addClassName(el,CSERROR);
         }
      }
   },
   removeError: function(el){
      if(el){
         var CSERROR = 'csError';
         if(Element.hasClassName(el,CSERROR)){
            Element.removeClassName(el,CSERROR);
         }
      }
   },
   parseInputDate: function(el){
      var val = this.getValueUnmasked(el,true);
      var dt_now = this.createDate();
      var pattern = this.getCustomAttributePattern(el,'');
      var rc = this.parseDateFieldVal(el, val, pattern, dt_now);
      return rc;
   },
   addProperty: function(key,locale,val){
      CS.I18N.addProperty(key,locale,val);
   },
   findProperty: function(el,key){
      var rc = null;
      if(el){
         var ar = this.deriveLocales(el);
         var size = this.getLength(ar);
         var i = 0;
         var locale;
         while(rc==null && i<size){
            locale = ar[i];
            rc = this.findPropertyLocale(key,locale);
            i++;
         }
      }
      return rc;
   },
   findPropertyLocale: function(key,locale){
      var hash = this.findHash(key,locale);
      var rc = CS.I18N.I18NValues[hash];
      return rc;
   },
   findHash: function(key,locale){
      return CS.I18N.findHash(key,locale);
   },
   reverseString: function(val){
      var rc;
      if(val){
         var sb = new StringBuffer();
         var str = new String(val);
         var x = this.getLength(str);
         var s;
         while(x--){
            s = this.getCharAt(str,x);
            sb.append(s);
         }
         rc = sb.toString(); sb=null;
      } else {
         rc = '';
      }
      return rc;
   },
   trimString: function(str){
      str = str.replace(/^\s+/,'');
      for(var i = str.length - 1; i >= 0; i--){
         if(/\S/.test(str.charAt(i))){
            str = str.substring(0, i + 1);
            break;
         }
      }
      return str;
   },
   getCharAt:function(str,i){
      var rc;
      if(i < str.length){
         rc = str.charAt(i);
      } else {
         rc = '';
      }
      return rc;
   },
   setCharAt:function(str,i,chr){
      var rc;
      if(i > str.length-1){
         rc = str;
      } else {
         rc = str.substr(0,i) + chr + str.substr(i+1);
      }
      return rc;
   },
   replaceString:function(str,oldVal,newVal){
      var rc = str;
      var b = this.isEmpty(str);
      if(!b){
         rc = str.toString();
         var old = str;
         var changed = true;
         // loop until all are replaced
         while(changed){
            rc = rc.replace(oldVal,newVal);
            if(rc==old){
               changed = false;
            } else {
               //changed = true;
               old = rc;
            }
         }
      }
      return rc;
   },
   getInnerText: function(el){
      var rc;
      if(el){
         var str = el.textContent;
         var b = this.isEmpty(str);
         if(!b){
            rc = str;
         } else {
            var txt = el.innerText;
            rc = txt;
         }
      } else {
         rc = '';
      }
      return rc;
   },
   setInnerText: function(el,str){
      if(el){
         if(typeof el.textContent != 'undefined'){
            el.textContent = str;
         } else {
            if(typeof el.innerText != 'undefined'){
               el.innerText = str;
            } else {
               if(typeof el.removeChild != 'undefined'){
                  while (el.hasChildNodes()){
                     el.removeChild(el.lastChild);
                  }
                  el.appendChild(document.createTextNode(str));
               }
            }
         }
      }
   },
   getCurrentSize: function(el){
      var val = this.getValueUnmasked(el,true);
      var str = this.getLength(val);
      var rc = parseInt(str,10);
      return rc;
   },
   truncateString: function(val,maxsize){
      var rc = val;
      if(this.getLength(val)>maxsize){
         // TODO this will require future work for surrogate/supplemental Unicode bytes!
         rc = val.substring(0,maxsize);
      }
      return rc;
   },
   rightString: function(str,size){
      var rev = this.reverseString(str);
      var trunc = this.truncateString(rev,size);
      var rc = this.reverseString(trunc);
      return rc;
   },
   assertId: function(el,suffix){
      /* @deprecated */
      var rc = this.createUniqueIdName(el);
      return rc;
   },
   createUniqueIdName: function(el,suffix){
      /* @deprecated */
      var rc = Element.identify(el);
      return rc;
   },
   mergeArrays: function(ar1,ar2){
		//this.log('mergeArrays:init');
      var rc;
      if(ar1==null){
         rc = ar2;
      } else {
         var ar = ar1.concat(ar2);
         rc = Array.uniq(ar);
      }
/*
      var size1 = this.getLength(ar1);
      var size2 = this.getLength(ar2);
      if(size1>0){
         if(size2>0){
            var tmp = ar1;
            var obj;
            var i = size2;
            while(i--){
               obj = ar2[i];
               tmp.push(obj);
            }
            rc = Array.uniq(tmp);
         } else {
            rc = ar1;
         }
      } else {
         rc = ar2;
      }
*/
		//this.log('mergeArrays:exit');
      return rc;
   },
   getStringFromChar: function(el,c){
      var rc = String.fromCharCode(c);
      return rc;
   },
   isNumeric: function(el,s){
      var ar;
      if(el){
         ar = this.getNumbers(el);
      }
      var rc = this.isFoundInArray(ar,s);
      return rc;
   },
   isAlpha: function(el,s){
      var ar;
      if(el){
         ar = this.getCharacters(el);
      }
      var rc = this.isFoundInArray(ar,s);
      return rc;
   },
   isAlphanumeric: function(el,s){
      var rc = false;
      if(el){
         var ar = this.getCharacters(el);
         rc = this.isFoundInArray(ar,s);
         if(!rc){
            ar = this.getNumbers(el);
            rc = this.isFoundInArray(ar,s);
            if(!rc){
               ar = this.getEmailLocal(el);
               this.isFoundInArray(ar,s);
               if(!rc){
                  ar = this.getSymbols(el);
                  this.isFoundInArray(ar,s);
               }
            }
         }
      }
      return rc;
   },
   isFoundInArray: function(ar,s){
      var rc = false;
      if(ar){
         var i = ar.length;
         var obj;
         while(i--){
            obj = ar[i];
            if(s == obj){
               rc = true;
               break;
            }
         }
      }
      return rc;
   },
   getPlaceholder: function(el){
      var rc = this.getDataAttribute(el,'data-csplaceholder','');// could use 'title' instead, but may not work in all cases
      return rc;
   },
   setPlaceholder: function(el,str){
      this.setDataAttribute(el,'data-csplaceholder',str);
   },
   getCustomAttributeMaxsize: function(el){
      var rc = 0;
      if(el){
         var def = '0';
         var str = this.getDataAttribute(el,'data-csmaxsize',def);
         rc = parseInt(str,10);
      }
      return rc;
   },
   getCustomAttributeMinsize: function(el){
      var rc = 0;
      if(el){
         var def = '0';
         var str = this.getDataAttribute(el,'data-csminsize',def);
         rc = parseInt(str,10);
      }
      return rc;
   },
   getCustomAttributeSubmitted: function(form){
      return CS.v3.I18N.getDataAttribute(form,'data-ukpwsSubmitted',false);
   },
   setCustomAttributeSubmitted: function(form,val){
      CS.v3.I18N.setDataAttribute(form,'data-ukpwsSubmitted',val);
   },
   split: function(str,delim){
      /* TODO: enhance for Unicode supplemental bytes */
      var rc = str.split(delim);
      return rc;
   },
   log: function(s,e){
      CS.v3.Logger.log("I18N:" + s,e);
   }
};
if(!CS.Validators){
   CS.Validators = [];
}
/* JavaScipt SCOTT */
CS.v3.Components.JSEnabled = function(el,options){new CS.v3.Config.JSEnabled(el,options);};
CS.v3.Config.JSEnabled = Class.create(CS.v3.Base.prototype, {
   initialize: function(el,options){
      var keycss = this.getCssClass();
      var s = "[class*='" + keycss + "']";
      this.allJSEnabled = Element.select(el,s);
      if(options.bind==='true'){this.bind();}
   },
   bind: function(){
      var keycss = this.getCssClass();
      var keynam = this.getKeyName();
      var keyvaltrue = this.getKeyValueTrue();
      var ar = this.allJSEnabled;
      var tag;
      var el;
      var i = ar.length;
      var FLAG = new String('csJSEnabled-init');
      var b;
      while(i--){
         el = ar[i];
         b=Element.hasClassName(el,FLAG);
         if(!b){
            Element.addClassName(el,FLAG);
            tag = el.tagName;
            switch(tag){
               case 'A':
                  this.updateHrefValue(el,keynam,keyvaltrue);
                  break;
               case 'FORM':
                  this.updateFormValue(el,keynam,keyvaltrue,keycss);
                  break;
               default:
                  CS.v3.I18N.setValue(el,keyvaltrue);//el.value = keyvaltrue;
                  break;
            }
         }
      }
   },
   updateFormValue: function(el,nam,val,css){
      CS.v3.insertHiddenInput(el,nam,val,css);
   },
   updateHrefValue: function(el,nam,val){
      var url = CS.v3.UrlFunctions.getHref(el);
      var myurl = CS.v3.UrlFunctions.hrefChanger(url,nam,val);
      CS.v3.UrlFunctions.setHref(el,myurl);
   },
   removeHrefValue: function(el,nam){
      //NOTE: this will purge the key if found.
   },
   getCssClass: function(){
      return 'csJSEnabled';
   },
   getKeyName: function(el){
      //NOTE: may make this a data-attribute
      return 'JSEnabled';
   },
   getKeyValueTrue: function(el){
      //NOTE: may make this a data-attribute
      return 'true';
   },
   getKeyValueFalse: function(el){
      //NOTE: may make this a data-attribute
      return 'false';
   },
   log: function(s,e){
      CS.v3.Logger.log("JSEnabled:" + s,e);
   }
});
CS.v3.UrlFunctions = {
   getHref: function(el){
      var rc = CS.v3.I18N.getDataAttribute(el,'href','#');//el.href;
      return rc;
   },
   setHref: function(el,val){
      CS.v3.I18N.updateAttribute(el,'href',val);//el.href=val
   },
   hrefChanger:function(url,nam,val){
		//this.log('hrefChanger:nam=' + nam + '|val=' + val  + '|url=' + url);
      var rc = '';
      var QUEST = new String('?');
      var AMP = new String('&');
      var HASH = new String('#');
      var isFound=false;
      var str;
      var act;
      var pairs;
      var anchor;
      var ar1 = CS.v3.I18N.split(url,HASH);
      var len1 = ar1.length;
      if(len1>0){
         str = ar1[0];
         if(len1>1){
            anchor = ar1[1];
         }
      }
      if(!CS.v3.I18N.isEmpty(str)){
         if(str.indexOf(QUEST) >=0){
            var ar2 = CS.v3.I18N.split(str,QUEST);
            var len2 = ar2.length;
            if(len2>0){
               act = ar2[0];
               if(len2>1){
                  pairs = ar2[1];
               }
            }
         }
      }
      if(!CS.v3.I18N.isEmpty(act)){
         rc = act;
      }
      if(!CS.I18N.isEmpty(pairs)){
         var clean = CS.v3.I18N.replaceString(pairs,'&amp;',AMP);
         var parms = CS.v3.I18N.split(clean,AMP);
         var i = parms.length;
         var parm;
         var pair;
         var mynam;
         var myval;
         var CACHEBUST = this.getCacheBust();
         var EQUALS = new String('=');
         while(i--){
            parm=parms[i];
            pair=CS.v3.I18N.split(parm,EQUALS);
            mynam=pair[0];
            myval=pair[1];
            if(mynam!=CACHEBUST){
               if(mynam==nam){
                  myval=val;
                  isFound=true;
               }
               rc = this.urlAppender(rc,mynam,myval);
            }
         }
      }
      if(!isFound){
         rc = this.urlAppender(rc,nam,val);
      }
      var b = CS.v3.I18N.isEmpty(anchor);
      if(!b){
         rc = rc + HASH + anchor;
      }
      //rc = CS.v3.I18N.replaceString(rc,'&','&amp;');//XHTML safe
      return rc;
   },
   urlAppenderCacheBuster: function(url){
      var CACHEBUST = this.getCacheBust();
      var val = CS.v3.I18N.createTime();
      var rc = this.urlAppender(url,CACHEBUST,val);
      return rc;
   },
   urlAppenderLocale: function(url,locale){
      var LOCALE = this.getLocaleKey();
      var rc = this.urlAppender(url,LOCALE,locale);
      return rc;
   },
   urlAppender: function(url,nam,val){
      var delim = new String('?');
      var EQUALS = new String('=');
      if(url.indexOf(delim) >=0){ delim = '&'; }
      var sb = new StringBuffer();
      sb.append(url);
      sb.append(delim);
      sb.append(nam);
      sb.append(EQUALS);
      sb.append(val);
		//var rc = url + delim + nam + '=' + val;
      var rc = sb.toString(); sb=null;
      return rc;
   },
   getCacheBust: function(){
      return '.cache';
   },
   getLocaleKey: function(){
      return '.locale';
   },
   forward: function(url,isReplace){
      var rc = false;
      var b = CS.v3.I18N.isEmpty(url);
      if(!b){
         try{
            if(isReplace){
               window.location.replace(url);
            } else {
               window.location.href = url;
            }
            rc = true;
         }
         catch(e){
            rc = false;
            this.log('forward:url=' + url,e);
         }
      }
      return rc;
   },
   log: function(s,e){
      CS.v3.Logger.log("UrlFunctions:" + s,e);
   }
};
/* NOTE: this API will likely be expanded to support session cookies as well as GoogleGEARS or HTML5 localStorage and sessionStorage */
CS.v3.PersistState = {
  get: function(key){
   var body = CS.v3.getBody();
   var rc = CS.v3.I18N.getDataAttribute(body,key,'');
   return rc;
  },
  set: function(key,val){
   var body = CS.v3.getBody();
   CS.v3.I18N.setDataAttribute(body,key,val);
  },
  replace: function(key,val){
   this.set(key,val);
  },
  insert: function(key,val){
   var s = this.get(key);
   var rc = s + '|' + val;
   this.set(key,rc);
   return rc;
  },
  remove: function(key,val){
   var s = this.get(key);
   var rc = CS.v3.I18N.replaceString(s,val,'');
   return rc;
  },
  setCookie: function(nam,val){
      // NOTE: this is currently for 1 day, refactor to allow config.
      var oneDay = 1*24*60*60*1000;
      var expDate = CS.v3.I18N.createDate();
      var expTime = CS.v3.I18N.getTime(expDate);
      expDate.setTime(expTime + oneDay);
      var expires = expDate.toGMTString();
      var rc = this.buildCookie(nam,val,expires);
      document.cookie = rc;
  },
  getCookie: function(nam){
   var rc = '';
   var obj = document.cookie;
   if(obj.length>0){
      var c_start=obj.indexOf(nam + '=');
      if(c_start!=-1){
         c_start=c_start + nam.length+1;
         var c_end=obj.indexOf(';',c_start);
         if(c_end==-1){
            c_end=obj.length;
            rc = unescape(obj.substring(c_start,c_end));
         }
      }
   }
   return rc;
  },
  clearCookie: function(nam){
   var pastExpires = new String("Sun, 09-Nov-97 01:00:00 GMT");
   var rc = this.buildCookie(nam,'CLEAR',pastExpires);
   document.cookie = rc;
  },
  buildCookie: function(nam,val,expires){
   var sb = new StringBuffer();
   sb.append(nam);
   sb.append('=');
   sb.append( escape(val) );
   sb.append('; expires=');
   sb.append(expires);
   var rc = sb.toString(); sb=null;
   return rc;
  },
  log: function(s,e){
   CS.v3.Logger.log("PersistState:" + s,e);
  }
};
CS.v3.calendarActive = '';
CS.v3.contextualMenuActive = '';
CS.v3.hintActive = '';
CS.v3.Components.Overlays = function(section,options){
   new CS.v3.Overlays(section,options);
};
CS.v3.Overlays = Class.create({
   initialize: function(){
      CS.v3.containerWrap = CS.v3.castToEl('csContainerWrap');
      CS.v3.ContainerCalendar = CS.v3.castToEl('csContainerCalendar');
      CS.v3.contextualMenu = CS.v3.castToEl('csMenuContextual');
      CS.v3.ContainerSearchNSelect = CS.v3.castToEl('csSearchNSelect');
      CS.v3.ContainerHint = CS.v3.castToEl('csPanelHint');
      CS.v3.csPanelCalendar = CS.v3.castToEl('csPanelCalendar');
      CS.v3.contextualMenuContent = CS.v3.castToEl('csMenuContextualContent');
      CS.v3.contextualMenuScroll = CS.v3.castToEl('csMenuContextualScroll');
      CS.v3.panelHintScroll = CS.v3.castToEl('csPanelHintScroll');
      CS.v3.csPanelHintWrap = CS.v3.castToEl('csPanelHintWrap');
      CS.v3.csHintChevron = CS.v3.castToEl('csHintChevron');
      CS.v3.MenuLogOn = CS.v3.castToEl('csMenuLogOn');
      CS.v3.MenuScrollLogOn = CS.v3.castToEl('csMenuLogOnScroll');
      CS.v3.MenuContentLogOn = CS.v3.castToEl('csMenuLogOnContent');
      this._bind(CS.v3.ContainerCalendar);
      //this._bind(CS.v3.ContainerSearchNSelect);
      //this._bind(CS.v3.contextualMenu);
      this._bind(CS.v3.ContainerHint);
   },
   _bind: function(node){
      Event.observe(node,'keydown', function(event){
         var target = $(Event.element(event));
         if(Element.hasClassName(target,'csOverlayFirstTab')){
            this._firstTab(target,event,node);
         }
         if(Element.hasClassName(target,'csOverlayLastTab')){
            this._lastTab(target,event,node);
         }
         if(event.keyCode===Event.KEY_ESC){// TODO - does this require the keyCode to compare?
            this._returnfocus(node);
         }
      }.bind(this));
      Event.observe(CS.v3.csPanelHintWrap,'mouseleave', function(event){
         var toEl;
         if(Prototype.Browser.IE){
            toEl = event.toElement;
         } else {
            toEl = $(event.relatedTarget);
         }
         if(toEl){
            if(!Element.hasClassName(toEl,'cskeyFocus')){
               CS.v3.ContainerHint.style.display = 'none';
            }
         }
      });
   },
   _firstTab: function(target,event,node){
      if(event.shiftKey && event.keyCode===Event.KEY_TAB){
         event.preventDefault(event);
         var obj = Element.down(node,'.csOverlayLastTab');
         CS.v3.focus(obj);
      }
   },
   _lastTab: function(target,event,node){
      if(!event.shiftKey && event.keyCode===Event.KEY_TAB){
         event.preventDefault(event);
         var obj = Element.down(node,'.csOverlayFirstTab');
         CS.v3.focus(obj);
      }
   },
   _returnfocus: function(node){
      this._hide(node);
      var obj;
      var myid = Element.identify(node);//node.id
      switch(myid){
         case 'csContainerCalendar':
            obj = CS.v3.calendarActive;
            break;
         case 'csContainerContextualMenu':
            obj = CS.v3.contextualMenuActive;
            break;
         case 'csContainerHint':
            obj = CS.v3.hintActive;
            break;
         default:
            // NOOP
            break;
      }
      CS.v3.focus(obj);
   },
   _hide: function(node){
      node.style.display = 'none';
   }
});
CS.v3.findOverlayWrapper = function(el){
   var rc;
   if(el){
      rc = CS.v3.getParentElementWithClass(el,'csOverlayContainer');// NOTE: find the overlay
   }
   return rc;
};
CS.v3.isInsideOverlay = function(el){
   var rc = false;
   if(el){
      var overlayel = this.findOverlayWrapper(el);
      if(overlayel){
         rc = true;
      }
   }
   return rc;
};
CS.v3.OverlaysObj = Class.create({
   initialize: function(m,event,content){
      CS.v3.containerWrap = event.findElement('div.csContainerWrap');
      if (!CS.v3.containerWrap) {
         // If can't find above, check for the content portlet pattern.
         CS.v3.containerWrap = event.findElement('div.ukpwsContainerWrap');
      }
      this.containerWrapLeft = CS.v3.containerWrap.offsetLeft;
      this.content = content;
      var wrap = m.wrap;
      if(wrap){
         this.pos = Element.cumulativeOffset(wrap);
      } else {
         this.pos = 0;
      }
      this.left = this.pos.left;
      // IE 7 & 8
      if(Prototype.Browser.IE && CS.v3.containerWrap.currentStyle.hasLayout){
         if(wrap){
            this.IELeft = wrap.getBoundingClientRect().left;
            this.left = this.IELeft -2;
         }
      }
      this.top = this.pos.top;
      this.dimen = Element.getDimensions(wrap);
      this.height = this.dimen.height;
      this.width = this.dimen.width;
      this.align = Element.readAttribute(m.target,'data-alignoverlay');
      /* Scroll */
      this.pos = Element.cumulativeOffset(wrap);
      this.posScroll = Element.cumulativeScrollOffset(wrap);
      this.viewScroll = document.viewport.getScrollOffsets();
      this.viewScrollTop = this.viewScroll.top;
      this.top = this.pos.top - this.posScroll.top + this.viewScrollTop;
      /* Scroll */
   },
   log: function(s,e){
      CS.v3.Logger.log("OverlaysObj:" + s,e);
   }
});
/* backgroundImageFix is for MSIE (prior to 3.2.1 this was in UICS-ie6.js) */
CS.v3.Components.backgroundImageFix = function(el,options){
   new CS.v3.Config.backgroundImageFix(el,options);
};
CS.v3.Config.backgroundImageFix = Class.create(CS.v3.Base.prototype, {
   initialize: function(el,options){
      this.bind();
   },
   bind: function(){
      // NOTE: this is a legacy MSIE issue, while other browsers will fall into the 'catch' block, it's best to skip this altogether!
      if(Prototype.Browser.IE){
         try{
            document.execCommand("BackgroundImageCache",false,true);
         }
         catch(e){
            this.log('bind:',e);
         }
      }
   },
   log: function(s,e){
      CS.v3.Logger.log("backgroundImageFix:" + s,e);
   }
});
CS.v3.OverlayIE6IFrame = {
   show: function(container,obj){
      var ie6Cover = Element.down(container,'iframe');
      if(ie6Cover){
         ie6Cover.style.width = obj.width;
         ie6Cover.style.height = obj.height;
      }
   }
};
CS.v3.Components.Security = function(el,options){
   new CS.v3.Security(el,options);
};
CS.v3.Security = Class.create(CS.v3.Base.prototype, {
   initialize: function(el,options){
		//this.log('initialize:init');
      this.forceNavigationMode(el);
      this.breakOutOfFrames(el);
      //this.preventBackButton(el);
      this.allAutocompleteOff = Element.select(el,"INPUT[class*='csAutocompleteOff']");
      if(options.bind==='true'){this.bind();}
   },
   bind: function(){
      var ar = this.allAutocompleteOff;
      if(ar){
         var i = ar.length;
         var el;
         while(i--){
            el = ar[i];
            this.preventAutocomplete(el);
         }
      }
   },
   breakOutOfFrames: function(el){
      /* @see: http://labs.idefense.com/intelligence/vulnerabilities/display.php?id=77 */
      if(window.top != window.self){
         window.top.location=window.self.location;
      }
   },
   preventAutocomplete: function(el){
      // NOTE: this is available via a CSS class as the attribute is not valid XHTML, but supported in most browsers.
      CS.v3.I18N.updateAttribute(el,'autocomplete','off');
   },
   preventBackButton: function(el){
      window.history.forward();
   },
   doForward: function(url){
      /* NOTE: this is used to prevent back  button to the previous page */
      CS.v3.UrlFunctions.forward(url,true);
   },
   forceNavigationMode: function(el){
      /* NOTE: this is used to fire the onload event when 'BACK' is used, cross-browser */
      window.history.navigationMode = 'compatible';
   },
   log: function(s,e){
      CS.v3.Logger.log("Security:" + s,e);
   }
});
/* Used by panels */
CS.v3.updateContent = Class.create(CS.v3.Base.prototype, {
   initialize: function(target){
      this.readInsertTarget = Element.readAttribute(target,'data-csinsertnodetarget');
      this.insertTargets = $$(this.readInsertTarget);
      this.insertTargetsLength = this.insertTargets.length;
      this.insertTarget = this.insertTargets[0];
      this.readToInsert = Element.readAttribute(target,'data-cscontentfromnode');
      this.InsertableNodes = $$(this.readToInsert);
      this.InsertableNodesLength = this.InsertableNodes.length;
      this.targetid = Element.identify(target);
   },
   _insert: function(content){
      // NOOP?
   }
});
CS.v3.appendContent = Class.create(CS.v3.Base.prototype, {
   initialize: function(target){
      this.updateNodes = new CS.v3.updateContent(target);
      this.prevInserted = Element.select(this.updateNodes.insertTarget,'.csInserted');
      this.prevInsertedLength = this.prevInserted.length;
      this.prevInsertedNum = this.prevInsertedLength;
      this.InsertableNode = $$(this.updateNodes.readToInsert)[this.prevInsertedNum];
      this.readToInsertTop = Element.readAttribute(target,'data-csinsertnodetop');
      if(!this.InsertableNode){
         this.InsertableNode = this.updateNodes.InsertableNodes[0];
      }
      if(this.readToInsertTop == 'true'){
         this.lastOrFirstElem = $$(this.updateNodes.readInsertTarget + " > *:first-child");
      } else {
         this.lastOrFirstElem = $$(this.updateNodes.readInsertTarget + " > *:last-child");
      }
   },
   _insert: function(content){
      var el = this.updateNodes;
      var obj = el.insertTarget;
      if(this.readToInsertTop == 'true'){
         Element.insert(obj,{top: content});
      } else {
         Element.insert(obj,{bottom: content});
      }
      new CS.v3.zIndex();
   }
});
CS.v3.addAddEntryContent = Class.create(CS.v3.Base.prototype, {
   initialize: function(target){
      var aC = new CS.v3.appendContent(target);
      var form = CS.v3.getParentElement(target,'FORM');
      Element.addClassName(form,'csHideAddEntryRow');
      var CSZEBRA = 'csZebra';// TODO try String
      var sb = new StringBuffer();
      sb.append('<tr class="csInserted ');
      var b = Element.hasClassName(aC.lastOrFirstElem[0],CSZEBRA);
      if(!b){
         sb.append(CSZEBRA);
      }
      sb.append(' csRowEm">');
      var innerCode = aC.InsertableNode.innerHTML;
      sb.append(innerCode);
      sb.append('</tr>');
      var str = sb.toString(); sb=null;
      this.rowInnerWrap = str; // NOTE: not sure if we need this outside of this function, should probably remove!
      aC._insert(str);
      var tr = Element.down(form,'TR.csInserted');
      var textarea = Element.down(tr,'TEXTAREA');// TODO add support for INPUT.csTextField
      if(textarea){
         Element.addClassName(textarea,'csVal-required');
      }
   }
});
CS.v3.cancelAddEntryContent = Class.create(CS.v3.Base.prototype, {
   initialize: function(target){
      var form = CS.v3.getParentElement(target,'FORM');
      if(form){
         var errorBox = Element.down(form,'.csFormStatus');
         if(errorBox){
            Element.addClassName(errorBox,'csBoxHide');
         }
         Element.removeClassName(form,'csHideAddEntryRow');
      }
   }
});
CS.v3.updateFix = function(el,str){
   if(el){
      //Element.update(el,str);
      el.innerHTML = str;
   }
};
CS.v3.isArray = function(obj){
	var rc = false;
	if(obj.constructor.toString().indexOf('Array') > -1){
		rc = true;
	}
   return rc;
};
CS.v3.getBody = function(el){
   var ar = document.getElementsByTagName('body');//$$('body');// could also use 'document.body;'
   var rc = ar[0];
   return rc;
};
/* AJAX visual identifier - as Ajax is being increasingly used, we SHOULD also use the Ajax.Responders.register  functions to reset the session timeout locally (when Ajax response is received) */
/*
Ajax.Responders.register({
   onCreate: CS.v3.updateAjaxIcon,
   onComplete: CS.v3.updateAjaxIcon
});
CS.v3.updateAjaxIcon = function(){
   var el = CS.v3.castToEl('csAjaxProgress');// DEMO only!
   if(el){
      var i = Ajax.activeRequestCount;// number of connections
      var CLS = 'ajax-spinner';
      if(i > 0){
         //el.style.display = 'inline';
         Element.addClassName(el,CLS);
      } else {
         //el.style.display = 'none';
         Element.removeClassName(el,CLS);
      }
      CS.v3.I18N.updateAttribute(el,'title',i);
      CS.v3.Timer.resetSessionTimerLocalAPI(el);
   }
};
*/
setTimeout(function(){
CS.v3.I18N.addProperty('Interstitial-message','','Loading, please wait...');
CS.v3.I18N.addProperty('months_short','',["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]);
CS.v3.I18N.addProperty('months_long','',["January","February","March","April","May","June","July","August","September","October","November","December"]);
CS.v3.I18N.addProperty('months_numeric','',["01","02","03","04","05","06","07","08","09","10","11","12"]);
CS.v3.I18N.addProperty('days_short','',["S","M","T","W","T","F","S"]);
CS.v3.I18N.addProperty('days_long','',["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]);
CS.v3.I18N.addProperty('days_numeric','',["01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"]);
CS.v3.I18N.addProperty('days_suffix','',["1st","2nd","3rd","4th","5th","6th","7th","8th","9th","10th","11th","12th","13th","14th","16th","17th","18th","19th","20th","21st","22nd","23rd","24th","25th","26th","27th","28th","29th","30th","31st"]);
CS.v3.I18N.addProperty('hours_numeric','',["00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23"]);
CS.v3.I18N.addProperty('minutes_numeric','',["00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59"]);
CS.v3.I18N.addProperty('seconds_numeric','',["00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59"]);
CS.v3.I18N.addProperty('time_am_pm','',["AM","PM"]);
CS.v3.I18N.addProperty('eras','',["BC","AD"]);
CS.v3.I18N.addProperty('baseyear','',"1970");
CS.v3.I18N.addProperty('numbers','',["0","1","2","3","4","5","6","7","8","9"]);
CS.v3.I18N.addProperty('characters','',["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]);
CS.v3.I18N.addProperty('units_long','',["day(s)","hour(s)","minute(s)","second(s)","mms"]);
CS.v3.I18N.addProperty('units_short','',["D","H","M","S","mms"]);
CS.v3.I18N.addProperty('email_local','',[".","@","!","#","$","&","'","*","+","-","/","=","?","^","_","`","{","|","}","~"]);
CS.v3.I18N.addProperty('symbols','',[",",".",":",";"," "]);
CS.v3.I18N.addProperty('numeric','',["-","+"]);
CS.v3.I18N.addProperty('date_delimiters','',["/","-",".",","," "]);
CS.v3.I18N.addProperty('currency','',null);
/* FR */
CS.v3.I18N.addProperty('months_short','fr',["janv","fevr","mars","avr","mai","juin","juil","aout","sept","oct","nov","dec"]);
CS.v3.I18N.addProperty('months_long','fr',["janvier","fevrier","mars","avril","mai","juin","juillet","aout","septembre","octobre","novembre","decembre"]);
CS.v3.I18N.addProperty('days_long','fr',["dimanche","lundi","mardi","mercredi","jeudi","vendredi","samedi"]);
CS.v3.I18N.addProperty('days_short','fr',["D","L","M","M","J","V","S"]);
});

