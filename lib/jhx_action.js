/*****************************************************************
** COPYRIGHT. HSBC HOLDINGS PLC ccyy. ALL RIGHTS RESERVED.
** THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPOSE FOR WHICH IT
** HAS BEEN PROVIDED. NO PART OF IT IS TO BE REPRODUCED,
** DISASSEMBLED, TRANSMITTED, STORED IN A RETRIEVAL SYSTEM NOR
** TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY OR
** FOR ANY OTHER PURPOSES WHATSOEVER WITHOUT THE PRIOR WRITTEN
** CONSENT OF HSBC HOLDINGS PLC.
*******************************************************************
** Navigation Event Management Javascript
** This script groups navigation event management functions.
** Extension for ESRA

** @file				action.js
** @version
*******************************************************************/

function registerEventValue(formId,action,value) {

	try {
			var divId = formId + "_divSetEvent";
			var form = getForm(formId);

		if (form != null) 
			{
				var div = getOrCreateDiv( form, divId);
				var eventInput = document.createElement("INPUT");
				eventInput.type = "hidden";
				eventInput.name = action;
				if (value != null) {
					eventInput.value = value;
				}
				div.appendChild(eventInput);
			}
	}
	catch (e) {
		//alert("Error durring field creation : " + e.message);
	}
}


function submitForm(formId) {
   var form = getForm(formId);
   CS.v3.submitForm(form);
}

function actionSubmit(formId,action,value) {
	registerEventValue(formId,action,value);
	submitForm(formId);
}

function getForm(formId)
{
	return document.getElementById(formId);
}

function getOrCreateDiv(form,divId)
{
	try {
	var div = document.getElementById(divId);
	if (div == null)
		{
		div = document.createElement("DIV");
		div.id = divId;
		div.style.display = "none";
		form.appendChild(div);
		}
	} catch(e) {	
		//alert("Error during the div creation : " + e.message);
	}	
	return div;
}

function jhx_openWindow(obj,args)
{
	var target=obj.getAttribute('target');
	var url=obj.getAttribute('href');
	var popup = window.open(url,target,args);
	popup.focus();
	return false;
}


function myFireEvent(element,event){
    if (document.createEvent) {
        // dispatch for firefox + others
        var evt = document.createEvent("HTMLEvents");
        evt.initEvent(event, true, true ); // event type,bubbling,cancelable
        return !element.dispatchEvent(evt);
    } else if (document.createEventObject){
        // dispatch for IE
        var evt = document.createEventObject();
        return element.fireEvent('on'+event,evt)
    }    
}
	
	function jhx_dd_refresh(obj, idDependent, idObject,aValue){

		var dependentDropdown = $(idDependent);
		if (typeof aValue === "undefined") {
		    aValue = dependentDropdown.value;
		}
		var aCollection ;
		try {
		aCollection= eval(idObject+ '[\'' +$F(obj)+'\']');
		if (aCollection != null) { 
				var aContent = '';
				for (var i=0;i<aCollection.size();i++) {
					if (aValue!=null && aValue==aCollection[i].v) {
					 aContent = aContent + '<option selected="selected" value="'+ aCollection[i].v + '">' + aCollection[i].l +'</option>';
					} else {
						aContent = aContent + '<option value="'+ aCollection[i].v + '">' + aCollection[i].l +'</option>';
					}
				}
				dependentDropdown.update(aContent);
				dependentDropdown.replace(dependentDropdown);
		} else {
			dependentDropdown.update("");
		dependentDropdown.replace(dependentDropdown);
		}
	} catch (e) {
		dependentDropdown.update("");
		dependentDropdown.replace(dependentDropdown);
	}
	try {
		myFireEvent(dependentDropdown,'change');
	} catch(e) {
	}
}
