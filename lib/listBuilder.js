/* Theme Pack 3.2.1+ DO NOT modify */
CS.v3.Components.ListBuilder = function(el,options){new CS.v3.Config.ListBuilder(el,options); };
CS.v3.Config.ListBuilder = Class.create(CS.v3.Base.prototype,{
	initialize: function(el,options){
		this.allBuilders = CS.v3.ListBuilder.findAllListBuilders(el);
		if(options.bind==='true'){this.bind();}
	},
	bind: function(){
		var ar = this.allBuilders;
		if(ar){
			var i = ar.length;
			var el;
			while(i--){
				el = ar[i];
				CS.v3.ListBuilder.addDecoration(el);
			}
		}
	}
});
CS.v3.ListBuilder = {
	findAllListBuilders: function(el){
		var rc;
		if(el){
			rc = Element.select(el,"div[class*='csListBuilder']");
		}
		return rc;
	},
	addDecoration: function(el){
		var FLAG = new String('csListBuilder-init');
		var b=Element.hasClassName(el,FLAG);
		if(!b){
			Element.addClassName(el,FLAG);
			//ADD
			var buttonAdd = Element.select(el,"button.csButtonAdd");
			var btnAddId = '';
			var i = buttonAdd.length;
			var obj;
			var frm;
			var b1;
			while(i--){
				obj = buttonAdd[i];
				btnAddId = this.decorateButton(obj);
				Event.observe(obj,'click', function(event){ this.addButtonClickEv(event); }.bind(this));
				frm = CS.v3.getParentElement(obj,'FORM');
				if(frm){
					b1 = Element.hasClassName(frm,FLAG);
					if(!b1){
						Element.addClassName(frm,FLAG);
						Event.observe(frm,'submit', function(event){ this.formSubmitEv(event); }.bind(this));
					}
				}
			}
			//REMOVE
			var buttonRemove = Element.select(el,"button.csButtonRemove");
			var btnRemoveId = '';
			var i2 = buttonRemove.length;
			var b2;
			while(i2--){
				obj = buttonRemove[i2];
				btnRemoveId = this.decorateButton(obj);
				b2 = Element.hasClassName(obj,FLAG);
				if(!b2){
					Element.addClassName(obj,FLAG);
					Event.observe(obj,'click', function(event){ this.removeButtonClickEv(event); }.bind(this));
				}
			}
			//CHECKBOXES
			var checkboxes = this.findCheckboxes(el);
			if(checkboxes){
				var x = checkboxes[0];
				var divBuilder = this.findParentDiv(x);
				var isCopy = this.isCopyItems(divBuilder);
				var isRemove = !isCopy;
				var i3 = checkboxes.length;
				var b3;
				while(i3--){
					obj = checkboxes[i3];
					Event.observe(obj,'click', function(event){ this.checkboxClickEv(event); }.bind(this));
					// keystroke definition
					if(!this.isToggleAll(obj)){
						b3 = Element.hasClassName(obj,FLAG);
						if(!b3){
							Element.addClassName(obj,FLAG);
							Event.observe(obj,'keydown', function(event){ this.checkboxKeydownEv(event); }.bind(this));
						}
					}
					this.decorateCheckbox(obj,btnAddId,btnRemoveId,isRemove);
				}
				this.removeDupesOnLeftAfterLoad(x);
			}
			// Expand all and Collapse all
			var categories = Element.select(el,'div.csTree A.csNavToggle');
			if(!categories || categories.length === 0){
				var headRight = Element.select(el,'div.csHeadingCol');
				var i4 = headRight.length;
				while(i4--){
					Element.remove(headRight[i4]);
				}
			} else {
				// Expand all
				var expandAll = Element.select(el,".csExpandAll");
				var i5 = expandAll.length;
				var b5;
				while(i5--){
					obj = expandAll[i5];
					b5 = Element.hasClassName(obj,FLAG);
					if(!b5){
						Element.addClassName(obj,FLAG);
						Event.observe(obj,'click', function(event){ this.expandAllClickEv(event); }.bind(this));
						Event.observe(obj,'keydown', function(event){ this.expandAllKeydownEv(event); }.bind(this));
					}
				}
				// Collapse all
				var collapseAll = Element.select(el,".csCollapseAll");
				var i6 = collapseAll.length;
				var b6;
				while(i6--){
					obj = collapseAll[i6];
					b6 = Element.hasClassName(obj,FLAG);
					if(!b6){
						Element.addClassName(obj,FLAG);
						Event.observe(obj,'click', function(event){ this.collapseAllClickEv(event); }.bind(this));
						Event.observe(obj,'keydown', function(event){ this.collapseAllKeydownEv(event); }.bind(this));
					}
				}
			}
			// categories
			var elCategory = Element.select(el,'a.csNavToggle');
			var i7 = elCategory.length;
			var b7;
			while (i7--){
				obj = elCategory[i7];
				b7 = Element.hasClassName(obj,FLAG);
				if(!b7){
					Element.addClassName(obj,FLAG);
					Event.observe(obj,'keydown', function(event){ this.categoryKeydownEv(event); }.bind(this));
				}
			}
			// process with button style behavior
			this.processButtonStyleBehavior(buttonAdd[0]);
		}
	},
	decorateButton: function(el){
		CS.v3.I18N.updateAttribute(el,'TYPE','button'); // force to use JS! (should they build no-js version);
		var rc = '';
		var btnId = Element.identify(el);
		if(btnId){
			rc = btnId;
		} else {
			var btnName = Element.readAttribute(el,'name');
			CS.v3.I18N.updateAttribute(el,'id',btnName);
			rc = btnName;
		}
		return rc;
	},
	decorateCheckbox: function(el,btnAddId,btnRemoveId,isRemove){
		var b = this.isToggleAll(el);
		if(!b){
			var nam = Element.readAttribute(el,'name');
			var id = Element.identify(el);
			if(nam){
				this.setCustomAttributeCheckName(el,nam);
				CS.v3.I18N.updateAttribute(el,'name','');
			}
			if(id){
				this.setCustomAttributeCheckId(el,id);
				CS.v3.I18N.updateAttribute(el,'id','');
			}
			var section = this.findLeftColumnSection(el);
			this.setCustomAttributeCheckSection(el,section);
			this.setCustomAttributeAddButton(el,btnAddId);
			this.setCustomAttributeRemoveButton(el,btnRemoveId);
			var isChecked = CS.v3.I18N.isChecked(el);
			if(isChecked){
				var divBuilder = this.findParentDiv(el);
				var myCol = this.findCol1(divBuilder);
				var otherCol = this.findCol2(divBuilder);
				var isCopy = this.isCopyItems(divBuilder);
				this.moveItem(el,myCol,otherCol,isCopy,isRemove);
				// clear the checkbox after initial
				this.setCheckboxValue(el,false);
			}
			this.styleCheckboxRow(el,CS.v3.I18N.isChecked(el));
		}
	},
	removeDupesOnLeftAfterLoad: function(el){
		try{
			var divBuilder = this.findParentDiv(el);
			var isCopy = this.isCopyItems(divBuilder);
			if(isCopy){
				var leftCol = this.findCol1(divBuilder);
				var ar = this.findCheckboxes(leftCol);
				if(ar){
					var myar = [];
					var i = ar.length;
					var obj;
					var nam;
					var j;
					var b;
					var dupe;
					var liup;
					while(i--){
						obj = ar[i];
						b = this.isToggleAll(obj);
						if(!b){
							//nam = this.getCustomAttributeCheckName(obj);
							nam = this.getCheckboxValue(obj);
							j = myar.length;
							dupe = false;
							while(j--){
								if(nam == myar[j]){
									dupe=true;
								}
							}
							if(dupe){
								liup = CS.v3.getParentElement(obj,'LI');//obj.up('LI');
								Element.remove(liup);
							} else {
								myar.push(nam);
							}
						}
					}
				}
			}
		}
		catch(e){
			this.log('removeDupesOnLeftAfterLoad:',e);
		}
	},
	findLeftColumnSection: function(el){
		var rc = this.getSectionTitle(el);
		return rc;
	},
	updateOtherColumnSection: function(el,section,elToColumn,liHtml,isCopy,isRemove, event){
		var rc = null;
		var isFound = false;
		try{
			//var isChecked = CS.v3.I18N.isChecked(el);
			var mycls;
			/*if(isChecked){
				mycls = ' csNavOn';
			}*/
			var divTree = Element.down(elToColumn,'DIV.csTree');
			var ulHeader = Element.down(divTree,'UL');
			if(ulHeader){
				var ar = Element.select(ulHeader,'A.csNavToggle');
				if(ar){
					var i = ar.length;
					var obj;
					var str;
					var x;
					var insertEl;
					while(i--){
						//try{
							obj = ar[i];
							x = obj.innerHTML;
							if(x==section){
								isFound = true;
								rc = obj;
								break;
							}
						//}
						//catch(e){
						//	this.log('updateOtherColumnSection(inside):',e);
						//}
					}
					var bsection = CS.v3.I18N.isEmpty(section);
					if(bsection){
						str = this.getLiItem(mycls,liHtml);
						Element.insert(ulHeader,{bottom: str});
						insertEl = ulHeader.childElements().last();
					} else {
						var liToggle;
						var divContent;
						var ulContent;
						if(isCopy){
							if(isFound){
								liToggle = Element.up(rc,'LI.csNavToggle-0');
								divContent = Element.down(liToggle,'DIV.csNavContent');
								ulContent = Element.down(divContent,'UL');
								str = this.getLiItem(mycls,liHtml);
								Element.insert(ulContent,{bottom: str});
								insertEl = ulContent.childElements().last();
							} else {
								if(!isRemove){
									str = this.getLiToggleItem(section,mycls,liHtml);
									Element.insert(ulHeader,{bottom: str});
									insertEl = ulHeader.childElements().last();
								}
							}
						} else {
							// move
							if(isFound){
								liToggle = Element.up(rc,'LI.csNavToggle-0');
								divContent = Element.down(liToggle,'DIV.csNavContent');
								ulContent = Element.down(divContent,'UL');
								str = this.getLiItem(mycls,liHtml);
								Element.insert(ulContent,{bottom: str});
								insertEl = ulContent.childElements().last();
							} else {
								if(!isRemove){
									str = this.getLiToggleItem(section,mycls,liHtml);
									Element.insert(ulHeader,{bottom: str});
									insertEl = ulHeader.childElements().last();
								} else {
									str = this.getLiToggleItem(section,mycls,liHtml);
									Element.insert(ulHeader,{bottom: str});
									insertEl = ulHeader.childElements().last();
								}
							}
						}// if iscopy
					}//if bsection
					if(insertEl){
						// clear the checkbox when it is added or removed
						var insertCkb = Element.down(insertEl,'input[type=checkbox]');
						this.setCheckboxValue(insertCkb, false);
						// reset event on the checkbox and category
						Event.observe(insertCkb,'click', function(event){ this.checkboxClickEv(event); }.bind(this));
						Event.observe(insertCkb,'keydown', function(event){ this.checkboxKeydownEv(event); }.bind(this));
						if(!bsection && !isFound){
							var insertCategory = Element.down(insertEl,'A.csNavToggle');
							Event.observe(insertCategory,'keydown', function(event){ this.categoryKeydownEv(event); }.bind(this));
						}
						// start: auto toggle the category when its items have been added or removed
						if(!bsection && event){
							var categoryObj;
							if(isFound){
								categoryObj = rc;
							} else {
								categoryObj = Element.down(insertEl,'A.csNavToggle');
							}
							var m = new CS.v3.MenuToggle(categoryObj, event);
							var CSNAVSHOW = 'csNavShow';// TODO try String
							if(!Element.hasClassName(m.toggle,CSNAVSHOW)){
								CS.v3.MenuVis.show(m, event);
							}
						}
						//end: auto toggle
					}
				}//if ar
			}//if ulHeader
		}
		catch(e){
			this.log('updateOtherSection(outside):',e);
		}
		return rc;
	},
	getLiItem: function(mycls,liHtml){
		var sb = new StringBuffer();
		sb.append('<li class="csNavItem-0');
		sb.append(mycls);
		sb.append('">');
		sb.append(liHtml);
		sb.append('</li>');
		var rc = sb.toString(); sb=null;
		return rc;
	},
	getLiToggleItem: function(section,mycls,liHtml){
		var sb = new StringBuffer();
		sb.append('<li class="csNavToggle-0"><div class="csNavWrap"><a href="#" class="csNavToggle">');
		sb.append(section);
		sb.append('</a></div><div class="csNavContent"><ul>');
		var str = this.getLiItem(mycls,liHtml);
		sb.append(str);
		sb.append('</ul></div></li>');
		var rc = sb.toString(); sb=null;
		return rc;
	},
	getDisabledVal: function(el){
		var rc = false;
		if(el){
			rc = el.disabled;
		}
		return rc;
	},
	buttonClickEv: function(el,myCol,otherCol,isCopy,isRemove,event){
		if(el){
			var ar = this.findCheckboxes(myCol);
			if(ar){
				var divBuilder = this.findParentDiv(myCol);
				var maxDupNum = Element.readAttribute(divBuilder,'data-csmaxduplicatenum');
				var overlayControl = this.getOverlayControl(el);
				var reachedMaxNum = false;
				var i = ar.length;
				var obj;
				var isChecked;
				var CSNAVSHOW = 'csNavShow';// TODO try String
				while(i--){
					obj = ar[i];
					isChecked = CS.v3.I18N.isChecked(obj);
					if(isChecked){
						// start: auto toggle the category when its items have been added or removed
						var section = this.getCustomAttributeCheckSection(obj,'');
						var bsection = CS.v3.I18N.isEmpty(section);
						if(!isCopy && !bsection && event){
							var liToggle = Element.up(obj,'LI.csNavToggle-0');
							var categoryObj = Element.down(liToggle,'A.csNavToggle');
							var m = new CS.v3.MenuToggle(categoryObj, event);
							if(!Element.hasClassName(m.toggle,CSNAVSHOW)){
								CS.v3.MenuVis.show(m,event);
							}
						}
						//end: auto toggle
						if(isCopy && !isRemove){
							if(!maxDupNum || maxDupNum > this.getDupNumOnRight(obj)){
								this.moveItem(obj,myCol,otherCol,isCopy,isRemove, event);
							} else {
								reachedMaxNum = true;
							}
						} else {
							this.moveItem(obj,myCol,otherCol,isCopy,isRemove, event);
						}
					}
				}
				if(reachedMaxNum){
					var msg = this.getMaxDuplicatesMsg(divBuilder);
					this.showOverlay(el,msg,event);
				}
			}
			// process with button style behavior
			this.processButtonStyleBehavior(el);
		}
	},
	getOverlayControl: function(el){
		var rc;
		if(el){
			var par = this.findParentDiv(el);
			var data = Element.readAttribute(par,'data-csOverlayControl');
			if(data){
				rc = $$(data)[0];
			}
		}
		return rc;
	},
	getButtonNotAvailableMsg: function(el){
		var rc;
		if(el){
			var data = Element.readAttribute(el,'data-csbuttonnotavailablemsg');
			if(data){
				var obj = $$(data)[0];
				rc = obj.innerHTML;
			}
		}
		return rc;
	},
	getMaxDuplicatesMsg: function(el){
		var rc;
		if(el){
			var data = Element.readAttribute(el,'data-csmaxduplicatesmsg');
			if(data){
				var obj = $$(data)[0];
				rc = obj.innerHTML;
			}
		}
		return rc;
	},
	isCopyItems: function(el){
		var rc = false;
		if(el){
			rc = Element.hasClassName(el,'copy');
		}
		return rc;
	},
	findCheckboxes: function(el){
		var rc;
		if(el){
			rc = Element.select(el,"input[type=checkbox]");
		}
		return rc;
	},
	findParentDiv: function(el){
		var rc;
		if(el){
			rc = Element.up(el,'DIV.csListBuilder');
		}
		return rc;
	},
	findCol1: function(el){
		var rc;
		if(el){
			rc = Element.down(el,'DIV.csList-1');
		}
		return rc;
	},
	findCol2: function(el){
		var rc;
		if(el){
			rc = Element.down(el,'DIV.csList-2');
		}
		return rc;
	},
	addButtonClickEv: function(event){
		var el = $(Event.element(event));
		var divBuilder = this.findParentDiv(el);
		// Button Behavior
		if(!this.isAddButtonAvailable(el)){
			if(!this.isButtonStyleBehaviorEnabled(divBuilder)){
				var msg = this.getButtonNotAvailableMsg(divBuilder);
				this.showOverlay(el,msg,event);
			}
		} else {
			var myCol = this.findCol1(divBuilder);
			var otherCol = this.findCol2(divBuilder);
			var isCopy = this.isCopyItems(divBuilder);
			var isRemove = false;
			this.buttonClickEv(el,myCol,otherCol,isCopy,isRemove, event);
		}
		//event.preventDefault();
	},
	removeButtonClickEv: function(event){
		var el = $(Event.element(event));
		var divBuilder = this.findParentDiv(el);
		// Button Behavior
		if(!this.isRemoveButtonAvailable(el)){
			if(!this.isButtonStyleBehaviorEnabled(divBuilder)){
				var msg = this.getButtonNotAvailableMsg(divBuilder);
				this.showOverlay(el,msg,event);
			}
		} else {
			var myCol = this.findCol2(divBuilder);
			var otherCol = this.findCol1(divBuilder);
			var isCopy = this.isCopyItems(divBuilder);//false; // remove is always a MOVE
			var isRemove = true;
			this.buttonClickEv(el,myCol,otherCol,isCopy,isRemove, event);
		}
		//event.preventDefault();
	},
	showOverlay: function(el, content, event){
		var overlayControl = this.getOverlayControl(el);
		if(overlayControl){
			var attr = new CS.v3.ComponentAttributes(overlayControl);
			new CS.v3.PanelOverlay(overlayControl,event,attr);
			// make focus back to the button.
			var CLS = 'csPopToggleActive';
			Element.removeClassName(overlayControl,CLS);
			Element.addClassName(el,CLS);
			// update the overlay message
			var p = new CS.v3.getPanelAttributes(null,attr);
			Element.down(p.content,'p.warningMsg').update(content);
		}
	},
	formSubmitEv: function(event){
		var el = $(Event.element(event));
		//var divBuilder = this.findParentDiv(el);
		//if(!divBuilder){
		//	divBuilder = Element.down(el,'DIV.csListBuilder');
		//}
		var arAll = this.findAllListBuilders(el);
		if(arAll){
			var j = arAll.length;
			var i;
			var obj;
			var nam;
			var ar;
			var myCol;
			var divBuilder;
			while(j--){
				divBuilder = arAll[j];
				myCol = this.findCol2(divBuilder);
				ar = this.findCheckboxes(myCol);
				if(ar){
					i = ar.length;
					while(i--){
						obj = ar[i];
						nam = this.getCustomAttributeCheckName(obj);
						CS.v3.I18N.updateAttribute(obj,'name',nam);
						obj.checked = true;
					}
				}
			}
		}
	},
	styleCheckboxRow: function(el,isChecked){
		var liEl =  CS.v3.getParentElement(el,'LI');
		if(liEl){
			var CSNAVON = 'csNavOn';// TODO try String
			if(isChecked){
				if(!Element.hasClassName(liEl,CSNAVON)){
					Element.addClassName(liEl,CSNAVON);
				}
			} else {
				if(Element.hasClassName(liEl,CSNAVON)){
					Element.removeClassName(liEl,CSNAVON);
				}
			}
		}
	},
	checkboxClickEv: function(event){
		var el = $(Event.element(event));
		if(el){
			var isChecked = CS.v3.I18N.isChecked(el);
			var b = this.isToggleAll(el);
			if(b){
				this.toggleAllCheckboxes(el,isChecked);
			} else {
				this.styleCheckboxRow(el,isChecked);
			}
		}
		// process with button style behavior
		this.processButtonStyleBehavior(el);
		//event.preventDefault();
	},
	isToggleAll: function(el){
		var rc=false;
		if(el){
			rc = Element.hasClassName(el,'csCheckAll');
		}
		return rc;
	},
	toggleAllCheckboxes: function(el,isChecked){
		var myCol = Element.up(el,'div',2);
		var ar = this.findCheckboxes(myCol);
		if(ar){
			var i=ar.length;
			var b;
			var obj;
			while(i--){
				obj = ar[i];
				b = this.isToggleAll(obj);
				if(!b){
					this.setCheckboxValue(obj,isChecked);
				}
			}
		}
	},
	getSectionTitle: function(el){
		var rc = '';
		if(el){
			//try{
				var divContent = Element.up(el,'DIV.csNavContent');
				if(divContent){
					var liToggle = Element.up(divContent,'LI.csNavToggle-0');
					var divSection = Element.down(liToggle,'DIV.csNavWrap');
					var divToggle = Element.down(divSection,'A.csNavToggle');
					if(divToggle){
						var sectionTitle = divToggle.innerHTML;
						rc = sectionTitle.escapeHTML();// helps with GT/LT and such!
					}
				}
			//}
			//catch(e){
			//	this.log('getSectionTitle:',e);
			//}
		}
		return rc;
	},
	moveItem: function(elCheck,myCol,otherCol,isCopy,isRemove, event){
		var isHideLI = false;
		var isHideUL = false;
		try{
			var isDisabled = this.getDisabledVal(elCheck);
			if(!isDisabled){
				var elLi = Element.up(elCheck,'LI.csNavItem-0');
				if(elLi){
					var liHtml = elLi.innerHTML;
					if(isHideLI){
						CS.v3.hideFunction(elLi);
					} else {
						var elUl = CS.v3.getParentElement(elLi,'UL');
						var b = (isCopy && !isRemove);
						if(!b){
							Element.remove(elLi);
						}
						if(isHideUL){
							CS.v3.hideFunction(elUl);
						} else {
							var ar = Element.select(elUl,'LI');
							if(ar){
								var len = ar.length;
								if(len===0){
									var liup = CS.v3.getParentElement(elUl,'LI');
									if(liup){
										var ar2 = Element.select(liup,'DIV');
										var divs = ar2.length;
										if(divs>=2){
											Element.remove(liup);
										}
									}
								}
							}
						}
					}
					var section = this.getCustomAttributeCheckSection(elCheck,'');
					var dummy;
					if(isCopy){
						if(!isRemove){
							dummy = this.updateOtherColumnSection(elCheck,section,otherCol,liHtml,isCopy,isRemove,event);
						}
					} else {
						if(isRemove){
							dummy = this.updateOtherColumnSection(elCheck,section,otherCol,liHtml,isCopy,isRemove,event);
						} else {
							dummy = this.updateOtherColumnSection(elCheck,section,otherCol,liHtml,isCopy,isRemove,event);
						}
					}
				}
			}
		}
		catch(e){
			this.log('moveItem:',e);
		}
	},
	getCheckboxValue: function(el){
		//NOTE: CS.v3.I18N.getValue(el,true) not used as we are not relying on 'checked'
		var rc;
		if(el){
			rc = Element.readAttribute(el,'value');
		}
		return rc;
	},
	setCheckboxValue: function(el,b){
		//NOTE: CS.v3.I18N.setValue(el,b) not used here as value is irrelevant or unknown
		if(el){
			if(b){
				el.checked = true;
			} else {
				el.checked = false;
			}
			this.styleCheckboxRow(el,b);
		}
	},
	getCustomAttributeAddButton: function(el,def){
		var rc = CS.v3.I18N.getDataAttribute(el,'data-csaddlistbutton',def);
		return rc;
	},
	setCustomAttributeAddButton: function(el,val){
		CS.v3.I18N.setDataAttribute(el,'data-csaddlistbutton',val);
	},
	getCustomAttributeRemoveButton: function(el,def){
		var rc = CS.v3.I18N.getDataAttribute(el,'data-csremovelistbutton',def);
		return rc;
	},
	setCustomAttributeRemoveButton: function(el,val){
		CS.v3.I18N.setDataAttribute(el,'data-csremovelistbutton',val);
	},
	getCustomAttributeCheckId: function(el,def){
		var rc = CS.v3.I18N.getDataAttribute(el,'data-csaddlistid',def);
		return rc;
	},
	setCustomAttributeCheckId: function(el,val){
		CS.v3.I18N.setDataAttribute(el,'data-csaddlistid',val);
	},
	getCustomAttributeCheckName: function(el,def){
		var rc = CS.v3.I18N.getDataAttribute(el,'data-csaddlistname',def);
		return rc;
	},
	setCustomAttributeCheckName: function(el,val){
		CS.v3.I18N.setDataAttribute(el,'data-csaddlistname',val);
	},
	getCustomAttributeCheckSection: function(el,def){
		var rc = CS.v3.I18N.getDataAttribute(el,'data-csaddlistsection',def);
		return rc;
	},
	setCustomAttributeCheckSection: function(el,val){
		CS.v3.I18N.setDataAttribute(el,'data-csaddlistsection',val);
	},
	isAddButtonAvailable: function(el){
		var divBuilder = this.findParentDiv(el);
		var addCol = this.findCol1(divBuilder);
		var arAdd = Element.select(addCol,"input[type=checkbox]");
		var addAvailable = false;
		var i = arAdd.length;
		while (i--){
			if(arAdd[i].checked){
				addAvailable = true;
				break;
			}
		}
		return addAvailable;
	},
	isRemoveButtonAvailable: function(el){
		var divBuilder = this.findParentDiv(el);
		var removeCol = this.findCol2(divBuilder);
		var arRemove = Element.select(removeCol,"input[type=checkbox]");
		var removeAvailable = false;
		var i = arRemove.length;
		while (i--){
			if(arRemove[i].checked){
				removeAvailable = true;
				break;
			}
		}
		return removeAvailable;
	},
	isButtonStyleBehaviorEnabled: function(el){
		var rc = false;
		if(el){
			rc = Element.hasClassName(el,'csButtonStyleBehavior');
		}
		return rc;
	},
	processButtonStyleBehavior: function(el){
		var divBuilder = this.findParentDiv(el);
		// Button Style Behavior
		if(this.isButtonStyleBehaviorEnabled(divBuilder)){
			var addAvailable = this.isAddButtonAvailable(el);
			var removeAvailable = this.isRemoveButtonAvailable(el);
			var buttonAdd = Element.select(divBuilder,"button.csButtonAdd");
			var buttonRemove = Element.select(divBuilder,"button.csButtonRemove");
			var i = buttonAdd.length;
			var obj;
			var CLS_DISABLED = new String('csDisabled');// TODO use 'csDisabledRounded'
			while(i--){
				obj = buttonAdd[i];
				if(addAvailable && Element.hasClassName(obj,CLS_DISABLED)){
					Element.removeClassName(obj,CLS_DISABLED);
					obj.disabled = false;
				} else {
					if(!addAvailable && !Element.hasClassName(obj,CLS_DISABLED)){
						Element.addClassName(obj,CLS_DISABLED);
						obj.disabled = true;
					}
				}
			}
			i = buttonRemove.length;
			while(i--){
				obj = buttonRemove[i];
				if(removeAvailable && Element.hasClassName(obj,CLS_DISABLED)){
					Element.removeClassName(obj,CLS_DISABLED);
					obj.disabled = false;
				} else {
					if(!removeAvailable && !Element.hasClassName(obj,CLS_DISABLED)){
						Element.addClassName(obj,CLS_DISABLED);
						obj.disabled = true;
					}
				}
			}
		}
	},
	expandAllClickEv: function(event){
		event.preventDefault(event);
		this.showAll(event);
	},
	expandAllKeydownEv: function(event){
		var key = event.keyCode;
		if(key===Event.KEY_RETURN){
			event.preventDefault(event);
			this.showAll(event);
		}
	},
	collapseAllClickEv: function(event){
		event.preventDefault(event);
		this.hideAll(event);
	},
	collapseAllKeydownEv: function(event){
		var key = event.keyCode;
		if(key===Event.KEY_RETURN){
			event.preventDefault(event);
			this.hideAll(event);
		}
	},
	showAll: function(event){
		var el = $(Event.element(event));
		var divBuilder = this.findParentDiv(el);
		var nodes = Element.readAttribute(el,'data-csnodestoshow');
		var obj = Element.down(divBuilder,nodes);
		var ar = Element.select(obj,'A.csNavToggle');
		var i = ar.length;
		var categoryObj;
		var CSNAVSHOW = 'csNavShow';// TODO try String
		var CSNAVFIRSTITEM = 'csNavFirstItem';// TODO try String
		var m;
		var elLi;
		var ar2;
		while(i--){
			categoryObj = ar[i];
			m = new CS.v3.MenuToggle(categoryObj, event);
			if(!Element.hasClassName(m.toggle,CSNAVSHOW)){
				// stay the focus on the expand all button after the expand menu effect
				elLi = Element.up(categoryObj,'li.csNavToggle-0');
				ar2 = Element.select(elLi,'input[type=checkbox]');
				ar2.each(function(value){
					Element.removeClassName(value,CSNAVFIRSTITEM);
				});
				CS.v3.MenuVis.show(m,event);
			}
		}
	},
	hideAll: function(event){
		var el = $(Event.element(event));
		var divBuilder = this.findParentDiv(el);
		var nodes = Element.readAttribute(el,'data-csnodestohide');
		var obj = Element.down(divBuilder,nodes);
		var ar = Element.select(obj,'A.csNavToggle');
		var i = ar.length;
		var categoryObj;
		var CSNAVSHOW = 'csNavShow';// TODO try String
		var m;
		while(i--){
			categoryObj = ar[i];
			m = new CS.v3.MenuToggle(categoryObj, event);
			if(Element.hasClassName(m.toggle,CSNAVSHOW)){
				CS.v3.MenuVis.hide(m,event);
			}
		}
	},
	getDupNumOnRight: function(el){
		var dupNum = 0;
		var currentValue = this.getCheckboxValue(el);
		var divBuilder = this.findParentDiv(el);
		var rightCol = this.findCol2(divBuilder);
		var ar = this.findCheckboxes(rightCol);
		if(ar){
			var i = ar.length;
			var obj;
			while(i--){
				obj = ar[i];
				if(currentValue == this.getCheckboxValue(obj)){
					dupNum++;
				}
			}
		}
		return dupNum;
	},
	checkboxKeydownEv: function(event){
		var el = $(Event.element(event));
		var elLi = Element.up(el,'li.csNavItem-0');
		var isCategory = !CS.v3.I18N.isEmpty(this.getSectionTitle(el));
		var target;
		var obj;
		var obj2;
		var key = event.keyCode;
		// prevent default event except the TAB key and SPACE key
		if(key!==Event.KEY_TAB && key!==32){
			event.preventDefault(event);
		}
		switch(key){
			case Event.KEY_UP:
				target = Element.previous(elLi,'li.csNavItem-0');
				if(target){
					obj = Element.down(target,'input[type=checkbox]');
					CS.v3.focus(obj);
				}
				break;
			case Event.KEY_DOWN:
				target = Element.next(elLi,'li.csNavItem-0');
				if(target){
					obj = Element.down(target,'input[type=checkbox]');
					CS.v3.focus(obj);
				}
				break;
			case Event.KEY_ESC:
				if(isCategory){
					obj = Element.up(elLi,'li.csNavToggle-0');
					obj2 = Element.down(obj,'a.csNavToggle');
					CS.v3.focus(obj2);
				} else {
					obj = Element.up(elLi,'div',1);
					obj2 = Element.previous(obj);
					target = Element.down(obj2,'a.csExpandAll');
					if(!target){
						target = Element.down(obj2,'input.csCheckAll');
					}
					CS.v3.focus(target);
				}
				break;
			default:
				// NOOP
				break;
		}
	},
	categoryKeydownEv: function(event){
		var el = $(Event.element(event));
		var elLi = Element.up(el,'li.csNavToggle-0');
		var target;
		var obj;
		var key = event.keyCode;
		if(key!==Event.KEY_TAB){
			event.preventDefault(event);
		}
		switch(key){
			case Event.KEY_LEFT:
				target = Element.previous(elLi,'li.csNavToggle-0');
				if(target){
					obj = Element.down(target,'a.csNavToggle');
					CS.v3.focus(obj);
				}
				break;
			case Event.KEY_RIGHT:
				target = Element.next(elLi,'li.csNavToggle-0');
				if(target){
					obj = Element.down(target,'a.csNavToggle');
					CS.v3.focus(obj);
				}
				break;
			case Event.KEY_ESC:
				obj = Element.up(elLi,'div',1);
				obj2 = Element.previous(obj);
				target = Element.down(obj2,'a.csExpandAll');
				if(!target){
					target = Element.down(obj2,'input.csCheckAll');
				}
				CS.v3.focus(target);
				break;
			case Event.KEY_DOWN:
				var m = new CS.v3.MenuToggle(el, event);
				if(Element.hasClassName(m.toggle,'csNavShow')){
					// the menu has already been expanded, just move the focus to its first item
					obj = Element.down(elLi,'input[type=checkbox]');
					CS.v3.focus(obj);
					//stop the default collapse menu effect
					Event.stop(event);
				} else {
					// expand the menu and move the focus to its first item using default expand menu effect
					obj = Element.down(elLi,'input[type=checkbox]');
					Element.addClassName(obj,'csNavFirstItem');
				}
				break;
			default:
				// NOOP
				break;
		}
	},
	log: function(s,e){
		CS.v3.Logger.log("ListBuilder:" + s,e);
	}
};
