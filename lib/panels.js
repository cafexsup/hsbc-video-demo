/* Theme Pack 3.2.1+ DO NOT modify */
/*** Panels ***/
CS.v3.Panel = Class.create(CS.v3.Base.prototype, {
	initialize: function(target){
		this.target = target;
		this.heading = CS.v3.getParentElementWithClass(target,'csPanelHeading');
		this.toggle = CS.v3.getParentElementWithClass(target,'csPanel');
		var CLS = '.csPanelContent';
		this.toggleTarget = Element.next(this.heading,CLS) || Element.previous(this.heading,CLS);
	}
});
CS.v3.PanelVis = {
	toggle: function(m){
		var b = Element.hasClassName(m.toggle,'csPanelHide');
		if(b){
			CS.v3.PanelVis.show(m);
		} else {
			CS.v3.PanelVis.hide(m);
		}
	},
	show: function(m){
		var isServer = false;
		var target = m.target;
		if(Element.hasClassName(target,'csPanelServer')){
			var url = Element.readAttribute(target,'data-cspanelopenurl');
			isServer = CS.v3.UrlFunctions.forward(url);
		}
		if(!isServer){
			var time = CS.v3.Base.prototype.getBlindDuration(m.toggleTarget);
			new Effect.BlindDown(m.toggleTarget,{duration: time,
				beforeStart: function(){
					m.toggleTarget.style.display = 'none';
					m.toggleTarget.style.height = 'auto';
					Element.removeClassName(m.toggle,'csPanelHide');
				}.bind(this),
				afterFinish: function(){
					//ie 6 only items disappear without this
					m.toggleTarget.style.height = '';
					m.toggleTarget.style.display = 'block';
					//ie 6 only
				}.bind(this)
			});
		}
	},
	hide: function(m){
		var isServer = false;
		var target = m.target;
		if(Element.hasClassName(target,'csPanelServer')){
			var url = Element.readAttribute(target,'data-cspanelcloseurl');
			isServer = CS.v3.UrlFunctions.forward(url);
		}
		if(!isServer){
			var dimen = m.toggleTarget.getDimensions();
			var height = dimen.height;
			var time = CS.v3.Base.prototype.getBlindDuration(m.toggleTarget);
			new Effect.BlindUp(m.toggleTarget,{
				duration: time,
				beforeStart: function(){
					m.toggleTarget.style.height = height;
					m.toggleTarget.style.overflow = 'hidden';
				}.bind(this),
				afterFinish: function(){
					Element.addClassName(m.toggle,'csPanelHide');
				}.bind(this)
			});
		}
	},
	showAll: function(attr){
		var ar = attr.showContent;
		var i = ar.length;
		var panel;
		var panelWrap;
		var panelHeading;
		var m;
		while(i--){
			panel = ar[i];
			panelWrap = CS.v3.getParentElementWithClass(panel,'csPanel');
			panelHeading = CS.v3.getParentElementWithClass(panel,'csPanelHeading');
			if(Element.hasClassName(panelWrap,'csPanelHide') && Element.hasClassName(panelHeading,'csClick')){
				m = new CS.v3.Panel(panel);
				CS.v3.PanelVis.show(m);
			}
		}
	},
	hideAll: function(attr){
		var ar = attr.hideContent;
		var i = ar.length;
		var panel;
		var panelWrap;
		var panelHeading;
		var m;
		while(i--){
			panel = ar[i];
			panelWrap = CS.v3.getParentElementWithClass(panel,'csPanel');
			panelHeading = CS.v3.getParentElementWithClass(panel,'csPanelHeading');
			if(!Element.hasClassName(panelWrap,'csPanelHide') && Element.hasClassName(panelHeading,'csClick')){
				m = new CS.v3.Panel(panel);
				CS.v3.PanelVis.hide(m);
			}
		}
	}
};
/*** Next Best Action Panel ***/
CS.v3.TabsNBA = Class.create(CS.v3.Base.prototype, {
	initialize: function(el){
		this.el = el;
		this.wrap = Element.up(el,'.csTabs-x') || Element.up(el,'.csTabs-y');
		this.wrapNBA = Element.up(el,'.csPanelNextBestAction');
		var attr = new CS.v3.ComponentAttributes(this.wrap);
		this.item = this.el.up(1);
		var s = new CS.v3.getSelected(this.item,'li');
		this.selectedItem = s.selectedIndex;
		this.contentModulesX = $$('.' + this.targetClass);
		var ar = attr.toggleContent;
		var module;
		var i = ar.length;
		while(i--){
			module = ar[i];
			Element.removeClassName(module,'csShow');
		}
		this.removedItem = this.navStatus(s.collection,this.item);
		var NBA_ID = new String('csActiveNBA');
		var isActive = $(NBA_ID);
		if(isActive){
			isActive.id = '';
			this.el.id = NBA_ID;
		} else {
			this.el.id = NBA_ID;
		}
		this.show(attr);
	},
	show: function(attr){
		var CSSHOW = new String('csShow');
		if(attr){
			var ar = attr.toggleContent;
			var sel = this.selectedItem;
			// no targetClass attribute
			if(ar.length===0){
				var rem = this.removedItem;
				var obj = Element.next(this.wrap,rem);
				Element.removeClassName(obj,CSSHOW);
				this.module = this.wrap.next(sel,'.csContentModule');// WTF
			} else { //targetClass attribute
				this.module = ar[sel];
			}
		}
		Element.addClassName(this.module,CSSHOW);
		var el = Element.down(this.wrapNBA,'.csNBAModule');
		CS.v3.focus(el);
	}
});
/*** Overlay Panel ***/
CS.v3.Components.PanelPop = function(el,options){
	new CS.v3.Config.PanelPop(el,options);
};
CS.v3.Config.PanelPop = Class.create(CS.v3.Base.prototype, {
	initialize: function(el,options){
		this.pops = Element.select(el,"div[class*='csOverlayContainer csHide']");
		this.bind();
	},
	bind: function(){
		var ar = this.pops;
		var dragHandle;
		var pop;
		var i = ar.length;
		while(i--){
			pop = ar[i];
			dragHandle = Element.down(pop,'.csPanelHeading');
			this.resizeHandle = Element.down(pop,'.csResizeHandle');
			new Draggable(pop, {handle: dragHandle});
			if(this.resizeHandle){
				this.panel = Element.down(pop,'.csPanel');
				this.contentScroll = Element.down(pop,'.csPanelScroll');
				this.content = Element.down(pop,'.csPanelContent');
				new Resizable(pop,{handle:this.resizeHandle});
				new Resizable(this.content,{handle:this.resizeHandle});
				new Resizable(this.content,{constraint:'vertical', handle:this.resizeHandle});
			}
			Event.observe(pop,'keydown', function(event){
				var key = event.keyCode;
				var target = Event.element(event);
				if(key===Event.KEY_ESC){
					new CS.v3.PanelPopClose(target,event);
				}
				//if(key===Event.KEY_TAB){
					//TODO
				//}
			});
			new CS.v3.PanelKeyboardMovable(pop,{handle:dragHandle,amount:5});
		}
	}
});
CS.v3.PanelKeyboardMovable=Class.create(CS.v3.Base.prototype,{
	initialize:function(target){
		var originalOpacity=Element.getOpacity(target);
		var options=Object.extend({target:target,handle:target,amount:5,opacity:0.7,duration:0.2,_opacity:originalOpacity},arguments[1]||{});
		var handle=options.handle;
		if(handle){
			Event.observe(handle,'keydown',this.eventKeyDown.bindAsEventListener(this,options));
			Event.observe(handle,'keyup',this.eventKeyUp.bindAsEventListener(this,options));
		} else { 
			this.log('initialize:no handle - was problem only for new prototype release!');
		}
	},
	eventKeyDown:function(event,options){
		var key=event.keyCode;
		if(key>=37&&key<=40){
			this.startMove(options,key);
			event.preventDefault();
		}
	},
	eventKeyUp:function(event,options){
		var key=event.keyCode;
		if(key>=37&&key<=40){
			this.finishMove(options,key);
			event.preventDefault();
		}
	},
	startMove:function(options,key){
		var target=options.target;
		var amount=options.amount;
		if(!this.isMoving(options)) new Effect.Opacity(target,{duration:options.duration,from:options._opacity,to:options.opacity});
		switch(key){
			case 37:
				if(!options.moveLeftFlag){
					options.moveLeftFlag=true;
					new PeriodicalExecuter(function(s){
						if(options.moveLeftFlag) target.setStyle({left:(parseInt(target.getStyle('left'),10)-amount)+'px'});
						else s.stop();
					},0.001);
				}
				break;
			case 38:
				if(!options.moveUpFlag){
					options.moveUpFlag=true;
					new PeriodicalExecuter(function(s){
						if(options.moveUpFlag) target.setStyle({top:(parseInt(target.getStyle('top'),10)-amount)+'px'});
						else s.stop();
					},0.001);
				}
				break;
			case 39:
				if(!options.moveRightFlag){
					options.moveRightFlag=true;
					new PeriodicalExecuter(function(s){
						if(options.moveRightFlag) target.setStyle({left:(parseInt(target.getStyle('left'),10)+amount)+'px'});
						else s.stop();
					},0.001);
				}
				break;
			case 40:
				if(!options.moveDownFlag){
					options.moveDownFlag=true;
					new PeriodicalExecuter(function(s){
						if(options.moveDownFlag) target.setStyle({top:(parseInt(target.getStyle('top'),10)+amount)+'px'});
						else s.stop();
					},0.001);
				}
				break;
			default: // NOOP
				break;
		}
	},
	finishMove:function(options,key){
		switch(key){
			case 37:options.moveLeftFlag=false;break;
			case 38:options.moveUpFlag=false;break;
			case 39:options.moveRightFlag=false;break;
			case 40:options.moveDownFlag=false;break;
			default: // NOOP
				break;
		}
		if(!this.isMoving(options)) new Effect.Opacity(options.target,{duration:options.duration,from:options.opacity,to:options._opacity});
	},
	isMoving:function(options){
		return options.moveLeftFlag||options.moveUpFlag||options.moveRightFlag||options.moveDownFlag;
	},
	log: function(s,e){
		CS.v3.Logger.log("PanelKeyboardMovable:" + s,e);
	}
});
CS.v3.getToggleAttributes = Class.create(CS.v3.Base.prototype, {
	initialize: function(target,event,attr){
		this.target = target;
		// Origin of overlay launch, values are target or window
		this.overlayOrigin = Element.readAttribute(target,'data-csoverlayorigin');
		// heading
		this.overlayHeadingContent = Element.readAttribute(target,'data-csoverlayheading');
		// Lightbox effect, value is true
		this.lightbox = Element.readAttribute(target,'data-cslightbox');
		// set width on panel
		this.originalWidth = Element.readAttribute(target,'data-cssetpanelwidth');
		this.ContentOrigin = new CS.v3.updateContent(target);
		// set width on panel
		this.offsetY = 0;
		this.offsetX = 0;
		this.getOffsetY = Element.readAttribute(target,'data-csoverlayoriginyoffset');
		this.getOffsetX = Element.readAttribute(target,'data-csoverlayoriginxoffset');
		if(this.getOffsetY){
			this.offsetY = this.getOffsetY;
			this.offsetY = parseInt(this.offsetY,10);
		}
		if(this.getOffsetY){
			this.offsetX = this.getOffsetX;
			this.offsetX = parseInt(this.offsetX,10);
		}
		if(Element.hasClassName(target,'csOpacity')){
			this.effect = 'opacity';
		} else {
			this.effect = 'scaleMove';
		}
		this._getEndPosition(event);
	},
	_getEndPosition: function(event){
		this.y = null;
		this.x = null;
		if(this.overlayOrigin == 'window'){
			var scrollYOffset;
			var viewWidth;
			if(Prototype.Browser.IE){
				scrollYOffset = document.documentElement.scrollTop;
				viewWidth = document.body.clientWidth;
			} else {
				scrollYOffset = window.scrollY;
				viewWidth = window.innerWidth;
			}
			this.y = 120 + scrollYOffset;
			this.x = (viewWidth - this.originalWidth) / 2;
			this.originX = 0;
			this.originY = 0;
			this.mode = 'absolute';
		}
		if(this.overlayOrigin == 'target'){
			var typ = event.type;
			this.x = 0;
			this.y = 0;
			var pos = CS.v3.offsetHelper.offset(this.target);
			this.originX = pos.left + (this.offsetX);
			this.originY = pos.top + (this.offsetY);
			this.mode = 'relative';
		}
	}
});

CS.v3.offsetHelper = {
	offset: function(el){
		var pos = Element.cumulativeOffset(el);
		var left = pos.left;
		var posScroll = Element.cumulativeScrollOffset(el);
		var viewScroll = document.viewport.getScrollOffsets();
		var top = pos.top - posScroll.top + viewScroll.top;
		if(Prototype.Browser.IE){
			CS.v3.containerWrap = el.up('div.csContainerWrap');
			if (CS.v3.containerWrap.currentStyle.hasLayout) {
				var IELeft = el.getBoundingClientRect().left;
				left = IELeft -2;
			}
		}
		return {top:top, left:left};
	}
};

CS.v3.getPanelAttributes = Class.create(CS.v3.Base.prototype, {
	initialize: function(t,attr){
		this.pop = attr.toggleContent[0];
		this.toggle = Element.down(this.pop,'.csPanelToggle');
		this.content = Element.down(this.pop,'.csPanelScroll');
		this.overlayHeading = Element.down(this.pop,'.csPanelOverlayHeading');
		this.fader = CS.v3.castToEl('csFader');
	}
});
CS.v3.PanelOverlay = Class.create(CS.v3.Base.prototype, {
	initialize: function(target,event,attr){
		//this.log('initialize:init');
		var t = new CS.v3.getToggleAttributes(target,event);
		var p = new CS.v3.getPanelAttributes(t,attr);
		var hintPanel = Element.down(p.pop,'div.csIconHint');
		if(hintPanel){
			var showHintPanelFlag = Element.readAttribute(target,'data-csshowhintpanel');
			if(showHintPanelFlag && showHintPanelFlag!=='true'){
				hintPanel.style.display = 'none';
			}else{
				hintPanel.style.display = '';
			}
		}
		//var body = CS.v3.getBody();
		//body.style.overflow = 'hidden';
		// add active state to target
		Element.addClassName(t.target,'csPopToggleActive');
		// IE 6 Cover
		if(CS.v3.Base.prototype.coverIE){
			CS.v3.Base.prototype.coverIE(p.pop);
		}
		// Get AJAX Content
		if(Element.hasClassName(t.target,'csAjax')){
			if(t.overlayHeadingContent){
				Element.update(p.overlayHeading,t.overlayHeadingContent);
			}
			new CS.v3.AjaxNav(t.target,p.content,{bind:false});
		// Get JSON Content
		} else {
			new CS.v3.getupDatePanelNodes(t,p);
		}
		//Display
		var CSHIDE = new String('csHide');
		if(t.lightbox == 'true' || t.effect == 'opacity'){
			p.fader.toggleClassName(CSHIDE);
			if(CS.v3.fixed){
				CS.v3.fixed['fader']();
			}
		}
		// toggle class
		p.pop.toggleClassName(CSHIDE);
		// set dimensions and position
		p.pop.style.width = t.originalWidth + 'px';
		p.pop.style.left = t.originX + 'px';
		p.pop.style.top = t.originY + 'px';
		// store settings
		Element.writeAttribute(p.pop,'csOriginX',t.originX);
		Element.writeAttribute(p.pop,'csOriginY',t.originY);
		Element.writeAttribute(p.pop,'csOriginalWidth',t.originalWidth);
		Element.writeAttribute(p.pop,'csEffect',t.effect);
		// get Height Dimension
		this.dimen = p.pop.getDimensions();
		this.originalHeight = this.dimen.height;
		switch(t.effect){
			case 'scaleMove':
				this._scaleMove(t,p);
				break;
			case 'opacity':
				this._opacity(t,p);
				break;
			default:
				// NOOP
				break;
		}
		//this.log('initialize:exit');
	},
	_scaleMove: function(t,p){
		// Set size
		p.pop.style.height = '5px';
		p.pop.style.width = '5px';
		//Move
		new Effect.Move(p.pop, {x: t.x, y: t.y, mode: t.mode, duration:0.4});
		//Scale
		new Effect.Scale(p.pop, 100, {duration:0.4, scaleFrom: 5,
			scaleMode: {originalHeight: this.originalHeight, originalWidth: t.originalWidth},
			scaleContent: false,
			beforeStart: function(){
				p.pop.style.overflow = 'hidden';
			}.bind(this),
			afterFinish: function(){
				p.pop.style.overflow = 'visible';
				p.pop.style.height = 'auto';
				CS.v3.focus(p.toggle);
			}.bind(this)
		});
		if(t.lightbox == 'true'){
			new Effect.Opacity(p.fader, {from: 0.0 , to: 0.7, duration:0.4});
		}
	},
	_opacity: function(t,p){
		p.pop.style.left = t.x + 'px';
		p.pop.style.top = t.y + 'px';
		p.fader.addClassName("csPanelOpacity");
		CS.v3.focus(p.toggle);
	},
	log: function(s,e){
		CS.v3.Logger.log("PanelOverlay:" + s,e);
	}
});
/*** Get Overlay Content ***/
CS.v3.getupDatePanelNodes = Class.create(CS.v3.Base.prototype, {
	initialize: function(t,p){
		//this.log('initialize:init');
		// Local Content
		var targetAttrs = t.ContentOrigin;
		var targetid = targetAttrs.targetid;
		var ar = targetAttrs.InsertableNodes;
		var node;
		var localHeadingTest;
		var localContentTest;
		var localParentTest;
		var localHeading;
		var localContent;
		var obj;
		var i = ar.length;
		var LOCAL_CONTENT = new String('.csPanelContentLocal');
		var LOCAL_HEADING = new String('.csPanelHeadingLocal');
		while(i--){
			node = ar[i];
			localHeadingTest = Element.down(node,LOCAL_HEADING);
			localContentTest = Element.down(node,LOCAL_CONTENT);
			localParentTest =  Element.down(node,'.csOverlaySubmitParent');
			if(localParentTest){
				CS.v3.I18N.setDataAttribute(localParentTest,'data-csparentid',targetid);
				var targetel = CS.v3.castToEl(targetid);
				if(targetel){
					var HREF = 'href';//NOTE: test this... new String('href');
					CS.v3.I18N.copyAttribute(targetel,localParentTest,HREF,'#');
				}
			}
			if(p.overlayHeading && localHeadingTest){
				obj = Element.down(node,LOCAL_HEADING);
				localHeading = obj.innerHTML;
				Element.update(p.overlayHeading,localHeading);
			}
			if(p.content && localContentTest){
				obj = Element.down(node,LOCAL_CONTENT);
				localContent = obj.innerHTML;
				Element.update(p.content,localContent);
				new CS.v3.Config(p.content,{bind:'true', set:'true'});
			}
		}
		//})
		// Json - deprecated
		var ar2 = CS.v3.PopContent;
		//this.log('initialize:ar2=' + ar2);
		if(ar2){
			obj = ar2[t.targetContent];
			if(obj){
				this.heading = obj.heading || null;
				this.content = obj.content || null;
				if(this.heading){
					Element.update(p.toggle,this.heading);
				}
				if(this.content){
					Element.update(p.content,this.content);
				}
			}
		}
		new CS.v3.zIndex();
		//this.log('initialize:exit');
	},
	log: function(s,e){
		CS.v3.Logger.log("getupDatePanelNodes:" + s,e);
	}
});
/*** Close Pop Panel ***/
CS.v3.PanelPopClose = Class.create(CS.v3.Base.prototype, {
	initialize: function(el){
		this.pop = CS.v3.findOverlayWrapper(el);
		//var body = CS.v3.getBody();
		//body.style.overflow = 'auto';
		// remove active state to target
		var popToggleActive = $$(".csPopToggleActive");
		popToggleActive.each(function(value,index){
			var obj = popToggleActive[index];
			CS.v3.focus(obj);
			Element.removeClassName(obj,'csPopToggleActive');
		});
		this.x = Element.readAttribute(this.pop,'csOriginX');
		this.y = Element.readAttribute(this.pop,'csOriginY');
		this.effect = Element.readAttribute(this.pop,'csEffect');
		//this.log('initialize:effect=' + this.effect);
		this.originalHeight = this.pop.getHeight();
		this.originalWidth = Element.readAttribute(this.pop,'csOriginalWidth');
		this.fader = CS.v3.castToEl('csFader');
		switch(this.effect){
			case 'scaleMove':
				this._scaleMove();
				break;
			case 'opacity':
				this._opacity();
				break;
			default:
				// NOOP
				break;
		}
	},
	_scaleMove: function(){
		if(!Element.hasClassName(this.fader,'csHide')){
			new Effect.Opacity(this.fader, {from:0.5, to: 0.0,
		      afterFinish: function(){
		          this.fader.writeAttribute('style',null);
		      }.bind(this)});
		}
		new Effect.Move(this.pop, {x: this.x, y: this.y, mode: 'absolute', duration:0.4,
			afterFinish: function(){
				var CSHIDE = new String('csHide');
				if(!Element.hasClassName(this.fader,CSHIDE)){
					this.fader.toggleClassName(CSHIDE);
				}
				this.pop.addClassName(CSHIDE);
			}.bind(this)
		});
		//Scale
		new Effect.Scale(this.pop, 5, {duration: 0.4, scaleFrom: 100, scaleMode: {originalHeight: this.originalHeight,
			originalWidth: this.originalWidth}, scaleContent: false,
			afterFinish: function(){
				this.pop.style.height = 'auto';
			}.bind(this)
		});
	},
	_opacity: function(){
       var CSHIDE = new String('csHide');
       if(!Element.hasClassName(this.fader,CSHIDE)){
           Element.addClassName(this.fader,CSHIDE);
       }
       Element.removeClassName(this.fader,'csPanelOpacity');
       Element.addClassName(this.pop,CSHIDE);
	},
	log: function(s,e){
		CS.v3.Logger.log("PanelPopClose:" + s,e);
	}
});
CS.v3.PopContent = {
	content1: {
		heading:'Heading 1',
		content:'<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do' +
		'eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>'
	}
};
/*** Confirmation Panel ***/
CS.v3.Components.Confirmation = function(el,options){new CS.v3.Config.Confirmation(el,options); };
CS.v3.Config.Confirmation = Class.create(CS.v3.Base.prototype, {
	initialize: function(el,options){
		this.allSourceButtons = this.findSourceButtons(el);
		this.allTargetButtons = this.findTargetButtons(el);
		this.el=el;
		if(options.bind==='true'){this.bind();}
	},
	bind: function(){
		var el = this.el;
		var tag = el.tagName;
		if(tag==='BODY'){
			this.allSourceButtons = this.findSourceButtons(el);
			this.allTargetButtons = this.findTargetButtons(el);
			var btnIds = this.bindSource();
			this.bindTarget(btnIds);
		}
	},
	findSourceButtons: function(el){
		var rc = Element.select(el,["input[class*='csConfirmationButton']","button[class*='csConfirmationButton']"]);
		return rc;
	},
	findTargetButtons: function(el){
		var rc = Element.select(el,["input[class*='csConfirmationSubmit']","button[class*='csConfirmationSubmit']"]);
		return rc;
	},
	bindSource: function(){
		var rc = new Object();
		var ar = this.allSourceButtons;
		if(ar){
			var i = ar.length;
			var button;
			var srcBtnId;
			while(i--){
				button = ar[i];
				srcBtnId = CS.v3.Confirmation.setupSourceButton(button);
				var b = CS.v3.I18N.isEmpty(srcBtnId);
				if(!b){
				   var btnIndex = CS.v3.Confirmation.getCustomAttributeConfirmationIndex(button);
				   rc[btnIndex] = srcBtnId;
				}
			}
		}
		return rc;
	},
	bindTarget: function(btnIds){
		var ar = this.allTargetButtons;
		if(ar){
			var i = ar.length;
			var button;
			while(i--){
				button = ar[i];
				var btnIndex = CS.v3.Confirmation.getCustomAttributeConfirmationIndex(button);
				CS.v3.Confirmation.setupTargetButton(button,btnIds[btnIndex]);
			}
		}
	}
});
CS.v3.Confirmation = {
	getFlag: function(){
		var rc = new String('csConfirmation-init');
		return rc;
	},
	setupSourceButton: function(el){
		var rc;
		var formEl = CS.v3.getParentElement(el,'FORM');
		if(formEl){
			var btnid = Element.identify(el);
			this.setCustomAttributeFormSubmit(el,btnid);
			rc = btnid;
			var FLAG = this.getFlag();
			var b = Element.hasClassName(el,FLAG);
			if(!b){
				Element.addClassName(el,FLAG);
				Event.observe(el,'click', function(event){ this.confirmationSourceButtonClickedEV(event); }.bind(this));
			}
			this.setupSourceForm(formEl);
		} else {
			rc = '';
		}
		return rc;
	},
	setupSourceForm: function(el){
		if(el){
			var ar = this.getSourceControls(el);
			if(ar){
				var i = ar.length;
				var control;
				while(i--){
					control = ar[i];
					this.setupSourceControl(control);
				}
			}
		}
	},
	setupTargetButton: function(el,btnid){
		var myid = Element.identify(el);
		var FLAG = this.getFlag();
		var b = Element.hasClassName(el,FLAG);
		if(!b){
			Element.addClassName(el,FLAG);
			this.setCustomAttributeFormSubmit(el, btnid);
			Event.observe(el,'click', function(event){ this.confirmationTargetButtonClickedEV(event); }.bind(this));
			CS.v3.I18N.updateAttribute(el,'onclick','return CS.v3.Confirmation.confirmationTargetButtonClickedEV(event);');
		}
	},
	getSourceContainer: function(el){
		var rc = CS.v3.getBody();
		return rc;
	},
	getSourceControls: function(el){
		var obj = this.getSourceContainer(el);
		var rc = Element.select(obj,'.csConfirmationSource');
		return rc;
	},
	getTargets: function(el){
		var obj = this.getSourceContainer(el);
		var rc = Element.select(obj,'DIV.csConfirmationTarget');
		return rc;
	},
	setupSourceControl: function(el){
		if(el){
			var FLAG = this.getFlag();
			var b = Element.hasClassName(el,FLAG);
			if(!b){
				Element.addClassName(el,FLAG);
				Event.observe(el,'change', function(){ this.confirmationUpdate(el);}.bind(this));
				var tag = el.tagName;
				if(tag==='INPUT'){
					var typ = el.type;
					if(typ==='radio'){
						Event.observe(el,'click', function(){ this.confirmationUpdate(el);}.bind(this));
					}
				}
			}
		}
	},
	confirmationControlChangeEV: function(event){
		var el = $(Event.element(event));
		this.confirmationUpdate(el);
	},
	confirmationSourceButtonClickedEV: function(event){
		var btnEl = $(Event.element(event));
		var ar = this.getSourceControls(btnEl);
		var i = ar.length;
		var el;
		while(i--){
			el = ar[i];
			this.confirmationUpdate(el);
		}
		event.preventDefault();//Event.stop(event);
	},
	confirmationTargetButtonClickedEV: function(event){
		var rc = false;
		var el = $(Event.element(event));
		var btnid = this.getCustomAttributeFormSubmit(el);
		var btnEl = CS.v3.castToEl(btnid);
		var jssubmitnam = this.getCustomAttributeSubmitButtonName(btnEl);
		var jssubmitval = this.getCustomAttributeSubmitButtonValue(btnEl);
		var frmEl = CS.v3.getParentElement(btnEl,'FORM');
		if(frmEl){
			CS.v3.I18N.updateAttribute(btnEl,'value',jssubmitval);//
			CS.v3.insertHiddenInput(frmEl,jssubmitnam,jssubmitval,'');
			CS.v3.submitForm(frmEl);
		}
		return rc;
	},
	confirmationUpdate: function(el){
		var sourceIndex = this.getCustomAttributeConfirmationIndex(el);
		var sourceValue = CS.v3.I18N.getValueUnmasked(el,false);
		this.updateTargetValue(el, sourceValue, sourceIndex);
	},
	updateTargetValue: function(el, sourceValue, sourceIndex){
		var arTarget = this.getTargets(el);
		var i = arTarget.length;
		var obj;
		var targetIndex;
		while(i--){
			obj = arTarget[i];
			targetIndex = this.getCustomAttributeConfirmationIndex(obj);
			if(sourceIndex==targetIndex){
				CS.v3.I18N.setValue(obj,sourceValue);
				//break;//NOTE: we could break here, but want to allow for multiple instances of the same target ID (in rare cases)
			}
		}
	},
	getCustomAttributeConfirmationIndex: function(el){
		var def = '';
		var rc = CS.v3.I18N.getDataAttribute(el,'data-csconfirmationindex',def);
		return rc;
	},
	setCustomAttributeFormSubmit: function(el,val){
		CS.v3.I18N.setDataAttribute(el,'data-csconfirmationformsubmit',val);
	},
	getCustomAttributeFormSubmit: function(el){
		var def = '';
		var rc = CS.v3.I18N.getDataAttribute(el,'data-csconfirmationformsubmit',def);
		return rc;
	},
	getCustomAttributeSubmitButtonName: function(el){
		var def = Element.readAttribute(el,'name');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-csconfirmationsubmitbuttonname',def);
		return rc;
	},
	getCustomAttributeSubmitButtonValue: function(el){
		var def = CS.v3.I18N.getValue(el,true);
		var rc = CS.v3.I18N.getDataAttribute(el,'data-csconfirmationsubmitbuttonvalue',def);
		return rc;
	},
	log: function(s,e){
		CS.v3.Logger.log("Confirmation:" + s,e);
	}
};