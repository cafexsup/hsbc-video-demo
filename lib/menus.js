/* Theme Pack 3.2.1+ DO NOT modify */
/* Tabs */
CS.v3.Tabs = Class.create(CS.v3.Base.prototype,{
	initialize: function(el){
		this.el = el;
		this.wrap = Element.up(el,'.csTabs-x') || Element.up(el,'.csTabs-y');
		var attr = new CS.v3.ComponentAttributes(this.wrap);
		this.item = el.up(1);
		var s = new CS.v3.getSelected(this.item,'li');
		this.selectedItem = s.selectedIndex;
		//modes
		if(Element.hasClassName(this.wrap,'csAjax')){
			this.mode = 'ajax';
		} else {
			this.mode = 'dhtml';
		}
		if(this.mode == 'ajax'){
			this.navStatus(s.collection,this.item);
			this.ajax();
		}
		if(this.mode == 'dhtml'){
			this.contentModulesX = $$('.' + this.targetClass);
			var ar = attr.toggleContent;
			var module;
			var i = ar.length;
			while(i--){
				module = ar[i];
				Element.removeClassName(module,'csShow');
			}
			this.removedItem = this.navStatus(s.collection,this.item);
			this.show(attr);
		}
	},
	show: function(attr){
		var CSSHOW = 'csShow';// NOTE: try String
		if(attr){
			var ar = attr.toggleContent;
			var sel = this.selectedItem;
			// no targetClass attribute
			if(ar.length===0){
				var rem = this.removedItem;
				var obj = Element.next(this.wrap,rem);
				Element.removeClassName(obj,CSSHOW);
				this.module = this.wrap.next(sel,'.csContentModule');// WTF
			} else { //targetClass attribute
				this.module = ar[sel];
			}
		}
		Element.addClassName(this.module,CSSHOW);
	},
	ajax: function(){
		this.targets.each(function(value,index){
			var updateTarget = $(this.targets[index]);
			new CS.v3.AjaxNav(this.el,updateTarget,{bind:true});
		}.bind(this));
	},
	log: function(s,e){
		CS.v3.Logger.log("Tabs:" + s,e);
	}
});
/* Menus */
CS.v3.CloseNavsX = Class.create(CS.v3.Base.prototype,{
	initialize: function(target,event){
		if(!target.hasClassName('csNavItem-1')){
			CS.v3.MenuVis.hideActiveNavXToggle(target,event);
		}
	}
});
CS.v3.MenuToggle = Class.create(CS.v3.Base.prototype,{
	initialize: function(target,event){
		this.target = target;
		this.targetEvent = Event.element(event);
		this.event = event;
		this.toggle = CS.v3.getParentElement(this.target,'LI');
		this.toggleTarget = Element.down(this.toggle,'.csNavContent');
		this.wrap = Element.up(this.target,'.csClick') || Element.up(this.target,'.csOver');
		this.navList = Element.down(this.wrap,'.csNavList');
		this.tier = '.csNavToggle-0';
	}
});
CS.v3.MenuItem = Class.create(CS.v3.Base.prototype,{
	initialize: function(target,event){
		this.target = target;
		this.event = event;
		this.toggle = CS.v3.getParentElement(this.target,'LI');
		this.wrap = Element.up(this.target,'.csClick') || Element.up(this.target,'.csOver');
		this.togglePrev = Element.previous(this.toggle,'li');
		this.toggleNext = Element.next(this.toggle,'li');
		if(this.togglePrev){
			this.togglePrevTarget = Element.down(this.togglePrev,'.csNavItem-1') || Element.down(this.togglePrev,'.csNavToggle');
		}
		if(this.toggleNext){
			this.toggleNextTarget = Element.down(this.toggleNext,'.csNavItem-1') || Element.down(this.toggleNext,'.csNavToggle');
		}
		this.wrap = Element.up(this.target,'.csClick') || Element.up(this.target,'.csOver');
	}
});
CS.v3.MenuVis = {
	effect: function(m,event){
		var CSNAVSHOW = 'csNavShow';// TODO try String()
		// Vertical Menu
		if(Element.hasClassName(m.wrap,'csNav-y')){
			if(Element.hasClassName(m.wrap,'csBlind')){
				this.toggle(m,event);
			}
			if(Element.hasClassName(m.wrap,'csAccordian')){
				this.hideActiveNavYToggle(m,event);
				if(!m.toggle.hasClassName(CSNAVSHOW)){
					this.toggle(m,event);
				}
			}
		}
		// Horizontal Menu
		if(Element.hasClassName(m.wrap,'csNav-x')){
			if(!Element.hasClassName(m.toggle,CSNAVSHOW)){
				this.toggle(m,event);
			}
		}
	},
	toggle: function(m,event){
		if(Element.hasClassName(m.toggle,'csNavShow')){
			this.hide(m,event);
		} else {
			this.show(m,event);
		}
	},
	show: function(m,event){
		var key = event.keyCode;
		if(Element.hasClassName(m.wrap,'csMenuContextual')){
			CS.v3.contextualMenuOverlay.show(m,event);
		}
		if(Element.hasClassName(m.wrap,'csMenuLogOn')){
			CS.v3.MenuLogOnOverlay.show(m,event);
		}
		/* New Focus */
		var ul = m.toggleTarget;
		if((key===Event.KEY_DOWN) && Element.hasClassName(m.toggle,'csNavShow')){
			var a = Element.down(ul,'.csNavFirstItem') || Element.down(ul,'.csPanelFirstItem');// was 'A', but SearchNSelect uses an 'INPUT'
			if(a){
				CS.v3.focus(a);
			}
			return;
		}
		if(ul){
			var time = CS.v3.Base.prototype.getBlindDuration(ul);
			new Effect.BlindDown(ul, {duration: time, queue: {position: 'end', scope: 'shownLi'},
				beforeStart: function(){
					Element.addClassName(m.target,'csTargetShow'); // for trees
					m.toggleTarget.style.display = 'none';
					m.toggleTarget.style.height = 'auto';
					Element.addClassName(m.toggle,'csNavShow');
				},
				afterFinish: function(){
					//ie 6 only items disappear without this
					if(m.target.id != 'csActiveNavXToggle' && Element.hasClassName(m.wrap,'csNav-x')){
						m.target.id = 'csActiveNavXToggle';
						m.toggle.id = 'csActiveNavXToggleLi';
					}
					m.toggleTarget.style.height = '';
					m.toggleTarget.style.display = 'block';
					//ie 6 only
					/* New Focus
					if((key===Event.KEY_DOWN) || (key===Event.KEY_RETURN)){
						var ul = m.toggleTarget;
						var a = Element.down(ul,'.csNavFirstItem') || Element.down(ul,'.csPanelFirstItem') ;// was 'A', but SearchNSelect uses an 'INPUT'
						CS.v3.focus(a);
					}
					*/
				}
			});
		}
	},
	hide: function(m,event){
		var hideAll;
		if(Element.hasClassName(m.wrap,'csMenuContextual')){
			if(m.targetEvent.id == 'csActiveNavXToggle'){
				hideAll = 'true';
			}
			m.toggleTarget = CS.v3.contextualMenuContent;
		}
		if(Element.hasClassName(m.wrap,'csMenuLogOn')){
			if(m.targetEvent.id == 'csActiveNavXToggle'){
				hideAll = 'true';
			}
			m.toggleTarget = CS.v3.MenuContentLogOn;
		}
		var dimen = m.toggleTarget.getDimensions();
		var height = dimen.height;
		var time = CS.v3.Base.prototype.getBlindDuration(m.toggleTarget);
		new Effect.BlindUp(m.toggleTarget, {duration: time, queue: {position: 'front', scope: 'shownLi2'},
			beforeStart: function(){
				if(Element.hasClassName(m.wrap,'csNav-x')){
					m.target.id = '';
					m.toggle.id = '';
				}
				Element.removeClassName(m.target,'csTargetShow');
				m.toggleTarget.style.height = height;
				m.toggleTarget.style.overflow = 'hidden';
			}.bind(this),
			afterFinish: function(){
				Element.removeClassName(m.toggle,'csNavShow');
				if(Element.hasClassName(m.wrap,'csMenuContextual')){
					if(hideAll == 'true'){
						CS.v3.contextualMenu.style.display='none';
					}
					//Element.update(CS.v3.contextualMenuScroll,'');
				}
				if(Element.hasClassName(m.wrap,'csMenuLogOn')){
					if(hideAll == 'true'){
						CS.v3.MenuLogOn.style.display='none';
					}
				}
			}
		});
	},
	showAll: function(attr,event){
		var ar = attr.showContent;
		var i = ar.length;
		var menu;
		var m;
		while(i--){
			menu = ar[i];
			m = new CS.v3.MenuToggle(menu,event);
			this.show(m,event);
		}
	},
	hideAll: function(attr,event){
		var ar = attr.hideContent;
		var i = ar.length;
		var menu;
		var m;
		while(i--){
			menu = ar[i];
			m = new CS.v3.MenuToggle(menu,event);
			this.hide(m,event);
		}
	},
	hideActiveNavXToggle: function(m,event){
		var target = $('csActiveNavXToggle');// TODO - why does this override the argument
		var key = event.keyCode;
		if(target){
			var t = new CS.v3.MenuToggle(target,event);
			this.hide(t,event);
			if(key===Event.KEY_ESC){
				CS.v3.focus(target);
			}
		}
	},
	hideActiveNavYToggle: function(m,event){
		var shown = Element.select(m.wrap,'li.csNavShow');
		shown.each(function(value,index){
			var target = Element.down(shown[index],'.csNavToggle');
			var m2 = new CS.v3.MenuToggle(target,event);
			CS.v3.MenuVis.toggle(m2,event);
		});
	}
};
CS.v3.contextualMenuOverlay = {
	show: function(m,event){
		var obj = new CS.v3.OverlaysObj(m,event);
		var width = m.wrap.getWidth();
		var CLS = 'csMenuContextualRight'; // TODO try String
		if(obj.align == CLS){
			Element.addClassName(CS.v3.contextualMenu,CLS);
		} else {
			Element.removeClassName(CS.v3.contextualMenu,CLS);
		}
		CS.v3.contextualMenu.style.left = obj.left - obj.containerWrapLeft + 'px';
		CS.v3.contextualMenu.style.top = obj.top + obj.height + 'px';
		CS.v3.contextualMenu.style.width = width + 'px';
		CS.v3.contextualMenu.style.display = 'block';
		var content = Element.down(m.toggleTarget,'div.csPanelScroll').innerHTML;
		m.toggleTarget.style.display = 'none';
		Element.update(CS.v3.contextualMenuScroll,content);
		m.toggleTarget = CS.v3.contextualMenuContent;
	}
};
CS.v3.MenuLogOnOverlay = {
	show: function(m,event){
		var obj = new CS.v3.OverlaysObj(m,event);
		var width = m.wrap.getWidth();
		if(obj.align == 'csMenuContextualRight'){
			Element.addClassName(CS.v3.MenuLogOn,obj.align);
		} else {
			Element.removeClassName(CS.v3.MenuLogOn,'csMenuContextualRight');
		}
		CS.v3.MenuLogOn.style.left = obj.left - obj.containerWrapLeft + 'px';
		CS.v3.MenuLogOn.style.top = obj.top + obj.height + 'px';
		CS.v3.MenuLogOn.style.width = width + 'px';
		CS.v3.MenuLogOn.style.display = 'block';
		var content = Element.down(m.toggleTarget,'div.csPanelScroll').innerHTML;
		m.toggleTarget.style.display = 'none';
		Element.update(CS.v3.MenuScrollLogOn,content);
		//******************* New LogOn
		m.toggleTarget = CS.v3.MenuContentLogOn;
		//******************* New LogOn
	}
};
CS.v3.Components.MenuMouseOut = function(section,options){
	new CS.v3.MenuMouseOut(section,options);
};
CS.v3.MenuMouseOut = Class.create(CS.v3.Base.prototype,{
	initialize: function(section){
		this.observeNodes = $$('.csOver li.csNavToggle-0');
		this._bindOver();
	},
	_bindOver: function(){
		var ar = this.observeNodes;
		var i = ar.length;
		var node;
		while(i--){
			node = ar[i];
			Event.observe(node,'mouseout', function(event){
				var toEl;
				if(Prototype.Browser.IE){
					toEl = event.toElement;
				} else {
					toEl =  $(event.relatedTarget);
				}
				var desc = toEl.descendantOf('csActiveNavXToggleLi');
				if(!desc){
					CS.v3.MenuVis.hideActiveNavXToggle(toEl,event);
				}
			});
			var navContent = Element.down(node,'.csNavContent');
			Event.observe(navContent,'mouseout', function(event){
				var toEl;
				if(Prototype.Browser.IE){
					toEl = event.toElement;
				} else {
					toEl =  $(event.relatedTarget);
				}
				var desc = toEl.descendantOf('csActiveNavXToggleLi');
				if(!desc){
					CS.v3.MenuVis.hideActiveNavXToggle(toEl,event);
				}
			});
		}
	}
});
CS.v3.MenuNewMouseOut = Class.create(CS.v3.Base.prototype,{
	initialize: function(event){
		this.target = $(Event.element(event));
		this.item = CS.v3.getParentElement(this.target,'LI');
	}
});
CS.v3.TogglesVis = {
	showAll: function(attr,event){
		var ar = attr.showContent;
		var i = ar.length;
		var toggle;
		var targetWrap;
		var attr2;
		var CSTOGGLESHOW = 'csToggleShow';// NOTE: try String
		while(i--){
			toggle = ar[i];
			targetWrap = toggle.up(0,'.csIcon');
			attr2 = new CS.v3.ComponentAttributes(toggle);
			if(!Element.hasClassName(toggle,CSTOGGLESHOW)){
				Element.addClassName(toggle,CSTOGGLESHOW);
				if(targetWrap){
					Element.addClassName(targetWrap,'csIconShow');
				}
				new CS.v3.ToggleDisplay(toggle,attr2);
			}
		}
	},
	hideAll: function(attr,event){
		var ar = attr.hideContent;
		var i = ar.length;
		var toggle;
		var targetWrap;
		var attr2;
		var CSTOGGLESHOW = 'csToggleShow';// NOTE: try String
		while(i--){
			toggle = ar[i];
			targetWrap = toggle.up(0,'.csIcon');
			attr2 = new CS.v3.ComponentAttributes(toggle);
			if(Element.hasClassName(toggle,CSTOGGLESHOW)){
				Element.removeClassName(toggle,CSTOGGLESHOW);
				if(targetWrap){
					Element.removeClassName(targetWrap,'csIconShow');
				}
				new CS.v3.ToggleDisplay(toggle,attr2);
			}
		}
	}
};
// Used by table column overflow show more/less
CS.v3.tableShowMoreLess = Class.create(CS.v3.Base.prototype,{
	initialize: function(target){
		var attr = new CS.v3.ComponentAttributes(target);
		var targetWrap = target.up(0,'.csIcon');
		new CS.v3.ToggleDisplay(target,attr);
		target.toggleClassName('csToggleShow');
		if(targetWrap){
			targetWrap.toggleClassName('csIconShow');
		}
		var ar = Element.select(target,'span.csToggleText');
		if(ar){
			var CSHIDE = 'csHide';// TODO try String('csHide');
			var i = ar.length;
			var spanEl;
			while(i--){
				spanEl = ar[i];
				spanEl.toggleClassName(CSHIDE);
			}
		}
	}
});
