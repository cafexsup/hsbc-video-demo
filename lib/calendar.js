/* Theme Pack 3.2.1+ DO NOT modify */
/* Contains Calendar, Datepicker, Clocks and Timers */
CS.v3.Components.DatePicker = function(el,options){new CS.v3.Config.DatePicker(el,options); };
CS.v3.Config.DatePicker = Class.create(CS.v3.Base.prototype, {
	initialize: function(el,options){
		this.allPickers = CS.v3.DatePicker.findDatePickers(el);
		if(options.bind==='true'){this.bind();}
	},
	bind: function(){
		var ar = this.allPickers;
		if(ar){
			var i = ar.length;
			var el;
			var tag;
			while(i--){
				el = ar[i];
				CS.v3.DatePicker.addDecoration(el);
			}
		}
	}
});
CS.v3.DatePicker = {
	findDatePickers: function(el){
		// TODO  must be updated for HTML5 input types
		return Element.select(el,["SELECT[class*='csPicker']","INPUT[class*='csPicker']"]);
	},
	addDecoration: function(el){
		var fieldset = CS.v3.getParentElement(el,'FIELDSET');
		var FLAG = new String('csDatepicker-init');
		var b = Element.hasClassName(fieldset,FLAG);
		if(!b){
			var locale = CS.v3.I18N.initialize(el);
			CS.v3.I18N.initialize(fieldset);
			Element.addClassName(fieldset,FLAG);
			Element.wrap(fieldset,'DIV',{'class':'csDatepicker-wrapper'});
			var node = Element.up(el,'DIV.csDate-wrapper');
			if(node){
				CS.v3.I18N.setLocale(node,locale);
				var parms = CS.v3.I18N.writeLocale(locale);
				var sb = new StringBuffer();
				sb.append('<div class="csDatepicker-holder"');
				sb.append(parms);
				sb.append('>&nbsp;</div><div class="csDatepicker-display"');
				sb.append(parms);
				sb.append('>&nbsp;</div>');
				var str = sb.toString(); sb=null;
				// NOTE: considering merging code so that even the picker can show a calendar!
				Element.insert(node,{bottom:str});
			}
			Event.observe(el,'change', function(event){ this.changeDateEv(event); }.bind(this));
			Event.observe(el,'blur', function(event){ this.changeDateEv(event); }.bind(this));
			tag = el.tagName;
			if(tag==='INPUT' || tag==='TEXTAREA'){
				Event.observe(el,'keyup', function(event){ this.changeDateEv(event); }.bind(this));
			}
			var frm = CS.v3.getParentElement(el,'FORM');
			if(frm){
				if(!Element.hasClassName(frm,FLAG)){
					Element.addClassName(frm,FLAG);
					Event.observe(frm,'reset', function(event){ this.resetFormDatePickerEv(event); }.bind(this));
				}
			}
			Element.identify(el);
		}
		this.updateDate(el);
	},
	resetFormDatePickerEv: function(event){
		var el = $(Event.element(event));//frm
		var ar = this.findDatePickers(el);
		if(ar){
			var i = ar.length;
			var obj;
			while(i--){
				obj = ar[i];
				this.updateDate(obj);
			}
		}
		//this.log('resetFormCalendarEv:TODO');
	},
	changeDateEv: function(event){
		var el = $(Event.element(event));
		this.updateDate(el);
	},
	updateDate: function(el){
		CS.v3.I18N.initialize(el);
		var node = Element.up(el,'DIV.csDatepicker-wrapper');
		var fieldset = Element.down(node,'FIELDSET.csDatepicker-init');
		var childs = fieldset.childElements();// node.children;
		var dt = CS.v3.I18N.createDate();
		var isTZ = dt.getTimezoneOffset();
		var isYR;
		var isMO;
		var isDA;
		var isHR;
		var isMN;
		var isSE;
		var isMS;
		var isAP;
		if(childs){
			var EMPTY = new String('');
			var i1 = childs.length;
			var item1;
			while(i1--){
				item1 = childs[i1];
				//CS.v3.I18N.initialize(item1);
				//isTZ = this.tester(isTZ,item1,'?','csOffset',-2400,2400,'');
				isYR = this.tester(isYR,item1,'Y','csFullyear',1900,9999,EMPTY);
				isMO = this.tester(isMO,item1,'M','csMonth',0,12,EMPTY);
				isDA = this.tester(isDA,item1,'D','csDay',1,31,EMPTY);
				isHR = this.tester(isHR,item1,'h','csHour',0,24,EMPTY);
				isMN = this.tester(isMN,item1,'m','csMinute',0,60,EMPTY);
				isSE = this.tester(isSE,item1,'s','csSecond',0,60,EMPTY);
				//isAP = this.tester(isAM,item1,'a','csAmpm',EMPTY,EMPTY,'AM');
			}//end for
			// day of week only available when all DAY/MONTH and YEAR are known
			if(isDA>0 && isMO>0){// && isYR>0)
				var daysInMonth = CS.v3.I18N.getDaysInMonth(isMO+1,isYR);
				// update UI and fix the date to be the last one in the month if greater
				if(childs){
					var i2=childs.length;
					var item2;
					while(i2--){
						item2 = childs[i2];
						//CS.v3.I18N.initialize(item2);
						isDA = this.tester(isDA,item2,'D','csDay',0,daysInMonth,daysInMonth);
					} // end for
				}
			} //end if(isDA...)
		}
		/*
		var p = Element.down(node,'DIV.csDatepicker-display');
		if(p){
			CS.v3.I18N.initialize(p);
			var pattern = CS.v3.I18N.getCustomAttributePattern(p,''),;
			if(CS.v3.I18N.isEmpty(pattern)){
				pattern = new String('EEEE, MMMM DD YYYY');
			}
			//this.log'update-pattern=' + pattern + '|' + isYR + '|' + isMO + '|' + isDA);
			var msg = CS.v3.I18N.pattern_replace(p,pattern,isTZ,isYR,isMO+1,isDA,isHR,isMN,isSE,isMS,isAP);
			var x = CS.v3.I18N.trimString(msg);
			//if(x!='-1'){
				Element.update(p,msg);
			//}
		}
		*/
	},
	tester: function(rc,el,abbr,cls,minVal,maxVal,repVal){
		var str = CS.v3.I18N.getValueUnmasked(el,true);
		if(CS.v3.I18N.isEmpty(str)){
			str = '-1';
		}
		if(str){
			var sb = new StringBuffer();
			sb.append(abbr);
			sb.append(abbr);
			if(abbr=='Y'){
				sb.append(abbr);
				sb.append(abbr);
			}
			var pattern = sb.toString(); sb=null;
			var EMPTY = new String('');
			var valStr = CS.v3.I18N.parseCommon(el,str,pattern,abbr,EMPTY);
			var val = parseInt(valStr,10);
			if(val>=0){
				if(abbr=='M'){
					val = val - 1;
				}
				if(Element.hasClassName(el,cls)){
					var tr = CS.v3.getParentElement(el,'TR');
					if((val>=minVal) && (val<=maxVal)){
						rc=val;
						CS.v3.I18N.removeError(tr);
					} else {
						var b = CS.v3.I18N.isEmpty(repVal);
						if(b){
							CS.v3.I18N.addError(tr);
						} else {
							rc = repVal;
							CS.v3.I18N.setValue(el,repVal);
						}
					}
				}
			}
		}
		return rc;
	},
	log: function(s,e){
		CS.v3.Logger.log("DatePicker:" + s,e);
	}
};
CS.v3.Components.Calendar = function(el,options){ new CS.v3.Config.Calendar(el,options);};
CS.v3.Config.Calendar = Class.create(CS.v3.Base.prototype, {
	initialize: function(el,options){
		this.arCalendars = CS.v3.Calendar.findCalendars(el);
		if(options.bind==='true'){this.bind();}
	},
	bind: function(){
		var ar = this.arCalendars;
		if(ar){
			var i = ar.length;
			var el;
			while(i--){
				el = ar[i];
				CS.v3.Calendar.addDecoration(el);
			}
		}
	}
});
CS.v3.Calendar = {
	findCalendars: function(el){
		// TODO  must be updated for HTML5 input types
		return Element.select(el,"INPUT[class*='csCalendar']");
	},
	bindKeys: function(event){
		var key = event.keyCode;//
		switch(key){
			case Event.KEY_TAB:
				this.bindKeysTab(event);
				break;
			case Event.KEY_ESC:
				this.bindKeysClose(event);
				break;
			case Event.KEY_RETURN:
				this.bindKeysClose(event);
				break;
			case Event.KEY_END:
				this.bindKeysClose(event);
				break;
			case Event.KEY_BACKSPACE:
				this.bindKeysDelete(event);
				break;
			case Event.KEY_DELETE:
				this.bindKeysDelete(event);
				break;
			case Event.KEY_UP:
				this.bindKeysMove(event);
				break;
			case Event.KEY_DOWN:
				this.bindKeysMove(event);
				break;
			case Event.KEY_LEFT:
				this.bindKeysMove(event);
				break;
			case Event.KEY_RIGHT:
				this.bindKeysMove(event);
				break;
			case Event.KEY_PAGEUP:
				this.bindKeysClose(event);
				break;
			case Event.KEY_PAGEDOWN:
				this.bindKeysClose(event);
				break;
			default:
				this.bindKeysDefault(event);
				break;
		}
	},
	bindKeysTab: function(event){
		this.bindKeysClose(event);
		/*
		var el = $(Event.element(event));
		var SHOWFLAG = this.getCalendarShowFlag(el);
		var spanwrap = this.findCalendarWrapper(el);
		var b = Element.hasClassName(spanwrap,SHOWFLAG)
		if(!b){
			var outerDiv = CS.v3.Calendar.findSingleCalendarContainer(el);
			CS.v3.Calendar.focusFirstCalendarOverlayField(outerDiv);
			event.preventDefault(event);
		} else {
			this.bindKeysClose(event);
		}
		*/
	},
	bindKeysClose: function(event){
		CS.v3.Calendar.blurCalendarEv(event);
	},
	bindKeysDelete: function(event){
		CS.v3.Calendar.blurCalendarEv(event);
	},
	bindKeysMove: function(event){
		// TODO add keyboard arrow controls
		CS.v3.Calendar.blurCalendarEv(event);
		//event.preventDefault(event);
	},
	bindKeysDefault: function(event){
		//var el = $(Event.element(event));
		//var c = event.keyCode || event.charCode;
		//var s = CS.v3.I18N.getStringFromChar(el,c);
		//alert('key=' + key + '=[' + s + ']');
		//CS.v3.Calendar.blurCalendarEv(event);
		this.bindKeysClose(event);
	},
	addDecoration: function(input){
		var FLAG = new String('csCalendar-init');
		var b=Element.hasClassName(input,FLAG);
		if(!b){
			var node = this.findCalendarWrapper(input);
			CS.v3.I18N.initialize(node);
			CS.v3.I18N.initialize(input);
			Element.addClassName(input,FLAG);
			Element.addClassName(input,'csVal-isCalendarSpaces');// IMPLIED validation!
			Element.addClassName(input,'csVal-isCalendarRange');// IMPLIED validation!
			var ICON = this.getCalendarIconFlag(input);
			var boutside = Element.hasClassName(input,ICON);
			var icondiv = this.findCalendarInputIconDiv(node);
			if(boutside){
				Element.addClassName(node,ICON);
				if(icondiv){
					Event.observe(icondiv,'click', function(event){ this.focusCalendarIconEv(event); }.bind(this));
					Event.observe(icondiv,'keydown', function(event){ this.focusCalendarIconEv(event); }.bind(this));
				}
			} else {
				Event.observe(input,'click', function(event){ this.focusCalendarEv(event); }.bind(this));
				if(icondiv){
					Event.observe(icondiv,'click', function(event){ this.focusCalendarIconEv(event); }.bind(this));
					Event.observe(icondiv,'keydown', function(event){ this.focusCalendarIconEv(event); }.bind(this));
				}
			}
			Event.observe(input,'change', function(event){ this.changeCalendarEv(event); }.bind(this));
			Element.identify(input);// used later in WAI-ARIA calendar
			var ipattern = this.getInputPattern(input);
			CS.v3.I18N.updateAttribute(input,'title',ipattern);
			CS.v3.I18N.updateAttribute(input,'autocomplete','off');
			this.decorateInputRange(input,ipattern);
			//CS.v3.Security.preventAutocomplete(input);
			var months2show = this.getInputMonths2Show(input);
			var sb = new StringBuffer();
			sb.append('<div class="csPanel csPanelCalendar ');// ending space intentional!
			if(months2show==1){
				var CSONEMONTH = this.getCalendarOneMonthFlag(input);
				sb.append(CSONEMONTH);
			}
			sb.append('"></div>');
			/* removed display */
			//sb.append('<div class="csCalendar-holder"></div><div class="csCalendar-display"></div>');
			/* end removed display */
			var htm = sb.toString(); sb=null;
			Element.insert(node,{bottom:htm});
			Event.observe(input,'keydown', function(event){ this.bindKeys(event); }.bind(this));
			//Event.observe(input,'focus', function(event){ this.focusCalendarEv(event); }.bind(this));
			//Event.observe(input,'blur', function(event){ this.blurCalendarEv(event); }.bind(this));
			var frm = CS.v3.getParentElement(input,'FORM');
			if(frm){
				if(!Element.hasClassName(frm,FLAG)){
					Element.addClassName(frm,FLAG);
					Event.observe(frm,'reset', function(event){ this.resetFormCalendarEv(event); }.bind(this));
				}
			}
		}
		this.updateCalendar(input);
	},
	decorateInputRange: function(el,pattern){
		var dt_now = CS.v3.I18N.createDate();
		var patternPaging = this.getPagingPattern(el); //'YYYYMMDD'
		// set max value when not already defined
		//var n = this.getCustomAttributeCalendarInputRangeMax(el,'');
		//var bn = CS.v3.I18N.isEmpty(n);
		//if(bn){
			var calValNext = this.getNextYear(dt_now);
			var calN = CS.v3.I18N.pattern_obj(el,patternPaging,calValNext);
			this.setCustomAttributeCalendarInputRangeMax(el,calN);
		//}
		// set min value when not already defined
		//var p = this.getCustomAttributeCalendarInputRangeMin(el);
		//var bp = CS.v3.I18N.isEmpty(p);
		//if(bp){
			var calValPrev = this.getPrevYear(dt_now);
			var calP = CS.v3.I18N.pattern_obj(el,patternPaging,calValPrev);
			this.setCustomAttributeCalendarInputRangeMin(el,calP);
		//}
	},
	updateCalendar: function(input){
		var isDisplay = false;//NOTE: is this a requirement ?  possible performance  issues!
		if(isDisplay){
			var spanwrap = this.findCalendarWrapper(input);
			var p = Element.down(spanwrap,'DIV.csCalendar-display');
			if(p){
				var displayPattern = this.getDisplayPattern(input);
				var pattern = CS.v3.I18N.getCustomAttributePattern(p,displayPattern);
				var str = CS.v3.I18N.getValueUnmasked(input,true);
				var rc;
				var b = CS.v3.I18N.isEmpty(str);
				if(!b){
					CS.v3.I18N.initialize(p);
					var ipattern = this.getInputPattern(input);
					var calVal = this.getCurrentDate(input,pattern);
					rc = CS.v3.I18N.pattern_obj(input,ipattern,calVal);
				} else {
					rc = '';
/*					var bm = CS.v3.I18N.isEmpty(msg);
					if(bm){
						//var ipattern = this.getInputPattern(input);
						//rc = ipattern;
						rc = new String('');
					}
*/
				}
				CS.v3.updateFix(p,rc);
			}
		}
		//this.log('updateCalendar:exit');
	},
	changeCalendarEv: function(event){
		var el = $(Event.element(event));
		if(el){
			var tag = el.tagName;
			if(tag==='INPUT'){
				this.hideCalendar(el);
				this.updateCalendar(el);
			}
		}
	},
	resetFormCalendarEv: function(event){
		var el = $(Event.element(event));//frm
		var ar = this.findCalendars(el);
		if(ar){
			var i = ar.length;
			var obj;
			while(i--){
				obj = ar[i];
				this.updateCalendar(obj);
			}
		}
		//this.log('resetFormCalendarEv:TODO');
	},
	focusCalendarIconEv: function(event){
		var el = $(Event.element(event));
		var key = event.keyCode;
		var typ = event.type;
		if((typ==='click') || (key===Event.KEY_RETURN)){
			event.preventDefault(event);
			var node = this.findCalendarWrapper(el);
			var ar = this.findCalendars(node);
			var b = CS.v3.isArray(ar);
			if(b){
				var i = ar.length;
				if(i>0){
					var cal = ar[0];
					var showHelpIcon = typ==='keydown';
					this.setCustomAttributeCalendarShowHelpIcon(cal, showHelpIcon);
					this.showCalendar(cal,'');
					var outerDiv = this.findSingleCalendarContainer(cal);
					this.focusFirstCalendarOverlayField(outerDiv);
				}
			}
		}
	},
	focusCalendarEv: function(event){
		var el = $(Event.element(event));
		if(el){
			var tag = el.tagName;
			if(tag==='INPUT'){
				var showHelpIcon = event.type==='keydown';
				this.setCustomAttributeCalendarShowHelpIcon(el, showHelpIcon);
				this.showCalendar(el,'');
				var outerDiv = this.findSingleCalendarContainer(el);
				this.focusFirstCalendarOverlayField(outerDiv);
			}
		}
	},
	blurCalendarEv: function(event){
		var el = $(Event.element(event));
		if(el){
			var tag = el.tagName;
			if(tag==='INPUT'){
				this.hideCalendar(el);
			}
		}
	},
	handleCalendarEv: function(event){
		var el = $(Event.element(event));
		if(el){
			var tag = el.tagName;
			if(tag==='A'){
				var isClick = event.type==='click';
				var li = Element.up(el,'LI.csIconClose');
				if(li){
					if(isClick){
						this.closeCalendar(el);
						Event.stop(event);
					}
					return;
				}
				var input = this.findCalendarInput(el);
				if(input){
					var outerDiv = this.findSingleCalendarContainer(input);
					var div = Element.up(el,'DIV.csCalendar-next');
					var valStr,newIcon;
					if(div){
						if(isClick){
							valStr = this.getCustomDataAttributeValue(div);
							this.showCalendar(input,valStr);
							newIcon=Element.down(outerDiv, 'DIV.csCalendar-next a');
							CS.v3.focus(newIcon);
							Event.stop(event);
						}else if(event.keyCode===Event.KEY_LEFT){
							newIcon=Element.down(outerDiv, 'DIV.csCalendar-previous a');
							CS.v3.focus(newIcon);
							Event.stop(event);
						}
						return;
					}
					div = Element.up(el,'DIV.csCalendar-previous');
					if(div){
						if(isClick){
							valStr = this.getCustomDataAttributeValue(div);
							this.showCalendar(input,valStr);
							newIcon=Element.down(outerDiv, 'DIV.csCalendar-previous a');
							CS.v3.focus(newIcon);
							Event.stop(event);
						}else if(event.keyCode===Event.KEY_RIGHT){
							newIcon=Element.down(outerDiv, 'DIV.csCalendar-next a');
							CS.v3.focus(newIcon);
							Event.stop(event);
						}
						return;
					}
					var td=CS.v3.getParentElement(el,'TD');
					if(td){
						if(isClick){
							this.clickCalendar(el,input);
							Event.stop(event);
						}else{
							var keyCode=event.keyCode;
							this.navigateDateGrid(td,keyCode,event.ctrlKey);
							if(keyCode>=37&&keyCode<=40){
								Event.stop(event);
							}
						}
					}
				}
			}
		}
	},
	navigateDateGrid: function(tdEl,keyCode,ctrlKey){
		var trEl = CS.v3.getParentElement(tdEl,'tr');
		var tableEl = CS.v3.getParentElement(trEl,'table');
		var colDiv = CS.v3.getParentElement(tableEl,'div');
		var index = Element.previousSiblings(tdEl).size();
		var target;
		switch(keyCode){
			case Event.KEY_LEFT:
				if(ctrlKey){
					colDiv = Element.previous(colDiv);
					if(colDiv){
						target = Element.down(colDiv,'tbody a');
						CS.v3.focus(target);
					}
					break;
				}
				while(true){
					tdEl = Element.previous(tdEl);
					if(!tdEl){
						trEl = Element.previous(trEl);
						if(trEl){
							tdEl = Element.childElements(trEl).last();
						}else{
							break;
						}
					}
					target = Element.down(tdEl,'a');
					if(target){
						CS.v3.focus(target);
						break;
					}
				}
				break;
			case Event.KEY_RIGHT:
				if(ctrlKey){
					colDiv = Element.next(colDiv);
					if(colDiv){
						target = Element.down(colDiv,'tbody a');
						CS.v3.focus(target);
					}
					break;
				}
				while(true){
					tdEl = Element.next(tdEl);
					if(!tdEl){
						trEl = Element.next(trEl);
						if(trEl){
							tdEl = Element.childElements(trEl).first();
						}else{
							break;
						}
					}
					target = Element.down(tdEl,'a');
					if(target){
						CS.v3.focus(target);
						break;
					}
				}
				break;
			case Event.KEY_UP:
				while(true){
					trEl = Element.previous(trEl);
					if(!trEl) break;
					tdEl = Element.childElements(trEl)[index];
					target = Element.down(tdEl,'a');
					if(target){
						CS.v3.focus(target);
						break;
					}
				}
				break;
			case Event.KEY_DOWN:
				while(true){
					trEl = Element.next(trEl);
					if(!trEl) break;
					tdEl = Element.childElements(trEl)[index];
					target = Element.down(tdEl,'a');
					if(target){
						CS.v3.focus(target);
						break;
					}
				}
				break;
			default:break;
		}
	},
	closeCalendar: function(el){
		//this.log('closeCalendar:el=' + el);
		//var obj = this.findSingleCalendarContainer(el);
		CS.v3.CalendarVis.hide(el);
	},
	findSingleCalendarContainer: function(el){
		/*NOTE: 'el' is passed, but not currently used */
		var rc = CS.v3.ContainerCalendar;
		//var rc = $('div#csContainerCalendar');
		//var rc;
		return rc;
	},
	clickCalendar: function(el,input){
		//this.log('clickCalendar:el=' + el);
		//this.log('clickCalendar:input=' + input);
		//var val = CS.v3.I18N.getValue(el,true); //BUTTON
		var valStr = this.getCustomDataAttributeValue(el); // A(nchor)
		CS.v3.I18N.setValue(input,valStr);
		var processDays = this.getCustomAttributeProcessDays(input);
		if(processDays<1){
			this.hideCalendar(input);
		} else {
			this.showCalendar(input,'');
			this.updateCalendar(input);
			//CS.v3.focus(input);
		}
		CS.v3.fireOnchange(input);
	},
	hideCalendar: function(el){
		//this.log('hideCalendar:el=' + el);
		//var SHOWFLAG = this.getCalendarShowFlag(el);
		//var b = Element.hasClassName(el,SHOWFLAG);
		//if(b){
			//var obj = this.findSingleCalendarContainer(el);
			//var obj = this.findCalendarInput(div);
			CS.v3.CalendarVis.hide(el);
		//}
	},
	getInputMonths2Show: function(el){
		var rc = 2;// NOTE: was '1'
		var CSONEMONTH = this.getCalendarOneMonthFlag(el);
		var b1=Element.hasClassName(el,CSONEMONTH);
		if(b1){
			rc = 1;
		}
/* no need for this at this time
		... else {
			var b2=Element.hasClassName(el,'csTwomonths');
			if(b2){
				rc = 2;
			}
		}
*/
		return rc;
	},
	getInputWeeks2Show: function(el){
		var rc = false;
		var b=Element.hasClassName(el,'csWeeks');
		if(b){
			rc = true;
		}
		return rc;
	},
	getCurrentDate: function(el,pattern){
		//this.log('getCurrentDate:init');
		//var pattern = this.getInputPattern(el);
		//CS.v3.I18N.initialize(el);
		var dt_now = CS.v3.I18N.createDate();
		var rc = CS.v3.I18N.parseDateField(el,pattern,dt_now);
		//this.log('getCurrentDate:exit:rc=' + rc);
		//alert('pattern=' + pattern + '|rc=' + rc + '|dt_now=' + dt_now);
		return rc;
	},
	getAllHolidays: function(el){
		//this.log('getAllHolidays:init');
		var rc;
		var keys = this.getCustomDataAttributeCountry(el);
 		var b1 = CS.v3.I18N.isEmpty(keys);
 		if(!b1){
			var b2 = (Element.hasClassName(el,'csHolidays') || Element.hasClassName(el,'isNoHolidays'));
			if(b2){
				rc = this.findHolidays(el,keys);
			}
		} else {
			rc = null;
		}
		//this.log('getAllHolidays:exit');
		return rc;
	},
	showCalendar: function(el,valStr){
		//this.log('showCalendar:init:el=' + el);
		var id = Element.identify(el);
		var obj = this.findSingleCalendarContainer(el);
		this.setCalendarInputId(obj,id);
		var spanup = this.findCalendarWrapper(el);
		//this.log('showCalendar:spanup=' + spanup);
		if(spanup){
			CS.v3.I18N.initialize(el);
			var locale = CS.v3.I18N.getLocale(el);
			var todayVal = CS.v3.I18N.createDate();
			var pattern = this.getInputPattern(el);
			var patternPaging = this.getPagingPattern(el); //'YYYYMMDD'
			var months2show = this.getInputMonths2Show(el);
			var isShowWeek = this.getInputWeeks2Show(el);
			var currentVal;
			var bval = CS.v3.I18N.isEmpty(valStr);
			if(bval){
				currentVal = this.getCurrentDate(el,pattern);
			} else {
				currentVal = CS.v3.I18N.parseDateFieldVal(el,valStr,patternPaging,todayVal);
			}
			//this.log('showCalendar:currentVal=' + currentVal);
			if((!currentVal) || currentVal=='Invalid Date'){ // NaN?
				this.log('showCalendar:bad date!');
				var tr = CS.v3.getParentElement(el,'TR');
				//alert('currentVal=' + currentVal);
				CS.v3.I18N.addError(tr);
			} else {
				//CS.v3.I18N.removeError(el);
				var caldiv = this.findSingleCalendarContainer(el);
				var FLAG = new String('csCalendarOverlay-init');
				var b = Element.hasClassName(caldiv,FLAG);
				if(!b){
					Element.addClassName(caldiv,FLAG);
					Event.observe(caldiv,'click', function(event){ this.handleCalendarEv(event); }.bind(this));
					Event.observe(caldiv,'keydown', function(event){ this.handleCalendarEv(event); }.bind(this));
				}
				var closeLabel = this.getCustomAttributeCloseLabel(el);
				var summaryLabel = this.getCustomAttributeSummaryLabel(el);
				var captionLabel = this.getCustomAttributeCaptionLabel(el);
				var startOnDOW = this.getCustomAttributeStartOnDOW(el);
				var processDays = this.getCustomAttributeProcessDays(el);
				var patternMonth = this.getHeaderPattern(el);//'MMMM YYYY'; // '(LL)'
				var patternTitle = this.getDisplayPattern(el);//'EEEE, MMMM DD YYYY';
				var headerText = this.getCustomAttributeHeaderText(el);
				var footerText = this.getCustomAttributeFooterText(el);
				var patternDay = this.getDayPattern(el);
				var year = currentVal.getFullYear();
				var month = currentVal.getMonth()+1;
				var day = currentVal.getDate();
				var allHolidays = this.getAllHolidays(el);
				var calVal1 = CS.v3.I18N.setDateObj(year,month,1);// TODO is month correct here?
				var calValNext = this.getNextMonth(calVal1);
				var calValPrev = this.getPrevMonth(calVal1);
				var calHolidays1 = this.filterHolidays(el,allHolidays,calVal1);
				var calN = CS.v3.I18N.pattern_obj(el,patternPaging,calValNext);
				var calP = CS.v3.I18N.pattern_obj(el,patternPaging,calValPrev);
				var NBSP = new String('&nbsp;');
				var EMPTYLINK = new String('<a href="#">' + NBSP + '</a>');
				var sbPrev = new StringBuffer();
				sbPrev.append('<div class="csCalendar-previous" data-csvalue="');
				sbPrev.append(calP);
				sbPrev.append('">');
				sbPrev.append(EMPTYLINK);
				sbPrev.append('</div>');
				var msgPrev = sbPrev.toString(); sbPrev = null;
				var sbNext = new StringBuffer();
				sbNext.append('<div class="csCalendar-next" data-csvalue="');
				sbNext.append(calN);
				sbNext.append('">');
				sbNext.append(EMPTYLINK);
				sbNext.append('</div>');
				var msgNext = sbNext.toString(); sbNext = null;
				var msg1Left = msgPrev;
				var msg1Right = NBSP;
				var msg2Left = NBSP;
				var msg2Right = NBSP;
				var is2months=false;
				switch(months2show){
					case 1:
						msg1Right = msgNext;
						break;
					case 2:
						msg2Right = msgNext;
						is2months=true;
						break;
					default:
						break;
				}
				var caldays = CS.v3.I18N.findArray(el,patternDay);
				var cal1 = this.getCalendar(el,pattern,patternTitle,patternMonth,caldays,calVal1,currentVal,todayVal,calHolidays1,isShowWeek,startOnDOW,processDays,msg1Left,msg1Right,1,months2show,summaryLabel,captionLabel);
				var showHelpIcon = this.getCustomAttributeCalendarShowHelpIcon(el);
				var helpIcon = showHelpIcon ? this.getHelpIconMsg(el,is2months) : null;
				var sb = new StringBuffer();
				sb.append('<div class="csPanelTopLeft"><div class="csPanelTopRight"><div class="csPanelContent"><div class="csPanelScroll"><div class="csRow csCols-2 csCalendarControls" dir="ltr"><div class="csCol-1">');
/* added for 3.2 */
				if(headerText){
					sb.append('<div class="csCalendarHeaderText"><p>');
					sb.append(headerText);
					sb.append('</p></div>');
				}
				if(showHelpIcon && is2months){
					sb.append('<div class="csNav-x"><ul class="csNavList">');
					sb.append(helpIcon);
					sb.append('</ul></div>');
				}
/*removed for 3.2
				if(processDays>0){
					var valSendLabel = this.getCustomAttributeSendLabel(el);
					var valArriveLabel = this.getCustomAttributeArriveLabel(el);
					sb.append('<div class="csNav-x"><ul class="csNavList"><li class="csNavItem-0 csNavFirst csNavIcon csCalendar-sendLabel"><div class="csNavWrap"><a href="#">');
					sb.append(valSendLabel);
					sb.append('</a></div></li><li class="csNavItem-0 csNavLast csNavIcon csCalendar-arriveLabel"><div class="csNavWrap"><a href="#">');
					sb.append(valArriveLabel);
					sb.append('</a></div></li></ul></div>');
				}
*/
				sb.append('</div><div class="csCol-2"><div class="csNav-x"><ul class="csNavList">');
				if(showHelpIcon && !is2months){
					sb.append(helpIcon);
				}
				sb.append('<li class="csNavItem-0 csNavFirst csNavIcon csIconClose"><div class="csNavWrap"><a href="#" class="csOverlayFirstFocus');
				if(!showHelpIcon){
					sb.append(' csOverlayFirstTab');
				}
				sb.append('" data-csvalue="');
				var tmpInitial = CS.v3.I18N.getValueUnmasked(el,true);
				sb.append(tmpInitial); // TODO initial value
				sb.append('">');
				sb.append(closeLabel);
				sb.append('</a></div></li></ul></div></div></div><div class="csRow csCols-');
				sb.append(months2show);
				sb.append('">');
				sb.append(cal1);
				if(is2months){
					var calVal2 = this.getNextMonth(calVal1);
					var calHolidays2 = this.filterHolidays(el,allHolidays,calVal2);
					var cal2 = this.getCalendar(el,pattern,patternTitle,patternMonth,caldays,calVal2,currentVal,todayVal,calHolidays2,isShowWeek,startOnDOW,processDays,msg2Left,msg2Right,2,months2show,summaryLabel,captionLabel);
					sb.append(cal2);
				}
				sb.append('</div>');
				if(footerText){
					sb.append('<div class="csRow csCalendarFooterText"><p>');
					sb.append(footerText);
					sb.append('</p></div>');
				}
				sb.append('</div></div></div></div><div class="csPanelBottomLeft"><div class="csPanelBottomRight"></div></div>');
				var msg = sb.toString(); sb=null;
				this.updateCalendarDiv(el,spanup,msg,locale);
			}
		}
		//this.log('showCalendar:exit');
	},
	getHelpIconMsg: function(el,is2months){
		var helpLabel = this.getCustomAttributeHelpLabel(el);
		var helpContentNode = this.getCustomAttributeHelpContentNode(el);
		var helpPanelShowHint = this.getCustomAttributeHelpPanelShowHint(el);
		var helpPanelWidth = parseInt(this.getCustomAttributeHelpPanelWidth(el),10);
		var helpPanelPosition = this.getCustomAttributeHelpPanelPosition(el);
		var helpPanelYOffset = -14;
		var helpPanelXOffset = (helpPanelPosition==='left')?(-helpPanelWidth-18):(is2months?428:210);
		var helpIconSb = new StringBuffer();
		helpIconSb.append('<li class="csNavItem-0 csNavFirst csNavIcon csIconHelp"><div class="csNavWrap"><a href="#" class="csOverlayToggle csClick csOverlayFirstTab" data-csnodestotoggle="#panelOverlayHelp" data-csoverlayorigin="target" data-cssetpanelwidth="');
		helpIconSb.append(helpPanelWidth);
		helpIconSb.append('" data-csoverlayoriginYOffset="');
		helpIconSb.append(helpPanelYOffset);
		helpIconSb.append('" data-csoverlayoriginXOffset="');
		helpIconSb.append(helpPanelXOffset);
		helpIconSb.append('" data-csshowhintpanel="');
		helpIconSb.append(helpPanelShowHint);
		if(helpContentNode && helpContentNode!=''){
			helpIconSb.append('" data-cscontentfromnode="');
			helpIconSb.append(helpContentNode);
		}
		helpIconSb.append('">');
		helpIconSb.append(helpLabel);
		helpIconSb.append('</a></div></li>');
		var helpIcon = helpIconSb.toString(); helpIconSb=null;
		return helpIcon;
	},
	getCalendar: function(el,pattern,patternTitle,patternMonth,caldays,calVal,currentVal,todayVal,calHolidays,isShowWeek,startOnDOW,processDays,msgLeft,msgRight,monthIndexThis,monthIndexLast,summaryLabel,captionLabel){
		var isUseTooltip = true; // NOTE this adds some  performance overhead, but is less 'heavy' than the Hints alternative
		var isLastMonth = (monthIndexThis == monthIndexLast);
		//var tz = calVal.getTimezoneOffset();
		var year = calVal.getFullYear();
		var month = calVal.getMonth() + 1;
		var dow = calVal.getDay();
		var cal = parseInt(1-dow,10);
		var NBSP = new String("&nbsp;");
		var SPACE = new String(' ');
		var QUOTE = new String('"');
		// TODO implement ... startOnDOW
		CS.v3.I18N.initialize(el);
		var id = Element.identify(el);
		var calid = new String('cal' + id);
		var month_year = CS.v3.I18N.pattern_obj(el,patternMonth,calVal);
		var isHideWeekend = Element.hasClassName(el,'isNoWeekends');
		var isHideHoliday = Element.hasClassName(el,'isNoHolidays');
		var isHidePast = Element.hasClassName(el,'isNoPast');
		var isHideFuture = Element.hasClassName(el,'isNoFuture');
		var isHideToday = Element.hasClassName(el,'isNoToday');
		var isHideSend = Element.hasClassName(el,'isNoSend');
		var isHideArrive = Element.hasClassName(el,'isNoArrive');
		var sendVal = currentVal;
		var arriveVal = this.getArriveDate(el,sendVal,processDays,calHolidays);
		var cols;
		var colWidth;
		if(isShowWeek){
			cols = 8;
			colWidth = new String('12.5%');
		} else {
			cols = 7;
			colWidth = new String('14%');
		}
		var startdow;//startOnDOW;
		if(cols===8){
			startdow = startonDOW-1;
		} else {
			startdow = 0;
		}
		var colspan = cols-2;
		var isPast;
		var isFuture;
		var isSend;
		var isArrive;
		var isToday;
		var isShow;
		var isCurrent;
		var da;
		var x;
		var bm;
		var weekrow;
		var dt;
		var dd;
		var wk;
		var msg;
		var val;
		var arCssClass;
		var cssClass;
		var arTitle;
		var titles;
		var title;
		var label;
		var calMonth;
		var holidayStr;
		var isWeekend;
		var isHoliday;
		var calTime;
		var todTime;
		var idx;
		var i;
		var i2;
		var CSFIRST = 'csFirst';
		var CSLAST = 'csLast';
		var CSWEEK = 'csWeek';
		var daysInMonth = -1;
		if(isLastMonth){// only do this for the last month shown!
			daysInMonth = CS.v3.I18N.getDaysInMonth(month,year);
		}
		var ddd = CS.v3.I18N.findArray(el,'DDD');
		var sb = new StringBuffer();
		sb.append('<div class="csCol-');
		sb.append(monthIndexThis);
		sb.append('"><table role="grid" class="csTableCalendar" cellspacing="0" dir="ltr" summary="');
		sb.append(summaryLabel);
		sb.append('"><caption>');
		sb.append(captionLabel);
		sb.append('</caption><colgroup>');
		i = cols;
		while(i--){
			sb.append('<col style="width:');
			sb.append(colWidth);
			sb.append(';" />');
		}
		sb.append('</colgroup><thead><tr class="csMonth"><th>');
		sb.append(msgLeft);
		sb.append('</th><th scope="col" colspan="');
		sb.append(colspan);
		sb.append('">');
		sb.append(month_year);
		sb.append('</th><th>');
		sb.append(msgRight);
		sb.append('</th></tr><tr class="csDay">');
		i = cols;
		while(i--){
			arCssClass = [];
			msg = null;
			da = 6-i;
			//arCssClass.push('d'+ d);
			//arCssClass.push('i'+ i);
			if(da===0){ arCssClass.push('csWeekend-sun'); }
			if(da===6){ arCssClass.push('csWeekend-sat'); }
			if(i===0){ arCssClass.push(CSLAST); }
			if(i===cols-1){ arCssClass.push(CSFIRST); }
			if(i===8){
				//msg=NBSP;
				arCssClass.push(CSWEEK);
			} else {
				msg = ddd[da];
			}
			sb.append('<th scope="col" role="columnheader" id="');
			sb.append(calid + '-head' + da);
			sb.append(QUOTE);
			x = arCssClass.length;
			if(x>0){
				sb.append(' class="');
				while(x--){
					cssClass = arCssClass[x];
					sb.append(cssClass);
					sb.append(SPACE);
				}
				sb.append(QUOTE);
			}
			sb.append('>');
			bm = CS.v3.I18N.isEmpty(msg);
			if(bm){
				sb.append(NBSP);
			} else {
				sb.append(msg);
			}
			sb.append('</th>');
		}
		sb.append('</tr></thead><tbody>');
		var start = 0;
		if(cols===8){
			start = -1;
		}
		for(idx=0; idx<36; idx=idx+7){
			sb.append('<tr>');
			i = cols;
			while(i--){
				//da = cal + idx + i;
				da = cal + idx + (6-i);
				arCssClass = [];
				arTitle = [];
				calVal = CS.v3.I18N.setDateObj(year,month,da);
				if(i===cols-1){ arCssClass.push(CSFIRST); }
				if(i===0){ arCssClass.push(CSLAST); }
				if(i===cols){
					arCssClass.push(CSWEEK);
					wk = CS.v3.I18N.getWeek(el,calVal);
					sb.append('<td role="gridcell" headers="');
					sb.append(calid + '-head' + monthIndexThis + '-' + da);
					sb.append(QUOTE);
					x = arCssClass.length;
					if(x>0){
						sb.append(' class="');
						while(x--){
							cssClass = arCssClass[x];
							sb.append(cssClass);
							sb.append(SPACE);
						}
					}
					sb.append('">');
					sb.append(wk);
				} else {
					calMonth = calVal.getMonth()+1; // to see if we've rolled months!
					//isCurrent = false;
					//isPast = false;
					//isFuture = false;
					//isSend = false;
					//isArrive = false;
					//isToday = false;
					isShow = false;
					if(month == calMonth){
						isShow = true;
						if(processDays>0){
							isSend = CS.v3.I18N.equalDates(calVal,sendVal);
							if(isSend){
								arCssClass.push('csDate-send');
								label = this.getCustomAttributeSendLabel(el);
								arTitle.push(label);
								if(isHideSend){ isShow = false; }
							}
							isArrive = CS.v3.I18N.equalDates(calVal,arriveVal);
							if(isArrive){
								arCssClass.push('csDate-arrive');
								label = this.getCustomAttributeArriveLabel(el);
								arTitle.push(label);
								if(isHideArrive){ isShow = false; }
							}
						}
						isCurrent = CS.v3.I18N.equalDates(calVal,currentVal);
						if(isCurrent){
							arCssClass.push('csDate-current');
							label = this.getCustomAttributeCurrentLabel(el);
							arTitle.push(label);
						}
						isToday = CS.v3.I18N.equalDates(calVal,todayVal);
						if(isToday){
							arCssClass.push('csDate-today');
							label = this.getCustomAttributeTodayLabel(el);
							arTitle.push(label);
							if(isHideToday){ isShow = false; }
						} else {
							calTime = CS.v3.I18N.getTime(calVal);
							todTime = CS.v3.I18N.getTime(todayVal);
							isPast = calTime<todTime;
							if(isPast){
								arCssClass.push('csDate-past');
								if(isHidePast){ isShow = false; }
							} else {
								isFuture = calTime>todTime;
								if(isFuture){
									arCssClass.push('csDate-future');
									if(isHideFuture){ isShow = false; }
								}
							}
						}
						isWeekend = this.isWeekend(calVal);
						if(isWeekend){
							arCssClass.push('csDate-weekend');
							if(isHideWeekend){ isShow = false; }
						}
						holidayStr = this.getHoliday(el,calHolidays,calVal);
						isHoliday = !CS.v3.I18N.isEmpty(holidayStr);
						if(isHoliday){
							arCssClass.push('csDate-holiday');
							arTitle.push(holidayStr);
							if(isHideHoliday){ isShow = false; }
						}
					}
					if(isShow){
						if(isUseTooltip){
							title = CS.v3.I18N.pattern_obj(el,patternTitle,calVal);
							arTitle.push(title);
						}
					} else {
						arCssClass.push('csDate-noDate');
					}
				}
				sb.append('<td role="gridcell" headers="');
				sb.append(calid + '-head' + monthIndexThis + '-' +(cols-1-i));
				sb.append(QUOTE);
				i2 = arCssClass.length;
				if(i2>0){
					sb.append(' class="');
					while(i2--){
						cssClass = arCssClass[i2];
						sb.append(cssClass);
						sb.append(SPACE);
					}
					sb.append(QUOTE);
				}
				titles = arTitle.length;
				if(titles>0){
					sb.append(' title="');
					while(titles--){
						title = arTitle[titles];
						sb.append(title);
						sb.append(SPACE);
					}
					sb.append(QUOTE);
				}
				sb.append('>');
				//CS.v3.I18N.initialize(el);
				if(isShow){
					dd = calVal.getDate() -1;
					msg = caldays[dd];//CS.v3.I18N.pattern_cal(el,patternDay,calVal);
					val = CS.v3.I18N.pattern_cal(el,pattern,calVal);
					sb.append('<a href="#" data-csvalue="');
					sb.append(val);
					if(dd+1 == daysInMonth){
						sb.append('" class="csOverlayLastTab');
					}
					sb.append('">');
					sb.append(msg);
					sb.append('</a>');
				} else {
					sb.append(NBSP);
				}
				sb.append('</td>');
			}
			sb.append('</tr>');
		}
		sb.append('</tbody></table></div>');
		var rc = sb.toString(); sb=null;
		return rc;
	},
	findHolidays: function(el,keys){
		//this.log('findHolidays:init');
		var rc;
		var ar = this.deriveCountries(keys);
		if(ar){
			var i = ar.length;
			var country;
			var b;
			var str;
			var key;
			var ar2;
			while(rc==null && i--){
				country = ar[i];
				b = CS.v3.I18N.isEmpty(country);
				if(!b){
					key = this.makeHolidayKey(country);
					ar2 = CS.v3.I18N.findProperty(el,key);
					rc = CS.v3.I18N.mergeArrays(rc,ar2);
				}
			}
		} else {
			rc = null;
		}
		//this.log('findHolidays:exit');
		return rc;
	},
	makeHolidayKey: function(country){
		var sb = new StringBuffer();
		sb.append('CalendarHolidays');
		var b = CS.v3.I18N.isEmpty(country);
		if(!b){
			sb.append('-');
			sb.append(country);
		}
		var rc = sb.toString(); sb=null;
		return rc;
	},
	deriveCountries: function(keys){
		var rc;
		var bc = CS.v3.I18N.isEmpty(keys);
		if(!bc){
			var COMMA = new String(',');
			var SPACE = new String(' ');
			var clean = CS.v3.I18N.replaceString(keys,COMMA,SPACE);
			var ar = CS.v3.I18N.split(clean,SPACE);
			var i = ar.length;
			if(i>0){
				rc = [];
				var country;
				var b;
				while(i--){
					country = ar[i];
					b = CS.v3.I18N.isEmpty(country);
					if(!b){
						rc.push(country);
					}
				}
			}
		} else {
			rc = null;
		}
		return rc;
	},
	deriveCountriesFromLocale: function(locale){
		var rc;
		var bl = CS.v3.I18N.isEmpty(locale);
		var country;
		if(!bl){
			var DASH = new String('-');
			var SCORE = new String('_');
			var clean = CS.v3.I18N.replaceString(locale,DASH,SCORE);
			var ar = CS.v3.I18N.split(clean,SCORE);
			var language = ar[0];
			country = ar[1];
		}
		var def = CS.v3.I18NDefaultLocale;//'default';
		var bc = CS.v3.I18N.isEmpty(country);
		if(!bc){
			rc = [def,country];
		} else {
			rc = [def];
		}
		return rc;
	},
	getHoliday: function(el, ar, dt_cal){
		var rc;
		if(ar!=null){
			//CS.v3.I18N.initialize(el);
			var EQUALS = new String('=');
			var sb = new StringBuffer();
			var pagingPattern = this.getPagingPattern(el);
			var obj;
			var item;
			var dateStr;
			var title;
			var dt_hol;
			var b;
			var i = ar.length;
			while(i--){
				obj = ar[i];
				item = CS.v3.I18N.split(obj,EQUALS);
				dateStr = item[0];
				title = item[1];
				dt_hol = CS.v3.I18N.parseDateFieldVal(el,dateStr,pagingPattern,dt_cal);
				b = CS.v3.I18N.equalDates(dt_hol,dt_cal);
				if(b){
					sb.append(title);
				}
			}
			rc = sb.toString(); sb=null;
		} else {
			rc = '';
		}
		return rc;
	},
	getArriveDate: function(el,dt,processDays,calHolidays){
		var rc;
		if(processDays>0){
			var step = 1;// NOTE: use -1 for calculating send from arrive
			rc = this.getIncrDays(dt,processDays);
			var holidayStr = this.getHoliday(el,calHolidays,rc);
			var isHoliday = !CS.v3.I18N.isEmpty(holidayStr);
			var isWeekend = this.isWeekend(rc);
			while(isHoliday || isWeekend){
				rc = this.getIncrDays(rc,step);
				holidayStr = this.getHoliday(el,calHolidays,rc);
				isHoliday = !CS.v3.I18N.isEmpty(holidayStr);
				isWeekend = this.isWeekend(rc);
			}
		} else {
			rc = null;
		}
		return rc;
	},
	isWeekend: function(dt){
		var rc = false;
		if(dt){
			var dow = dt.getDay();
			rc = (dow === 0 || dow === 6);
		}
		return rc;
	},
	getIncrDays: function(dt,val){
		var dayOld = dt.getDate();
		var dayNew = parseInt(dayOld,10) + parseInt(val,10);
		var rc = new Date(dt.getFullYear(),dt.getMonth(),dayNew);
		return rc;
	},
	getNextMonth: function(dt){
		var rc = new Date(dt.getFullYear(),dt.getMonth()+1,dt.getDate());
		return rc;
	},
	getPrevMonth: function(dt){
		var rc = new Date(dt.getFullYear(),dt.getMonth()-1,dt.getDate());
		return rc;
	},
	getNextYear: function(dt){
		var rc = new Date(dt.getFullYear()+1,dt.getMonth(),dt.getDate());
		return rc;
	},
	getPrevYear: function(dt){
		var rc = new Date(dt.getFullYear()-1,dt.getMonth(),dt.getDate());
		return rc;
	},
	getPagingPattern: function(el){
		return new String('YYYYMMDD');
	},
	getHolidayPattern: function(el){
		return new String('YYYYMM');
	},
	getDayPattern: function(el){
		return new String('DD');
	},
	getCalendarShowFlag: function(el){
		return new String('csCalendar-show');
	},
	getCalendarIconFlag: function(el){
		return new String('csCalendar-iconoutside');
	},
	getCalendarOneMonthFlag: function(el){
		return new String('csOnemonth');
	},
	getCalendarInputId: function(el){
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cscalendarinputid','');
		return rc;
	},
	setCalendarInputId: function(el,val){
		CS.v3.I18N.setDataAttribute(el,'data-cscalendarinputid',val);
	},
	getInputPattern: function(el){
		var def = CS.v3.I18N.findProperty(el,'Calendar-inputPattern');
		var rc = CS.v3.I18N.getCustomAttributePattern(el,def);
		return rc;
	},
	getCustomDataAttributeCountry: function(el,def){
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cscountry',def);
		return rc;
	},
	getCustomDataAttributeValue: function(el){
		var def = new String('');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-csvalue',def);
		return rc;
	},
	getDisplayPattern: function(el){
		var def = CS.v3.I18N.findProperty(el,'Calendar-displayPattern');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-csdisplaypattern',def);
		return rc;
	},
	getHeaderPattern: function(el){
		var def = CS.v3.I18N.findProperty(el,'Calendar-headerPattern');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-csheaderpattern',def);
		return rc;
	},
	getCustomAttributeProcessDays: function(el){
		var def = CS.v3.I18N.findProperty(el,'Calendar-processDays'); //0
		var rc = CS.v3.I18N.getDataAttribute(el,'data-csprocessdays',def);
		return rc;
	},
	getCustomAttributeStartOnDOW: function(el){
		var def = CS.v3.I18N.findProperty(el,'Calendar-startDow'); // '0' = sunday in JS!
		var rc = CS.v3.I18N.getDataAttribute(el,'data-csstartdow',def);
		return rc;
	},
	getCustomAttributeCloseLabel: function(el){
		var def = CS.v3.I18N.findProperty(el,'Calendar-closeLabel');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cscloselabel',def);
		return rc;
	},
	getCustomAttributeHelpLabel: function(el){
		var def = CS.v3.I18N.findProperty(el,'Calendar-helpLabel');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cshelplabel',def);
		return rc;
	},
	getCustomAttributeHelpContentNode: function(el){
		var def = CS.v3.I18N.findProperty(el,'Calendar-helpContentNode');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cscontentfromnode',def);
		return rc;
	},
	getCustomAttributeHelpPanelShowHint: function(el){
		var def = CS.v3.I18N.findProperty(el,'Calendar-helpPanelShowHint');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-csshowhintpanel',def);
		return rc;
	},
	getCustomAttributeHelpPanelPosition: function(el){
		var def = CS.v3.I18N.findProperty(el,'Calendar-helpPanelPosition');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cshelppanelposition',def);
		return rc;
	},
	getCustomAttributeHelpPanelWidth: function(el){
		var def = CS.v3.I18N.findProperty(el,'Calendar-helpPanelWidth');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cssetpanelwidth',def);
		return rc;
	},
	getCustomAttributeSummaryLabel: function(el){
		var def = CS.v3.I18N.findProperty(el,'Calendar-summaryLabel');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cssummarylabel',def);
		return rc;
	},
	getCustomAttributeCaptionLabel: function(el){
		var def = CS.v3.I18N.findProperty(el,'Calendar-captionLabel');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cscaptionlabel',def);
		return rc;
	},
	getCustomAttributeSendLabel: function(el){
		var def = CS.v3.I18N.findProperty(el,'Calendar-sendLabel');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cssendlabel',def);
		return rc;
	},
	getCustomAttributeArriveLabel: function(el){
		var def = CS.v3.I18N.findProperty(el,'Calendar-arriveLabel');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-csarrivelabel',def);
		return rc;
	},
	getCustomAttributeTodayLabel: function(el){
		var def = CS.v3.I18N.findProperty(el,'Calendar-todayLabel');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cstodaylabel',def);
		return rc;
	},
	getCustomAttributeCurrentLabel: function(el){
		var def = CS.v3.I18N.findProperty(el,'Calendar-currentLabel');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cscurrentlabel',def);
		return rc;
	},
	getCustomAttributeHeaderText: function(el){
		var def = CS.v3.I18N.findProperty(el,'Calendar-headerText');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-csheadertext',def);
		return rc;
	},
	getCustomAttributeFooterText: function(el){
		var def = CS.v3.I18N.findProperty(el,'Calendar-footerText');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-csfootertext',def);
		return rc;
	},
	filterHolidays: function(el,ar,calVal){
		//this.log('filterHolidays:init:calVal=' + calVal);
		var rc = [];
		if(calVal!=null){
			var patternHolidayFilter = this.getHolidayPattern(el);
			var startYYYYMM = CS.v3.I18N.pattern_obj(el,patternHolidayFilter,calVal);
			var b = Object.isArray(ar);
			if(b){
				var YEARMASK = new String('####');
				var obj;
				var i = ar.length;
				var year = calVal.getFullYear();//I!8N?
				var dateStr;
				var dateStartsWith;
				while(i--){
					obj = ar[i];
					dateStr = obj.replace(YEARMASK,year);
					dateStartsWith = CS.v3.I18N.truncateString(dateStr,6);
					if(dateStartsWith == startYYYYMM){
						rc.push(dateStr);
					}
				}
			}
		}
		//this.log('filterHolidays:exit');
		return rc;
	},
	hideOtherCalendars: function(el){
	},
	insertHint: function(el,str){
		var sb = new StringBuffer();
		sb.append('<div class="csIcon csIconHint">');
		sb.append('<a href="#" class="csIcon csHintToggle csOver" data-csoverlaycontextnode="div.csLabel">');
		sb.append('<span class="csIconAlt">?</span></a><div class="csPanel csPanelHint"><div class="csPanelTopLeft">');
		sb.append('<div class="csPanelTopRight"><div class="csPanelContent"><div class="csPanelScroll"><div class="csHintMsg"><p>');
		sb.append(str);
		sb.append('</p></div></div></div></div></div><div class="csPanelBottomLeft">');
		sb.append('<div class="csPanelBottomRight"></div></div><div class="csChevron csChevronTop"></div></div></div>');
		var rc = sb.toString(); sb=null;
		return sb;
	},
	findCalendarWrapper: function(el){
		var rc;
		if(el){
			rc = Element.up(el,'DIV.csCalendar-wrapper');
		}
		return rc;
	},
	findCalendarInput: function(el){
		var outerDiv = this.findSingleCalendarContainer(el);
		var id = this.getCalendarInputId(outerDiv);
		var rc = CS.v3.castToEl(id);
		return rc;
	},
	findCalendarInputIconDiv: function(el){
		var rc = Element.down(el,'DIV.csIconCalendar');
		return rc;
	},
	updateCalendarDiv: function(input,el,msg,locale){
		if(el){
			var innerDiv = CS.v3.csPanelCalendar;//$('csPanelCalendar');// find the child of the body
			if(innerDiv){
				var months2show = this.getInputMonths2Show(input);
				var CSONEMONTH = this.getCalendarOneMonthFlag(input);
				if(months2show===1){
					Element.addClassName(innerDiv,CSONEMONTH);
				} else {
					Element.removeClassName(innerDiv,CSONEMONTH);
				}
				var i = Element.identify(input);
				var outerDiv = this.findSingleCalendarContainer(el);
				this.showEl(input);
				CS.v3.I18N.setLocale(innerDiv,locale);
				CS.v3.I18N.setLocale(outerDiv,locale);
				CS.v3.updateFix(innerDiv,msg);
				//this.focusFirstCalendarOverlayField(outerDiv);
			}
		}
	},
	focusFirstCalendarOverlayField: function(overlay){
		//var overlay = this.findSingleCalendarContainer(el);
		var ar = Element.select(overlay,'.csOverlayFirstFocus');//csOverlayFirstTab
		if(ar){
			var focusFld = ar[0];
			var b = CS.v3.I18N.isEmpty(focusFld);
			if(!b){
				//var id = Element.identify(focusFld);
				//CS.v3.calendarActive = focusFld;
				CS.v3.focus(focusFld);
			}
		}
	},
	showEl: function(el){
		//this.log('showEl:el=' + el);
		var event=null;
		var obj = this.findCalendarWrapper(el);
		var input = this.findCalendarInput(el);
		el.wrap = obj;
		el.target = input;
		CS.v3.CalendarVis.show(el,event);
	},
	hideEl: function(el){
		var event=null;
		var obj = this.findCalendarWrapper(el);
		el.wrap = obj;
		CS.v3.CalendarVis.hide(el);
	},
	setCustomAttributeCalendarInputRangeMin: function(el,val){
		CS.v3.I18N.setDataAttribute(el,'data-cscalendarrangemin',val);
	},
	getCustomAttributeCalendarInputRangeMin: function(el){
		var def = CS.v3.I18N.findProperty(el,'Calendar-rangeMin');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cscalendarrangemin',def);
		return rc;
	},
	setCustomAttributeCalendarInputRangeMax: function(el,val){
		CS.v3.I18N.setDataAttribute(el,'data-cscalendarrangemax',val);
	},
	getCustomAttributeCalendarInputRangeMax: function(el){
		var def = CS.v3.I18N.findProperty(el,'Calendar-rangeMax');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cscalendarrangemax',def);
		return rc;
	},
	getCustomAttributeCalendarShowHelpIcon: function(el){
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cscalendarshowhelpicon',false);
		return rc;
	},
	setCustomAttributeCalendarShowHelpIcon: function(el,val){
		CS.v3.I18N.setDataAttribute(el,'data-cscalendarshowhelpicon',val);
	},
	getSupportedCalendarFormats: function(el){
		var rc = CS.v3.I18N.findProperty(el,'Calendar-supportedFormats');
		return rc;
	},
	isSupportedCalendarFormat: function(el,pattern){
		var rc;
		var ar = this.getSupportedCalendarFormats(el);
		if(ar){
			rc = CS.v3.I18N.isFoundInArray(ar,pattern);
		} else {
			rc = false;
		}
		return rc;
	},
	log: function(s,e){
		CS.v3.Logger.log("Calendar:" + s,e);
	}
};
CS.v3.CalendarVis = {
	show: function(m,event){
		try{
			var obj = new CS.v3.OverlaysObj(m,event);
			//Element.update(CS.v3.csPanelCalendar,obj.content);
			var width = m.wrap.getWidth();
			var inputdimen = Element.getDimensions(m);
			var inputwidth = inputdimen.width + 1;
			var CLSRIGHT = 'csCalendarRight'; // TODO try String
			var CLSRIGHTONE = 'csCalendarRightOne'; // TODO try String
			var CLSRIGHTTWO = 'csCalendarRightTwo'; // TODO try String
			var leftadjust;
			if(obj.align == CLSRIGHT){
				var months2show = CS.v3.Calendar.getInputMonths2Show(m);
				if(months2show==1){
					leftadjust = obj.left + (width * 2);
					Element.addClassName(CS.v3.ContainerCalendar,CLSRIGHTONE);
					Element.removeClassName(CS.v3.ContainerCalendar,CLSRIGHTTWO);
					//var CSONEMONTH = CS.v3.Calendar.getCalendarOneMonthFlag(m);
					//Element.addClassName(CS.v3.ContainerCalendar,CSONEMONTH);
				} else {
					leftadjust = obj.left + (width * 4);
					Element.addClassName(CS.v3.ContainerCalendar,CLSRIGHTTWO);
					Element.removeClassName(CS.v3.ContainerCalendar,CLSRIGHTONE);
				}
			} else {
				leftadjust = obj.left + inputwidth;
				Element.removeClassName(CS.v3.ContainerCalendar,CLSRIGHTONE);
				Element.removeClassName(CS.v3.ContainerCalendar,CLSRIGHTTWO);
			}
			CS.v3.ContainerCalendar.style.left = leftadjust + 'px';//- obj.containerWrapLeft + 'px';
			CS.v3.ContainerCalendar.style.top = obj.top + 'px';//+ obj.height + 'px';
			//CS.v3.ContainerCalendar.style.width = width + 'px';// new
			CS.v3.calendarActive = m.target;
			CS.v3.ContainerCalendar.style.display = 'block';
			//if(CS.v3.Base.prototype.coverIE){
				//CS.v3.OverlayIE6IFrame.show(CS.v3.ContainerCalendar,obj)
			//}
		}
		catch(e){
			this.log('show:',e);
		}
	},
	hide: function(el){
		CS.v3.ContainerCalendar.style.display = 'none';
	},
	log: function(s,e){
		CS.v3.Logger.log("CalendarVis:" + s,e);
	}
};
/* Clock */
CS.v3.Components.Clock = function(el,options){ new CS.v3.Config.Clock(el,options); };
CS.v3.Config.Clock = Class.create(CS.v3.Base.prototype, {
	initialize: function(el,options){
		this.allClocks = Element.select(el,"span[class*='csClock']");
		if(options.bind==='true'){this.bind();}
	},
	bind: function(){
		var ar = this.allClocks;
		var i = ar.length;
		var el;
		while(i--){
			el = ar[i];
			CS.v3.Clock.setupClock(el);
		}
	}
});
CS.v3.Clock = {
	setupClock: function(el){
		var FLAG = new String('csClock-init');
		var b = Element.hasClassName(el,FLAG);
		if(!b){
			Element.addClassName(el,FLAG);
			Element.identify(el);
			CS.v3.I18N.initialize(el);
			this.startClock(el);
		}
	},
	startClock: function(el){
		/* el = $(el); */
		var isUTC = true;// add condition
		var offset = CS.v3.I18N.getCustomAttributeOffset(el,'','+HHMM');
		var pattern = this.getClockPattern(el);
		var seconds = CS.v3.I18N.calculateTickInterval(pattern);
		var clocktick = function(){
			var b = Element.hasClassName(el,'csHide');// only update when not hidden
			if(!b){
				var dt = CS.v3.I18N.offsetDate(offset,isUTC);
				var msg = CS.v3.I18N.pattern_obj(el,pattern,dt);// + "&nbsp;";
				var sortable_pattern = CS.v3.Clock.getSortablePattern(el);
				var sortable_value = CS.v3.I18N.pattern_obj(el,sortable_pattern,dt);//NOTE: was 'null' vs. 'el'
				CS.v3.I18N.setValue(el,msg);
				CS.v3.I18N.setCustomAttributeDateTime(el,sortable_value);
			}
			if(seconds>0){
				var interval = 1000 * seconds;
				window.setTimeout(clocktick,interval);
			}
		};
		clocktick();
	},
	getSortablePattern: function(el){
		var rc = CS.v3.I18N.findProperty(el,'clock-sortable-pattern');
		return rc;
	},
	getClockPattern: function(el){
		var def = CS.v3.I18N.findProperty(el,'clock-pattern');
		var rc = CS.v3.I18N.getCustomAttributePattern(el,def);
		return rc;
	},
	log: function(s,e){
		CS.v3.Logger.log("Clock:" + s,e);
	}
};
/* Timer */
CS.v3.Components.Timer = function(el,options){ new CS.v3.Config.Timer(el,options); };
CS.v3.Config.Timer = Class.create(CS.v3.Base.prototype, {
	initialize: function(el,options){
		this.allTimers = CS.v3.Timer.findAllTimers(el);
		if(options.bind==='true'){this.bind();}
	},
	bind: function(){
		var ar = this.allTimers;
		var i = ar.length;
		var el;
		while(i--){
			el = ar[i];
			CS.v3.Timer.setupTimer(el);
		}
	}
});
CS.v3.Timer = {
	findAllTimers: function(el){
		var rc = Element.select(el,"[class*='csCountdownTimer']");//was: "input[class*='csCountdownTimer']"
		return rc;
	},
	getFlag: function(){
		var rc = new String('csCountdownTimer-init');
		return rc;
	},
	setupTimer: function(el){
		var FLAG = this.getFlag();
		var b = Element.hasClassName(el,FLAG);
		if(!b){
			Element.addClassName(el,FLAG);
			var tag = el.tagName;
			if((tag==='INPUT') || (tag==='TEXTAREA')){
				CS.v3.I18N.updateAttribute(el,'readonly','readonly');
				CS.v3.I18N.updateAttribute(el,'disabled','disabled');
			}
			Element.identify(el);
			//var action = this.getCustomAttributeTimerAction(el);
			//this.saveValues(el,action);
			CS.v3.I18N.initialize(el);
			var duration = this.getTimerDuration(el);
			var delim = this.getCustomAttributeTimerDelimiter(el);
			var day = 0;
			var hrs = 0;
			var min = 1 * this.timeoutMinutes(duration,delim);
			var sec = 0 + this.timeoutSeconds(duration,delim);
			var mms = CS.v3.I18N.createTime();
			var expiry = CS.v3.I18N.calcOffset('+',day,hrs,min,sec,mms);
			this.setCustomAttributeTimerExpiry(el,expiry);
			var sessiontimeout = CS.v3.Timer.getCustomAttributeTimerSessionExpire(el);
			if(sessiontimeout=='true'){
				this.attachSessionButtons(el);
			}
			this.startTimer(el);
		}
	},
	startTimer: function(el){
		/* el = $(el); */
		var pattern = this.getTimerPattern(el);
		var seconds = CS.v3.I18N.calculateTickInterval(pattern);
		var timertick = function(){
			var tm = CS.v3.I18N.createTime();
			var expiry = CS.v3.Timer.getCustomAttributeTimerExpiry(el);
			var diff = CS.v3.I18N.timeDiff(expiry,tm);
			if(diff<0){
				diff=0;
			}
			var b = Element.hasClassName(el,'csHide');// to save performance, do not update in this case
			if(!b){
				var msg = CS.v3.I18N.timeDiffPattern(el,diff,pattern);
				CS.v3.I18N.setValue(el,msg);
			}
			if(diff===0){
				//var sessiontimeout = CS.v3.Timer.getCustomAttributeTimerSessionExpire(el);
				//if(sessiontimeout=='true'){
					CS.v3.Timer.timeoutExpire(el);
				//} else {
					var sessionpopup = CS.v3.Timer.getCustomAttributeTimerSessionPopup(el);
					if(sessionpopup=='true'){
						CS.v3.Timer.timeoutShowPopup(el);
					} else {
						var action = CS.v3.Timer.getCustomAttributeTimerAction(el);
						//var id = Element.identify(el);
						if(action instanceof Function){
							action(el);
						} else {
							var obj = CS.v3.Timer.loadValues(el,action);
							if(obj instanceof Function){
								obj(el);
							}
						}
					}
				//}
			} else {
				if(seconds>0){
					var interval = 1000 * seconds;
					window.setTimeout(timertick,interval);
				}
			}
		};
		timertick();
	},
	findSessionOverlay: function(){
		var id = new String('sessionOverlay');
		var rc = CS.v3.castToEl(id);
		return rc;
	},
	attachSessionButtons: function(el){
		var timerid;
		var popupid;
		var elSessionOverlay = this.findSessionOverlay();
		if(elSessionOverlay){
			var FLAG = this.getFlag();
			var b = Element.hasClassName(elSessionOverlay,FLAG);
			if(!b){
				Event.observe(elSessionOverlay,'click', function(event){ this.sessionOverlayEv(event); }.bind(this));
				Event.observe(elSessionOverlay,'keydown', function(event){ this.sessionOverlayEv(event); }.bind(this));
				Element.addClassName(elSessionOverlay,FLAG);
			}
			var arTimers = this.findAllTimers(elSessionOverlay);
			if(arTimers){
				var str;
				var obj;
				var i = arTimers.length;
				while(i--){
					obj = arTimers[i];
					str = this.getCustomAttributeTimerSessionPopup(obj);
					if(str=='true'){
						popupid = Element.identify(obj);
					}
					str = this.getCustomAttributeTimerSessionExpire(obj);
					if(str=='true'){
						timerid = Element.identify(obj);
					}
				}
			}
			this.setCustomAttributeTimerSessionTimerId(elSessionOverlay,timerid);
			this.setCustomAttributeTimerPopupTimerId(elSessionOverlay,popupid);
		}
	},
	timeoutShowPopup: function(el){
		var id = Element.identify(el);
		Element.addClassName(el,'csOverlayToggle');
		Element.addClassName(el,'csClick');
		Element.addClassName(el,'csFormChange-except');// prevent FormChange from intercepting this!
		CS.v3.I18N.updateAttribute(el,'data-csnodestotoggle','#sessionOverlay');
		CS.v3.I18N.updateAttribute(el,'data-cscontentfromnode','');//'.csMyInsertableContent'
		CS.v3.I18N.updateAttribute(el,'data-csoverlayorigin','window');
		CS.v3.I18N.updateAttribute(el,'data-cslightbox','true');
		CS.v3.I18N.updateAttribute(el,'data-cssetpanelwidth','530');
		window.focus();// bring focus to this browser window if 'submarined'
		CS.v3.fireOnclick(el);
	},
	sessionButtonClickExtendEv: function(event){
		var el = $(Event.element(event));
		this.resetTimersInternal(true);
	},
	resetTimersInternal: function(isNotifyServer){
		var elSessionOverlay = this.findSessionOverlay();
		if(elSessionOverlay){
			var timerEl = this.getTimerElFromEl(elSessionOverlay);
			var popupEl = this.getPopupElFromEl(elSessionOverlay);
			this.resetTimerLocal(popupEl);
			this.resetTimerLocal(timerEl);
			if(isNotifyServer){
				this.resetSessionTimerServer(timerEl);
			}
		}
	},
	resetSessionTimerAPI: function(el,isNotifyServer){
		// TODO = this is a means for services to reset the clientside session timeout on their own, it should 'find' the button used to reset the session and call appropriate methods
		this.resetTimersInternal(isNotifyServer);
	},
	resetSessionTimerLocalAPI: function(el){
		// TODO = this is a means for services to reset the clientside session timeout on their own, it should 'find' the button used to reset the session and call appropriate methods
		this.resetTimerLocal(el);
	},
	resetSessionTimerServer: function(el){
		if(el){
			var url = this.getCustomAttributeTimerSessionExtendUrl(el);
			var locale = CS.v3.I18N.initialize(el);
			var i18nurl = CS.v3.UrlFunctions.urlAppenderLocale(url,locale);
			var myurl = CS.v3.UrlFunctions.urlAppenderCacheBuster(i18nurl);
			var httpHeaders = ['Accept-Language',locale];
			new Ajax.Request(myurl, {
				method: 'get',
				requestHeaders: httpHeaders,
				onSuccess: function(transport){
					CS.v3.Timer.log('Session Extension - Ajax Complete');
				},
				onFailure: function(){
					CS.v3.Timer.log('Session Extension - Ajax Failed.');
				}
			});
		}
	},
	resetTimerLocal: function(el){
		if(el){
			var FLAG = this.getFlag();
			var b = Element.hasClassName(el,FLAG);
			if(b){
				Element.removeClassName(el,FLAG);
			}
			this.setupTimer(el);
		}
	},
	getTimerElFromEl: function(el){
		var timerId = CS.v3.Timer.getCustomAttributeTimerSessionTimerId(el);
		var rc = CS.v3.castToEl(timerId);
		return rc;
	},
	getPopupElFromEl: function(el){
		var popupId = CS.v3.Timer.getCustomAttributeTimerPopupTimerId(el);
		var rc = CS.v3.castToEl(popupId);
		return rc;
	},
	sessionOverlayEv: function(event){
		var el = $(Event.element(event));
		var typ = event.type;
		switch(typ){
			case 'click':
				this.sessionOverlayClickEv(event);
				break;
			case 'keydown':
				var key = event.keyCode;//
				if(key === Event.KEY_RETURN){
					this.sessionOverlayClickEv(event);
				}
				break;
			default:
				// NOOP
				break;
		}
	},
	sessionOverlayClickEv: function(event){
		var el = $(Event.element(event));
		var elSessionOverlay = this.findSessionOverlay();
		if(elSessionOverlay){
			var timerid = this.getCustomAttributeTimerSessionTimerId(elSessionOverlay);
			var popupid = this.getCustomAttributeTimerPopupTimerId(elSessionOverlay);
			var isExtend = Element.hasClassName(el,'csSessionExtend');
			var isEnd = Element.hasClassName(el,'csSessionEnd');
			if(isExtend){
				this.resetTimersInternal(true);
			} else {
				if(isEnd){
					this.timeoutSessionLogoff(el);
				}
			}
		}
	},
	sessionButtonClickEndEv: function(event){
		var btnEl = $(Event.element(event));
		var timerEl = this.getTimerElFromEl(btnEl);
		if(timerEl){
			this.timeoutSessionLogoff(timerEl);
		}
	},
	timeoutSessionLogoff: function(el){
		var elSessionOverlay = this.findSessionOverlay();
		if(elSessionOverlay){
			var timerEl = this.getTimerElFromEl(elSessionOverlay);
			var url = this.getCustomAttributeTimerSessionLogoffUrl(timerEl);
			this.doForward(url);
		}
	},
	timeoutExpire: function(el){
		var url = this.getCustomAttributeTimerExpireUrl(el);
		this.doForward(url);
	},
	addValues: function(nam,locale,val){
		// NOTE: move to CS.v3.PersistState?
		var key = new String('TimerAction_' + nam);
		CS.v3.I18N.addProperty(key,locale,val);
	},
	doForward: function(url){
		//CS.v3.Security.doForward(url);
		/* NOTE: 'true' is used to prevent back  button to the previous page */
		CS.v3.UrlFunctions.forward(url,true);
	},
	saveValues:function(el,val){
		// NOTE: move to CS.v3.PersistState?
		var nam = Element.identify(el);
		var locale = CS.v3.I18N.getLocale(el);
		this.addValues(nam,locale,val);
	},
	loadValues:function(el,nam){
		// NOTE: move to CS.v3.PersistState?
		var rc;
		if(nam){
			var key = new String('TimerAction_' + nam);
			rc = CS.v3.I18N.findProperty(el,key);
		}
		return rc;
	},
	timeoutMinutes:function(data,delim){
		var i = data.length;
		while(i--){
			if(data.substring(i,i+1) == delim){
				break;
			}
		}
		var rc = (data.substring(0,i));
		return rc;
	},
	timeoutSeconds:function(data,delim){
		var len = data.length;
		var i = len;
		while(i--){
			if(data.substring(i,i+1) == delim){
				break;
			}
		}
		var rc = (data.substring(i+1,len));
		return rc;
	},
	getTimerPattern: function(el){
		var def = CS.v3.I18N.findProperty(el,'timer-pattern');
		var rc = CS.v3.I18N.getCustomAttributePattern(el,def);
		return rc;
	},
	getTimerDuration: function(el){
		var def = CS.v3.I18N.findProperty(el,'timer-duration');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cstimerduration',def);
		return rc;
	},
	getCustomAttributeTimerSessionPopup: function(el){
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cstimersessionpopup','');
		return rc;
	},
	getCustomAttributeTimerSessionExpire: function(el){
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cstimersessionexpire','');
		return rc;
	},
	getCustomAttributeTimerExpireUrl: function(el){
		var def = CS.v3.I18N.findProperty(el,'timer-expireurl');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cstimerexpireurl','');
		return rc;
	},
	getCustomAttributeTimerSessionLogoffUrl: function(el){
		var def = CS.v3.I18N.findProperty(el,'timer-sessionlogoffurl');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cstimersessionlogoffurl','');
		return rc;
	},
	getCustomAttributeTimerSessionExtendUrl: function(el){
		var def = CS.v3.I18N.findProperty(el,'timer-sessionextendurl');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cstimersessionextendurl','');
		return rc;
	},
	getCustomAttributeTimerAction: function(el){
		var def = CS.v3.I18N.findProperty(el,'timer-action');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cstimeraction',def);
		return rc;
	},
	getCustomAttributeTimerHideId: function(el){
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cstimerhideid','');
		return rc;
	},
	getCustomAttributeTimerSessionTimerId: function(el){
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cstimersessiontimerid','');
		return rc;
	},
	setCustomAttributeTimerSessionTimerId: function(el,val){
		CS.v3.I18N.setDataAttribute(el,'data-cstimersessiontimerid',val);
	},
	getCustomAttributeTimerPopupTimerId: function(el){
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cstimerpopuptimerid','');
		return rc;
	},
	setCustomAttributeTimerPopupTimerId: function(el,val){
		CS.v3.I18N.setDataAttribute(el,'data-cstimerpopuptimerid',val);
	},
	getCustomAttributeTimerExpiry: function(el){
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cstimerexpiry','');
		return rc;
	},
	setCustomAttributeTimerExpiry: function(el,val){
		CS.v3.I18N.setDataAttribute(el,'data-cstimerexpiry',val);
	},
	getCustomAttributeTimerDelimiter: function(el){
		var def = CS.v3.I18N.findProperty(el,'timer-delimiter');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cstimerdelimiter',def);
		return rc;
	},
	log: function(s,e){
		CS.v3.Logger.log("Timer:" + s,e);
	}
};
setTimeout(function(){
CS.v3.I18N.addProperty('clock-pattern','','EEEE, MMMM DD YYYY HH:mm:ss AA');
CS.v3.I18N.addProperty('clock-sortable-pattern','','YYYYMMDDhhmmssSSS');
CS.v3.I18N.addProperty('timer-pattern','','mm:ss');
CS.v3.I18N.addProperty('timer-duration','','00:10');
CS.v3.I18N.addProperty('timer-delimiter','',':');
CS.v3.I18N.addProperty('timer-expireurl','','');
CS.v3.I18N.addProperty('timer-sessionextendurl','','#extend');
CS.v3.I18N.addProperty('timer-sessionlogourffl','','#logoff');
CS.v3.I18N.addProperty('timer-action','',
	function(el){
		var hideId = CS.v3.Timer.getCustomAttributeTimerHideId(el);
		var hideEl = CS.v3.castToEl(hideId);
		if(hideEl){
			Element.hide(hideEl);
		}
		//NOOP // var id = Element.identify(el);
	});
CS.v3.I18N.addProperty('Calendar-inputPattern','','MM/DD/YYYY');
CS.v3.I18N.addProperty('Calendar-displayPattern','','EEEE, MMMM DD YYYY');
CS.v3.I18N.addProperty('Calendar-headerPattern','','MMMM YYYY');
CS.v3.I18N.addProperty('Calendar-summaryLabel','','Calendar');
CS.v3.I18N.addProperty('Calendar-captionLabel','','Calendar');
CS.v3.I18N.addProperty('Calendar-arriveLabel','','Arrive Date');
CS.v3.I18N.addProperty('Calendar-closeLabel','','Close');
CS.v3.I18N.addProperty('Calendar-helpLabel','','Help');
CS.v3.I18N.addProperty('Calendar-helpContentNode','',null);
CS.v3.I18N.addProperty('Calendar-helpPanelShowHint','','true');
CS.v3.I18N.addProperty('Calendar-helpPanelPosition','','left');
CS.v3.I18N.addProperty('Calendar-helpPanelWidth','','230');
CS.v3.I18N.addProperty('Calendar-currentLabel','','(current)');
CS.v3.I18N.addProperty('Calendar-sendLabel','','Send Date');
CS.v3.I18N.addProperty('Calendar-todayLabel','','(today)');
CS.v3.I18N.addProperty('Calendar-processDays','','0');
CS.v3.I18N.addProperty('Calendar-startDow','','0');
CS.v3.I18N.addProperty('Calendar-headerText','','');
CS.v3.I18N.addProperty('Calendar-footerText','','');
CS.v3.I18N.addProperty('Calendar-rangeMin','','19000101');
CS.v3.I18N.addProperty('Calendar-rangeMax','','99991231');
CS.v3.I18N.addProperty('Calendar-supportedFormats','',['MM/DD/YYYY','DD/MM/YYYY','YYYY/MM/DD','MMDDYYYY','DDMMYYYYY','YYYYMMDD','MM-DD-YYYY','DD-MM-YYYY','YYYY-MM-DD','MM DD YYYY','DD MM YYYY','YYYY MM DD','MM.DD.YYYY','DD.MM.YYYY','YYYY.MM.DD']);
CS.v3.I18N.addProperty('CalendarHolidays','',null);
CS.v3.I18N.addProperty('CalendarHolidays-US','',["####0101=New Year's Day",
									   "20110103=New Year's Day (Observed)",
									   "20090119=Martin Luther King Jr. Day",
									   "20100118=Martin Luther King Jr. Day",
									   "20110117=Martin Luther King Jr. Day",
									   "20090216=Presidents Day",
									   "20100215=Presidents Day",
									   "20110221=Presidents Day",
									   "20090525=Memorial Day",
									   "20100531=Memorial Day",
									   "20110530=Memorial Day",
									   "20090703=Independence Day (Observed)",
									   "####0704=Independence Day",
									   "20090907=Labor Day",
									   "20100906=Labor Day",
									   "20110905=Labor Day",
									   "20091012=Columbus Day",
									   "20101011=Columbus Day",
									   "20111010=Columbus Day",
									   "####1111=Veteran's Day",
									   "20091126=Thanksgiving Day",
									   "20101125=Thanksgiving Day",
									   "20111124=Thanksgiving Day",
									   "####1225=Christmas Day",
									   "20111226=Christmas Day (Observed)"]);
CS.v3.I18N.addProperty('CalendarHolidays-CA','',["####0101=New Year's Day",
									   "20090701=Canada Day",
									   "####1225=Christmas Day"]);
CS.v3.I18N.addProperty('CalendarHolidays-CN','',["20090126=Chinese New Year 2009",
									   "20100214=Chinese New Year 2010"]);
CS.v3.I18N.addProperty('CalendarHolidays-FR','',["####0101=Jour de l‚Äôan",
										"20090413=Lundi de P√¢ques",
										"####0501=F√™te du Travail",
										"####0508=F√™te de la Victoire 1945",
										"20090521=jeudi de l'Ascension",
										"20090601=Lundi de Pentec√¥te",
										"####0714=F√™te nationale",
										"####0815=Assomption",
										"####1101=Toussaint",
										"####1111=Armistice",
			  							"####1225=No√´l"]);
});
