// Setting classes on the HTML node based on user agent string
// Do this inline so classes are in place before body is started
if (navigator.userAgent.indexOf(' Windows ') != -1) {
   $$('html').first().addClassName('windows');
}
if (navigator.userAgent.indexOf(' Mac ') != -1) {
   $$('html').first().addClassName('mac');
}
if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
   $$('html').first().addClassName('safari');
}
if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') != -1) {
   $$('html').first().addClassName('chrome');
}

// PIB logon/logoff fix - define webInteraction flag from ukpib theme, but set to false.
if (typeof webInteractionFlag === 'undefined') {
    var webInteractionFlag = false;
}

document.observe('dom:loaded', function() {
   // ************************************************************
   // MTP fixes of JHX behaviours
   // ************************************************************

   // Fix ukpwsTreeTable2 tree tables
   $$('table.ukpwsTreeTable2').each(UKPWSPrivateUtils.fixTreeTables);
   
   // Fix any children of csPanelToggle headings.
   $$('div.csV-3 div.csPanelHeading.csClick > .csPanelToggle').each(UKPWSPrivateUtils.fixPanelHeadings);
   
   // ************************************************************
   // End of MTP fixes of JHX behaviours
   // ************************************************************

   // Process all on page load behaviours
   $$('.ukpwsOnLoad').each( function(el) {
      if (el.hasClassName('ukpwsInvokeOnLoad')) {
         UKPWSPrivateUtils.invokeOverlay(el);
      }
      if (el.hasClassName('ukpwsOLDisable')) {
         UKPWSPrivateUtils.disableElement(el);
      }
      if (el.hasClassName('ukpwsOLScrollTo')) {
         el.scrollTo();
         el.focus();
         el.writeAttribute('href', false);
      }
   });
   
   // Primary button focus
   var els = $$('.csV-3 .oh span.ukpwsPrimary .ukpwsPrimary', '.csV-3 .oh span.csAct .csAct');
   els.each(function(el) {
      el.observe('focus', UKPWSPrivateUtils.primaryButtonFocus);
      el.observe('blur', UKPWSPrivateUtils.primaryButtonBlur);
   });
   
   // Augmenting server side messages
   var errors = $$('div.csPanelErrors div.csPanelContent > div.csPanelScroll > ul').first();
   if (errors) {
      var count = errors.select('li').length;
      var form = errors.up('form');
      CS.v3.I18N.initialize(form); // QC15853: needed to ensure I18N properties are present.
      if (count == 1) {
         var msg = CS.v3.I18N.findProperty(form,'val-error-header-count');   
      } else {
         var msg = CS.v3.I18N.findProperty(form,'val-errors-header-count');
      }
      errors.insert({before: new Element('h3').insert(msg)});
      var errorCount = form.down('span.csCount');
      errorCount.update(count);
      var errorHeading = form.down('.csPanelErrors h3');
      errorHeading.tabIndex = '0';
      errorHeading.focus();
   }

   // Fix columns that need to be same height
   $$('.ukpwsJSSameHeightColumns','.ukpwsVerticalTabs','.ukpwsSectionBoxed','div.ukpwsStepTracker2').each(UKPWSPrivateUtils.initialiseSameHeightColumns);

   if (Prototype.Browser.IE) {
      // Fix z-index issues with inline help in IE
      $$('div.csMain').each(function (el) {
         el.select('table.csTableForm tr td div.csFields').each(function(el,i) {
            el.setStyle({'zIndex':99999-i});  // 100000 is the z-index defined in CSS for 'active' components, see uicsBase.css '.csV-3 .csZindex' rule)
         });
      });
   }
});

/*
 * UKPWSUtils
 * This is the parent object for all UK PWS utility functions. 
 */
UKPWSUtils = {};

/*
 * UKPWSUtils.getValue
 *
 * DEPRECATED - see UKPWSUtils.getFieldValue instead.
 *
 */
UKPWSUtils.getValue = function(options, id, ix) {
   options = UKPWSPrivateUtils.extendOptions(options);
   var element = UKPWSPrivateUtils.getElFromDependentId(options, id, ix);
   return element.getValue();
};

/*
 * UKPWSUtils.executeClick
 * 
 * Performs a 'click' action on element with the provided id.
 * 
 * Parameters:
 *    options        JHX generated object, contains a dependentIds object
 *    id             the JHX ID of the element to 'click'.
 *    ix             [optional] The list index of the element, if it is defined within a datalist or similar iterative construct.
 */
UKPWSUtils.executeClick = function(options, id, ix) {
   options = UKPWSPrivateUtils.extendOptions(options);
   var element = UKPWSPrivateUtils.getElFromDependentId(options, id, ix);

   if ((Prototype.Browser.IE) && element.click) {
      element.click();
   } else {
      CS.v3.fireOnclick(element);
   }
};

/*
 * UKPWSUtils.recordClick
 * 
 * Records a 'click' action on element with the provided id.
 * 
 * Parameters:
 *    options        JHX generated object, contains a dependentIds object
 *    id             the JHX ID of the element record the 'click' action for.
 *    ix             [optional] The list index of the element, if it is defined within a datalist or similar iterative construct.
 */
UKPWSUtils.recordClick = function(options, id, ix) {
   options = UKPWSPrivateUtils.extendOptions(options);
   var element = UKPWSPrivateUtils.getElFromDependentId(options, id, ix);
   element._ukpwsClicked = true;
};

/*
 * UKPWSUtils.executeIf
 * 
 * Conditionally executes another function based on the 'condition' parameter.
 * 
 * Parameters:
 *    condition      boolean value
 *    function       the function to be executed if the condition is true
 *                   this can be an array containing two functions, in this case second function is 
 *                   executed if the condition is false
 *    ev             the event that caused this function to be invoked
 *                   passed to the called function as first parameter
 *    options        JHX generated object, contains a dependentIds object
 *                   passed to the called function as second parameter
 *    0..n           further arguments to be passed to the called function as 3rd, 4th, etc parameters
 */
UKPWSUtils.executeIf = function() {
   arguments = $A(arguments);
   var res = arguments.shift();
   var funcs = arguments.shift();
   funcs = UKPWSPrivateUtils.toArray(funcs);
   if (res) {
      funcs[0].apply(this, arguments);
   } else if (funcs.length > 1) {
      funcs[1].apply(this, arguments);
   }
};

/*
 * UKPWSUtils.executeIfValue
 * 
 * Conditionally executes another function if the 'value' parameter matches the current value
 * of the element that invoked the event (expected to be a form field).
 * Optionally, if there is no match an alternate function can be executed.
 * 
 * Parameters:
 *    value          the value to be matched.
 *    function       the function to be executed if the value matches.
 *                   This can be an array containing two functions, in this case second function is 
 *                   executed if there is no match. The first function can be null, in which case this
 *                   behaves as a straight "execute if value is NOT".
 *    evOrEl         either the event that caused this function to be invoked, or the JHX id of an element
 *                   whose value to use in the comparison. 
 *                   Passed to the called function as first parameter.
 *    options        JHX generated object, contains a dependentIds object.
 *                   Passed to the called function as second parameter.
 *    0..n           further arguments to be passed to the called function as 3rd, 4th, etc parameters.
 */
UKPWSUtils.executeIfValue = function() {
   arguments = $A(arguments);
   var val = arguments.shift();
   var funcs = arguments.shift();
   funcs = UKPWSPrivateUtils.toArray(funcs);
   var evOrEl = arguments[0];
   var options = UKPWSPrivateUtils.extendOptions(arguments[1]);
   var el = UKPWSPrivateUtils.elFromEventOrElement(evOrEl, options); 
   if (val == el.getValue()) {
      if (funcs[0]) {
         funcs[0].apply(this, arguments);
      }
   } else if (funcs[1]) {
      funcs[1].apply(this, arguments);
   }
};

/*
 * UKPWSUtils.noValueBehaviours
 * 
 * Performs various dynamic behaviour based on element no value checking.
 * No value, in this case, means 'no value' or 'no selection' or 'not checked' or 'no clicked'.
 * 
 * The following source elements are allowed:
 * - select-one   'no value' true when the selected option has a value of 0
 * - checkbox     'no value' true when the checkbox is not checked
 * - button       'no value' true when it has not been clicked during the lifetime of the page
 * - radioGroup   'no value' true when the none of the elements in the group are checked
 * - checkboxGroup'no value' true when the none of the elements in the group are checked
 *
 * Note - for radio and check groups, pass in either the id of a selectOneRadio / selectManyCheckbox,
 * or the groupId of a checkbox / radioButton.
 * 
 * The following dynamic behaviours are applied to the target elements:
 * - submit button   disable if all no values set, otherwise enable
 * - static element  hide if all no values set, otherwise show
 * 
 * Static elements are any element that does not have a getValue() function, e.g. DIV, SPAN, etc.. 
 * 
 * Parameters:
 *    ev             the event that caused this function to be invoked
 *    options        JHX generated object, contains a dependentIds object
 *    source         array of element IDs to be checked for default values
 *    target         array of element IDs to perform dynamic behaviour on 
 */
UKPWSUtils.noValueBehaviours = function(ev, options, source, target) {
   options = UKPWSPrivateUtils.extendOptions(options, [source, target]);
   source = UKPWSPrivateUtils.toArray(source, true);
   
   var groupComponents = [];
   // separate group ids from source to groupComponents.
   source.each(function(id) {
      var el = $(options.dependentIds.get(id));
      if (CS.v3.I18N.isEmpty(el)) { 
         groupComponents.push(id);
      }else if(el.id) {
         // for IE7 - id selector will return first element whose prefix matches the given id. 
         // Check to see the returned element's id ends with JHX radio/check group enhancement.
         if(new RegExp(options.dependentIds.get(id) + "_\\d+$").test(el.id)) {
            groupComponents.push(id);
         }
      }
   });
   groupComponents.each(function(groupId) {
      source.splice(source.indexOf(groupId),1);
   });

   var defaultValueFound = source.any(function(id) {
      el = $(options.dependentIds.get(id));
      var res = false;
      if (CS.v3.UKPWS.FormUtils.isHideShowVisible(el)) {
         if (el.type == 'select-one') {
            res = el.getValue() == 0;
         }
         if (el.type == 'checkbox') {
            res = !el.checked;
         }
         if (el.type == 'button') {
            res = !el._ukpwsClicked;
         }  
      }
      return res;
   });
   
   // check if the group component is checked
   var defaultGroupValueFound = defaultValueFound || groupComponents.any(function(id) {
      var groupName = options.dependentIds.get(id);
      var groupEls = $$("input[name='" + groupName + "']");
      var res = false;
      if ((groupEls.length > 0) && CS.v3.UKPWS.FormUtils.isHideShowVisible(groupEls[0])) {
         if (groupEls[0].type == 'radio') {
            res = UKPWSUtils.getSelectOneRadioValue(options, id) == null ? true : false;
         }
         if (groupEls[0].type == 'checkbox') {
            res = !groupEls.any(function(el) {
               return el.checked;
            });
         }
      }
      return res;
   });

   if (defaultValueFound || defaultGroupValueFound) {
      $A(target).each(function(id) {
         el = $(options.dependentIds.get(id));
         if (el.type == 'submit') {
            UKPWSPrivateUtils.disableButton(el);
         } else if (typeof el.disabled != 'undefined') {
            el.addClassName('csDisabled').disabled = true;
         } else if (!el.getValue) {
            el.addClassName('csHide');
         }
      });
   } else {
      $A(target).each(function(id) {
         el = $(options.dependentIds.get(id));
         if (el.type == 'submit') {
            UKPWSPrivateUtils.enableButton(el);
         } else if (typeof el.disabled != 'undefined') {
            el.removeClassName('csDisabled').disabled = false;
         } else if (!el.getValue) {
            el.removeClassName('csHide');
         }
      });
   }
};
/*
 * Legacy function - please use UKPWSUtils.noValueBehaviours instead. 
 */
UKPWSUtils.noValueBeahviours = function(ev, options, source, target) {
   return UKPWSUtils.noValueBehaviours(ev, options, source, target);
}

/*
 * UKPWSUtils.validateForm
 * 
 * Runs the 'submit' validation for the form the event was fired from. 
 * A wrapper for the core 'validate form' function.
 * 
 * Parameters:
 *    ev             the event that caused this function to be invoked
 */
UKPWSUtils.validateForm = function(ev) {
   CS.v3.validateFormsSubmit(ev);
};

/*
 * UKPWSUtils.checkAll
 * 
 * Check (or uncheck) all checkboxes with the supplied name.
 * Checkboxes are unchecked when the firing checkbox is unchecked.
 * 
 * Parameters:
 *    ev             the event that caused this function to be invoked
 *    groupId        the JHX group ID of the checkboxes
 * 
 * Fires:
 *    ukpws:change   for each element checked (or unchecked)
 */
UKPWSUtils.checkAll = function(ev, groupId) {
   var sourceEl = ev.findElement();
   var sourceChecked = sourceEl.type === "checkbox" ? sourceEl.checked : true;
   $$('[name$='+groupId+']').each(function(el, index) {
      if (el != sourceEl && el.checked != sourceChecked) {
         el.checked = sourceChecked;
         el.fire('ukpws:change');
      }
   });
};

/*
 * UKPWSUtils.checkNone
 * 
 * Unhecks all checkboxes with the supplied name.
 * 
 * Parameters:
 *    ev             the event that caused this function to be invoked
 *    groupId        the JHX group ID of the checkboxes
 * 
 * Fires:
 *    ukpws:change   for each element checked (or unchecked)
 */
UKPWSUtils.checkNone = function(ev, groupId) {
   var sourceEl = ev.findElement();
   $$('[name$='+groupId+']').each(function(el, index) {
      if (el.checked) {
         el.checked = false;
         el.fire('ukpws:change');
      }
   });
};

/*
 * UKPWSUtils.mutuallyExclusiveCheckboxGroup
 * 
 * Iterates over all checkboxes in a group. For each checked checkbox that isn't the source element:
 * - uncheck it
 * - fires a custom "ukpws:change" event for it
 * 
 * Parameters:
 *    ev             the event that caused this function to be invoked
 * 
 * Fires:
 *    ukpws:change
 */
UKPWSUtils.mutuallyExclusiveCheckboxGroup = function(ev) {
   var sourceEl = ev.findElement();
   $$('[name='+sourceEl.name+']').each(function(el, index) {
      if (el != sourceEl && el.checked) {
         el.checked = false;
         el.fire('ukpws:change');
      }
   });
};

/*
 * UKPWSUtils.disableElements
 * 
 * Disables all elements whose IDs are passed in the parameters
 * 
 * Parameters:
 *    ev             the event that caused this function to be invoked
 *    options        JHX generated object, contains a dependentIds object
 *    ids            JHX ID or array of JHX IDs for element(s) to be disabled
 *    ix             [optional] The list index of the element, if it is defined within a datalist or similar iterative construct.
 */
UKPWSUtils.disableElements = function(ev, options, ids, ix) {
   options = UKPWSPrivateUtils.extendOptions(options);
   ids = UKPWSPrivateUtils.toArray(ids);
   $A(ids).each(function(id) {
      window.setTimeout( function() {
         var el = UKPWSPrivateUtils.getElFromDependentId(options, id, ix);
         if (el) {
            UKPWSPrivateUtils.disableElement(el);
         } else {
            $$('[id*=' + id + ']').each(function(el) {
               UKPWSPrivateUtils.disableElement(el);
            });
         }
      }.bind(id, options, ix), 1);
   });
   return true;
};

/*
 * UKPWSUtils.enableElements
 * 
 * Enables all elements whose IDs are passed in the parameters
 * 
 * Parameters:
 *    ev             the event that caused this function to be invoked
 *    options        JHX generated object, contains a dependentIds object
 *    ids            JHX ID or array of JHX IDs for element(s) to be enabled
 *    ix             [optional] The list index of the element, if it is defined within a datalist or similar iterative construct.
 */
UKPWSUtils.enableElements = function(ev, options, ids, ix) {
   options = UKPWSPrivateUtils.extendOptions(options);
   ids = UKPWSPrivateUtils.toArray(ids);
   $A(ids).each(function(id) {
      window.setTimeout( function() {
         var el = UKPWSPrivateUtils.getElFromDependentId(options, id, ix);         
         if (el) {
            UKPWSPrivateUtils.enableElement(el);
         } else {
            $$('[id*=' + id + ']').each(function(el) {
               UKPWSPrivateUtils.enableElement(el);
            });
         }
      }.bind(id, options, ix), 1);
   });
   return true;
};

/*
 * UKPWSUtils.hideElements
 * 
 * Hides all elements whose IDs are passed in the parameters by adding
 * the CSS class 'csHide'
 * 
 * Parameters:
 *    ev             the event that caused this function to be invoked
 *    options        JHX generated object, contains a dependentIds object
 *    ids            JHX ID or array of JHX IDs for element(s) to be hidden
 *    ix             [optional] The list index of the element, if it is defined within a datalist or similar iterative construct.
 */
UKPWSUtils.hideElements = function(ev, options, ids, ix) {
   UKPWSConsole.group('hideElements', arguments);
   options = UKPWSPrivateUtils.extendOptions(options);
   ids = UKPWSPrivateUtils.toArray(ids);
   $A(ids).each(function(id) {
      window.setTimeout(function() {
         UKPWSConsole.log(id);
         var el = UKPWSPrivateUtils.getElFromDependentId(options, id, ix);
         UKPWSConsole.log(el);
         if (el) {
            el.addClassName('csHide');
         }
      }.bind(id,options,ix), 10);
   });
   UKPWSConsole.groupEnd();
   return true;
};

/*
 * UKPWSUtils.showElements
 * 
 * Shows all elements whose IDs are passed in the parameters by removing
 * the CSS class 'csHide'
 * 
 * Parameters:
 *    ev             the event that caused this function to be invoked
 *    options        JHX generated object, contains a dependentIds object
 *    ids            JHX ID or array of JHX IDs for element(s) to be shown
 *    ix             [optional] The list index of the element, if it is defined within a datalist or similar iterative construct.
 */
UKPWSUtils.showElements = function(ev, options, ids, ix) {
   UKPWSConsole.group('showElements', arguments);
   options = UKPWSPrivateUtils.extendOptions(options);
   ids = UKPWSPrivateUtils.toArray(ids);
   $A(ids).each(function(id) {
      window.setTimeout( function() {
         UKPWSConsole.log(id);
         var el = UKPWSPrivateUtils.getElFromDependentId(options, id, ix);
         UKPWSConsole.log(el);
         if (el) {
            el.removeClassName('csHide');
         }
      }.bind(id,options,ix), 10);
   });
   UKPWSConsole.groupEnd();
   return true;
};

/*
 * UKPWSUtils.showAllPanels
 * 
 * Shows all collapsed panel content for child panels of the wrapping element
 * 
 * Parameters:
 *    ev             the event that caused this function to be invoked
 *    options        JHX generated object, contains a dependentIds object
 *    id             JHX ID for wrapping element of the panels to be shown
 *    ix             [optional] The list index of the element, if it is defined within a datalist or similar iterative construct.
 */
UKPWSUtils.showAllPanels = function(ev, options, id, ix) {
   ev.stop();
   
   options = UKPWSPrivateUtils.extendOptions(options);
   UKPWSPrivateUtils.getElFromDependentId(options, id, ix).select('div.csPanelHide .csPanelToggle').each(function(el) { 
      CS.v3.fireOnclick(el);
   });
   return true;
};

/*
 * UKPWSUtils.hideAllPanels
 * 
 * Hides all collapsed panel content for child panels of the wrapping element
 * 
 * Parameters:
 *    ev             the event that caused this function to be invoked
 *    options        JHX generated object, contains a dependentIds object
 *    id             JHX ID for wrapping element of the panels to be hidden
 *    ix             [optional] The list index of the element, if it is defined within a datalist or similar iterative construct.
 */
UKPWSUtils.hideAllPanels = function(ev, options, id, ix) {
   ev.stop();
   options = UKPWSPrivateUtils.extendOptions(options);
   UKPWSPrivateUtils.getElFromDependentId(options, id, ix).select('div.csPanel').each(function(el) {
      if (!el.hasClassName('csPanelHide')) {
         el = el.select('.csPanelToggle').first()
         CS.v3.fireOnclick(el);
      }
   });
   return true;
};

/*
 * UKPWSUtils.addStyle
 * 
 * Adds the provided style classes to the JHX components with the given ids.
 * 
 * Parameters:
 *    ev             the event that caused this function to be invoked.
 *    options        JHX generated object, contains a dependentIds object.
 *    toAdd          a style or array of styles to add to each element. 
 *    ids            JHX ID or array of JHX IDs for element(s) to be shown.
 *    ix             [optional] The list index of the element, if it is defined within a datalist or similar iterative construct.
 */
UKPWSUtils.addStyle = function(ev, options, toAdd, ids, ix) {
   UKPWSUtils.updateStyle(ev, options, toAdd, [], ids, ix);
};

/*
 * UKPWSUtils.removeStyle
 * 
 * Removes the provided style classes from the JHX components with the given ids.
 * 
 * Parameters:
 *    ev             the event that caused this function to be invoked.
 *    options        JHX generated object, contains a dependentIds object.
 *    toRemove       a style or array of styles to remove from each element. 
 *    ids            JHX ID or array of JHX IDs for element(s) to be shown.
 *    ix             [optional] The list index of the element, if it is defined within a datalist or similar iterative construct.
 */
UKPWSUtils.removeStyle = function(ev, options, toRemove, ids, ix) {
   UKPWSUtils.updateStyle(ev, options, [], toRemove, ids, ix);
};

/*
 * UKPWSUtils.updateStyle
 * 
 * Updates the styles classes on the JHX components with the given ids, adding the styles in toAdd, then removing the styles
 * in toRemove. 
 * 
 * Parameters:
 *    ev             the event that caused this function to be invoked.
 *    options        JHX generated object, contains a dependentIds object.
 *    toAdd          a style or array of styles to add to each element. Should be an empty array if there are no styles to add.
 *    toRemove       a style or array of styles to remove from each element. Should be an empty array if there are no styles to
 *                   remove. 
 *    ids            JHX ID or array of JHX IDs for element(s) to be shown.
 *    ix             [optional] The list index of the element, if it is defined within a datalist or similar iterative construct.
 */
UKPWSUtils.updateStyle = function(ev, options, toAdd, toRemove, ids, ix) {
   options = UKPWSPrivateUtils.extendOptions(options);
   ids = UKPWSPrivateUtils.toArray(ids);
   toAdd = UKPWSPrivateUtils.toArray(toAdd);
   toRemove = UKPWSPrivateUtils.toArray(toRemove);
   $A(ids).each(function(id) {
      var el = UKPWSPrivateUtils.getElFromDependentId(options, id, ix);
      if (el) {
         toAdd.each(function(style) {
            el.addClassName(style);
         });
         toRemove.each(function(style) {
            el.removeClassName(style);
         });
      }
   });
};

/*
 * UKPWSUtils.sumTotal
 * 
 * Adds (or subtracts) the supplied value to the total field on the page.
 * The value is subtracted from the total if the element firing the event is a checkbox that is unchecked.
 * 
 * Parameters:
 *    ev             the event that caused this function to be invoked
 *    options        JHX generated object, contains a dependentIds object
 *    totalId        JHX ID for the total element
 *    value          the value to be added (subtracted) to (from) the total
 *    decimalPalces  the number of decimal places to be display the total with
 */
UKPWSUtils.sumTotal = function(ev, options, totalId, value, decimalPlaces) {
   options = UKPWSPrivateUtils.extendOptions(options);
   var sourceEl = ev.findElement();
   var value;
   switch (sourceEl.type) {
      case 'checkbox' : {
         if (!sourceEl.checked) {
            value = value * -1;
         }
         break;
      }
   }
   if (value) {
      var targetEl = $(options.dependentIds.get(totalId));
      targetEl.innerHTML = UKPWSPrivateUtils.addCommas(parseFloat(UKPWSPrivateUtils.stripCommas(targetEl.innerHTML)) + value, 3, decimalPlaces);
   }
};

/*
 * UKPWSUtils.addWarning
 * 
 * Adds a field 'warning' CSS class to the table cell containing the field if the value of the
 * field matches (one of) the value(s) passed.
 * The warning class is not added if the field's container cell is marked as being in error.
 * Additionally, displays/removes a warning message box at the top of the form as appropriate.
 * 
 * Parameters:
 *    el             the element to add warnings for
 *    values         value or array of values for which the warning is to be show
 */
UKPWSUtils.addWarning = function(el, values) {
   values = UKPWSPrivateUtils.toArray(values);

   // Initialise warning state for this field
   UKPWSUtils.addWarningInit(el, values);

   // Listen for the appropriate event based on element type
   if (el.tagName === "SELECT") {
      el.observe('change', UKPWSUtils.addWarningChangeListener.bind(this, el, values));
   } else {
      el.observe('click', UKPWSUtils.addWarningChangeListener.bind(this, el, values));
   }
   // Handlers for popup message
   el.observe('focus', UKPWSUtils.showWarning.bind(this, el));
   el.observe('blur', UKPWSUtils.hideWarning.bind(this, el));
};

/*
 * UKPWSUtils.minimumCheckedActions
 * 
 * Performs actions on elements based on the minimum checked test return value.
 * Actions that can be performed on elements on the page when the minimum check test passes (fails) are:
 *    enable    (disable)
 *    disable   (enable)
 *    show      (hide)
 *    hide      (show)
 * 
 * Parameters:
 *    ev             the event that caused this function to be invoked
 *    options        JHX generated object, contains a dependentIds object
 *    checkGroups    object with each property being the value of the name attribute used to group a checkbox group,
 *                   the property value is the minimum number of checkboxes that need to be checked for that checkbox group
 *                   for example:
 *                      { 'checkGroup1' : 3, 'checkGroup2' : 1 }
 *                   means
 *                      3 checkboxes from checkGroup1 and 1 checkbox from checkGroup2 must be checked for a true result
 *    actions        object defining the actions to be performed and which JHX IDs the actions are performed on,
 *                   object property names are the action IDs and the property values is the JHX ID or array of JHX IDs for 
 *                   element(s) that the action is to be performed on
 *                   for example:
 *                      {'enable': 'submitButton', 'disable': ['updateButton', 'confirmCheckbox']}
 */
UKPWSUtils.minimumCheckedActions = function(ev, options, checkGroups, actions) {
   UKPWSConsole.group('minimumCheckedActions', arguments);
   actions = $H(actions);
   UKPWSConsole.log(actions.keys());
   var test = UKPWSUtils.isMinimumChecked(checkGroups);
   actions.keys().each(function(action) {
      UKPWSConsole.log(action);
      switch (action) {
         case 'enable' : {
            func = test ? UKPWSUtils.enableElements : UKPWSUtils.disableElements;
            break;
         }
         case 'disable' : {
            func = test ? UKPWSUtils.disableElements : UKPWSUtils.enableElements;
            break;
         }
         case 'show' : {
            func = test ? UKPWSUtils.showElements : UKPWSUtils.hideElements;
            break;
         }
         case 'hide' : {
            func = test ? UKPWSUtils.hideElements : UKPWSUtils.showElements;
            break;
         }
      }
      UKPWSConsole.log(func);
      UKPWSPrivateUtils.toArray(actions.get(action)).each(function(id) {
         UKPWSConsole.log(id);
         func(ev, options, id);
      });
      
   });
   UKPWSConsole.groupEnd();
};

/*
 * UKPWSUtils.isMinimumChecked
 * 
 * Determines if the minimum number of checkboxes in a group are checked.
 * Note: multiple checkbox groups can be checked by multiple properties of the checkGroups parameter.
 * In this case, true is only returned if all checkbox groups have at least their minimum number checked.
 * 
 * Parameters:
 *    checkGroups    object with each property being the value of the name attribute used to group a checkbox group,
 *                   the property value is the minimum number of checkboxes that need to be checked for that checkbox group
 *                   for example:
 *                      { 'checkGroup1' : 3, 'checkGroup2' : 1 }
 *                   means
 *                      3 checkboxes from checkGroup1 and 1 checkbox from checkGroup2 must be checked for a true result
 *                   
 * Returns:
 *    True if the checkbox group has at least the minimum number of checkboxes checked, otherwise false
 */
UKPWSUtils.isMinimumChecked = function(checkGroups) {
   UKPWSConsole.group('isMinimumChecked', arguments);
   checkGroups = $H(checkGroups);
   var res = checkGroups.keys().any(function(key) {
      UKPWSConsole.log(key);
      return UKPWSUtils.countChecked(key) < checkGroups.get(key);
   });
   UKPWSConsole.log(res);
   UKPWSConsole.groupEnd();
   return !res;
};

/*
 * UKPWSUtils.countChecked
 * 
 * Counts the number of checkboxes in a group that are checked
 * 
 * Parameters:
 *    name           the value of the name attribute used to group the checkboxes
 * Returns:
 *    The number of checkboxes in the group that are checked
 */
UKPWSUtils.countChecked = function(name) {
   UKPWSConsole.group('countChecked', arguments);
   var res = $$('input[name='+name+']').inject(0, function(acc, el) {
      return acc + el.checked;
   });
   UKPWSConsole.log(res);
   UKPWSConsole.groupEnd();
   return res;
};

/*
 * UKPWSUtils.populateFromList
 * 
 * Populate elements from a selection made in a select element.
 * The selected option of the select element contains the data values to be used
 * separated by double colons (::). The first value is the 'real' value that the server
 * understands and is discarded; other values are populated in to the elements identified
 * by the array of IDs in order.
 * 
 * Parameters:
 *    evOrEl         the event that caused this function to be invoked or the JHX ID of the list field
 *    options        JHX generated object, contains a dependentIds object
 *    ids            JHX ID or array of JHX IDs for element(s) to be populated
 *    hideForValues  [optional] A string or array of strings for which non-input target elements are hidden.
 */
UKPWSUtils.populateFromList = function(evOrEl, options, ids, hideForValues) {
   options = UKPWSPrivateUtils.extendOptions(options);
   ids = UKPWSPrivateUtils.toArray(ids);
   hideForValues = UKPWSPrivateUtils.toArray(hideForValues);
   var el = UKPWSPrivateUtils.elFromEventOrElement(evOrEl, options);
   var values = el.value.split('::');
   values.shift();
   $A(ids).each(function(id, ix) {
      var el = $(options.dependentIds.get(id));
      if (el) {
         if (el.setValue) {
            el.setValue(values[ix]);
         } else {
            el.innerHTML = values[ix];
            if (hideForValues.any(function(hideForVal){
               return values[ix] === hideForVal;   
            })) {
               el.hide();
            } else {
               el.show();   
            }
         }
      }
   });
   return true;
};

/*
 * UKPWSUtils.setButtonType
 * 
 * Dynamic (runtime) setting of a roundedButton (or link styled as a button) type.
 * 
 * Parameters:
 *    ev             the event that caused this function to be invoked
 *    options        JHX generated object, contains a dependentIds object
 *    id             JHX ID of the button (or links styled as a button) whose type to change.
 *    oldType        The current button type.
 *    newType        The new button type.
 *    ix             [optional] The list index of the element, if it is defined within a datalist or similar iterative construct.
 */
UKPWSUtils.setButtonType = function(ev, options, id, oldType, newType, ix) {
   options = UKPWSPrivateUtils.extendOptions(options);
   var button = UKPWSPrivateUtils.getElFromDependentId(options, id, ix);
   var buttonWrap = button.up("span.csButtonWrap");
   button.removeClassName(oldType);
   buttonWrap.removeClassName(oldType);   
   button.addClassName(newType);
   buttonWrap.addClassName(newType);
};

/*
 * UKPWSUtils.setOverlayHeading
 * 
 * Dynamic (runtime) setting of the heading for an overlay.
 * 
 * Parameters:
 *    ev             the event that caused this function to be invoked
 *    options        JHX generated object, contains a dependentIds object
 *    overlayId      the JHX ID of the overlay for which the heading is to be set
 *    headingText    text to be set in the heading of the overlay
 */
UKPWSUtils.setOverlayHeading = function(ev, options, overlayId, headingText) {
   options = UKPWSPrivateUtils.extendOptions(options);
   $(options.dependentIds.get(overlayId)).select('[class~=csPanelHeadingLocal]').first().innerHTML = headingText;
};

/*
 * UKPWSUtils.setFieldValue
 * 
 * Sets the value of a field on the page to the supplied value.
 * 
 * Parameters:
 *    ev             the event that caused this function to be invoked
 *    options        JHX generated object, contains a dependentIds object
 *    id             the JHX ID of the field to set the value for
 *    value          the value to be set
 *    triggerEvent   [optional] if true, triggers a change event for field (click event for checkboxes in IE).
 *    ix             [optional] The list index of the element, if it is defined within a datalist or similar iterative construct.
 */
UKPWSUtils.setFieldValue = function(ev, options, id, value, triggerEvent, ix) {
   options = UKPWSPrivateUtils.extendOptions(options);
   var element = UKPWSPrivateUtils.getElFromDependentId(options, id, ix);
   element.setValue(value);
   if (triggerEvent) {
      if (Prototype.Browser.IE && ((element.tagName == "INPUT") && (element.type == "checkbox"))) { 
         CS.v3.fireOnclick(element);
      } else {
         CS.v3.fireOnchange(element);
      }
   }
};
/*
 * UKPWSUtils.setSelectOneRadioValue
 * 
 * Sets the value of the selectOneRadio component.
 * 
 * Parameters:
 *    ev             the event that caused this function to be invoked
 *    options        JHX generated object, contains a dependentIds object
 *    id             JHX ID of the selectOneRadio component to inspect
 *    itemKey        key used for the radio button to select
 *    triggerEvent   [optional] if true, triggers a change event for field (click event for radio buttons in IE).
 *    
 */
UKPWSUtils.setSelectOneRadioValue = function(ev, options, id, itemKey, triggerEvent) {
   options = UKPWSPrivateUtils.extendOptions(options);
   var radioButtons = $$("input[type='radio'][name='" + options.dependentIds.get(id) + "']");
   var radioToSelect = radioButtons.detect(function(button) {
      return button.value == itemKey; 
    });
   radioToSelect.checked = true;
   if (triggerEvent) {
      if (Prototype.Browser.IE && ((radioToSelect.tagName == "INPUT") && (radioToSelect.type == "radio"))) { 
         CS.v3.fireOnclick(radioToSelect);
      } else {
         CS.v3.fireOnchange(radioToSelect);
      }
   }
}

/*
 * UKPWSUtils.getFieldValue
 * 
 * Gets the string value of a field on the page. Supported elements are:
 * - jhx:textfield
 * - jhx:selectOneListbox
 * - jhx:selectManyListbox (returns an array of values)
 * 
 * Parameters:
 *    options        JHX generated object, contains a dependentIds object
 *    id             the JHX ID of the field whose value to get
 *    ix             [optional] The list index of the element, if it is defined within a datalist or similar iterative construct.
 *    
 * See also: UKPWSUtils.getSelectOneRadioValue
 * 
 */
UKPWSUtils.getFieldValue = function(options, id, ix) {
   options = UKPWSPrivateUtils.extendOptions(options);
   var element = UKPWSPrivateUtils.getElFromDependentId(options, id, ix);
   return $F(element);
};

/*
 * UKPWSUtils.getSelectOneRadioValue
 * 
 * Returns the value of the selected radio option, or null if there is no selection.
 * 
 * Parameters:
 *    options        JHX generated object, contains a dependentIds object
 *    id             JHX ID of the selectOneRadio component to inspect
 *    ix             [optional] The list index of the element, if it is defined within a datalist or similar iterative construct.
 */
UKPWSUtils.getSelectOneRadioValue = function(options,id,ix) {
   options = UKPWSPrivateUtils.extendOptions(options);
   var clientId = options.dependentIds.get(id);
   if (typeof ix !== "undefined") {
      clientId = clientId.replace(new RegExp(id + "$"),ix + ":" + id);
   }
   var radioButtons = $$("input[type='radio'][name='" + clientId + "']");
   var checkedButton = radioButtons.detect(function(button) {
     return button.checked; 
   });  
   
   return checkedButton ? checkedButton.value : null;   
}

/*
 * UKPWSUtils.synch
 * 
 * Synchronise values of related fields to the value of the field firing the event.
 * Usually called via change event.
 * 
 * Parameters:
 *    ev             the event that caused this function to be invoked
 *    options        JHX generated object, contains a dependentIds object
 *    ids            JHX ID or array of JHX IDs of the fields to be updated
 */
UKPWSUtils.synch = function(ev, options, ids) {
   options = UKPWSPrivateUtils.extendOptions(options);
   ids = UKPWSPrivateUtils.toArray(ids);
   var val = ev.findElement().getValue();
   $A(ids).each(function(id) {
      var el = $(options.dependentIds.get(id));
      if (el.setValue) {
         el.setValue(val);
      } else {
         el.innerHTML = val;
      }
   });
};

/*
 * UKPWSUtils.clearFields
 * 
 * Clears/resets form fields.
 * 
 * Parameters:
 *    ev             the event that caused this function to be invoked
 *    options        JHX generated object, contains a dependentIds object
 *    formId         the JHX ID of the form in which the fields to be cleared reside
 *    ids            JHX ID or array of JHX IDs of elements wrapping the fields to be cleared,
 *                   these elements must themselves be contained inside the form
 *    allowEvent     [optional] if truthy, allows the event that called this function to bubble and continue its default
 *                   action, otherwise the event is stopped. Defaults to false.
 */
UKPWSUtils.clearFields = function(ev, options, formId, ids, allowEvent) {
   if (!allowEvent) {
      ev.stop();   
   }
   options = UKPWSPrivateUtils.extendOptions(options);
   var form = $(options.dependentIds.get(formId));
   if (ids) {
      ids = UKPWSPrivateUtils.toArray(ids);
      $A(ids).each(function(id) {
         UKPWSPrivateUtils.clearFields(options.dependentIds.get(id), form);
      });
   } else {
      UKPWSPrivateUtils.clearFields(form.id, form);
   }
};

/*
 * UKPWSUtils.refreshSameHeightColumns()
 * 
 * Makes all elements in a container the same height as the tallest element in the group.
 * Should be called when some dynamic behaviour changes the height of a section that is already a 'same height' section.
 */
UKPWSUtils.refreshSameHeightColumns = function() {
   var els = $$('.ukpwsJSSameHeightColumns','.ukpwsVerticalTabs','.ukpwsSectionBoxed');
   els.each(function(el) {
      var wrappers = (el.hasClassName('ukpwsJSSameHeightColumns') || el.hasClassName('ukpwsSectionBoxed')) ? el.select('> div > div:first-child') : el.select('> div');
      wrappers.invoke('writeAttribute', 'style', '');
      UKPWSPrivateUtils.sameHeightColumns(el);
   });
};

/*
 * UKPWSUtils.fixIdsInFormInList(options, formId, ids, ix)
 * 
 * "Fixes" the JHX options object to contain the correct ids for fields inside a form that is
 * itself inside a datalist (or similar) component, by adding the missing list index to the id.
 * 
 * Parameters:
 *    options        JHX generated object, contains a dependentIds object. After calling this function,
 *                   this object will be updated with corrected ids, ready for use in other functions.
 *    formId         the JHX ID of the form in which contains the field ids that need correcting. This form
 *                   must be in a datalist or similar component.
 *    ids            JHX ID or array of JHX IDs that need correcting, these elements must themselves
 *                   be contained inside the form.
 *    ix             The current index of the datalist parent of the form the fields are contained in.
 */
UKPWSUtils.fixIdsInFormInList = function(options, formId, ids, ix) {
   options = UKPWSPrivateUtils.extendOptions(options);
   UKPWSPrivateUtils.toArray(ids).each(function (id) {
      var fixedId = options.dependentIds.get(id).replace(':'+formId+':', ':'+ix+':'+formId+':');
      options.dependentIds.set(id, fixedId);
   });
};

/*
 * UKPWSUtils.getListElements(options, ids, filter)
 * 
 * Returns a Prototype array of all matching elements from a JHX list (or an empty array if none can be found).
 * Can optionally be given a filter function. 
 * 
 * Parameters:
 *    options        JHX generated object, contains a dependentIds object.
 *    ids            JHX ID or array of JHX IDs of the elements to collect in the JHX list.
 *    filter         [optional] A function of the form function(el,index,options), returning a boolean. Only elements
 *                   for which this function is true will be included in the result.
 */
UKPWSUtils.getListElements = function(options, ids, filter) {
   options = UKPWSPrivateUtils.extendOptions(options);
   ids = UKPWSPrivateUtils.toArray(ids);
   var elems = new Array();
   ids.each(function(id) {
      var idPattern = new RegExp(options.dependentIds.get(id).replace(new RegExp(id + "$"),"(\\d+):" + id));
      
      $$("[id$='" + id + "']").each(function(el) {
         var match = idPattern.exec(el.id);
         if (match && (typeof filter === "undefined" || filter(el, match[1], options))) {
            elems.push(el);
         }
      });
   });
   return elems;
}

/*
 * UKPWSUtils.executeOnOpener
 * Executes a given UKPWSUtils function on the current window's opener (if it exists).
 * Parameters:
 *    funcName       the name of the UKPWSUtils function to execute. No action is taken if funcName cannot be found.
 * 
 * Any arguments provided after funcName are passed in the same order to the the funcName call.
 */
UKPWSUtils.executeOnOpener = function(funcName) {
   arguments = $A(arguments);
   if (window.opener) {
      UKPWSConsole.group("UKPWSUtils.executeOnOpener", arguments);
      UKPWSConsole.groupEnd();
      var func = window.opener['UKPWSUtils'][funcName];
      if (func) {
         arguments.shift(); // remove funcName, pass rest along.
         func.apply(arguments);
      }
   } else {
      UKPWSConsole.log("UKPWSUtils.executeOnOpener: No parent window");   
   }
}

/*
 * UKPWSUtils.clearFormsSubmittedState
 * Clears the UKPWS submitted flag on JHX forms. 
 * Parameters:
 *    formInView     [optional] If provided, only forms in the same JHX view as this will have their
 *                   submitted flag cleared. Otherwise, all the flag is cleared on all JHX forms on the page.
 */
UKPWSUtils.clearFormsSubmittedState = function(form) {
   form = form || null;
   UKPWSPrivateUtils.setFormsSubmittedState(null, form);
}

/*
 * UKPWSCalculator
 * This is the parent object for all UK PWS calculator functions. 
 */
UKPWSCalculator = {
   // Constants for operation identifiers
   ADD: 'add',
   SUBTRACT: 'subtract',
   // Constants for error behaviour when reading a field value.
   OK: 'ok',
   ABORT: 'abort',  
   IGNORE: 'ignore'
};

/*
 * UKPWSCalculator.updateCalculation
 * Fire's a calculation update event on the target element(s), causing them to re-run their calc.
 * 
 * Parameters:
 *    options        JHX generated object, contains a dependentIds object
 *    ids            A single id, or array of ids, of the element(s) on which to fire the update event.
 *                   These elements should be set up to listen for and update their calculation on the ukpws:change event.
 */
UKPWSCalculator.updateCalculation = function(options,ids) {
   options = UKPWSPrivateUtils.extendOptions(options);
   ids = UKPWSPrivateUtils.toArray(ids);
   $A(ids).each(function(id) {
      var el = UKPWSPrivateUtils.getElFromDependentId(options, id);
      el.fire("ukpws:change");
   });
}

/*
 * UKPWSCalculator.Simple
 * Can be used to chain up a series of calculations that take input either directly through parameters,
 * or from fields on the page. Each calculation operation returns the instance of UKPWSCalculator with the updated value,
 * on which further operations can be performed, or the calculator's current value can be written to a field.
 * 
 * Parameters:
 *    options              a Javascript object consisting of the following properties:
 *       jhxOptions           the JHX options object.  Must be provided if working with field ids.
 *       initValue            [optional] the initial value of the calulator.  Default is 0.
 *       errorBehaviour       [optional] sets the behaviour of the calculator when a non-numeric value is encountered:
 *                               UKPWSCalculator.ABORT - The default behaviour. No further calculations are performed,
 *                               and the writeValueToField function does nothing.
 *                               UKPWSCalculator.IGNORE - The calculation where the error was found is not performed,
 *                               but future calculations are still executed.
 *       formatPrefix         [optional] either a string prefix for both positive and negative results, or an array of either:
 *                               2 strings in format ['posPrefix','negPrefix'].
 *                               2 objects [posObj,negObj], where the object is of format
 *                                  { prefix: 'prefixStr', cssClasses: 'cssClassName' }
 *                                  cssClasses is optional. If defined, cssClassName can be a space-separated list of css classes,
 *                                  which are applied to the element being written to.
 *                            If a negative prefix or object is set, it will replace the standard '-' negative prefix.
 *                            defined, posPrefix is used for negative numbers as well.
 *       formatPrecision      [optional] the precision when writing the result.  Defaults to 0 (decimal places).
 *       formatWithSeparator  [optional] if true, the calculation result is formatted with thousands separators, and is padded to
 *                               formatPrecison decimal places. Defaults to true. 
 *       calculatorPrecision  [optional] the decimal precision with which each operation is performed. To avoid floating point
 *                               representation issues, operations are performed as integers. Javascript can accurately represent
 *                               integers up to 15 digits, so the maximum length numbers that should be used with this calculator
 *                               are of magnitude 15 - calculatorPrecision. calculatorPrecision defaults to 3.
 */
UKPWSCalculator.Simple = Class.create({

   initialize : function(options) {
      // User providable params
      this.options = UKPWSPrivateUtils.extendOptions(options.jhxOptions);
      if (typeof options.initValue === "number") {
         this.value = options.initValue;
      } else {
         this.value = 0;
      }
      this.errorBehaviour = (typeof options.errorBehaviour === 'undefined') ? UKPWSCalculator.ABORT : options.errorBehaviour;
      
      options.formatPrefix = UKPWSPrivateUtils.toArray(options.formatPrefix);
      this.formatPrefixPos = (typeof options.formatPrefix[0] === 'undefined') ? {'prefix': ''} : options.formatPrefix[0];
      this.formatPrefixNeg = (typeof options.formatPrefix[1] === 'undefined') ? {} : options.formatPrefix[1];

      this.formatPrecision = (typeof options.formatPrecision === 'undefined') ? 0 : options.formatPrecision;
      this.formatWithSeparator = (typeof options.formatWithSeparator === 'undefined') ? true : options.formatWithSeparator;
      this.calculatorPrecision = (typeof options.calculatorPrecision === 'undefined') ? 3 : options.calculatorPrecision;
      this.calculatorPrecisionFactor = Math.pow(10, this.calculatorPrecision);
      
      // Other setup
      this.state = UKPWSCalculator.OK;
      
   },
   getFieldValue: function(id) {
      var el = UKPWSPrivateUtils.getElFromDependentId(this.options, id);
      var elValue = null;
      switch (el.tagName) {
         case "INPUT":
            if (el.type === "text" || el.type === "hidden") {
               elValue = el.value;
            }
            break;
         case "SPAN":
            elValue = UKPWSPrivateUtils.stripCommas(el.innerHTML);
            break;
         default:
            break;
      }
      return (elValue === "") ? null : parseFloat(elValue);
   },
   /*
    * UKPWSCalculator.Simple.op
    * Perform operation using the current calc value, and the passed in value.
    * Params:
    *    opId        The id of the operation to use.  Can be one of:
    *                UKPWSCalculator.ADD - adds val to the current calculator value.
    *                UKPWSCalculator.SUBTRACT - subtracts val from the current calculator value.
    *    vals        Either a numeric value to use as the second operand, or a JHX field id that holds a numeric
    *                value.  If a field id, then it can either be a <jhx:textfield>, or a <jhx:section type="inline" />.
    *                Commas are automatically stripped from the field value before using as a number.
    *                This can be an array of values, in which case the operation is performed on each value in turn.  
    * Returns:
    *    This UKPWSCalulator instance, with the value updated according to func, unless there was an error reading a numeric value
    *    from the field. If there was an error, the calc value is unchanged, and the calculator behaves as follows:
    *       UKPWSCalculator.ABORT   The calculator value is not updated for the current and all further operations,
    *                               and writeValueToField() does nothing.
    *       UKPWSCalculator.IGNORE  Ignore this operation, and return the calculator unchanged.  Future operations
    *                               are still attempted.
    */
   op : function(opId, vals) {      
      vals = UKPWSPrivateUtils.toArray(vals);
      vals.each((function(val) {
         // Reset the state if not in ABORT behaviour.
         if (this.errorBehaviour != UKPWSCalculator.ABORT) {
            this.state = UKPWSCalculator.OK;
         }
      
         if (typeof val === 'string') {
            val = this.getFieldValue(val);
         } else if (typeof val === 'object' && typeof val.getValue !== 'undefined') {
            val = val.getValue();
         }

         if (val == null || isNaN(val)){
            this.state = this.errorBehaviour;
         }

         if (this.state == UKPWSCalculator.OK ) {
            this.value = Math.round(this.value*this.calculatorPrecisionFactor);
            val = Math.round(val*this.calculatorPrecisionFactor);
         
            switch (opId) {
            case UKPWSCalculator.ADD:
               this.value += val;
               break;
            case UKPWSCalculator.SUBTRACT:
               this.value -= val;
               break;
            default:
               //NO-OP
            }
         
            this.value = this.value/this.calculatorPrecisionFactor;
         }
      }).bind(this));
      return this;
   },
   /*
    * UKPWSCalculator.Simple.conditionalOp
    * The same as UKPWSCalculator.Simple.op, except will only execute the operation if condition is true.
    * Parameters:
    *   condition           A boolean condition.
    * Returns:
    *   If condition is true, returns this calulator with the operation applied.  Otherwise, returns this
    *   calculator unchanged.
    */
   conditionalOp : function(opId, val, condition) {
      if (condition) {
         return this.op(opId, val);
      } else {
         return this;
      }
   },
   /*
    * UKPWSCalculator.Simple.writeValueToField 
    *
    * Writes the current calculator value to a field, unless the current state is ABORT.
    *
    * Parameters:
    *   id                  The id of the element to update
    */
   writeValueToField : function(id) {
      if (this.state != UKPWSCalculator.ABORT) {
         var valueToWrite = this.value;
         if (this.formatPrecision < this.calculatorPrecision) {
            valueToWrite = Math.round(valueToWrite * Math.pow(10, this.formatPrecision)) / Math.pow(10, this.formatPrecision);
         }         
         if (this.formatWithSeparator) {
            valueToWrite =  UKPWSPrivateUtils.addCommas(valueToWrite,3,this.formatPrecision);
         }

         var classesToAdd = '';
         var classesToRemove = '';
         if (this.value < 0) {
            if ((typeof this.formatPrefixNeg.prefix !== 'undefined')) {
               valueToWrite = valueToWrite.replace(/^-/,this.formatPrefixNeg.prefix);
            }
            if (this.formatPrefixNeg.cssClasses) {
               classesToAdd = this.formatPrefixNeg.cssClasses;
            }
            if (this.formatPrefixPos.cssClasses) {
               classesToRemove = this.formatPrefixPos.cssClasses;  
            }
         } else {
            valueToWrite = this.formatPrefixPos.prefix + valueToWrite;
            if (this.formatPrefixPos.cssClasses) {
               classesToAdd = this.formatPrefixPos.cssClasses;
            }
            if (this.formatPrefixNeg.cssClasses) {
               classesToRemove = this.formatPrefixNeg.cssClasses;  
            }
         }

         var el = UKPWSPrivateUtils.getElFromDependentId(this.options, id);
         el.innerHTML = valueToWrite;
         
         $A(classesToRemove.split(" ")).each(function(className) {el.removeClassName(className);});
         $A(classesToAdd.split(" ")).each(function(className) {el.addClassName(className);});
      }
      return this;
   },
   /*
    * UKPWSCalculator.Simple.dynamicActions
    * 
    * Performs actions based on the result of an expression and the current calculated value, unless the current state is ABORT..
    * When the expression is false, the opposite action is performed. Supported actions are:
    *   hide/show
    * 
    * Parameters:
    *   expression          the expression to be evaluated, use {result} to mark the position of the current calculator value
    *   actions             object defining the actions and which JHX IDs the actions are performed on,
    *                       object property names are the actions and the property values is the JHX ID or array of JHX IDs for 
    *                       element(s) that the action is applied to
    *                       for example:
    *                          {'hide': 'field1', 'show': ['section2', 'element3']}
    */
   dynamicActions : function(expression, actions) {
      if (this.state != UKPWSCalculator.ABORT) {
         expression = expression.replace(/\{result\}/g, this.value);
         if (eval(expression)) {
            UKPWSUtils.hideElements(null, this.options, actions['hide']);
            UKPWSUtils.showElements(null, this.options, actions['show']);
         } else {
            UKPWSUtils.showElements(null, this.options, actions['hide']);
            UKPWSUtils.hideElements(null, this.options, actions['show']);
         }
      }
      return this;
   }
})


/*
 * UKPWSPrivateUtils
 * This is the parent object for all UK PWS internal/private utility functions.
 * These functions should not be called directly from application code. 
 */
UKPWSPrivateUtils = {};

/*
 * UKPWSPrivateUtils.extendOptions()
 *
 * Extends the options object (generated by JHX) by converting to a Prototype Hash.
 * Optionally, if 'fix' parameter is supplied, extend the options array with IDs for 
 * JHX components with an 'index' (e.g. selectOneRadioButtons or components contained
 * within a dataList).
 *
 * Parameters:
 *   options             The options array to be extended
 *   fix                 An array of IDs to be 'fixed' and added to options array
 *
 * Returns:
 *    The extended options objext
 */
UKPWSPrivateUtils.extendOptions = function(options, fix) {
   options.dependentIds = $H(options.dependentIds);

   if (fix) {
      fix = UKPWSPrivateUtils.toArray(fix).flatten();
      var newThings = [];
      fix.each(function(el, ix) {
         if (el.indexOf('*') > 0) {
            el = el.split('*');
            var id = options.dependentIds.get(el[0]);
            if (id) {
               for (var i=0; i<el[1]; i++) {
                  options.dependentIds.set(i+':'+el[0], id.replace(el[0], i+':'+el[0]));
               }
            }
         }
      });
   }
   
   return options;
};

/*
 * UKPWSPrivateUtils.getElFromDependentId()
 * Returns the element with the passed id, using the mappings in the options Hash.
 * ix is supplied when the element is inside a list (list, dataList, datTable, etc.) and
 * it's ID has been augmented with the list index.
 * If id is already an object, it is assumed it already is a resolved element, and id is
 * simply returned as-is.
 */
UKPWSPrivateUtils.getElFromDependentId = function(options, id, ix) {
   if (typeof id === 'object') {
      return id;
   } else if (typeof ix === 'undefined') {
      return $(options.dependentIds.get(id));
   } else {
      return $(options.dependentIds.get(id).replace(id, ix+':'+id));
   }
};

/*
 * UKPWSPrivateUtils.elFromEventOrElement()
 * Works out if the supplied parameter is an element ID or an event:
 * - if ID then returns the element with that ID
 * - if event then returns the element that fired the event
 */
UKPWSPrivateUtils.elFromEventOrElement = function(evOrEl, options) {
   return (typeof evOrEl === 'string') ? $(options.dependentIds.get(evOrEl)) : evOrEl.findElement();
};

/*
 * UKPWSPrivateUtils.toArray()
 * 
 * Returns the passed in object as an array, if it is already array then it just returns the object unaltered.
 * Optionally, if 'epxandIds' parameter is supplied, converts 'index IDs' to the individual IDs. An index ID is
 * one with the following format:
 *      myid*3
 * which is converted to:
 *      0:myid, 1:myid, 2:myid
 * This is to overcome a JHX deficiency where actual IDs are not passed in the dependentIds array for some
 * components (e.g. selectOneRadioButtons or components contained within a dataList).
 *
 * Parameters:
 *   thing               The object to be converted to an array - expected to be a string or already an array
 */
UKPWSPrivateUtils.toArray = function(thing) {
   if (!Object.isArray(thing)) {
      thing = [thing];
   }
   
   var newThings = [];
   thing.each(function(el, ix) {
      if ((el != null) && typeof el.indexOf == 'function' && el.indexOf('*') > 0) {
         el = el.split('*');
         for (var i=0; i<el[1]; i++) {
            newThings.push(i+':'+el[0]);
         }
         thing.splice(ix, 1);
      }
   });

   thing = thing.concat(newThings);
   
   return thing;
};

/*
 * UKPWSPrivateUtils.stringReverse()
 * Reverses the order of the passed in string and returns it.
 */
UKPWSPrivateUtils.stringReverse = function(s) {
   return $A(s).reverse().join('');
}

/*
 * UKPWSPrivateUtils.primaryButtonFocus()
 * Primary button focus handler
 */
UKPWSPrivateUtils.primaryButtonFocus = function(ev) {
   ev.findElement().up('span').addClassName('ukpwsFocus');
};

/*
 * UKPWSPrivateUtils.primaryButtonBlur()
 * Primary button blur handler
 */
UKPWSPrivateUtils.primaryButtonBlur = function(ev) {
   ev.findElement().up('span').removeClassName('ukpwsFocus');
};

/*
 * UKPWSPrivateUtils.enableElement()
 * Enables an element, taking in to account CSS classes used by core code
 */
UKPWSPrivateUtils.enableElement = function(el) {
   if (el.type=='submit') {
      UKPWSPrivateUtils.enableButton(el);
   } else if (typeof el.disabled != 'undefined') {
      el.removeClassName('csDisabled').disabled = false;
   } else if (!el.getValue) {
      el.removeClassName('csHide');
   }
};

/*
 * UKPWSPrivateUtils.enableButton()
 * Enables a button - only works on buttons that were generated enabled on page load and subsequently disabled 
 * using UKPWSPrivateUtils.disableButton()
 */
UKPWSPrivateUtils.enableButton = function(button) {
   button.disabled = false;
   button.writeAttribute('onclick', 'return true;');
   button.removeClassName('csDisabled csDisabledRounded');
   if (button.hasClassName('ukpwsPrimaryDisabled')) {
      button.removeClassName('ukpwsPrimaryDisabled').addClassName('ukpwsPrimary');
      button.up('span').removeClassName('ukpwsPrimaryDisabled').addClassName('ukpwsPrimary');
   }
};

/*
 * UKPWSPrivateUtils.disableElement()
 * Disables an element, taking in to account CSS classes used by core code
 */
UKPWSPrivateUtils.disableElement = function(el) {
   if (el.type=='submit') {
      UKPWSPrivateUtils.disableButton(el);
   } else if (typeof el.disabled != 'undefined') {
      el.addClassName('csDisabled').disabled = true;
   } else if (!el.getValue) {
      el.addClassName('csHide');
   }
};

/*
 * UKPWSPrivateUtils.disableButton()
 * Disables a button, taking in to account CSS classes used by core code
 */
UKPWSPrivateUtils.disableButton = function(button) {
   button.disabled = true;
   button.writeAttribute('onclick', 'return false;');
   button.addClassName('csDisabled csDisabledRounded');
   if (button.hasClassName('ukpwsPrimary')) {
      button.removeClassName('ukpwsPrimary').addClassName('ukpwsPrimaryDisabled');
      button.up('span').removeClassName('ukpwsPrimary').addClassName('ukpwsPrimaryDisabled');
   }
};

/*
 * UKPWSPrivateUtils.invokeOverlay()
 * Invokes the overlay identified by the passed in element
 */
UKPWSPrivateUtils.invokeOverlay = function(el) {
   var attr = new CS.v3.ComponentAttributes(el);
   new CS.v3.PanelOverlay(el, null, attr);
}

/*
 * UKPWSPrivateUtils.stripCommas()
 * Removes commas from a string
 */
UKPWSPrivateUtils.stripCommas = function(s) {
   s = s.toString(); // Force string
   return !(s.empty()) ? s.gsub(',', '').strip() : s;
}

/*
 * UKPWSPrivateUtils.addCommas()
 * Adds commas to a string, which is expected to be a number.
 * Commas are added every 'number' characters, zeros are added to account for the number of 'decimalPlaces' requested. 
 */
UKPWSPrivateUtils.addCommas = function(val, number, decimalPlaces) {
   var index = -number, slices = [];

   // Strip commas
   val = UKPWSPrivateUtils.stripCommas(val);
   
   // Remove minus sign if negative
   if (val.charAt(0) == '-') {
      var valPrefix = val.substr(0,1);
      val = val.substr(1,val.length-1);
   };

   // Separate in to integer and decimal parts
   var split = val.split('.');

   // Reverse the string
   split[0] = UKPWSPrivateUtils.stringReverse(split[0]);

   // Separate in to groups of 'number' characters
   while ((index += number) < split[0].length) {
      slices.push(split[0].slice(index, index+number));
   }

   // Force decimal places if requested
   if (decimalPlaces) {
      split[1] = UKPWSPrivateUtils.stringReverse(split[1]);
      split[1] = parseInt(split[1].length ? split[1] : '0').toPaddedString(decimalPlaces);
      split[1] = UKPWSPrivateUtils.stringReverse(split[1]);
   }

   // Add commas and join back together in the right order
   split[0] = $A(slices.join(',')).reverse().join('');
   var valWithCommas = split.join('.');

   return (valPrefix) ? valPrefix.concat(valWithCommas) : valWithCommas;
}

/*
 * UKPWSPrivateUtils.fixTreeTables()
 * 'Fixes' tree tables that need the 'hide'show' link in the last column and not the first 9as generated by JHX tag)
 */
UKPWSPrivateUtils.fixTreeTables = function(tableEl) {
   // Find all tree table 'row toggle' links 
   tableEl.select('tbody td.csFirst span.csIconRowToggle').each(function(elSpan) {
      var cloneSpan = elSpan.clone(true);
      var cloneLink = cloneSpan.down('a');
      var lastCell = elSpan.up('tr').select('td.csLast').first();
      var elCell = elSpan.up('td');

      var innerSpan = cloneLink.select('span.ukwpsTreeTableShowLink').first();
      cloneLink._ukpwsShowText = innerSpan.innerHTML;
      innerSpan.remove();

      innerSpan = cloneLink.select('span.ukwpsTreeTableHideLink').first();
      cloneLink._ukpwsHideText = innerSpan.innerHTML;
      innerSpan.remove();

      cloneSpan.removeClassName('csIconRowToggle');
      cloneLink.removeClassName('csIcon').addClassName('ukpwsLinkDown');
      elCell.innerHTML = cloneLink.innerHTML;
      cloneLink.innerHTML = cloneLink._ukpwsShowText;

      lastCell.insert(cloneSpan);

      cloneLink.observe('click', function(ev) {
         var el = ev.findElement();
         if (el.hasClassName('csToggleShow')) {
            el.innerHTML = el._ukpwsShowText;
            el.removeClassName('ukpwsLinkUp').addClassName('ukpwsLinkDown');
         } else {
            el.innerHTML = el._ukpwsHideText;
            el.removeClassName('ukpwsLinkDown').addClassName('ukpwsLinkUp');
         }
      });
      cloneLink.observe('keydown', function(ev) {
         if (ev.keyCode===Event.KEY_RETURN) {
            var el = ev.findElement();
            if (el.hasClassName('csToggleShow')) {
               el.innerHTML = el._ukpwsShowText;
               el.removeClassName('ukpwsLinkUp').addClassName('ukpwsLinkDown');
            } else {
               el.innerHTML = el._ukpwsHideText;
               el.removeClassName('ukpwsLinkDown').addClassName('ukpwsLinkUp');
            }
         }
      });
   });
};


/*
 * UKPWSPrivateUtils.fixPanelHeadings()
 * 'Fixes'clickable panel headings so that any click events on child elements of the heading will trigger expand/collapse behaviour. 
 */
UKPWSPrivateUtils.fixPanelHeadings = function(panelHeadingEl) {
   panelHeadingEl.childElements().each(function (el) {
      el.addClassName("csPanelToggleText");
   });
}


/*
 * UKPWSPrivateUtils.initialiseSameHeightColumns()
 * Should be called on page load for containers marked for same height columns.
 * Sets the initial height for those containers based on page-load contents, and
 * registers to update those heights if any hideShowSource in the continer is changed. 
 */
UKPWSPrivateUtils.initialiseSameHeightColumns = function(containerEl) {
   UKPWSPrivateUtils.sameHeightColumns(containerEl);
   containerEl.select(['input.csToggle','select.csToggle']).each(function(sourceEl) {
      sourceEl.observe('ukpws:toggleDisplay', function(ev) {
         UKPWSPrivateUtils.sameHeightColumns(containerEl);
      });
   });
}

/*
 * UKPWSPrivateUtils.sameHeightColumns()
 * Makes all elements in a container the same height as the tallest element in the group.
 * If container has the class 'ukpwsJSSameHeightColumns', then affected elements are all DIV grandchildren of the container,
 * otherwise affected elements are all DIV children of the container. 
 */
UKPWSPrivateUtils.sameHeightColumns = function(el) {
   var wrappersSelector = '';
   if (el.hasClassName('ukpwsJSSameHeightColumns') && el.hasClassName('ukpwsDynamicCols')) {
      wrappersSelector = '> div > div.ukpwsDynamicColsContainer > div:first-child';
   } else if (el.hasClassName('ukpwsJSSameHeightColumns') || el.hasClassName('ukpwsSectionBoxed')){
      wrappersSelector = '> div > div:first-child, > div > form > div:first-child';
   } else if (el.hasClassName('ukpwsStepTracker2')) {
      wrappersSelector = '> ul > li > div.csNavWrap';
   } else {
      wrappersSelector = '> div';
   }
   var wrappers = el.select(wrappersSelector);
   // Reset inline style to remove any previously set height, so the reported height for each wrapper is based on its content.
   wrappers.invoke('writeAttribute', 'style', null);
   var el = el.up('div.csPanelHide'); // We need to 'unhide' any parent that is hiding children to get real heights.
   if (el) {
      el.removeClassName('csPanelHide');
   }
   var x = wrappers.collect(function(s) {
      return s.getHeight() - parseInt($(s).getStyle('paddingTop')) - parseInt($(s).getStyle('paddingBottom'));
   });
   wrappers.invoke('writeAttribute', 'style', 'height:'+x.max()+'px');
   if (el) {
      el.addClassName('csPanelHide');
   }
};

/*
 * UKPWSPrivateUtils.addWarningInit()
 * Initialise the warning state for this field based on it's initial value (called on page load)
 */
UKPWSUtils.addWarningInit = function(el, values) {
   var cell = el.up('td');
   if (values.indexOf(el.getValue()) !== -1) {
      cell.addClassName('ukpwsWarning');
   }
   UKPWSPrivateUtils.warningBox(cell.up('form'));
};

/*
 * UKPWSPrivateUtils.addWarningChangeListener()
 * Set the warning state for this field based on it's changed value (called on change/click)
 */
UKPWSUtils.addWarningChangeListener = function(el, values) {
   var cell = el.up('td');
   if (values.indexOf(el.getValue()) !== -1) {
      cell.addClassName('ukpwsWarning');
      UKPWSUtils.showWarning(el);
   } else {
      cell.removeClassName('ukpwsWarning');
   }
   UKPWSPrivateUtils.warningBox(cell.up('form'));
};

/*
 * UKPWSPrivateUtils.showWarning()
 * Shows the warning popup panel if the field currently has a warning attached
 */
UKPWSUtils.showWarning = function(el) {
   var field = el.up('div.csFields');
   field.addClassName('csFocus');
   
   if (el.getValue()) {
      // Try and find the specific message for the fields current value
      var msg = field.down('div.csErrorLocal p[id$=ukpws-warning-'+el.getValue()+']');
      if (typeof msg === "undefined") {
         // No specific message, get generic message
         msg = field.down('div.csErrorLocal p[id$=ukpws-warning]');
      }
      msg = msg.clone(true);
      // If we don't yet have a warning panel for this field, create it
      var wPanel = field.select('div.ukpwsPanelWarning').first();
      if (!wPanel) {
         field.insert({bottom: UKPWSUtils.warningPopupPanel()});
         wPanel = field.select('div.ukpwsPanelWarning').first();
      }
      // Update the warnign panel with the warning message
      wPanel.down('div.csWarningMsg').update(msg);
   }
};

/*
 * UKPWSPrivateUtils.hideWarning()
 * Hides the warning popup panel
 */
UKPWSUtils.hideWarning = function(el) {
   el.up('div.csFields').removeClassName('csFocus');
}

/*
 * UKPWSPrivateUtils.warningPopupPanel()
 * Returns the HTML for the warning popup panel.
 */
UKPWSUtils.warningPopupPanel = function(){
   var sb = new StringBuffer();
   sb.append('<div class="csPanel ukpwsPanelWarning">');
   sb.append('<div class="csPanelTopLeft"><div class="csPanelTopRight">');
   sb.append('<div class="csPanelContent"><div class="csPanelScroll">');
   sb.append('<div class="csWarningMsg">');
   sb.append('</div></div></div></div></div>');
   sb.append('<div class="csPanelBottomLeft"><div class="csPanelBottomRight"></div></div>');
   sb.append('<div class="csChevron csChevronLeft"></div></div>');
   var rc = sb.toString();
   sb = null;
   return rc;
};

/*
 * UKPWSPrivateUtils.warningBox()
 * Displays a warning message box at the top of the form
 */
UKPWSPrivateUtils.warningBox = function(form) {
   var box = form.down('div.ukpwsPanelWarnings');
   if (box != null) {
      box.remove();
   }
   var msg = CS.v3.I18N.findProperty(form,'val-error-header-count');

   var warnings = form.select('td.ukpwsWarning');
   if (warnings.length > 0) {
      var sb = new StringBuffer();
      sb.append('<div class="csPanel csFormStatus csPanelStatus ukpwsPanelWarnings">');
      sb.append('<div class="csPanelTopLeft"><div class="csPanelTopRight">');
      sb.append('<div class="csPanelContent"><div class="csPanelScroll">');
      sb.append('<div class="csErrorMsg"><div class="csErrorMsgHeading"><h3>');
      sb.append(msg);
      sb.append('</h3></div><div class="ukpwsWarningMsgList"></div></div></div></div></div></div>');
      sb.append('<div class="csPanelBottomLeft"><div class="csPanelBottomRight"></div></div>');
      sb.append('<div class="csChevron csChevronLeft"></div></div>');
      
      var errorBox = form.down('div.csPanelErrors');
      if (typeof errorBox !== 'undefined') {
         errorBox.insert({after: sb.toString()});
      } else {
         form.insert({top: sb.toString()});
      }
      box = form.down('div.ukpwsPanelWarnings');
      box.on('click', CS.v3.ErrorCountBoxClick);
      sb = null;
      UKPWSPrivateUtils.warningBoxMessages(box, warnings);
   }
};

/*
 * UKPWSPrivateUtils.warningBoxMessages()
 * Populates a warning message box with the actual warning messages
 */
UKPWSPrivateUtils.warningBoxMessages = function(bx, errorCellsAr){
   var msgList = bx.down('div.ukpwsWarningMsgList');
   var uList = new Element('ul');
   msgList.insert({'bottom': uList});
   errorCellsAr.each(function(el) {
      var fieldContainer = el.down('div.csFields') || el.down('div.csLabel');
      fieldContainer.select('input','select','textarea').each(function(f) {
         if (f.type !== 'radio' || (f.type === 'radio' && f.checked)) {
            var msg = el.down('div.csErrorLocal p[id*='+f.name+'::ukpws-warning-'+f.getValue()+']');
            if (typeof msg === 'undefined') {
               msg = el.down('div.csErrorLocal p[id*='+f.name+'::ukpws-warning]');
            }
            uList.insert({'bottom': CS.v3.HTML.errorBoxMessageTemplate.evaluate(
               {
                  id:  f.id,
                  msg: msg.innerHTML.stripTags()
               })
            });
         }
      });
   });
};

/*
 * UKPWSPrivateUtils.clearFields()
 * Clears/resets fields within a form or within a container element in the form
 */
UKPWSPrivateUtils.clearFields = function(wrapperId, form) {
   var els = form.getElements();
   els = els.findAll( function(el) {
      return (el.up('[id='+wrapperId+']'));
   });

   els.each(function(el) {
      // Don't do anything with readonly, disabled and hidden fields, buttons and input buttons
      if (el.tagName == 'BUTTON'
         || el.readOnly
         || el.disabled
         || ['hidden', 'button', 'reset', 'submit']
            .include(el.type)) {
            return;
         }
      if (el.tagName == 'SELECT') {
         // For select tags, select top item if it's a dropdown, otherwise deselect all for lists
         el.selectedIndex = (el.size > 1) ? -1 : 0;
         
         // If a hideShowSource, fire a change event to trigger hideShow behaviour.
         if (el.select('option').any(function(e) {
           return (e.readAttribute('data-csnodestoshow') || e.readAttribute('data-csnodestohide')); 
         })) {
            CS.v3.fireOnchange(el);
         } 
         
      } else if (['checkbox', 'radio'].include(el.type)) {
         // Make sure no radio buttons and checkboxes are marked as checked
         el.checked = false;
      } else {
         // All other fields - text and textarea, have their value set to an empty string
         el.setValue('');
      }
   });
};

/*
 * UKPWSPrivateUtils.findJHXForms
 * Finds all JHX forms below the provided element, or across the whole page if container is not set.
 * Returns as a Prototype-extended array.
 * Parameters:
 *    container      A container element below which to search. If not provided, searchs across the whole page.
 */
UKPWSPrivateUtils.findJHXForms = function(container) {
   if (CS.v3.I18N.isEmpty(container)) {
      container = document.body;
   }
   return Element.select(container,["form[class*='csForm']","form[class*='csValidate-submit']","form[class*='csValidate-realtime']"]);
}

/*
 * UKPWSPrivateUtils.setFormsSubmittedState
 * Sets a submitted flag on JHX forms on the page. Use to prevent forms
 * from being submitted more than once per page render lifetime.
 * Parameters:
 *    state          'true' (string) to indicate a submit event is in progress. null to remove
 *                   the submitted flag.
 *    baseForm       [optional] if provided, then the JHX view id of this form will be used as a filter
 *                   - only forms which have the same viewId will be affected by this call. If not set,
 *                   then all JHX forms on the page will be targeted.
 */
UKPWSPrivateUtils.setFormsSubmittedState = function(state, baseForm) {
   var forms = UKPWSPrivateUtils.findJHXForms();
   if (!CS.v3.I18N.isEmpty(baseForm)) {
      var viewIdPattern = new RegExp("^" + baseForm.id.split(":").shift());
      forms = forms.findAll(function (form) {
         return form.id.match(viewIdPattern);
      })
   }
   UKPWSConsole.group("UKPWSPrivateUtils.setFormsSubmittedState");
   forms.each(function(form) {
      UKPWSConsole.log("State: " + state + " Form: " + form.id);   
      CS.v3.I18N.setCustomAttributeSubmitted(form, state);
   });
   UKPWSConsole.groupEnd();
   
}


/*
 * UKPWSChart
 * This is the parent object for all UK PWS Charting functions. 
 */
UKPWSChart = {};

UKPWSChart.Base = Class.create({
   initialize: function(options) {
      var jhxOptions = UKPWSPrivateUtils.extendOptions(options.jhxOptions);
      this.chartContainer = UKPWSPrivateUtils.getElFromDependentId(jhxOptions, options.id, options.idIx);

      this.options = options;
      this.decodeDescriptions(options.chartDataNames, options.chartDataProvider);
      this.chart.dataProvider = options.chartDataProvider;

      // Set chart container height (default 100px)
      var height = options.height ? options.height+'px' : '100px'; 
      this.chartContainer.writeAttribute({'style':'height:'+height});
      
      // Set up chart options
      var opts = {};
      Object.extend(opts, UKPWSChart.BaseProperties.chart);
      Object.extend(opts, this.chartProperties || {});
      Object.extend(opts, options.chartProperties || {});
      Object.extend(opts, options.chartStyles || {});
      Object.extend(this.chart, opts || {});
      Object.extend(this.chart.balloon, options.balloonStyles || {});

      // Application specific pre-render stuff
      this.preRender();

      return this;
   },
   addGraph: function() {

      // Set up graph options - same for all graphs at this time
      var opts = {}
      Object.extend(opts, UKPWSChart.BaseProperties.graph || {});
      Object.extend(opts, this.graphProperties || {});
      Object.extend(opts, this.options.graphProperties || {});
      Object.extend(opts, this.options.graphStyles || {});
      if (typeof this.options.chartDataBalloonText === 'string') {
         opts.balloonText = this.options.chartDataBalloonText;
      }
      
      // Generate the chart
      if (this.options.chartDataNames) {
         $H(this.options.chartDataNames).each(function(el) {
            this.addGraph2(el[0], el[1], opts);
         }.bind(this));
      } else {
         this.addGraph2('value', 'description', opts);
      }
      return this;
   },
   addValueAxis: function() {
      var valAxis = new AmCharts.ValueAxis();

      // Set up value axis options
      var options = {}
      Object.extend(options, UKPWSChart.BaseProperties.valueAxis || {});
      Object.extend(options, this.valueAxisProperties || {});
      Object.extend(options, this.options.valueAxisProperties || {});
      Object.extend(options, this.options.valueAxisStyles || {});

      Object.extend(valAxis, options || {});
      this.chart.addValueAxis(valAxis);
      return this;
   },
   addCategoryAxis: function() {

      // Set up category axis options
      var options = {}
      Object.extend(options, UKPWSChart.BaseProperties.categoryAxis || {});
      Object.extend(options, this.categoryAxisProperties || {});
      Object.extend(options, this.options.categoryAxisProperties || {});
      Object.extend(options, this.options.categoryAxisStyles || {});

      Object.extend(this.chart.categoryAxis, options);
      return this;
   },
   addLabels: function() {
      if (this.options.labels) {
         UKPWSPrivateUtils.toArray(this.options.labels).each(function(label) {
            if (label.x === 'right') {
               label.x = this.chartContainer.getWidth();
            }
            this.chart.addLabel(label.x, label.y, label.text, label.align);
         }.bind(this));
      }
      return this;
   },
   render: function() {
      this.chart.write(this.chartContainer);

      // Application specific post-render stuff
      this.postRender();
      return this;
   },
   addGraph2: function(valueName, descriptionName, opts) {
      var graph = new AmCharts.AmGraph();
      graph.valueField = valueName;
      graph.descriptionField = descriptionName;
      Object.extend(graph, opts);
      this.chart.addGraph(graph);
   },
   /*
    * UKPWS.Chart.Base.decodeDescriptions
    * Converts any defined description text of the form \uHHHH into its associated unicode character.
    */
   decodeDescriptions: function(dataNames, dataProviders) {
      if (!dataNames) {
         dataNames = {'value': 'description'};
      }
      for (dataName in dataNames) {
         for (var chartIndex = 0; chartIndex < dataProviders.length; chartIndex++) {
            var description = dataProviders[chartIndex][dataNames[dataName]];
            if (typeof description !== 'undefined') {
               var decodedStr = description.replace(/\\u([0-9A-F]{4})/gi, function(str, group1) {
                  return String.fromCharCode(parseInt(group1, 16));
               });
               dataProviders[chartIndex][dataNames[dataName]] = decodedStr;
            } 
         }
      }
   },
   preRender: function() {},
   postRender: function() {}
});

UKPWSChart.Column = Class.create(UKPWSChart.Base, {
   initialize: function($super, options) {
      this.chart = new AmCharts.AmSerialChart();
      $super(options);
      this.addGraph()
      this.addValueAxis()
      this.addCategoryAxis()
      this.addLabels()
      this.render()
   },
   chartProperties: {
      marginLeft:          50,
      marginBottom:        50
   },
   graphProperties: {
      type:                'column'
   }
});

UKPWSChart.StackedColumn = Class.create(UKPWSChart.Column, {
   valueAxisProperties: {
      stackType:           'regular'
   }
});

UKPWSChart.Bar = Class.create(UKPWSChart.Base, {
   initialize: function($super, options) {
      this.chart = new AmCharts.AmSerialChart();
      $super(options);
      this.addGraph()
      this.addValueAxis()
      this.addCategoryAxis()
      this.addLabels()
      this.render()
   },
   chartProperties: {
      rotate:              true,
      marginLeft:          50,
      marginBottom:        50
   },
   graphProperties: {
      type:                'column'
   }
});

UKPWSChart.StackedBar = Class.create(UKPWSChart.Bar, {
   valueAxisProperties: {
      stackType:           'regular'
   }
});

UKPWSChart.Line = Class.create(UKPWSChart.Base, {
   initialize: function($super, options) {
      this.chart = new AmCharts.AmSerialChart();
      this.chart.categoryField = 'category';
      $super(options);
      this.addGraph()
      this.addValueAxis()
      this.addCategoryAxis()
      this.addLabels()
      this.render()
   },
   chartProperties: {
      marginLeft:          50,
      marginBottom:        50
   },
   graphProperties: {
      type:                 'line',
      bullet:               'round',
      fillAlphas:           0
   }
});

UKPWSChart.Pie = Class.create(UKPWSChart.Base, {
   initialize: function($super, options) {
      this.chart = new AmCharts.AmPieChart();
      $super(options);
      this.addLabels()
      this.render()
   }
});


UKPWSChart.BaseProperties = {};
UKPWSChart.BaseProperties.chart= {
   fontFamily:          'Arial',
   fontSize:            13,
   balloonText:         '',
   startDuration:       0,
   categoryField:       'category',
   valueField:          'value',
   titleField:          'description',
   labelText:           '[[title]]',
   pathToImages:        '/'
};
UKPWSChart.BaseProperties.graph = {
   fillAlphas:          1,
   balloonText:         '',
   labelText:           '[[description]]'
};
UKPWSChart.BaseProperties.valueAxis = {};
UKPWSChart.BaseProperties.categoryAxis = {};

/*
 * UKPWSChart.BaseProperties.setChartPathToImages
 * 
 * Sets the pathToImages property of the chart.
 * 
 * Parameters:
 *   imgPath            A path to an image in the root of the of the charts images folder. The containing
 *                      folder of this image will be used as the pathToImages property (ending with the file
 *                      separator character).
 */
UKPWSChart.BaseProperties.setChartPathToImages = function(imgPath) {
   UKPWSChart.BaseProperties.chart.pathToImages = imgPath.replace(/([\\/])[^\\/]+$/, "$1");
};

/*
 * UKPWSConsole
 * This is the parent object for all UK PWS console utility functions.
 * These functions should not be called directly from application code. 
 */
UKPWSConsole = {};
UKPWSConsole.enabled = false;

UKPWSConsole.log = function() {
   if (UKPWSConsole.enabled && (typeof console !== "undefined")) {
      $A(arguments).each(function(arg) {
         console.log(arg);
      });
   }
};
UKPWSConsole.group = function(txt, args) {
   if (UKPWSConsole.enabled && (typeof console !== "undefined") && console.group) {
      console.group(txt);
      if (args) {
         console.log(args);
      }
   }
};
UKPWSConsole.groupEnd = function() {
   if (UKPWSConsole.enabled && (typeof console !== "undefined") && console.groupEnd) {
      console.groupEnd();
   }
};

   



/*
 * MTPHMCIBSlider Creates a two handle slider Note: actually uses three handles, but
 * 1st is always set to 0 position and hidden - this is to allow background
 * styling between handles Constructor requires slider wrapping element (DIV)
 */
MTPHMCIBSlider = Class.create({
   // HTML template for the slider
   sliderTemplate: new Template(
      '<div class="widget-slider-container">' +
         '<div class="widget-slider">' +
            '<span class="handle handle0">' +
               '<span class="button">&nbsp;</span>' +
               '<span id="handle0bubble" class="bubble csRow ukpwsSectionBackground3"></span>' +
               '<span class="arrow">&nbsp;</span>' +
            '</span>' +
            '<span class="handle handle1">' +
               '<span class="button">&nbsp;</span>' +
               '<span id="handle1bubble" class="bubble csRow ukpwsSectionBackground3"></span>' +
               '<span class="arrow">&nbsp;</span>' +
            '</span>' +
            '<span class="span span0 ukpwsSectionBackground4"></span>' +
            '<span class="span span1 ukpwsSectionBackground5">' +
               '<span class="difference1Text">#{sliderText1}</span>' +
            '</span>' +
         '</div>' +
         '<div class="throbber"></div>' +
         '<div class="widget-sliderscale-container"></div>' +
      '</div>'
   ),
   // HTML template for the 'in slider' text
   sliderText: [
      null,
      new Template('I could borrow<br/><big><b>&pound;<span>#{difference1}</span></b></big>')
   ],
   // HTML template for the slider scale
   scaleTemplate: new Template(
      '<div class="widget-slider-scale">' +
         '<ul>' +
            '<li class="start">#{scaleStart}</li>' +
            '<li>&nbsp</li>' +
            '<li class="middle">#{scaleMiddle}</li>' +
            '<li>&nbsp</li>' +
            '<li class="end">#{scaleEnd}</li>' +
         '</ul>' +
      '</div>'
   ),
   // The Scriptaculous slider control
   slider: null,
   // Prevent recursion when calling Slider#setValue() in slide/change functions
   recurse: false,
   // Prevent multiple Ajax requests
   ajaxInProgress: false,
   // The FORM containing the slider
   theform: null,
   // The DIV (JHX section) wrapping the JHX formLayout for the fields used by the slider
   wrapper: null,
   // The increment amount for one pixel of the slider
   increment: 1,
   // Array of values, each element in the array is the value represented by one pixel of the slider 
   values:                 [0],
   // Data values to be sent in the Ajax request
   ajaxData: $H(),
   // Options passed in by caller
   options: {
      activeHandleEl: null,
      activeHandleValue: null,
      difference1Value: null,
      scaleMax: null,
      scaleMaxThreshold: 1000
   },
   /* 
    * initialize
    * The constructor for the HMCIB slider component
    * 
    */
   initialize: function(options) {
      ////console.group('initialize');
      Object.extend(this.options,options);
         this.getFormInfo();
         this.createDOMElements();
         this.createSlider();
      ////console.groupEnd();
   },
   createDOMElements: function() {
      ////console.group('createDOMElements');

      // Create the slider container
      var sliderText1 = this.sliderText[1].evaluate({
         difference1:  UKPWSPrivateUtils.addCommas(this.options.difference1Value, 3, 0)
      });
      this.wrapper.insert({
         top: this.sliderTemplate.evaluate({
            sliderText1:  sliderText1
         })
      });

      // Move useful JHX stuff to the slider (to save creating these ourselves)
      for (var i=0; i<this.options.handles.length; i++) {
         $('handle'+i+'bubble')
            .insert(this.options.handles[i].label.up('div.csLabel'))
            .insert(this.options.handles[i].field.up('div.csFields'))
         ;
         if (this.options.handles[i].hidden) {
            $('handle'+i+'bubble').up().addClassName('csHide');
            ////console.log($$('.span'+i), i);
            this.wrapper.select('.span'+i).invoke('addClassName', 'csHide');
         }
      }

      // The slider element (DIV)
      this.sliderElement = this.wrapper.down('div.widget-slider')

      // All the handles
      this.handles = this.sliderElement.select('span.handle');

      this.spans = this.sliderElement.select('span.span');

      ////console.groupEnd();
   },
   createSlider: function() {
      ////console.group('createSlider');

      // The values of the fields on page load
      var sliderValues = this.options.handles.pluck('value');

      // Need to do this before we are able to get the slider width
      this.wrapper.removeClassName('csHide');

      // Calculate the values for each pixel represented by the slider
      this.options.scaleMax = Math.min(this.round(this.options.handles[1].value * 1.4, 4), this.options.handles[1].max);
      this.increment = this.round(this.options.scaleMax / this.sliderElement.getWidth(), 2);
      this.increment = 500; // Static increment until dynamic rule is proposed.
      this.values = [0];
      for (var i = this.increment; i < this.options.scaleMax; i = i + this.increment) {
         this.values.push(i);
      }
      this.values.push(this.options.scaleMax);

      // Create the slider
      delete this.slider;
      this.slider = null;
      this.slider = new Control.Slider(this.handles, this.sliderElement, {
         range:          $R(0, this.options.scaleMax),
         values:         this.values,
         sliderValue:    sliderValues,
         restricted:     true,
         onSlide:        this.slide.bind(this),
         onChange:       this.change.bind(this)
      });

      this.updateSlider();

      // Add observers for......................................
      this.options.handles.each(function(handle) {
         handle.field.observe('click', this.onClick.bind(this));
         handle.field.observe('focus', this.onFocus.bind(this));
         handle.field.observe('blur', this.onBlur.bind(this));
         handle.field.observe('change', this.onChange.bind(this));
      }.bind(this));

      this.wrapper.down('div.widget-sliderscale-container').update(
         this.scaleTemplate.evaluate({
            scaleStart:   0,
            scaleMiddle:  UKPWSPrivateUtils.addCommas(parseInt(this.options.scaleMax / 2), 3, 0),
            scaleEnd:     UKPWSPrivateUtils.addCommas(this.options.scaleMax, 3, 0)
      })
   );

   ////console.groupEnd();
   },
   slide: function(values, s) {
      ////console.group('slide');
      this.slideOrChange(values, s);
      ////console.groupEnd();
   },
   change: function(values, s) {
      ////console.group('change');
      ////console.log(arguments);
      if (this.recurse) {
         this.recurse = false;
         ////console.groupEnd();
         return;
      }

      this.recurse = true;
      this.slideOrChange(values, s);
      this.recalculate(s.event);
      ////console.groupEnd();
   },
   slideOrChange: function(values, s) {
      ////console.group('slideOrChange');

      // Update
      this.options.handles[s.activeHandleIdx].value = values[s.activeHandleIdx];
      if (values[s.activeHandleIdx] < this.options.handles[s.activeHandleIdx].min) {
      s.setValue(this.options.handles[s.activeHandleIdx].min, s.activeHandleIdx);
         this.options.handles[s.activeHandleIdx].value = this.options.handles[s.activeHandleIdx].min;
      }

      // Update the input field for the active handle
      this.options.handles[s.activeHandleIdx].field.setValue(values[s.activeHandleIdx]);

      // Update activeHandle field
      this.options.activeHandleEl.setValue(this.options.handles[s.activeHandleIdx].activeValue);

      ////console.groupEnd();
   },
   recalculate: function(ev) {
      ////console.group('recalculate');
      if (this.ajaxInProgress) {
         ////console.log('exiting: ajax request already in progress');
         ////console.groupEnd();
         return;
      }

      CS.v3.validateFormsSubmit(ev, this.theform);

      // Don't do anything if any input field is in error
      if (this.wrapper.select('.csError').length > 0) {
         ////console.log('exiting: fields in error');
         ////console.groupEnd();
         return;
      }

      this.ajaxInProgress = true;
      this.slider.setDisabled();
      this.wrapper.addClassName('widget-slider-disabled');

      this.ajaxData.set('activeHandle', this.options.activeHandleEl.getValue());
      this.ajaxData.set('handle0', this.options.handles[0].value);
      this.ajaxData.set('handle1', this.options.handles[1].value);

      new Ajax.Request(this.options.ajaxURL, {
         parameters:    this.ajaxData,
         onSuccess:     this.updatePage.bind(this)
      });
      ////console.groupEnd();
   },
   updatePage: function(transport) {
      ////console.group('updatePage');
      var response = eval('(' + transport.responseText + ')');

      this.wrapper.removeClassName('widget-slider-error');
      this.noProductsDiv.addClassName('csHide');
      this.errorDiv.addClassName('csHide');

      if (response.responseCode != 0) {
         this.handleBadResponse(response);
      } else {

         // Update instance variables
         this.options.handles[0].value = response.handle0Value;
         this.options.handles[1].value = response.handle1Value;
         this.options.difference1Value = response.difference1Value;
         this.updateFields($H(response.updateFields), response.hide, response.show);

         if ((this.options.handles[1].value >= (this.options.scaleMax - this.options.scaleMaxThreshold))
          || (this.options.handles[1].value < (this.options.scaleMax/3))) // 3 chosen to make sure borrow amount text is always visible with a bit of a margin
         {
            // rescale if property value ends up less than 25% of current scale
            UKPWSPrivateUtils.invokeOverlay(this.options.popupMessageEl);
            this.createSlider();
         } else {
            this.updateSlider();
         }
      }

      this.slider.setEnabled();
      this.wrapper.removeClassName('widget-slider-disabled');
      this.ajaxInProgress = false;
      ////console.groupEnd();
   },
   handleBadResponse: function(response) {
      this.recurse = false;
      this.wrapper.addClassName('widget-slider-error');
      //console.log(response.responseCode);
      switch (response.responseCode) {
         case '4' : {
            this.noProductsDiv.removeClassName('csHide');
            break;
         }
         default : {
            this.errorDiv.removeClassName('csHide');
         }
      }
   },
   updateSlider: function() {
      ////console.group('updateSlider');

      for (var i=0; i<this.options.handles.length; i++) {
         this.recurse = true;
         this.slider.setValue(this.options.handles[i].value, i); // check ].value
         this.options.handles[i].field.setValue(this.options.handles[i].value); // check ].value
      }

      this.sliderElement.select('span.difference1Text span').first().innerHTML = this.options.difference1Value;

      this.handles.invoke('removeClassName', 'selected');

      this.spans[0].style.width = parseInt(this.handles[0].style.left) + 'px';
      this.spans[1].style.width = parseInt(this.handles[1].style.left) - parseInt(this.spans[0].style.width) + 'px';
      if (this.spans[1].getWidth() < 140) {
         this.handles[0].addClassName('left');
         this.handles[1].addClassName('right');
      } else {
         this.handles[0].removeClassName('left');
         this.handles[1].removeClassName('right');
      }
      ////console.groupEnd();
   },
   updateFields: function(fields, hide, show) {
      ////console.group('updateLTV');
      fields.each(function(field) {
         $$('[id$='+field.key+']').first().innerHTML = field.value;
      });
      hide.each(function(field) {
         $$('[id$='+field+']').first().addClassName('csHide');
      });
      show.each(function(field) {
         $$('[id$='+field+']').first().removeClassName('csHide');
      });
      ////console.groupEnd();
   },
   getFormInfo: function() {
      ////console.group('getFormInfo');

      this.wrapper = $$('div[id$='+this.options.wrapperId+']').first();
      this.theform  = this.wrapper.up('form');
      this.wrapper.addClassName('widget-slider-wrapper');
      this.wrapper.down('fieldset').addClassName('csHide');

      this.noProductsDiv = this.wrapper.select('div[id$=slider-message-no-products]').first();
      this.errorDiv = this.wrapper.select('div[id$=slider-message-error]').first();

      this.options.activeHandleEl = this.wrapper.select('[id$='+this.options.activeHandleId+']').first();
   
      for (var i=0; i<this.options.handles.length; i++) {
         this.options.handles[i].field = this.wrapper.select('[id$='+this.options.handles[i].fieldId+']').first();
         this.options.handles[i].label = this.wrapper.select('[id$='+this.options.handles[i].labelId+']').first();
         this.options.handles[i].value = this.options.handles[i].field.getValue();
         this.options.handles[i].difference = (i>0) ? this.options.handles[i].value - this.options.handles[i-1].value : 0;

         if (this.options.handles[i].diffText) {
            this.sliderText[i] = new Template(this.options.handles[i].diffText.replace(/\{0\}/i, '<span>#{difference1}</span>'));
         }
      }

      this.options.difference1Value = $$('[id$='+this.options.difference1Id+']').first().innerHTML;

      this.options.associatedData.each(function(elId, ix) {
         var el = this.wrapper.select('input[id$='+elId+']').first();
         this.ajaxData.set(elId, el.getValue());
      }.bind(this));

      this.options.popupMessageEl = this.wrapper.select('input[id$='+this.options.scaleRecalcMessageId+']').first();
      ////console.groupEnd();
   },
   round: function(value, scale) {
      ////console.group('round');
      value = parseInt(value);
      var exponent = value.toString().length - scale;
      ////console.log(value, Math.pow(10, exponent), exponent);
      ////console.log(value / Math.pow(10, exponent));
      ////console.log(Math.ceil(value / Math.pow(10, exponent)));
      ////console.log(Math.ceil(value / Math.pow(10, exponent)) * Math.pow(10, exponent));
      ////console.groupEnd();
      return Math.ceil(value / Math.pow(10, exponent)) * Math.pow(10, exponent);
   },
   onClick: function(ev) {
      ////console.group('keyClick');
      // fix to allow click focus setting in text fields
      // problem is, click is swallowed by drag routine, to allow drag with bubbles
      ev.findElement().focus()
      ////console.groupEnd();
   },
   onFocus: function(ev) {
      ////console.group('keyFocus');
      var el = ev.findElement()
      el.up('.handle').addClassName('selected');
      ////console.groupEnd();
   },
   onBlur: function(ev) {
      ////console.group('keyBlur');
      var el = ev.findElement()
      el.up('.handle').removeClassName('selected');
      ////console.groupEnd();
   },
   onChange: function(ev) {
      ////console.group('keyChange')
      var el = ev.findElement()
      el.up('.handle').addClassName('selected');
      // Update activeHandle field and handle value
      var ix = (el.id == this.options.handles[0].field.id) ? 0 : 1;
      this.options.activeHandleEl.setValue(this.options.handles[ix].activeValue);
      this.options.handles[ix].value = this.options.handles[ix].field.getValue();
      this.recalculate(ev);
      ////console.groupEnd();
   }
});





/*** Blockers ***/
Object.extend(CS.v3.Blockers, {
   'ukpws-isInteger': {
      rule: function(el, c, ev, name) {
         CS.v3.RegularExpressions.block(el, c, ev, name);
      }
   },
   'ukpws-isDecimal': {
      rule: function(el, c, ev, name) {
         CS.v3.RegularExpressions.block(el, c, ev, name);
      }
   },
   'ukpws-isAlphaSpace': {
      rule: function(el, c, ev, name) {
         CS.v3.RegularExpressions.block(el, c, ev, name);
      }
   },
   'ukpws-isAlphaNumericSpace': {
      rule: function(el, c, ev, name) {
         CS.v3.RegularExpressions.block(el, c, ev, name);
      }
   },
   'ukpws-isAlphaNumericPeriod': {
      rule: function(el, c, ev, name) {
         CS.v3.RegularExpressions.block(el, c, ev, name);
      }
   },
   'ukpws-isAlphaNumericRPS': {
      rule: function(el, c, ev, name) {
         CS.v3.RegularExpressions.block(el, c, ev, name);
      }
   },
   'ukpws-isAlphaNumericRPSNewline': {
      rule: function(el, c, ev, name) {
         CS.v3.RegularExpressions.block(el, c, ev, name);
      }
   },
   'ukpws-isAlphaPeriodSpace': {
      rule: function(el, c, ev, name) {
         CS.v3.RegularExpressions.block(el, c, ev, name);
      }
   },
   'ukpws-isDate': {
      rule: function(el, c, ev, name) {
         CS.v3.RegularExpressions.block(el, c, ev, name);
      }
   },
   'ukpws-isUKPersonalName': {
      rule: function(el, c, ev, name) {
         CS.v3.RegularExpressions.block(el, c, ev, name);
      }
   },
   'ukpws-isUKPostcode': {
      rule: function(el, c, ev, name) {
         CS.v3.RegularExpressions.block(el, c, ev, name);
      }
   },
   'ukpws-isInternationalPhoneNumber': {
      rule: function(el, c, ev, name) {
         CS.v3.RegularExpressions.block(el, c, ev, name);
      }
   },
   'ukpws-isIDVSecurityAnswer': {
      rule: function(el, c, ev, name) {
         CS.v3.RegularExpressions.block(el, c, ev, name);
      }
   },
   'ukpws-isBacsEFTData': {
      rule: function(el, c, ev, name) {
         CS.v3.RegularExpressions.block(el, c, ev, name);
      }
   }
});

/*** Validators ***/
Object.extend(CS.v3.Validators, {
   'ukpws-isInteger': {
      rule: function(el,c,name) {
         return CS.v3.RegularExpressions.validate(el, c, name);
      },
      error: function(el,c) {
         return 'Error : must ukpws-isInteger';
      }
   },
   'ukpws-isDecimal': {
      rule: function(el,c,name) {
         var minDecPlaces = CS.v3.Validators.getValuesFromClassName(el, 'ukpwsMinDecPlaces-');
         var maxDecPlaces = CS.v3.Validators.getValuesFromClassName(el, 'ukpwsMaxDecPlaces-');
         minDecPlaces = minDecPlaces != null ? minDecPlaces : '0';
         maxDecPlaces = maxDecPlaces != null ? maxDecPlaces : '';
         decimalMultiplicity = (minDecPlaces == 0) ? '?' : '';
         var regExp = RegExp('^[0-9]*\\.'+decimalMultiplicity+'[0-9]{'+minDecPlaces+','+maxDecPlaces+'}$');
         return CS.v3.RegularExpressions.validate(el, c, regExp);
      },
      error: function(el,c) {
         return 'Error : must ukpws-isDecimal';
      }
   },
   'ukpws-isAlphaSpace': {
      rule: function(el,c,name) {
         return CS.v3.RegularExpressions.validate(el, c, name);
      },
      error: function(el,c) {
         return 'Error : must be ukpws-isAlphaSpace';
      }
   },
   'ukpws-isAlphaNumericSpace': {
      rule: function(el,c,name) {
         return CS.v3.RegularExpressions.validate(el, c, name);
      },
      error: function(el,c) {
         return 'Error : must be ukpws-isAlphaNumericSpace';
      }
   },
   'ukpws-isAlphaNumericPeriod': {
      rule: function(el,c,name) {
         return CS.v3.RegularExpressions.validate(el, c, name);
      },
      error: function(el,c) {
         return 'Error : must be ukpws-isAlphaNumericPeriod';
      }
   },
   'ukpws-isAlphaNumericRPS': {
      rule: function(el,c,name) {
         return CS.v3.RegularExpressions.validate(el, c, name);
      },
      error: function(el,c) {
         return 'Error : must be ukpws-isAlphaNumericRPS';
      }
   },
   'ukpws-isAlphaNumericRPSNewline': {
      rule: function(el,c,name) {
         return CS.v3.RegularExpressions.validate(el, c, name);
      },
      error: function(el,c) {
         return 'Error : must be ukpws-isAlphaNumericRPSNewline';
      }
   },
   'ukpws-isAlphaPeriodSpace': {
      rule: function(el,c,name) {
         return CS.v3.RegularExpressions.validate(el, c, name);
      },
      error: function(el,c) {
         return 'Error : must be ukpws-isAlphaPeriodSpace';
      }
   },
   'ukpws-isDate': {
      rule: function(el,c,name) {
         return CS.v3.RegularExpressions.validate(el, c, name);
      },
      error: function(el,c) {
         return 'Error : must be ukpws-isDate';
      }
   },
   'ukpws-isUKPersonalName': {
      rule: function(el,c,name) {
         return CS.v3.RegularExpressions.validate(el, c, name);
      },
      error: function(el,c) {
         return 'Error : must be ukpws-isUKPersonalName';
      }
   },
   'ukpws-isUKPostcode': {
      rule: function(el,c,name) {
         return CS.v3.RegularExpressions.validate(el, c, name);
      },
      error: function(el,c) {
         return 'Error : must be ukpws-isUKPostcode';
      }
   },
   'ukpws-isInternationalPhoneNumber': {
      rule: function(el,c,name) {
         return CS.v3.RegularExpressions.validate(el, c, name);
      },
      error: function(el,c) {
         return 'Error : must be ukpws-isInternationalPhoneNumber';
      }
   },
   'ukpws-isInRange': {
      rule: function(el,c,name) {
         var minMaxValues = CS.v3.Validators.getValuesFromClassName(el, 'ukpwsMinMaxValue-');
         if (parseInt(c.value0) > parseInt(minMaxValues[1])) {
            return false;
         } else if (parseInt(c.value0) < parseInt(minMaxValues[0])) {
            return false;
         }
         return true;
      },
      error: function(el,c) {
         return 'Error : must be ukpws-isInRange';
      }
   },
   'ukpws-isMinLength': {
      rule: function(el,c,name) {
         var minLength = CS.v3.Validators.getValuesFromClassName(el, 'ukpwsMinLength-');
         var regExp = RegExp('^.{'+minLength+',}$');
         return CS.v3.RegularExpressions.validate(el, c, regExp);
      },
      error: function(el,c) {
         return 'Error : must be ukpws-isMinLength';
      }
   },
   'ukpws-requiredWhenCmdIs': {
      rule: function(el,c) {
      if (CS.v3.I18N.isEmptyOrPlaceholderEl(el)) {
         var cmdList = CS.v3.Validators.getStringsFromClassName(el, 'cmd\\d?-');
            if (cmdList.any(function(cmd) {
            return cmd === CS.v3.validateFormsClickEl.name;
         })) {
            return false;
         }
      }
         return true;
      },
      error: function(el,c) {
         return 'Error : must be ukpws-requiredWhenCmdIs';
      }
   },
   'ukpws-isNotAllSpaces' : {
      rule: function(el,c,name) {
         return CS.v3.RegularExpressions.validate(el, c, name);
      },
      error: function(el,c) {
         return 'Error : must be ukpws-isNotAllSpaces';
      }
   },
   'ukpws-isIDVSecurityAnswer' : {
      rule: function(el,c,name) {
         return CS.v3.RegularExpressions.validate(el, c, name);
      },
      error: function(el,c) {
         return 'Error : must be ukpws-isIDVSecurityAnswer';
      }
   },
   'ukpws-isLuhn' : {
      rule: function(el,c,name) {
         return $A(c.value0).reverse().map(Number).inject(0, function(s, d, i) {
            return s + (i % 2 == 1 ? (d == 9 ? 9 : (d * 2) % 9) : d);
         }) % 10 == 0;
      },
      error: function(el,c) {
         return 'Error : must be ukpws-isLuhn';
      }
   },
   'ukpws-isBacsEFTData': {
      rule: function(el,c,name) {
         return CS.v3.RegularExpressions.validate(el, c, name);
      },
      error: function(el,c) {
         return 'Error : must be ukpws-isBacsEFTData';
      }
   },
   /**
    * getValuesFromClassName()
    *
    * Retrieve a value or values embedded in a CSS class.
    *
    * The CSS class is expected to be in the form:
    * - 'prefix-123', this example would return '123'.
    * - 'prefix-123_456', this example would return [123,456].
    *
    * Parameters:
    *    el    -  the element that has the CSS class applied to it
    *    name  -  the CSS class to get the value from
    *
    * Returns
    *    integer -   the numeric suffix of the CSS class as a string or array (if multiple)
    *                or null if no values found
    */
   getValuesFromClassName: function(el, name) {
      var re = new RegExp(name+'([0-9_]+)', 'i');
      var res = el.className.match(re);
      if (res) {
         res = res[1];
         if (res.indexOf('_') > -1) {
            res = res.split('_');
         }
      }
      return(res);
   },
   /**
   * getStringFromClassName()
   *
   * Retrieve strings embedded in a CSS class with a certain prefix.
   *
   * The CSS class is expected to be in the form:
   * - 'prefix-string', this example would return ['string'].
   *
   * Parameters:
   *    el     - the element that has the CSS class applied to it
   *    name   - the prefix of the CSS class to get the value from - can be a regexp pattern but must not
   *    contain a grouping.
   *
   * Returns
   *    [string] - An array of strings, representing the suffixes matching the given CSS class prefix.
   */
  getStringsFromClassName: function(el, name) {
     var re = new RegExp(name+'([\\w]+)', 'gi');
     var res = new Array();
     while (re.exec(el.className)) {
        res.push(RegExp.$1);
     }

    return(res);
  }
});

/**
 * Regular Expressions
 * 
 * This object contains regular expression patterns for key blocking and field validation.
 * 
 * Object properties are keyed by the CSS class that is applied to a field to invoke the blocking/validation.
 * 
 * Properties themselves are objects that have the following properties:
 *    validate -  (required) the RegExp pattern used for validation and, if acceptable, for key blocking
 *    block    -  (optional) the RegExp pattern used for key blocking when the validation pattern is not suitable
 *                e.g. when the validate pattern defines specific numbers of characters (see isDate)
 * 
 * Public functions:
 *    block()
 *    validate()
 * 
 */
CS.v3.RegularExpressions = {
   'ukpws-isInteger': {
      validate: /^[0-9]*$/
   },
   'ukpws-isDecimal': {
      validate: /^[0-9]*\.?[0-9]*$/
   },
   'ukpws-isAlphaSpace': {
      validate: /^[A-Z ]*$/i
   },
   'ukpws-isAlphaNumericSpace': {
      validate: /^[A-Z0-9 ]*$/i
   },
   'ukpws-isAlphaNumericPeriod': {
      validate: /^[A-Z0-9.]*$/i
   },
   'ukpws-isAlphaNumericRPS': {
      validate: /^[A-Z0-9 !"#$%&\'()*+\-.,\/:;=?@_\xA3]*$/i
   },
   'ukpws-isAlphaNumericRPSNewline': {
      validate: /^[A-Z0-9 !"#$%&\'()*+\-.,\/:;=?@_\xA3\n\r]*$/i
   },
   'ukpws-isAlphaPeriodSpace': {
      validate: /^[A-Z. ]*$/i
   },
   'ukpws-isDate': { // DD.MM.YY or DD.MM.YYYY separator could be on choice '.' '/' or '-' leap years compatible, 00 is treated as year 2000.
      validate: /^(((0?[1-9]|[12]\d|3[01])[\.\-\/](0?[13578]|1[02])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|[12]\d|30)[\.\-\/](0?[13456789]|1[012])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|1\d|2[0-8])[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|(29[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00)))$/,
      block: /^[0-9\.\/\-]*$/
   },
   'ukpws-isUKPersonalName': {
      // Not allowed consecutive apostrophes
      validate: /^(?!.*[\']{2})[A-Z \'\-.]*$/i
   },
   'ukpws-isUKPostcode': {
      // Note: this is not accurate as some alpha characters are not allowed in certain positions, however, allowing all alpha characters
      // here as it's possible these conventions may change in the future
      // ref: http://interim.cabinetoffice.gov.uk/govtalk/schemasstandards/e-gif/datastandards/address/postcode.aspx
      validate: /^((([A-Z]{1,2}[0-9]{1,2}?|[A-Z]{1,2}[0-9]{1}[A-Z]{1}?) [0-9]{1}[A-Z]{2})|GIR 0AA)$/i
   },
   'ukpws-isInternationalPhoneNumber': {
      validate: /^(\+[0-9])?[0-9]*$/
   },
   'ukpws-isNotAllSpaces': {
      validate: /[^\s\n\r]/
   },
   'ukpws-isIDVSecurityAnswer': {
      validate: /^[A-Z0-9 @_'\-.?\xD1\xF1\xC1\xC9\xCD\xD3\xDA\xDC]*$/i
   },
   'ukpws-isBacsEFTData': {
      validate: /^[A-Z0-9.&\/\- ]*$/
   },
   /**
    * block()
    *
    * Function to perform blocking of keyboard input. Stops the supplied event if the character typed matches the RegExp pattern
    *
    * Parameters:
    *    el    -  the element whose value is to be checked for blocking characters
    *    c     -  CS wrapper of the element
    *    ev    -  the firing event
    *    name  -  the name of the validator
    */
   block: function(el, c, ev, name) {
      var charCode = typeof ev.charCode != 'undefined' ? ev.charCode : ev.keyCode;
      if (!ev.ctrlKey && !ev.altKey && charCode) {
         var re = new RegExp(this[name].block ? this[name].block : this[name].validate);
         if (!String.fromCharCode(charCode).match(re)) {
            ev.stop();
         }
      }
   },
   /**
    * validate()
    *
    * Function to perform validation on user input strings.
    *
    * Validation is only performed on visible fields that have a non-empty value.
    *
    * Parameters:
    *    el    -  the element whose value is to be validated
    *    c     -  CS wrapper of the element
    *    name  -  the name of the validator
    *
    * Returns
    *    boolean  -  true if no validation required or value is valid, otherwise false
    */
   validate: function(el, c, nameOrExp) {
      if (el.up('.csHide')) {
         // If in future need to support validating elements in a collapsed panel, will need further checking here.
         return true;
      }
      if (CS.v3.I18N.isEmptyOrPlaceholderEl(el) || c.value0 == null || c.value0.length == 0) {
         // Only run RegExp test if there is some data
         return true
      }
      return this[nameOrExp] ? this[nameOrExp].validate.test(c.value0) : nameOrExp.test(c.value0);
   }
}