/* Theme Pack 3.2.1+ DO NOT modify */
/*** Settings ***/
CS.v3.Forms = {
   DefaultOptions: {
      cancelHintOnDone : true,
      disableFormButtonUntilTrue: true
   }
};
/*** Events ***/
CS.v3.Components.validateForms = function(el,options){
   new CS.v3.Config.validateForms(el,options);
};
CS.v3.Config.validateForms = Class.create(CS.v3.Base.prototype, {
   initialize: function(el,options){
      this.forms = Element.select(el,["form[class*='csForm']","form[class*='csValidate-submit']","form[class*='csValidate-realtime']"]);
      if(options.bind==='true'){this.bind();}
   },
   bind: function(){
      var ar;
      var arf = this.forms;
      var i = arf.length;
      var ii;
      var obj;
      var el;
      var controls;
      var isValidateRealtime;
      var isValidateSubmit;
      while(i--){
         obj = arf[i];
         controls = Form.getElements(obj);//obj.getElements();
         isValidateRealtime = Element.hasClassName(obj,'csValidate-realtime');
         isValidateSubmit = Element.hasClassName(obj,'csValidate-submit');
            // Real-time validation
         Event.observe(obj,'reset',function(event){ CS.v3.validateFormsReset(event); });
         if(isValidateRealtime){
            obj;// was: this.forms[i];
            ar = controls;//Form.getElements(obj);
            ii = ar.length;
            while(ii--){
               el = ar[ii];
               new Form.Element.EventObserver(el, function(el,value){
                  var tag=el.tagName;
                  var b = false;
                  switch(tag){
                     case 'INPUT':
                        b = CS.v3.supportedInputTypeEl(el);
                        break;
                     case 'TEXTAREA':
                        b = true;
                        break;
                     case 'SELECT':
                        b = true;
                        break;
                     default:
                        // NOOP
                        break;
                  }
                  if(b){
                     var comparison = this.getCustomAttribute(el,'csFireEvent-');
                     if(comparison != null){
                        var comparisonStr = comparison.toString();
                        var myel = CS.v3.castToEl(comparisonStr);
                        new CS.v3.getTextFieldError(myel,'realtime');
                     } else {
                        new CS.v3.getTextFieldError(el,'realtime');
                     }
                  }
               }.bind(this));
            }
            // Blockers
            Event.observe(obj,'keypress', function(event){ CS.v3.validateFormsKeyPress(event); });
         }
         ii = controls.length;
         var control;
         var tag;
         var typ;
         while(ii--){
            control = controls[ii];
            Event.observe(control,'click', function(event){ CS.v3.validateControlsClick(event); });
            Event.observe(control,'focus', function(event){ CS.v3.validateControlsFocus(event); });
            Event.observe(control,'blur', function(event){ CS.v3.validateControlsBlur(event); });
            Event.observe(control,'change', function(event){ CS.v3.validateControlsChange(event); });
            Event.observe(control,'keypress', function(event){ CS.v3.validateControlsKeypress(event); });
         }
         // Form Validation (On Submit)
         if(isValidateSubmit){
            Event.observe(obj,'submit', function(event){ 
               CS.v3.validateFormsSubmit(event);
               // Must be called *after* form validation
               CS.v3.postValidateFormsSubmit(event);
            });
            Event.observe(obj,'click', function(event){ CS.v3.validateFormsClick(event); });
            Event.observe(obj,'keydown', function(event){ CS.v3.validateFormsKeydown(event); });
         } else {
            // If not validating on submission, ensure post-validation form submission code is still run 
            Event.observe(obj,'submit', function(event){ CS.v3.postValidateFormsSubmit(event); });
         }
      }
   },
   log: function(s,e){
      CS.v3.Logger.log("validateForms:" + s,e);
   }
});
CS.v3.validateControlsClick = function(event){
   var el = $(Event.element(event));
   if(Prototype.Browser.IE){
      var tag=el.tagName;
      switch(tag){
         case 'INPUT':
            //var typ=el.type;
            //if(typ==='checkbox'){ Commented out to fix the Radio Buttons in IE
               if(Element.hasClassName(el,'csToggle')){
                  var attr = new CS.v3.ComponentAttributes(el);
                  new CS.v3.ToggleDisplay(el,attr);
                  el.fire('ukpws:toggleDisplay');
                  //event.preventDefault(event);
               }
            //}
            break;
         default:
            // NOOP
            break;
      }
   }
   // WebKit only (fixed checkbox focus issue)
   if(Prototype.Browser.WebKit){
      CS.v3.focus(el);
   }
};
CS.v3.validateControlsFocus = function(event){
   var el = $(Event.element(event));
   var tr = CS.v3.getParentElement(el,'TD') || CS.v3.getParentElementWithClass(el,'csRow');
   if(tr){
      if(Element.hasClassName(tr,'csError')){
         CS.v3.errorFocus.focus(el,event);
      }
   }
};
CS.v3.validateControlsBlur = function(event){
   var el = $(Event.element(event));
   if(Prototype.Browser.IE){
      var isFocus = false;
      var tag = el.tagName;
      switch(tag){
         case 'INPUT':
            var b = CS.v3.supportedInputTypeEl(el);
            if(b){
               isFocus = true;
            }
            break;
         case 'SELECT':
            isFocus = true;
            break;
         case 'TEXTAREA':
            isFocus = true;
            break;
         default:
            // NOOP
            break;
      }
      if(isFocus){
         Element.removeClassName(el,'csIeFocus');
      }
   }
   var tr = CS.v3.getParentElement(el,'TD') || CS.v3.getParentElementWithClass(el,'csRow');
   if(tr){
      if(Element.hasClassName(tr,'csError')){
         CS.v3.errorFocus.blur(el,event);
      }
   }
};
CS.v3.validateControlsChange = function(event){
   var el = $(Event.element(event));
   var b = Element.hasClassName(el,'csToggle');
   if(b){
      var attr;
      var tag = el.tagName;
      switch(tag){
         case 'INPUT':
            var isToggle=true;
            var typ = el.type;
            if(Prototype.Browser.IE){
               if(typ!=='checkbox'){
                  isToggle=false;
               }
            }
            if(isToggle){
               if(Prototype.Browser.IE){ // added to fix checkbox issues in IE
                  attr = new CS.v3.ComponentAttributes(el);
               } else {
                  attr = new CS.v3.ComponentAttributes(el);
                  new CS.v3.ToggleDisplay(el,attr);
                  el.fire('ukpws:toggleDisplay');
               }
            }
            break;
         case 'SELECT':
            var idx = el.selectedIndex;
            var ar = el.options;
            var elOption = CS.v3.castToEl(ar[idx]);
            attr = (elOption.value == '') ? new CS.v3.ComponentAttributesSelectNone(ar, idx) : new CS.v3.ComponentAttributes(elOption);
            new CS.v3.ToggleDisplay(elOption,attr);
            el.fire('ukpws:toggleDisplay');
            break;
         default:
            // NOOP
            break;
      }
   }
};
CS.v3.validateControlsKeypress = function(event){
   // Prevent Enter from submitting form (but allow for textarea newlines and submit buttons)
   var keyCode = (typeof event.which !== "undefined") ? event.which : event.keyCode;
   if (keyCode == Event.KEY_RETURN) {
      var el = Event.findElement(event);
      var elUpperTagName = el.tagName.toUpperCase();
      if (!((elUpperTagName == "TEXTAREA") ||
            ((elUpperTagName == "INPUT") && (el.type == "submit")))) {
         event.preventDefault();
      }
   }  
};
CS.v3.validateFormsReset = function(event){
   var el = $(Event.element(event));
   //alert('Re-Validation on RESET');
   // TODO add validation check on 'reset'
   CS.v3.Timer.resetSessionTimerAPI(el);
};
CS.v3.validateFormsKeyPress = function(event){
   var el = $(Event.element(event));
   var key = event.keyCode;
   if(key!==Event.KEY_TAB){
      var tag = el.tagName;
      var BLOCK = 'block';
      switch(tag){
         case 'INPUT':
            var b = CS.v3.supportedInputTypeEl(el);
            if(b){
               new CS.v3.getBlockers(el,BLOCK,event);
            }
            break;
         case 'TEXTAREA':
            new CS.v3.getBlockers(el,BLOCK,event);
            break;
         default:
            // NOOP
            break;
      }
   }
};
CS.v3.validateFormsSubmit = function(event, theform){
   // event - the event that caused the validation to be needed (e.g. submit button, enter key)
   // theform - the form to be validated - only needed if different from the (parent) form of the target element of the event
   //event.preventDefault(event);
   if (typeof theform != "undefined") {
      var form = $(theform);
   } else {
      var form = $(Event.element(event));
      if (form.tagName != 'FORM') {
         form = form.up('form');
      }
   }
   var CSEXCEPT = 'csExcept';// TODO try String
   var b = Element.hasClassName(form,CSEXCEPT);
   if(b){
      return true;
   } else {
      var CSREQUIRED = 'csVal-required';// TODO try String
      //var fieldset = Element.down(form,'fieldset');
      var controls = Form.getElements(form);//form.getElements();
      var falseResults = [];
      var j = controls.length;
      var nam;
      var el;
      //var value;
      var typ;
      var tag;

      // Reset 'popup' error messages before processing each field as we need to cater for
      // multiple fields per cell so cannot do this on a per field basis
      while(j--){
         var err = null;
         el = CS.v3.castToEl(controls[j]);
         var cell = CS.v3.getParentElement(el,'TD') || CS.v3.getParentElementWithClass(el,'csRow');
         if (cell) {
            cell.removeClassName('csError'); // Make sure no cells are marked as in error - this will get added if needed during validation
            var fields = cell.down('DIV.csFields');
            if (fields) {
               var boxtest = fields.down('.csPanelError');
               if (boxtest) {
                  boxtest.remove();
               }
               this.box = CS.v3.HTML.errorBox();
               fields.insert({bottom: this.box});
            }
         }
      }
      
      var j = controls.length;
      while(j--){
         var err = null;
         nam = controls[j];
         el = CS.v3.castToEl(nam);
         // Prevent validation on fields hidden via the JHX hideShowSource/Target tags
         if (!CS.v3.UKPWS.FormUtils.isHideShowVisible(el)) {
            continue;
         }

         tag = el.tagName;
         //value = $F(el);
         switch(tag){
            case 'INPUT':
               typ = el.type;
               var b2 = CS.v3.supportedInputType(typ);
               if(b2){
                  if(!Element.hasClassName(el,CSEXCEPT)){
                     err = new CS.v3.getTextFieldError(el,'submit');
                  }
               }
               if(typ==='radio' || typ==='checkbox'){
                  err = new CS.v3.getCheckError(el,form);
               }
               break;
            case 'TEXTAREA':
               if(!Element.hasClassName(el,CSEXCEPT)){
                  err = new CS.v3.getTextFieldError(el,'submit');
               }
               break;
            case 'SELECT':
               err = new CS.v3.getSelectError(el);
               break;
         }
         if(err && err.results){
            var hasErrors = err.results.any(function (x) { return x == false; });
            if(hasErrors){
               // As we're iterating fields in reverse order, prepend rather than append
               falseResults.unshift(el);
            }
         }
      }
      new CS.v3.ErrorCountBox(form,falseResults);
      if(Element.hasClassName(form,'csDisabled')){
         event.preventDefault(event);
      }
      return falseResults.length === 0;
   }
};
CS.v3.validateFormsClickEl = null;
CS.v3.validateFormsClick = function(event){
   CS.v3.validateFormsClickEl = $(Event.element(event));
   var b = Element.hasClassName(CS.v3.validateFormsClickEl,'csSave');
   if(b){
      var form = CS.v3.getParentElementWithClass(CS.v3.validateFormsClickEl,'.csForm');
      Element.addClassName(form,'csExcept');
   }
};
CS.v3.validateFormsKeydown = function(event){
   var el = $(Event.element(event));
   var key = el.keyCode;
   if(key===Event.KEY_RETURN){
      CS.v3.validateFormsClick(event);
   }
};
CS.v3.postValidateFormsSubmit = function(event){
   var form = $(Event.element(event));
   // Prevent double submission.
   if (CS.v3.I18N.getCustomAttributeSubmitted(form)) {
      event.preventDefault();
   } else { 
      if(!(event.defaultPrevented || Element.hasClassName(form,'csDisabled'))) {
         UKPWSPrivateUtils.setFormsSubmittedState('true',form);
      }
   }
};
CS.v3.supportedInputTypeEl = function(el){
   var rc = false;
   if(el){
      var typ = el.type;
      rc = CS.v3.supportedInputType(typ);
   }
   return rc;
};
CS.v3.supportedInputType = function(typ){
   var rc = false;
   if(typ==='text' || typ==='password'){// TODO  must be updated for HTML5 input types like 'search'
      rc = true;
   }
   return rc;
};
// Focus
CS.v3.errorFocus = {
   focus: function(el){
      this.cell = CS.v3.getParentElement(el,'TD') || CS.v3.getParentElementWithClass(el,'csRow');
      this.field = Element.down(this.cell,'DIV.csFields') || Element.down(this.cell,'DIV.csLabel');
      if(this.field){
         Element.addClassName(this.field,'csFocus');
      }
   },
   blur: function(el){
      this.cell = CS.v3.getParentElement(el,'TD') || CS.v3.getParentElementWithClass(el,'csRow');
      this.field = Element.down(this.cell,'DIV.csFields') || Element.down(this.cell,'DIV.csLabel');
      if(this.field){
         Element.removeClassName(this.field,'csFocus');
      }
   }
};
/*** Controls ***/
CS.v3.Control = Class.create(CS.v3.Base.prototype, {
   initialize: function(el){
      this.value0 = $F(el);
      this.classNames = $w(el.className);
      this.results = [];
      this.msgs = [];
      this.checked = [];
      this.cell = CS.v3.getParentElement(el,'TD') || CS.v3.getParentElementWithClass(el,'csRow');
      this.field = CS.v3.getParentElementWithClass(el,'csFields');
   },
   msgDisplay: function(el,c,validation,empty){
      var CLS_ERROR = new String('csError');
      var CLS_DONE = new String('csDone');
      if(empty && validation == 'realtime'){
         Element.removeClassName(c.cell,CLS_ERROR);
         Element.removeClassName(c.cell,CLS_DONE);
         return;
      }
      if(c.results.all()){ // all true
         if(c.results != ''){
            Element.addClassName(c.cell,CLS_DONE);
         }
      } else {
         Element.addClassName(c.cell,CLS_ERROR);
         Element.removeClassName(c.cell,CLS_DONE);
         new CS.v3.msgBox(el,c,'error');
      }
   }
});
/*** Get Errors ***/
// Textfield
CS.v3.getTextFieldError = Class.create(CS.v3.Forms, {
   initialize: function(el,validation){
      //this.log('initialize:enter');
      var c = new CS.v3.Control(el);
      c.value1 = null;
      var compareTo = CS.v3.Base.prototype.getCustomAttribute(el,'csCompareTo-');
      var value0Empty = c.value0.empty();
      var value1Empty = null;
      var empty = null;
      if(compareTo != null){
         var target = compareTo[0].toString();
         c.value1 = $F(target);
         value1Empty = c.value1.empty();
      }
      if(value0Empty || value1Empty){
         empty = true;
      }
      var ar = c.classNames;
      var i = ar.length;
      var nam;
      var result;
      var msg;
      var valobj;
      var str;
      while(i--){
         nam = ar[i];
         valobj = CS.v3.Validators[nam];
         if(valobj){
            result = valobj.rule(el,c,nam);
            if(result){
               c.results.push(result);
            } else {
               c.results.push(result);
               str = valobj.error(el,c);
               msg = CS.v3.ValidationErrorMessage(el,nam,str);
               c.msgs.push(msg);
            }
         } else {
            // legacy
            valobj = CS.Validators[nam];
            if(valobj){
               result = valobj.rule(el,c,nam);
               if(result){
                  c.results.push(result);
               } else {
                  c.results.push(result);
                  str = valobj.error(el,c);//legacy did not use ValidationErrorMessage to format
                  c.msgs.push(str);
               }
            }
         }
      }
      c.msgDisplay(el,c,validation,empty);
      //this.log('initialize:exit');
      return this.results = c.results;
   },
   log: function(s,e){
      CS.v3.Logger.log("getTextFieldError:" + s,e);
   }
});
// Checkbox & radio
CS.v3.getCheckError = Class.create(CS.v3.Forms, {
   initialize: function(el,form){
      var c = new CS.v3.Control(el);
      var nameAtt = Element.readAttribute(el,'name');
      var ar = Form.getInputs(form,null,nameAtt);
      this.check = false;
      var i = ar.length;
      var control;
      
      if (Element.hasClassName(el,'ukpws-requiredWhenCmdIs')) {
         var nam = 'ukpws-requiredWhenCmdIs';   
      } else if (Element.hasClassName(el,'csVal-required')) {
         var nam = 'csVal-required';
      } else {
         return null;
      }
      
      var valobj = CS.v3.Validators[nam];      
      
      while(i--){
         control = ar[i];
         if(valobj.rule(control,c)){
            this.check = true;
         }
      }
      if(this.check){
         c.results = [true];
      } else {
         c.results = [false];
         var str = valobj.error(el,c);
         var msg = CS.v3.ValidationErrorMessage(el,nam,str);
         c.msgs[0] = msg;
      }
      c.msgDisplay(el,c);
      return this.results = c.results;
   }
});
// Select
CS.v3.getSelectError = Class.create(CS.v3.Forms, {
   initialize: function(el){
      var c = new CS.v3.Control(el);
      
      if (Element.hasClassName(el,'ukpws-requiredWhenCmdIs')) {
         var nam = 'ukpws-requiredWhenCmdIs';   
      } else if (Element.hasClassName(el,'csVal-required')) {
         var nam = 'csVal-required';
      } else {
         return null;
      }
      
      var obj = CS.v3.Validators[nam];
      this.result = obj.rule(el,c);
      if(this.result){
         c.results = [true];
      } else {
         c.results = [false];
         var msg = obj.error(el,c);
         c.msgs[0] = CS.v3.ValidationErrorMessage(el,nam,msg);
      }
      c.msgDisplay(el,c);
      return this.results = c.results;
   }
});
/*** Get Blockers ***/
CS.v3.getBlockers = Class.create(CS.v3.Forms, {
   initialize: function(el,type,event){
      //this.log('initialize:enter');
      //var c = new CS.v3.Control(el);
      var ar = $w(el.className);//c.classNames;
      var i = ar.length;
      var nam;
      var obj;
      var c;
      var cached = CS.v3.Blockers;
      while(i--){
         nam = ar[i];
         //this.log('initialize:i=' + i + '|name=' + name);
         if(nam.startsWith('maxlength-')){
            if(!c){
               c = new CS.v3.Control(el);
            }
            c.max = CS.v3.Base.prototype.getCustomAttribute(el,'csMaxlength-');
            obj = cached['csMaxlength'];/* TODO verify case */
            obj.rule(el,c);
         }
         obj = cached[nam];
         if(obj){
            if(!c){
               c = new CS.v3.Control(el);
            }
            obj.rule(el,c,event,nam);
         }
      }
   },
   log: function(s,e){
      CS.v3.Logger.log("getBlockers:" + s,e);
   }
});
/*** Boxes ***/
CS.v3.ErrorCountBox = Class.create(CS.v3.Base.prototype, {
   initialize: function(form,elemsInError){
      var fieldErrorMessages = new Array();
      var processedCells = new Object();
      for (var elemIndex = 0; elemIndex < elemsInError.length; elemIndex++) {
         var cell = CS.v3.getParentElement(elemsInError[elemIndex],'TD') || CS.v3.getParentElementWithClass(elemsInError[elemIndex],'csRow');
         if (cell && !processedCells[Element.identify(cell)]) {
            // Find each error message (errMsg) for the current error cell (el) and add a list item, for it
            cell.select('div.csErrorMsg p').each(function(errMsg) {
               fieldErrorMessages.push(errMsg);
            });
            
            // Store this cell id so we only process each cell once (in case multiple fields are in same cell)
            processedCells[Element.identify(cell)] = true;
         }
      }
      
      var OPTS = new String('DIV.csPanelErrors');
      var errorsBox = Element.down(form,OPTS);
      if(errorsBox != null){
         errorsBox.remove();
      }
      var bx = CS.v3.HTML.errorBoxCount(form,fieldErrorMessages.length);
      bx = Element.insert(form,{top: bx});
      errorsBox = Element.down(form,OPTS);
      errorsBox.on('click', CS.v3.ErrorCountBoxClick);
      
      var CSDISABLED = new String('csDisabled');
      var CSBOXHIDE = new String('csBoxHide');
      if(fieldErrorMessages.length >0){
         Element.removeClassName(errorsBox,CSBOXHIDE);
         var errorsCount2 = Element.down(form,'SPAN.csCount');
         Element.update(errorsCount2,fieldErrorMessages.length);
         CS.v3.HTML.errorBoxMessages(bx,fieldErrorMessages);
         Element.addClassName(form,CSDISABLED);

         Element.scrollTo(errorsBox);
         var errorHeading = errorsBox.down('h3');
         errorHeading.tabIndex = '0';
         errorHeading.focus();
      } else {
         Element.removeClassName(form,CSDISABLED);
         Element.addClassName(errorsBox,CSBOXHIDE);
      }
   }
});
CS.v3.ErrorCountBoxClick = function(ev) {
    ev.stop();
    var el = ev.findElement();
    if (el.hasClassName('ukpwsErrorMsgLink')) {
        $(el.readAttribute('data-fieldid')).focus();
    }
    if (el.hasClassName('ukpwsErrorMsgToggle')) {
        if (el.hasClassName('closed')) {
            el.removeClassName('closed')
            this.down('div.ukpwsErrorMsgList').show();
        } else {
            el.addClassName('closed')
            this.down('div.ukpwsErrorMsgList').hide();
        }
    }
};
CS.v3.msgBox = Class.create(CS.v3.Base.prototype, {
   initialize: function(el,c,type){
      var cell = CS.v3.getParentElement(el,'TD') || CS.v3.getParentElementWithClass(el,'csRow');
      if(!cell){
         return;
      }
      var fields = Element.down(cell,'DIV.csFields') || Element.down(cell,'DIV.csLabel');
      if(c.msgs instanceof Array){
         this.msgs = c.msgs.join('');
      } else {
         this.msgs = c.msgs;
      }
      var boxError = Element.down(fields,'.csPanelError');
      var eMsg = Element.down(boxError,'.csErrorMsg');
      var hMsg = Element.down(boxError,'.csHintMsg');
      var hLocal = Element.down(fields,'.csHintLocal');
      var eLocal = Element.down(fields,'.csErrorLocal');
      switch(type){
         case 'error':
            // Determine if we need to replace or insert the error message
            // We need to replace when there is a message already there for group fields (fields with the same name but 
            // different IDs such as radio buttons & checkboxes) because fields are processed in reverse order and we
            // want to show the message for the first field in the group as this is a better experience when the message
            // is displayed in the error message box at the top of the form and the 'click to focus' functionality runs
            if ((boxError.down('p[data-fieldname='+el.name+']'))) {
               Element.update(eMsg,this.msgs);
            } else {
               Element.insert(eMsg,{top: this.msgs});
            }
            break;
         case 'hint':
            Element.update(hMsg,this.msgs);
            break;
         default:
            // NOOP
            break;
      }
      // Local Messages - Update
      var class1;
      var localMsg;
      var localMsgP;
      var storedMsgP;
      var name;
      c.classNames.each(function(name) {
         class1 = 'P.' + name;
         localMsgP = null;
         if(type == 'error' && eLocal){
            localMsgP = $(el.name+'::'+name);
            if(localMsgP){
               // If we have a local message, add some attributes for field name and ID
               // - name is used to prevent duplicate messages for group fields
               // - ID is used for 'click to focus' functionality
               localMsgP.writeAttribute('data-fieldname', el.name);
               localMsgP.writeAttribute('data-fieldid', el.id);
            }
         }
         if(type == 'hint' && hLocal){
            localMsgP = Element.down(hLocal,class1);
         }
         if(localMsgP){
            localMsg = localMsgP.cloneNode(true);
            storedMsgP = $(el.id+'::::'+name); // 'double' double colon set in CS.v3.ValidationErrorMessage
            if(storedMsgP){
               Element.replace(storedMsgP,localMsg);
            }
         }
      });
      // Update with Dynamic values
      var value0 = Element.down(eMsg,'.value0');
      var value1 = Element.down(eMsg,'.value1');
      if(value0){Element.update(value0,c.value0);}
      if(value1){Element.update(value1,c.value1);}
   }
});
/*** Validators ***/
CS.v3.Validators = {
   'tester': {
      rule: function(el,c){return true;},
      error: function(el,c){return CS.v3.I18N.findProperty(el,'val-tester-error'); },
      hint: function(el,c){return CS.v3.I18N.findProperty(el,'val-tester-hint'); }
   },
   'csVal-required': {
      rule: function(el,c){
         var rc = false;
         var b = CS.v3.I18N.isEmptyOrPlaceholderEl(el);
         if(!b){
            rc = true;
         }
         return rc;
      },
      error: function(el,c){return CS.v3.I18N.findProperty(el,'val-required-error'); },
      hint: function(el,c){return CS.v3.I18N.findProperty(el,'val-required-hint'); }
   },
   'csVal-isNumber': {
      rule: function(el,c){
         var b = CS.v3.I18N.isEmptyOrPlaceholderEl(el);
         return b || (!isNaN(c.value0) && (/^[0-9.]+$/.test(c.value0)));
      },
      error: function(el,c){return CS.v3.I18N.findProperty(el,'val-isNumber-error'); },
      hint: function(el,c){return CS.v3.I18N.findProperty(el,'val-isNumber-hint'); }
   },
   'csVal-isDigit': {
      rule: function(el,c){
         var b = CS.v3.I18N.isEmptyOrPlaceholderEl(el);
         return b || (!isNaN(c.value0) && !(/[^\d]/.test(c.value0)));
      },
      error: function(el,c){return CS.v3.I18N.findProperty(el,'val-isDigit-error'); },
      hint: function(el,c){return CS.v3.I18N.findProperty(el,'val-isDigit-hint'); }
   },
   'csVal-isAlpha': {
      rule: function(el,c){
         var b = CS.v3.I18N.isEmptyOrPlaceholderEl(el);
         return b || (/^[A-Z]+$/i.test(c.value0));
      },
      error: function(el,c){return CS.v3.I18N.findProperty(el,'val-isAlpha-error'); },
      hint: function(el,c){return CS.v3.I18N.findProperty(el,'val-isAlpha-hint'); }
   },
   'csVal-isAlphaNum': {
      rule: function(el,c){
         var b = CS.v3.I18N.isEmptyOrPlaceholderEl(el);
         return b || (/^[A-Z0-9]+$/i.test(c.value0));
      },
      error: function(el,c){return CS.v3.I18N.findProperty(el,'val-isAlphaNum-error'); },
      hint: function(el,c){return CS.v3.I18N.findProperty(el,'val-isAlphaNum-hint'); }
   },
   'csVal-isLessThan': {
      rule: function(el,c){
         var rc = false;
         var lessThanVal = parseFloat(CS.v3.Base.prototype.getCustomAttribute(el,'isLessThan-'));
         if((c.value0 == null) || (c.value0.length === 0) || isNaN(c.value0)){
            rc = true;
         } else {
            rc = parseFloat(c.value0) < lessThanVal;
         }
         return rc;
      },
      error: function(el){return CS.v3.I18N.findProperty(el,'val-isLessThan-error'); },
      hint: function(el,c){return CS.v3.I18N.findProperty(el,'val-isLessThan-hint'); }
   },
   'csVal-isGreaterThan': {
      rule: function(el,c){
         var rc = false;
         var greaterThanVal = parseFloat(CS.v3.Base.prototype.getCustomAttribute(el,'isGreaterThan-'));
         if((c.value0 == null) || (c.value0.length === 0) || isNaN(c.value0)){
            rc = true;
         } else {
            rc = parseFloat(c.value0) > greaterThanVal;
         }
         return rc;
      },
      error: function(el,c){return CS.v3.I18N.findProperty(el,'val-isGreaterThan-error'); },
      hint: function(el,c){return CS.v3.I18N.findProperty(el,'val-isGreaterThan-hint'); }
   },
   'csVal-isEmail': {
      rule: function(el,c){
         var rc = false;
         var val = c.value0;
         var b = CS.v3.I18N.isEmpty(val);
         if(b){
            rc = true;
         } else  {
            rc = (/^(?!.*[\`\u00AC\|\^\{\}\[\]~<>\\])[^\.\-@]+([\.-]?[^\.\-@]+)*@[^\.\-@]+([\.-]?[^\.\-@]+)*\.[^\.\-@]{2,6}$/.test(val));
         }
         return rc;
      },
      error: function(el,c){return CS.v3.I18N.findProperty(el,'val-isEmail-error'); },
      hint: function(el,c){return CS.v3.I18N.findProperty(el,'val-isEmail-hint'); }
   },
   'csVal-isMinLength': {
      rule: function(el,c){
         var rc = false;
         //var val = c.value0;
         var minsz = CS.v3.I18N.getCustomAttributeMinsize(el);
         var cursz = CS.v3.I18N.getCurrentSize(el);
         var b = (cursz >= minsz);
         if(b){
            rc = true;
            //c.value0 = cursz;
            //c.value1 = minsz;
         }
         return rc;
      },
      error: function(el,c){return CS.v3.I18N.findProperty(el,'val-isMinLength-error'); },
      hint: function(el,c){return CS.v3.I18N.findProperty(el,'val-isMinLength-hint'); }
   },
   'csVal-isMaxLength': {
      rule: function(el,c){
         var rc = false;
         //var val = c.value0;
         var maxsz = CS.v3.I18N.getCustomAttributeMaxsize(el);
         var cursz = CS.v3.I18N.getCurrentSize(el);
         var b = cursz <= maxsz;
         if(b){
            rc = true;
            //c.value0 = cursz;
            //c.value1 = maxsz;
         }
         return rc;
      },
      error: function(el,c){return CS.v3.I18N.findProperty(el,'val-isMaxLength-error'); },
      hint: function(el,c){return CS.v3.I18N.findProperty(el,'val-isMaxLength-hint'); }
   },
   'csVal-isWeakPin': {
      rule: function(el,c){
         var val = el.value;
         if(CS.v3.I18N.isEmpty(val)){
            // don't validate for empty case
            return true;
         }
         var sb = new StringBuffer();
         var i = val.length - 1;
         var d1;
         var d2 = val.charAt(i);
         while(i--){
            d1 = d2;
            d2 = val.charAt(i);
            sb.append(d2-d1);
         }
         var str = sb.toString(); sb=null;
         var invalidFlag = /(00)|(11)|(-1-1)/.test(str);
         return !invalidFlag;
      },
      error: function(el,c){return CS.v3.I18N.findProperty(el,'val-isWeakPin-error'); },
      hint: function(el,c){return CS.v3.I18N.findProperty(el,'val-isWeakPin-hint'); }
   },
   'csVal-isCurrency': {
      rule: function(el,c){
         var val = el.value;
         if(CS.v3.I18N.isEmpty(val)){
            // don't validate for empty case
            return true;
         }
         var attrs = new CS.v3.getCurrencyFormatAttributes(el);
         val = CS.v3.I18N.trimString(val);
         var sb = new StringBuffer();
         sb.append('^\\d+');
         if(!CS.v3.I18N.isEmpty(attrs.thouStr)){
            sb.append('(\\');
            sb.append(attrs.thouStr);
            sb.append('\\d+)*');
         }
         if(!CS.v3.I18N.isEmpty(attrs.deciStr)){
            sb.append('(\\');
            sb.append(attrs.deciStr);
            sb.append('\\d+)?');
         }
         sb.append('$');
         var str = sb.toString(); sb=null;
         var pattern = new RegExp(str);
         return pattern.test(val);
      },
      error: function(el,c){return CS.v3.I18N.findProperty(el,'val-isCurrency-error'); },
      hint: function(el,c){return CS.v3.I18N.findProperty(el,'val-isCurrency-hint'); }
   },
   'csVal-isCalendarSpaces': {
      rule: function(el,c){
         var rc = false;
         var val = c.value0;
         var b = CS.v3.I18N.isEmpty(val);
         if(b){
            rc = true;
         } else {
            var SPACE = new String(' ');
            // only allow spaces when pattern has spaces
            var pattern = CS.v3.I18N.getCustomAttributePattern(el,'');
            var patternspace = pattern.indexOf(SPACE);
            if(patternspace < 0){ // pattern has no spaces
               var valspace = val.indexOf(SPACE);
               if(valspace <0){ // value has no spaces
                  rc = true;
               } else {
                  rc = false;
               }
            } else {
               rc = true;
            }
         }
         return rc;
      },
      error: function(el,c){return CS.v3.I18N.findProperty(el,'val-isCalendarSpaces-error'); },
      hint: function(el,c){return CS.v3.I18N.findProperty(el,'val-isCalendarSpaces-hint'); }
   },
   'csVal-isCalendarRange': {
      rule: function(el,c){
         var rc = true;
         var val = c.value0;
         var b = CS.v3.I18N.isEmpty(val);
         if(!b){
            var patternInput = CS.v3.I18N.getCustomAttributePattern(el,'');
            var isSupportedFormat = CS.v3.Calendar.isSupportedCalendarFormat(el,patternInput);
            if(isSupportedFormat){
               var patternLength = CS.v3.I18N.getLength(patternInput);
               var valueLength = CS.v3.I18N.getLength(val);
               if(valueLength!=patternLength){
                  // fail if the data length does not match the pattern length as it indicates a problem!
                  rc = false;
               } else {
                  // check that date is within acceptable range
                  var patternPaging = CS.v3.Calendar.getPagingPattern(el);
                  var todayVal = CS.v3.I18N.createDate();
                  var strVal= CS.v3.I18N.parseDateFieldValString(el,val,patternInput);
                  var dtVal = CS.v3.I18N.parseDateFieldVal(el,strVal,patternPaging,todayVal);
                  var minVal = CS.v3.Calendar.getCustomAttributeCalendarInputRangeMin(el);
                  var dtMin = CS.v3.I18N.parseDateFieldVal(el,minVal,patternPaging,todayVal);
                  if(dtVal<dtMin){
                     rc = false;
                  } else {
                     var maxVal = CS.v3.Calendar.getCustomAttributeCalendarInputRangeMax(el);
                     var dtMax = CS.v3.I18N.parseDateFieldVal(el,maxVal,patternPaging,todayVal);
                     if(dtVal>dtMax){
                        rc = false;
                     }
                  }
               }
            }
         }
         return rc;
      },
      error: function(el,c){return CS.v3.I18N.findProperty(el,'val-isCalendarRange-error'); },
      hint: function(el,c){return CS.v3.I18N.findProperty(el,'val-isCalendarRange-hint'); }
   }
/*,
   'csVal-isMod10Card': {
      rule: function(el,c){
         var rc = false;
         var val = c.value0;
         var b = CS.v3.I18N.isEmpty(val);
         if(b){
            rc = true;
         } else  {
            // do validation
         }
         return rc;
      },
      error: function(el,c){return CS.v3.I18N.findProperty(el,'val-isMod10Card-error'); },
      hint: function(el,c){return CS.v3.I18N.findProperty(el,'val-isMod10Card-hint'); }
   }
*/
};
CS.v3.Blockers = {
   'csVal-maxlength': { /* TODO: verify case */
      rule: function(el,c){
         if(c.value0.length > c.max){
            el.value=el.value.substring(0,c.max);
         }
      }
   }
};
/*** Hint ***/
CS.v3.HintToggle = Class.create(CS.v3.Base.prototype, {
   initialize: function(target,event){
      this.target = target;
      var obj = Element.up(target,'.csIconHint');
      this.wrap = obj;
      this.event = Object.clone(event);
      var msg = Element.down(obj,'.csHintMsg');
      if(msg){
         this.content = msg.innerHTML;
      }
   }
});
CS.v3.HintVis = {
   show: function(m){
      var obj = new CS.v3.OverlaysObj(m,m.event); // newly moved
      if(m.event.type==='focus'){
         Element.addClassName(m.target,'cskeyFocus');
         //m.target.id = 'cskeyHintFocus';
      }
      Element.update(CS.v3.panelHintScroll,m.content);
      //var align = Element.readAttribute(m.target,'data-alignOverlay');
      CS.v3.ContainerHint.style.left = obj.left - obj.containerWrapLeft + 'px';
      CS.v3.ContainerHint.style.top = obj.top + 'px';
      CS.v3.hintActive = obj.target;
      CS.v3.ContainerHint.style.display = 'block';

      // Code to shift hint panel left/right when at edge of page
      CS.v3.csHintChevron.removeClassName('csChevronTopRight');
      CS.v3.csHintChevron.removeClassName('csChevronTopLeft');
      var chevronFix = 15; // Offset factor to get chevron positioned under the hint icon - can't get this from styles AFAIK
      if (obj.left + CS.v3.csPanelHintWrap.offsetLeft < 0) { // offsetLeft comes form CSS, and is a negative number (hence maths looks backeards)
         // Left edge is off page, so shift right
         CS.v3.csHintChevron.addClassName('csChevronTopLeft');
         obj.left = obj.left - CS.v3.csPanelHintWrap.offsetLeft - chevronFix;
         CS.v3.ContainerHint.style.left = obj.left - obj.containerWrapLeft + 'px';
      } else if (obj.left + CS.v3.csPanelHintWrap.getWidth() > document.viewport.getWidth()) {
         // Right  edge is off page, so shift left
         CS.v3.csHintChevron.addClassName('csChevronTopRight');
         obj.left = obj.left - (CS.v3.csPanelHintWrap.getWidth() / 2) + chevronFix;
         CS.v3.ContainerHint.style.left = obj.left - obj.containerWrapLeft + 'px';
      }
///      CS.v3.ContainerHint.style.top = (parseInt(CS.v3.ContainerHint.style.top) - CS.v3.ContainerHint.down().getHeight()) + 'px';
   },
   hide: function(m){
      var event = m.event;
      var toEl;
      if(Prototype.Browser.IE){
         toEl = event.toElement;
      } else {
         toEl = $(event.relatedTarget);
      }
      var myid = Element.identify(toEl);
      if(myid != 'csPanelHintWrap'){
         CS.v3.ContainerHint.style.display = 'none';
      }
   },
   blur: function(m){
      //var event = m.event;
      Element.removeClassName(m.target,'cskeyFocus');
      //m.target.id = 'cskeyHintFocus';
      CS.v3.ContainerHint.style.display = 'none';
   }
};
/* IE List Boxes */
// For IE 6
CS.csIeListBoxMouseDown = function(key){
   if(Prototype.Browser.IE){
      key.style.width = 'auto';
   }
};
if(Prototype.Browser.IE){
   CS.v3.Components.ieListBox = function(el,options){
      new CS.v3.Config.ieListBox(el,options);
   };
}
CS.v3.Config.ieListBox = Class.create(CS.v3.Base.prototype, {
   initialize: function(el,options){
      this.ieListBox = Element.select(el,"select[class*='csIeListBox']");
      this.ieDisabled = Element.select(el,"select");
      this.bind();
   },
   bind: function(){
      var ar = this.ieListBox;
      var i = ar.length;
      var obj;
      while(i--){
         obj = ar[i];
         /* Note: Reguires another keydown observe method elsewhere up the DOM  */
         Event.observe(obj,'keydown', function(event){
            var target = $(Event.element(event));
            var key = event.keyCode;
            if((key===Event.KEY_DOWN) && event.altKey){
               this._showOptions(target);
            }
         }.bind(this));
         // Is required also
         Event.observe(obj,'mousedown', function(event){
            //var target = $(Event.element(event));
            //this._showOptions(target);
         }.bind(this));
         Event.observe(obj,'focus', function(event){
            var target = $(Event.element(event));
            var keyMsg = Element.next(target,'.csKeyMsg-widen');
            Element.addClassName(keyMsg,'csShowKeyMsg');
         }.bind(this));
         Event.observe(obj,'blur', function(event){
            var target = $(Event.element(event));
            this._hideOptions(target);
            var keyMsg = Element.next(target,'.csKeyMsg-widen');
            Element.removeClassName(keyMsg,'csShowKeyMsg');
         }.bind(this));
         Event.observe(obj,'change', function(event){
            var target = $(Event.element(event));
            this._hideOptions(target);
         }.bind(this));
      }
      var disar = this.ieDisabled;
      var i2 = disar.length;
      while(i2--){
         obj = disar[i2];
         Event.observe(obj,'focus', function(event){
            var target = $(Event.element(event));
            this._disabledFocus(target);
         }.bind(this));
         Event.observe(obj,'change', function(event){
            var target = $(Event.element(event));
            this._disabledChange(target);
         }.bind(this));
      }
   },
   _showOptions: function(target){
      target.style.width = "auto";
   },
   _hideOptions: function(target){
      target.style.width = "";
   },
   _disabledFocus: function(target){
      this.index = target.selectedIndex;
      CS.v3.SelectedState = this.index;
   },
   _disabledChange: function(target){
      try{
         this.index = target.selectedIndex;
         this.option = target.options[this.index];
         this.optionExt = CS.v3.castToEl(this.option);
         if(Element.hasClassName(this.optionExt,'csDisabled')){
            target.selectedIndex = target.selectedIndex[CS.v3.SelectedState];
         }
      }
      catch(e){//ie 6 error catch
         this.log('disabledChange:',e);
      }
   },
   log: function(s,e){
      CS.v3.Logger.log("ieListBox:" + s,e);
   }
});
/* NOTE: Telephony and Dropdown were similar, merged as of 3.2 */
CS.v3.Components.DropdownSubmit = function(el,options){
   new CS.v3.Config.DropdownSubmit(el,options);
};
CS.v3.Config.DropdownSubmit = Class.create(CS.v3.Base.prototype, {
   initialize: function(el,options){
      this.allDropdownSubmit = Element.select(el,["select[class*='csDropdownSubmit']","select[class*='csSelectTelephony']"]);
      if(options.bind==='true'){this.bind();}
   },
   bind: function(){
      var ar = this.allDropdownSubmit;
      var i = ar.length;
      var el;
      while(i--){
         el = ar[i];
         CS.v3.DropdownSubmit.addDecoration(el);
      }
   }
});
CS.v3.DropdownSubmit = {
   addDecoration: function(el){
      var FLAG = new String('csDropdownSubmit-init');
      var b=Element.hasClassName(el,FLAG);
      if(!b){
         Element.addClassName(el,FLAG);
         //selEl.removeAttribute('name'); // remove these as they no purpose here!
         this.forceSingle(el);
         Event.observe(el,'change',function(event){ this.changeEv(event); }.bind(this));
      }
   },
   forceSingle:function(selEl){
      // force to be single (not MULTIPLE!) dropdowns
      CS.v3.I18N.updateAttribute(selEl,'size','1');
      CS.v3.I18N.updateAttribute(selEl,'multiple','');
   },
   changeEv: function(event){
      var el = $(Event.element(event));
      this._Options(el);
      Event.stop(event);
   },
   _Options: function(selEl){
      var optValu = CS.v3.I18N.getDropdownSelectedValue(selEl);
      var b = CS.v3.I18N.isEmpty(optValu);
      if(!b){
         var frmEl = CS.v3.getParentElement(selEl,'FORM');
         if(frmEl){
            var submitInput = this.findSubmitInput(selEl,frmEl);
            var submitName = Element.readAttribute(submitInput,'name');
            CS.v3.insertHiddenInput(frmEl,submitName,'','');
         }
         CS.v3.submitForm(frmEl);
      }
   },
   findSubmitInput: function(selEl,frmEl){
      var rc;
      var myid = CS.v3.I18N.getDataAttribute(selEl,'data-csbuttontarget','');
      if(!CS.v3.I18N.isEmpty(myid)){
         rc = CS.v3.castToEl(myid);
      }
      if(CS.v3.I18N.isEmpty(rc)){
         var ar = Element.select($(frmEl),["input[class*='csDropdownSubmitButton']","input[class*='csTelephonyButton']"]);
         var i = ar.length;
         if(i>0){
            rc = ar[0];
         }
      }
      //var btnid = Element.identify(rc);
      //this.log('finsSubmitInput:rc=' + rc + '|btnid='+ btnid);//
      return rc;
   },
   log: function(s,e){
      CS.v3.Logger.log("DropdownSubmit:" + s,e);
   }
};
/* Textarea */
CS.v3.Components.Textarea = function(el,options){new CS.v3.Config.Textarea(el,options); };
CS.v3.Config.Textarea = Class.create(CS.v3.Base.prototype, {
   initialize: function(el,options){
      this.el = el;
      this.allTextareas = CS.v3.Textarea.findAllTextareas(el);
      if(options.bind==='true'){this.bind();}
   },
   bind: function(){
      var el = this.el;
      var tag = el.tagName;
      if(tag==='BODY'){
         //this.allTextareas = CS.v3.Textarea.findAllTextareas(el);
         var ar = this.allTextareas;
         var i = ar.length;
         var obj;
         while(i--){
            obj = ar[i];
            CS.v3.Textarea.setup(obj);
         }
      }
   }
});
CS.v3.Textarea = {
   getClazz: function(){
      return new String('CS.v3.Textarea');
   },
   setup: function(el){
      if(el){
         //var tag = el.tagName;
         //if(tag==='TEXTAREA'){
            var maxsize = CS.v3.I18N.getCustomAttributeMaxsize(el);
            if(maxsize>0){
               this.addDecoration(el);
            }
         //}
      }
   },
   findAllTextareas: function(el){
      var rc;
      if(el){
         //NOTE: this component may also be used on INPUT tags if identified here.
         //TODO: remove use of 'csTextArea' (bad uppercase 'A')
         rc = Element.select(el,["textarea[class*='csTextarea']","textarea[class*='csTextArea']","input[class*='csTextarea']"]);
      }
      return rc;
   },
   keyupEv: function(event){
      var el = $(Event.element(event));
      if(el){
         this.checkLength(el,true);
      }
   },
   resetFormTextareaEv: function(event){
      var el = $(Event.element(event));
      if(el){
         var ar = this.findAllTextareas(el);
         if(ar){
            var i = ar.length;
            var obj;
            while(i--){
               obj = ar[i];
               this.updateMessage(obj);
            }
         }
      }
   },
   checkLength: function(el,isTrim){
      var maxsize = CS.v3.I18N.getCustomAttributeMaxsize(el);
      if(maxsize>0){
         var size = CS.v3.I18N.getCurrentSize(el);
         if(size > maxsize){
            if(isTrim){
               var str = CS.v3.I18N.getValueUnmasked(el,true);
               var myval = CS.v3.I18N.truncateString(str,maxsize);
               CS.v3.I18N.setValue(el,myval);
            } else {
               var tr = CS.v3.getParentElement(el,'TR');
               if(tr){
                  CS.v3.I18N.addError(tr);
               }
            }
         }
         this.updateMessage(el);
      }
   },
   updateMessage: function(el){
      var maxsize = CS.v3.I18N.getCustomAttributeMaxsize(el);
      var cursize = CS.v3.I18N.getCurrentSize(el);
      var remainsize = maxsize - cursize;
      var msgmax = this.getMessageMaxsize(el,maxsize);
      var msgremain = this.getMessageRemaining(el,remainsize);
      var sb = new StringBuffer();
      var b = CS.v3.I18N.isEmpty(msgmax);
      if(b){
         //OH 1.4
         sb.append('<p class="csCounter">');
         sb.append(msgremain);
         sb.append('</p>');
      } else {
         // legacy OH 1.3
         sb.append('<div class="csRow csCols-2"><div class="csCol-1">');
         sb.append(msgmax);
         sb.append('</div><div class="csCol-2">');
         sb.append(msgremain);
         sb.append('</div>');
      }
      var msg = sb.toString(); sb=null;
      var wrap = Element.up(el,'DIV.csTextarea-wrapper');
      if(wrap){
         var span = Element.down(wrap,'DIV.csTextarea-display');
         if(span){
            CS.v3.updateFix(span,msg);
         }
      }
   },
   addDecoration: function(el){
      var FLAG = new String('csTextarea-init');
      var b=Element.hasClassName(el,FLAG);
      if(!b){
         CS.v3.I18N.initialize(el);
         Event.observe(el,'keyup', function(event){ this.keyupEv(event); }.bind(this));
         var CLAZZ = this.getClazz();
         var act = CLAZZ + '.keyupEv(event);';
         CS.v3.I18N.updateAttribute(el,'onkeyup',act);// NOTE: this seems to be required for TableAddRow
         Element.addClassName(el,FLAG);
         var div = CS.v3.getParentElement(el,'DIV');
         Element.addClassName(div,'csTextarea-wrapper');
         var node = Element.up(el,'DIV.csTextarea-wrapper');
         Element.insert(node,{bottom:'<div class="csTextarea-display"></div>'});
         var frm = CS.v3.getParentElement(el,'FORM');
         if(frm){
            if(!Element.hasClassName(frm,FLAG)){
               Element.addClassName(frm,FLAG);
               Event.observe(frm,'reset',function(event){ this.resetFormTextareaEv(event); }.bind(this));
            }
         }
      }
      this.updateMessage(el);
   },
   getMessageRemaining: function(el,val){
      var def = CS.v3.I18N.findProperty(el,'textarea-messageRemain');
      var mask = CS.v3.I18N.findProperty(el,'textarea-messageMask');
      var str = CS.v3.I18N.getDataAttribute(el,'data-csmessageremain',def);
      var rc = CS.v3.I18N.replaceString(str,mask,val);//str.replace(mask,val);
      return rc;
   },
   getMessageMaxsize: function(el,val){
      var def = CS.v3.I18N.findProperty(el,'textarea-messageMaxsize');
      var mask = CS.v3.I18N.findProperty(el,'textarea-messageMask');
      var str = CS.v3.I18N.getDataAttribute(el,'data-csmessagemaxsize',def);
      var rc = CS.v3.I18N.replaceString(str,mask,val);//str.replace(mask,val);
      return rc;
   },
   log: function(s,e){
      CS.v3.Logger.log("Textarea:" + s,e);
   }
};
/* format */
CS.v3.Components.Format = function(el,options){ new CS.v3.Config.Format(el,options); };
CS.v3.Config.Format = Class.create(CS.v3.Base.prototype, {
   initialize: function(el,options){
      this.allFormatCurr = Element.select(el,"fieldset[class*='csFormatCurrency']");
      this.allFormatInput = Element.select(el,"input[class*='csFormat']");
      if(options.bind==='true'){this.bind();}
   },
   bind: function(){
      var ar = this.allFormatCurr;
      var i = ar.length;
      var el;
      while(i--){
         el = ar[i];
         CS.v3.Format.setupCurrency(el);
      }
      // now for text fields
      ar = this.allFormatInput;
      i = ar.length;
      while(i--){
         el = ar[i];
         CS.v3.Format.setupFormat(el);
      }
   }
});
CS.v3.Format = {
   setupCurrency: function(el){
      CS.v3.I18N.initialize(el);
      var input = this.findFields(el);
      //this.log('setupCurrency:inp=' + input);
      if(input){
         CS.v3.I18N.initialize(input);
         //Element.wrap(input,'DIV',{'class':'csFormat-wrapper'} );
         var pattern = this.createPattern(input);
         //this.log('setupCurrency:pattern=' + pattern);
         try{
            CS.v3.I18N.setCustomAttributePattern(input,pattern);
         }
         catch(e){
            this.log('setupCurrency:',e);
         }
      }
   },
   findFields: function(el){
      var rc = Element.select(el,'input[type=text]');//NOTE: update for HTML5
      return rc;
   },
   setupFormat: function(input){
      //var input = Element.select(el,'input[type=text]');
      if(input){
         var FLAG = new String('csFormat-init');
         var b = Element.hasClassName(input,FLAG);
         if(!b){
            CS.v3.I18N.initialize(input);
            var pattern = CS.v3.I18N.getCustomAttributePattern(input,'');
            //this.log('setupFormat:pattern=' + pattern);
            var bp = CS.v3.I18N.isEmpty(pattern);
            if(!bp){
               Element.addClassName(input,FLAG);
               CS.v3.I18N.updateAttribute(input,'title',pattern);
               this.reformat(input);
               Event.observe(input,'blur', function(event){ this.blurEv(event); }.bind(this));
               Event.observe(input,'focus', function(event){ this.focusEv(event); }.bind(this));
            }
         }
      }
   },
   reformat: function(el){
      try{
         var FILLER = new String(' ');
         var HASH = new String('#');
         var sourceRaw = this.parseInputAmount(el);
         var patternRaw = CS.v3.I18N.getCustomAttributePattern(el,'');
         var sourceTrim = CS.v3.I18N.trimString(sourceRaw);
         var sourceSB = CS.v3.I18N.reverseString(sourceTrim);
         var patternTrim = CS.v3.I18N.trimString(patternRaw);
         var patternSB = CS.v3.I18N.reverseString(patternTrim);
         //this.log('reformat:val2=' + sourceSB + '|pattern2=' + patternSB);
         var sourceSize = CS.v3.I18N.getLength(sourceSB);
         var patternSize = CS.v3.I18N.getLength(patternSB);
         var j = 0;
         var i = 0;
         var cSource;
         var cPattern;
         while(i<=patternSize){
            cSource = FILLER;
            cPattern = CS.v3.I18N.getCharAt(patternSB,i);
            if(cPattern == HASH){
               if(j<sourceSize){
                  cSource = CS.v3.I18N.getCharAt(sourceSB,j);
               } else {
                  cSource = FILLER;
               }
               j++;
               patternSB = CS.v3.I18N.setCharAt(patternSB,i,cSource);
            } else {
               if(j>sourceSize){
                  cPattern = FILLER;
                  patternSB = CS.v3.I18N.setCharAt(patternSB,i,cSource);
               }
            }
            i++;
         }
         /* EXTRA?...
         while(j<sourceSize){
            patternSB += CS.v3.I18N.getCharAt(sourceSB,j);
            j++;
         }
         */
         var raw = CS.v3.I18N.reverseString(patternSB);
         var rc = CS.v3.I18N.trimString(raw);
         //this.log('reformat:rc=' + rc);
         CS.v3.I18N.setValue(el,rc);
      }
      catch(e){
         this.log('reformat:',e);
      }
   },
   focusEv: function(event){
      var el = $(Event.element(event));
      try{
         var val = this.parseInputAmount(el);
         CS.v3.I18N.setValue(el,val);
      }
      catch(e){
         this.log('focusEv:',e);
      }
   },
   blurEv: function(event){
      var el = $(Event.element(event));
      this.reformat(el);
   },
   getCustomAttributeThousandSeparator: function(el){
      var def = CS.v3.I18N.findProperty(el,'format_thousand_separator');//',';
      var rc = CS.v3.I18N.getDataAttribute(el,'data-csthousandseparator',def);
      return rc;
   },
   getCustomAttributeDecimalSeparator: function(el){
      var def = CS.v3.I18N.findProperty(el,'format_decimal_separator');//'.';
      var rc = CS.v3.I18N.getDataAttribute(el,'data-csdecimalseparator',def);
      return rc;
   },
   getCustomAttributeNumberOfDecimals: function(el){
      var def = CS.v3.I18N.findProperty(el,'format_number_of_decimals');//'2';
      var rc = CS.v3.I18N.getDataAttribute(el,'data-csnumberofdecimals',def);
      return rc;
   },
   getCustomAttributeGrouping: function(el){
      var def = CS.v3.I18N.findProperty(el,'format_grouping');//'3';
      var rc = CS.v3.I18N.getDataAttribute(el,'data-csgrouping',def);
      return rc;
   },
   getCustomAttributeZero: function(el){
      var def = CS.v3.I18N.findProperty(el,'format_zero');//'0';
      var rc = CS.v3.I18N.getDataAttribute(el,'data-cszero',def);
      return rc;
   },
   createPattern: function(el){
      var sb = new StringBuffer();
      try{
         var nodStr = this.getCustomAttributeNumberOfDecimals(el);
         var deciStr = this.getCustomAttributeDecimalSeparator(el);
         var thouStr = this.getCustomAttributeThousandSeparator(el);
         var groupStr = this.getCustomAttributeGrouping(el);
         var zeroStr = this.getCustomAttributeZero(el);
         var HASH = new String("#");
         var maxgroups = 4;
         var group = parseInt(groupStr,10);
         var nod = parseInt(nodStr,10);
         for(var i=0;i<maxgroups;++i){
            if(i>0){
               sb.append(thouStr);
            }
            for(var g=0;g<group;++g){
               sb.append(HASH);
            }
         }
         if(nod>0){
            sb.append(deciStr);
         }
         for(var d=0;d<nod;++d){
            sb.append(HASH);
         }
      }
      catch(e){
         this.log('createPattern:',e);
      }
      var rc = sb.toString(); sb=null;
      return rc;
   },
   parseInputAmount: function(el){
      var EMPTY = new String('');
      var rc = EMPTY;
      var raw = CS.v3.I18N.getValueUnmasked(el,true);
      var x = CS.v3.I18N.trimString(raw);
      var b = CS.v3.I18N.isEmpty(x);
      if(!b){
         var ar = CS.v3.I18N.findProperty(el,'format_remove');
         var i = ar.length;
         if(i>0){
            var c;
            while(i--){
               c = ar[i];
               x = CS.v3.I18N.replaceString(x,c,EMPTY);
            }
         }
         rc = x;
      }
      return rc;
   },
   formatNumber: function(el,val, decimal, delim_thou, delim_frac){
      //decimal  - the number of decimals after the digit from 0 to 3
      //-- Returns the passed number as a string in the xxx,xxx.xx format.
      var ZERO = CS.v3.I18N.getCustomAttributeZero(el);//'0'
      var EMPTY = new String('');
      var divider;
      switch(decimal){
         case 0: divider=1; break;
         case 1: divider=10; break;
         case 2: divider=100; break;
         case 3: divider=1000; break;
         case 4: divider=10000; break;
         case 5: divider=100000; break;
         case 6: divider=1000000; break;
         default: //for 3 decimal places
            divider=1000;
            break;
      }
      var workNum=Math.abs((Math.round(val*divider)/divider));
      var workStr=EMPTY+workNum;
      if(workStr.indexOf(delim_frac)==-1){
         workStr+=delim_frac;
      }
      var dStr=workStr.substr(0,workStr.indexOf(delim_frac));
      dNum=dStr-0;
      var pStr=workStr.substr(workStr.indexOf(delim_frac));
      while (pStr.length-1< decimal){
         pStr+=ZERO;
      }
      if(pStr == delim_frac){ pStr = EMPTY; }
      var dLen = 0;
      //--- Adds a comma in the thousands place.
      if(dNum>=1000){
         dLen=dStr.length;
         dStr=parseInt(EMPTY+(dNum/1000),10) + delim_thou + dStr.substring(dLen-3,dLen);
      }
      //-- Adds a comma in the millions place.
      if(dNum>=1000000){
         dLen=dStr.length;
         dStr=parseInt(EMPTY+(dNum/1000000),10) + delim_thou + dStr.substring(dLen-7,dLen);
      }
      var rc = dStr + pStr;
      //-- Put numbers in parentheses if negative.
      //if(val<0){ rc='('+rc+')'; }
      //You could include a dollar sign in the return value.
      //rc =  "$"+rc;
      return rc;
   },
   removeCents:function(el,decimals,delim_thou,delim_frac){
      var val = el.value;
      var pos = val.indexOf(delim_frac);
      var str;
      if(pos>-1){
         str = val.substr(0,val.indexOf(delim_frac));
      } else {
         str = val;
      }
      obj.value = this.formatNumber(el,str,decimal,delim_thou,delim_frac);
   },
   validateAmount:function(el, min, mineq, max, maxeq, msgOperator, msgString){
      /* TODO = legacy - remove? */
      var isModifiable = true;
      if(min != null && !obj.value > min){
         isModifiable = false;
      } else if(max != null && !obj.value < max){
         isModifiable = false;
      }else if(mineq != null && !obj.value >= mineq){
         isModifiable = false;
      }else if(maxeq != null && !obj.value <= maxeq){
         isModifiable = false;
      }
      var rc=true;
      if(!isModifiable){
         rc = this.confirmSubmit("You are only authorised to enter value " + msgOperator + " " + msgString + " in the amount field. Do you still want to continue?");// TODO i18n?
      }
      return rc;
   },
   confirmSubmit: function(msg){
      /* TODO = legacy - remove? */
      var rc = confirm(msg);
      return rc;
   },
   log: function(s,e){
      CS.v3.Logger.log("Format:" + s,e);
   }
};
CS.v3.Components.DependentDropdowns = function(el,options){new CS.v3.Config.DependentDropdowns(el,options); };
CS.v3.Config.DependentDropdowns = Class.create(CS.v3.Base.prototype, {
   initialize: function(el,options){
      this.allDependentDropdowns = CS.v3.DependentDropdowns.findAllDependentDropdowns(el);
      if(options.bind==='true'){this.bind();}
   },
   bind: function(){
      var ar = this.allDependentDropdowns;
      var ar2 = this.bindSelects(ar);
      this.bindParents(ar2);
   },
   bindSelects: function(ar){
      var i = ar.length;
      var divEl;
      var b;
      var par;
      var rc = [];
      while(i--){
         divEl = ar[i];
         CS.v3.DependentDropdowns.setup(divEl /*,'dddiv' + i */);
         par = CS.v3.DependentDropdowns.getCustomAttributeParent(divEl);
         b = CS.v3.I18N.isEmpty(par);
         if(b){
            rc.push(divEl);
         }
      }
      return rc;
   },
   bindParents: function(ar){
      var i = ar.length;
      var divEl;
      var nam;
      var namlast = null;
      if(i>1){
         ar.sort(); // only sort if more than 1, we need this to dedupe
      }
      while(i--){
         divEl = ar[i];
         nam = CS.v3.DependentDropdowns.getCustomAttributeName(divEl);
         if(nam!=namlast){
            CS.v3.DependentDropdowns.showChildDivsRecurseTop(divEl);
            namlast = nam;
         }
      }
      // do last one too!
      if(nam!=namlast){
         CS.v3.DependentDropdowns.showChildDivsRecurseTop(divEl);
      }
      CS.v3.DependentDropdowns.changeObservation(divEl);
   }
});
CS.v3.DependentDropdowns = {
   changeObservation: function(el){
      var ardd = this.findTopDependentDropdown(el);
      var tmp = ardd.length;
      if(tmp>1){
         this.log('changeObservation:i=' + tmp + ' should be just 1 in FORM');
      }
      var x = ardd[0];
      if(x){
         var frmEl = this.findFormUpEl(x);
         var ar = this.findObservers(frmEl);
         if(ar){
            var i = ar.length;
            var val = this.observeChildDivsRecurse(x);
            var obj;
            while(i--){
               obj = ar[i];
               CS.v3.I18N.setValue(obj,val);
               CS.v3.fireOnchange(obj);
            }
         }
      }
   },
   setup: function(divEl){
      var divId = Element.identify(divEl);
      var ar = Element.select(divEl,'select');
      var i = ar.length;
      var selEl;
      while(i--){
         selEl = ar[i];
         this.decorateSelect(divEl,selEl);
         Event.observe(selEl,'change', function(event){ this.changeEv(event); }.bind(this));
      }
   },
   hideChildDivsRecurse: function(divEl){
      var childNam = this.getCustomAttributeChild(divEl);
      try{
         var b = CS.v3.I18N.isEmpty(childNam);
         if(!b){
            var ar = this.findFormDropdownDivs(divEl);
            var i = ar.length;
            var objdiv;
            var objchild;
            var objname;
            while(i--){
               objdiv = ar[i];
               objname = this.getCustomAttributeName(objdiv);
               if(objname == childNam){
                  this.hideChildSelects(objdiv);
                  this.hideChildDivsRecurse(objdiv);
               }
            }
         }
      }
      catch(e){
         this.log('hideChildDivsRecurse:',e);
      }
   },
   hideChildSelects: function(divEl){
      var ar = this.findDivSelects(divEl);
      var i = ar.length;
      var selobj;
      while(i--){
         selobj = ar[i];
         this.hideSelect(selobj);
      }
   },
   decorateSelect: function(divEl,selEl,suffix){
      try{
         Element.addClassName(selEl,'csDependentSelect');
         this.forceSingle(selEl);
         var id = Element.identify(selEl);
         var selVal = CS.v3.I18N.getDropdownSelectedValue(selEl);
         // (begin) clone DIV attributes to the select
         var parentKey = this.getCustomAttributeParent(divEl);
         this.setCustomAttributeParent(selEl,parentKey);
         var childKey = this.getCustomAttributeChild(divEl);
         this.setCustomAttributeChild(selEl,childKey);
         var nameKey = this.getCustomAttributeName(divEl);
         this.setCustomAttributeName(selEl,nameKey);
         //var valueKey = this.getCustomAttributeValue(divEl);
         //this.setCustomAttributeValue(selEl,valueKey);
         // (end) clone DIV attributes to the select
         //this.log('decorateSelect:selVal=' + selVal);
         //var b = CS.v3.I18N.isEmpty(selVal);
         //if(b){
            //var selected = selEl.selectedIndex;
            //selEl.selectedIndex = selected +1;
            //this.log('decorateSelect:changedSelected');
         //}
         this.hideChildDivsRecurse(divEl);
      }
      catch(e){
         this.log('decorateSelect:',e);
      }
   },
   forceSingle:function(selEl){
      // force to be single (not MULTIPLE!) dropdowns
      CS.v3.I18N.updateAttribute(selEl,'size','1');
      CS.v3.I18N.updateAttribute(selEl,'multiple','');
   },
   showSelect: function(selEl){
      selEl.removeAttribute('disabled');
      var divEl = this.findDivElUp(selEl);
      var nam = this.getCustomAttributeName(divEl);
      CS.v3.showFunction(divEl);
      CS.v3.I18N.updateAttribute(selEl,'name',nam);
      CS.v3.showFunction(selEl);
   },
   hideSelect: function(selEl){
      var par = this.getCustomAttributeParent(selEl);
      var b = CS.v3.I18N.isEmpty(par);
      if(!b){
         CS.v3.I18N.updateAttribute(selEl,'disabled','disabled');
         var nam = Element.readAttribute(selEl,'name');
         var bn = CS.v3.I18N.isEmpty(nam);
         if(!bn){
            // these should NOT be defined, but we will save it here (but not use it)
            this.setCustomAttributeNameOriginal(selEl,nam);
            selEl.removeAttribute('name');
         }
         CS.v3.hideFunction(selEl);
      }
   },
   changeEv: function(event){
      var selEl = $(Event.element(event));
      this.updateDD(selEl);
      this.changeObservation(selEl);
   },
   updateDD: function(selEl){
      try{
         var divEl = this.findDivElUp(selEl);
         this.hideChildDivsRecurse(divEl);
         var frmEl = this.findFormUpEl(selEl);
         var selChildKey = this.getCustomAttributeChild(selEl);
         var val = CS.v3.I18N.getDropdownSelectedValue(selEl);
         var divar = this.findAllDependentDropdownsName(frmEl,selChildKey);
         var i = divar.length;
         var divobj;
         var selar;
         var j;
         var selobj;
         var selval;
         while(i--){
            divobj = divar[i];
            divid = Element.identify(divobj);
            selar = this.findDivSelects(divobj);
            j = selar.length;
            while(j--){
               selobj = selar[j];
               selval = this.getCustomAttributeValue(selobj);
               if(selval == val){
                  this.showSelect(selobj);
               } else {
                  this.hideSelect(selobj);
               }
            }
         }
      }
      catch(e){
         this.log('updateDD:',e);
      }
   },
   setCustomAttributeChild: function(el,val){
      CS.v3.I18N.setDataAttribute(el,'data-csdropdownchild',val);
   },
   getCustomAttributeChild: function(el){
      var rc = CS.v3.I18N.getDataAttribute(el,'data-csdropdownchild','');
      return rc;
   },
   setCustomAttributeParent: function(el,val){
      CS.v3.I18N.setDataAttribute(el,'data-csdropdownparent',val);
   },
   getCustomAttributeParent: function(el){
      var rc = CS.v3.I18N.getDataAttribute(el,'data-csdropdownparent','');
      return rc;
   },
   setCustomAttributeName: function(el,val){
      CS.v3.I18N.setDataAttribute(el,'data-csdropdownname',val);
   },
   getCustomAttributeName: function(el){
      var rc = CS.v3.I18N.getDataAttribute(el,'data-csdropdownname','');
      return rc;
   },
   setCustomAttributeNameOriginal: function(el,val){
      CS.v3.I18N.setDataAttribute(el,'data-csdropdownnameoriginal',val);
   },
   getCustomAttributeNameOriginal: function(el){
      var rc = CS.v3.I18N.getDataAttribute(el,'data-csdropdownnameoriginal','');
      return rc;
   },
   setCustomAttributeValue: function(el,val){
      CS.v3.I18N.setDataAttribute(el,'data-csdropdownvalue',val);
   },
   getCustomAttributeValue: function(el){
      var rc = CS.v3.I18N.getDataAttribute(el,'data-csdropdownvalue','');
      return rc;
   },
   findAllDependentDropdowns: function(el){
      var rc;
      if(el){
         rc = Element.select(el,"div[class*='csDependentDropdowns']");
      }
      return rc;
   },
   findTopDependentDropdown: function(el){
      var rc = [];
      var frmEl = this.findFormUpEl(el);
      var ar = this.findAllDependentDropdowns(frmEl);
      if(ar){
         var obj;
         var par;
         var i = ar.length;
         while(i--){
            obj = ar[i];
            par = this.getCustomAttributeParent(obj);
            if(par == ''){
               rc.push(obj);
            }
         }
      }
      return rc;
   },
   findAllDependentDropdownsName: function(fieldsEl,namVal){
      var rc = [];
      var ar = this.findAllDependentDropdowns(fieldsEl);
      var obj;
      var nam;
      var i = ar.length;
      while(i--){
         obj = ar[i];
         nam = this.getCustomAttributeName(obj);
         if(nam == namVal){
            rc.push(obj);
         }
      }
      return rc;
   },
   findFormUpEl: function(el){
      var rc;
      try{
         if(el){
            rc = CS.v3.getParentElement(el,'FORM');
         }
      }
      catch(e){
         this.log('findDivFieldsUpEl:el=' + el,e);
      }
      return rc;
   },
   findDivElUp: function(el){
      var rc;
      if(el){
         rc = Element.up(el,'DIV.csDependentDropdowns');
      }
      return rc;
   },
   findObservers: function(el){
      var rc;
      if(el){
         rc = Element.select(el,'INPUT.csDependentSelectObserver');//can remove the INPUT to make more flexible!
      }
      return rc;
   },
   findFormDropdownDivs: function(divEl){
      var rc;
      var frmEl = this.findFormUpEl(divEl);
      if(frmEl){
         rc = Element.select(frmEl,'DIV.csDependentDropdowns');
      }
      return rc;
   },
   findDivSelects: function(divEl){
      var rc;
      if(divEl){
         rc = Element.select(divEl,'SELECT.csDependentSelect');
      }
      return rc;
   },
   showChildDivsRecurseTop: function(divEl){
      var ar = this.findDivSelects(divEl);
      var i = ar.length;
      if(i!==1){
         this.log('showChildDivsRecurseTop:i=' + i + ' should be 1');
      }
      var selEl = ar[0];
      var selVal = CS.v3.I18N.getDropdownSelectedValue(selEl);
      this.showChildDivsRecurse(divEl,selVal);
   },
   showChildDivsRecurse: function(divEl,selVal){
      var childNam = this.getCustomAttributeChild(divEl);
      try{
         var b = CS.v3.I18N.isEmpty(childNam);
         if(!b){
            var ar = this.findFormDropdownDivs(divEl);
            var i = ar.length;
            var objdiv;
            var objchild;
            var objname;
            var dummyVal;
            while(i--){
               objdiv = ar[i];
               objname = this.getCustomAttributeName(objdiv);
               if(objname == childNam){
                  dummyVal = this.showChildSelect(objdiv,selVal);
                  this.showChildDivsRecurse(objdiv,dummyVal);
               }
            }
         }
      }
      catch(e){
         this.log('showChildDivsRecurse:',e);
      }
   },
   showChildSelect: function(divEl,selNam){
      var rc;
      var ar = this.findDivSelects(divEl);
      var i = ar.length;
      var selobj;
      var selval;
      while(i--){
         selobj = ar[i];
         selval = this.getCustomAttributeValue(selobj);
         if(selval == selNam){
            this.showSelect(selobj);
            rc = CS.v3.I18N.getDropdownSelectedValue(selobj);
         }
      }
      return rc;
   },
   observeChildDivsRecurse: function(divEl){
      var rc;
      var childNam = this.getCustomAttributeChild(divEl);
      try{
         var b = CS.v3.I18N.isEmpty(childNam);
         if(!b){
            var ar = this.findFormDropdownDivs(divEl);
            var i = ar.length;
            var objdiv;
            var objchild;
            var objname;
            var str='';
            var x='';
            while(i--){
               objdiv = ar[i];
               objname = this.getCustomAttributeName(objdiv);
               if(objname == childNam){
                  str = this.observeChildSelects(objdiv);
                  if(!CS.v3.I18N.isEmpty(str)){
                     x = this.observeChildDivsRecurse(objdiv);
                     if(!CS.v3.I18N.isEmpty(x)){
                        str = x;
                     }
                  }
               }
               if(!CS.v3.I18N.isEmpty(str)){
                  rc = str;
               }
            }
         }
      }
      catch(e){
         this.log('observeChildDivsRecurse:',e);
      }
      return rc;
   },
   observeChildSelects: function(divEl){
      var rc;
      var ar = this.findDivSelects(divEl);
      var i = ar.length;
      var selobj;
      var str='';
      var b;
      while(i--){
         selobj = ar[i];
         str = this.observeSelect(selobj);
         b = CS.v3.I18N.isEmpty(str);
         if(!b){
            rc = str;
         }
      }
      return rc;
   },
   observeSelect: function(selEl){
      var rc;
      var nam = Element.readAttribute(selEl,'name');
      var b = CS.v3.I18N.isEmpty(nam);
      if(!b){
         if(!Element.hasClassName(selEl,'csHide')){
            rc = CS.v3.I18N.getDropdownSelectedValue(selEl);
         }
      }
      return rc;
   },
   log: function(s,e){
      CS.v3.Logger.log("Dropdowns:" + s,e);
   }
};
/* Placeholder */
CS.v3.Components.Placeholder = function(el,options){new CS.v3.Config.Placeholder(el,options);};
CS.v3.Config.Placeholder = Class.create(CS.v3.Base.prototype, {
   initialize: function(el,options){
      var myel = CS.v3.castToEl(el);
      this.allPlaceholder = CS.v3.Placeholder.getAllElements(myel);
      if(options.bind==='true'){this.bind();}
   },
   bind: function(){
      var ar = this.allPlaceholder;
      var tag;
      var obj;
      var i = ar.length;
      while(i--){
         obj = ar[i];
         CS.v3.Placeholder.start(obj);
      }
   }
});
CS.v3.Placeholder = {
   getFlag: function(){
      var rc = new String('csPlaceholder-init');
      return rc;
   },
   start: function(el){
      var FLAG = this.getFlag();
      var b = Element.hasClassName(el,FLAG);
      if(!b){
         var b2 = this.initialize(el);
         if(b2){
            Element.addClassName(el,FLAG);
            Event.observe(el,'focus', function(event){ this.focusEv(event); }.bind(this));
            Event.observe(el,'blur', function(event){ this.blurEv(event); }.bind(this));
            Event.observe(el,'change', function(event){ this.changeEv(event); }.bind(this));
            //CS.v3.I18N.updateAttribute(el,'onkeyup','return CS.v3.Placeholder.keyupEv(event);');
            this.attachFormEvent(el);
         }
      }
   },
   attachFormEvent: function(el){
      var frm = Element.up(el,'FORM.csForm');
      if(frm){
         var FLAG = this.getFlag();
         var b = Element.hasClassName(frm,FLAG);
         if(!b){
            Element.addClassName(frm,FLAG);
            Event.observe(frm,'submit', function(event){ this.formSubmitEv(event); }.bind(this));
            Event.observe(frm,'reset', function(event){ this.resetFormEv(event); }.bind(this));
         }
      }
   },
   getAllElements: function(el){
      var rc;
      // if we're rebinding as single, skip the rest
      if(this.isSupportedElement(el)){
         if(Element.hasClassName(el,'csPlaceholder')){
            rc = el;
         }
      } else {
         rc = Element.select(el,["input[class*='csPlaceholder']","textarea[class*='csPlaceholder']"]);
      }
      return rc;
   },
   initialize: function(el){
      var rc = this.isSupportedElement(el);
      if(rc){
         Element.identify(el);
         this.changeField(el);
      }
      return rc;
   },
   isSupportedElement: function(el){
      // TODO  must be updated for HTML5 input types
      var rc = false;// NOTE: we prevent abuse on non supported elements (like dropdowns, radio and checkbox) by evaluating them here before attaching the events
      var tag = el.tagName;
      switch(tag){
         case 'INPUT':
            var typ = el.type;
            if(typ==='text'){
               rc = true;
            }
            break;
         case 'TEXTAREA':
            rc = true;
            break;
         default:
            // NOOP
            break;
      }
      return rc;
   },
   focusEv: function(event){
      var el = $(Event.element(event));
      this.erasePlaceholder(el);
   },
   blurEv: function(event){
      var el = $(Event.element(event));
      var b = CS.v3.I18N.isEmptyOrPlaceholderEl(el);
      if(b){
         this.changeField(el);
      }
   },
   changeField: function(el){
      var val = CS.v3.I18N.getValue(el);
      var mtval = CS.v3.I18N.getPlaceholder(el);
      var b = CS.v3.I18N.isEmptyOrBeginsWithMask(val,mtval);
      if(b){
         val = CS.v3.I18N.replaceString(val,mtval,'');
      }
      var b1 = CS.v3.I18N.isEmpty(val);
      if(b1){
         var b2 = CS.v3.I18N.isEmpty(mtval);
         if(!b2){
            this.doMask(el,mtval);
         }
      } else {
         this.doMask(el,val);
      }
   },
   changeEv: function(event){
      var el = $(Event.element(event));
      this.changeField(el);
   },
   formSubmitEv: function(event){
      var frm = $(Event.element(event));
      var ar = this.getAllElements(frm);
      var el;
      var i = ar.length;
      while(i--){
         el = ar[i];
         if(this.isSupportedElement(el)){
            this.erasePlaceholder(el);
         }
      }
   },
   resetFormEv: function(event){
      this.formSubmitEv(event);
   },
   erasePlaceholder: function(el){
      var b = CS.v3.I18N.isEmptyOrPlaceholderEl(el);
      if(b){
         //val = CS.v3.I18N.replaceString(val,mtval,'');
         this.doMask(el,'');
      }
   },
   doMask: function(el,val){
      var mtval = CS.v3.I18N.getPlaceholder(el);
      var b = CS.v3.I18N.isEmptyOrBeginsWithMask(val,mtval);
      var MASK = new String('csPlaceholderMask');
      if(b){
         if(!Element.hasClassName(el,MASK)){
            Element.addClassName(el,MASK);
         }
      } else {
         if(Element.hasClassName(el,MASK)){
            Element.removeClassName(el,MASK);
         }
      }
      CS.v3.I18N.setValue(el,val);
   },
   log: function(s,e){
      CS.v3.Logger.log("Placeholder:" + s,e);
   }
};
/* Print */
CS.v3.Components.Print = function(el,options){new CS.v3.Config.Print(el,options); };
CS.v3.Config.Print = Class.create(CS.v3.Base.prototype, {
   initialize: function(el,options){
      this.el = el;
      this.allPrint = Element.select(el,"[class*='csPrint']");
      if(options.bind==='true'){this.bind();}
   },
   bind: function(){
      var ar = this.allPrint;
      var i = ar.length;
      var obj;
      while(i--){
         obj = ar[i];
         CS.v3.Print.addDecoration(obj);
      }
   }
});
CS.v3.Print = {
   addDecoration: function(el){
      var FLAG = new String('csPrint-init');
      var b = Element.hasClassName(el,FLAG);// only change once!
      if(!b){
         Element.addClassName(el,FLAG);
         Event.observe(el,'click', function(event){ this.printEv(event); }.bind(this));
         Event.observe(el,'keydown', function(event){ this.printEv(event); }.bind(this));
      }
   },
   printEv: function(event){
      var el = $(Event.element(event));
      var typ = event.type;
      switch(typ){
         case 'click':
            this.printPage(el);
            break;
         case 'keydown':
            var key = event.keyCode;//
            if(key === Event.KEY_RETURN){
               this.printPage(el);
            }
            break;
         default:
            // NOOP
            break;
      }
   },
   printPage: function(el){
      this.beforePrint(el);
      if(window.print){
         window.print();
      } else {
         this.log('Error may have occurred.');
      }
      this.afterPrint(el);
   },
   beforePrint: function(el){
      // API - override for extension!
      this.log('beforePrint:el=' + el);
   },
   afterPrint: function(el){
      // API - override for extension!
      this.log('afterPrint:el=' + el);
   },
   log: function(s,e){
      CS.v3.Logger.log("Print:" + s,e);
   }
};
/* Telephone */
CS.v3.Components.Telephone = function(el,options){new CS.v3.Config.Telephone(el,options); };
CS.v3.Config.Telephone = Class.create(CS.v3.Base.prototype, {
   initialize: function(el,options){
      this.el = el;
      this.allTelephone = Element.select(el,"fieldset[class*='csTelephone']");
      if(options.bind==='true'){this.bind();}
   },
   bind: function(){
      var ar = this.allTelephone;
      var i = ar.length;
      var obj;
      var j;
      var obj2;
      while(i--){
         obj = ar[i];
         ar2 = Element.select(obj,"[class*='csTelephoneDialer']");
         if(ar2){
            j = ar2.length;
            var b;
            var FLAG = CS.v3.Telephone.getFlag();
            while(j--){
               obj2 = ar2[j];
               b = Element.hasClassName(obj2,FLAG);
               if(!b){
                  Element.addClassName(obj2,FLAG);
                  Event.observe(obj2,'click', function(event){ CS.v3.Telephone.dialerEv(event); } );
                  Event.observe(obj2,'keydown', function(event){ CS.v3.Telephone.dialerEv(event); } );
               }
            }
         }
         /* HTML5 future
         ar2 = Element.select(obj,"[class*='csTelephone']");
         if(ar2){
            j = ar2.length;
            while(j--){
               obj2 = ar2[j];
               CS.v3.Telephone.decoratePhone(obj2);
            }
         }
         */
      }
   }
});
CS.v3.Telephone = {
   getFlag: function(){
      var rc = new String('csTelephone-init');
      return rc;
   },
   decoratePhone: function(el){
      if(el){
         var tag = el.tagName;
         if(tag==='INPUT'){
            var ATTR_TYPE = 'type';
            CS.v3.I18N.updateAttribute(el,ATTR_TYPE,'tel');// HTML5 support
            var typ = el.type;
            if(typ!=='tel'){
               CS.v3.I18N.updateAttribute(el,ATTR_TYPE,'text');// standard component (we do not want to break existing)
            }
         }
      }
   },
   dialerEv: function(event){
      var el = $(Event.element(event));
      var typ = event.type;
      switch(typ){
         case 'click':
            this.clickDialerEv(event);
            break;
         case 'keydown':
            var key = event.keyCode;//
            if(key === Event.KEY_RETURN){
               this.clickDialerEv(event);
            }
            break;
         default:
            // NOOP
            break;
      }
   },
   clickDialerEv: function(event){
      // API - override for extension!
      var el = $(Event.element(event));
      var rc = this.assembleNumber(el);
      alert('Dialer API Not Defined in Requirements!\n\nnumber=[' + rc + ']');
      this.log('clickDialerEv:el=' + rc);
   },
   assembleNumber: function(el){
      var rc;
      var parEl = Element.up(el,'FIELDSET.csTelephone');
      if(parEl){
         var ar = Element.select(parEl,'.csTelephone');//supports SELECT and INPUT!
         if(ar){
            ar.reverse();// as we're doing 'reverse while'
            var i = ar.length;
            var obj;
            var val;
            var SPACE = new String(' ');
            var sb = new StringBuffer();
            while(i--){
               obj = ar[i];
               val = CS.v3.I18N.getValue(obj,true);
               sb.append(val);
               if(i>0){
                  sb.append(SPACE);
               }
            }
            rc = sb.toString(); sb=null;
         }
      }
      return rc;
   },
   log: function(s,e){
      CS.v3.Logger.log("Telephone:" + s,e);
   }
};
/* Telephone */
CS.v3.Components.Currency = function(el,options){new CS.v3.Config.Currency(el,options); };
CS.v3.Config.Currency = Class.create(CS.v3.Base.prototype, {
   initialize: function(el,options){
      this.el = el;
      this.allCurrency = Element.select(el,"fieldset[class*='csCurrency']");
      if(options.bind==='true'){this.bind();}
   },
   bind: function(){
      var ar = this.allCurrency;
      var i = ar.length;
      var obj;
      var j;
      var obj2;
      while(i--){
         obj = ar[i];
         ar2 = Element.select(obj,"[class*='csCurrencyCalculator']");
         if(ar2){
            j = ar2.length;
            while(j--){
               obj2 = ar2[j];
               Event.observe(obj2,'click', function(event){ CS.v3.Currency.clickCalculatorEv(event); } );
            }
         }
         /* If required
         ar2 = Element.select(obj,"[class*='csCurrency']");
         if(ar2){
            j = ar2.length;
            while(j--){
               obj2 = ar2[j];
               CS.v3.Currency.decorateCurrency(obj2);
            }
         }
         */
      }
   }
});
CS.v3.Currency = {
   decorateCurrency: function(el){
      //if(el){
      //}
   },
   clickCalculatorEv: function(event){
      // API - override for extension!
      var el = $(Event.element(event));
      alert('Calculator API Not Defined in Requirements!');
      this.log('clickCalculatorEv');
   },
   log: function(s,e){
      CS.v3.Logger.log("Currency:" + s,e);
   }
};
CS.v3.ValidationErrorMessage = function(el,nam,str){
   var rc;
   if(el){
      var sb = new StringBuffer();
      sb.append('<p id="');
      sb.append(el.id);
      sb.append('::::'); // 'double' double colon to ensure ID is unique as we may need to get the element later
      sb.append(nam);
      sb.append('" data-fieldname="'); // name is used to prevent duplicate messages for group fields
      sb.append(el.name);
      sb.append('" data-fieldid="'); // ID is used for 'click to focus' functionality
      sb.append(el.id);
      sb.append('">');
      sb.append(str);
      sb.append('</p>');
      rc = sb.toString();
      sb=null;
   }
   return rc;
};
// Used by add table row overlay
CS.v3.addAddEntryRow = Class.create(CS.v3.Base.prototype, {
   initialize: function(target){
      var aC = new CS.v3.appendContent(target);
      //var form = CS.v3.getParentElement(target,'form');
      var form = CS.v3.getParentElementWithClass(target,'csForm');
      this.readToInsert = Element.readAttribute(target,'data-cscontentvalue');
      //this.InsertableNodes = InsertableNodes.length;
      Element.addClassName(form,'csHideAddEntryRow');
      var innerCode = aC.InsertableNode.innerHTML;
      var sb = new StringBuffer();
      sb.append('<tr class="csInserted ');// NOTE: space intentional
      var CSZEBRA = 'csZebra';// NOTE: try String
      var b = Element.hasClassName(aC.lastOrFirstElem[0],CSZEBRA);
      if(!b){
         sb.append(CSZEBRA);
      }
      sb.append(' csRowEm">');
      sb.append(innerCode);
      sb.append('</tr>');
      this.rowInnerWrap = sb.toString(); sb=null;
      aC._insert(this.rowInnerWrap);
   }
});
CS.v3.Components.CurrencyDisplay = function(el,options){
   new CS.v3.Config.CurrencyDisplay(el,options);
};
CS.v3.Config.CurrencyDisplay = Class.create(CS.v3.Base.prototype, {
   initialize: function(el,options){
      this.allFilters = $$('.csCurrencyAmount');
      if(options.bind==='true'){this.bind();}
   },
   bind: function(){
      var ar = this.allFilters;
      if(ar){
         var i = ar.length;
         var el;
         while(i--){
            el = ar[i];
            new CS.v3.CurrencyDisplay(el);
         }
      }
   }
});
CS.v3.CurrencyExchangeRate = {
//ajax may be used to get the most recent currency exchange rates, currently using statics for demonstration
   getExchangeRate: function(fromCurrency,toCurrency){
   USD_EUR = 0.792558; //1 USD = 0.792558 EUR
   EUR_USD = 1.26151; //1 EUR = 1.26151 USD
   USD_GBP = 0.648973; //1 USD = 0.648973 GBP
   GBP_USD = 1.54145; //1 GBP = 1.54145 USD
   GBP_EUR = 1.22105; //1 GBP = 1.22105 EUR
   EUR_GBP = 0.818965; //1 EUR = 0.818965 GBP
   EUR_JPY = 107.527; //1 EUR = 107.527 JPY
   JPY_EUR = 0.00930003; //1 JPY = 0.00930003 EUR
   USD_JPY = 84.5664; //1 USD = 84.5664 JPY
   JPY_USD = 0.0118250; //1 JPY = 0.0118250 USD
   GBP_JPY = 131.287; //1 GBP = 131.287 JPY
   JPY_GBP = 0.00761622; //1 JPY = 0.00761622 GBP
   JPY_CAD = 0.0124848; //1 JPY = 0.0124848 CAD
   CAD_JPY = 80.0972; //1 CAD = 80.0972 JPY
   CAD_EUR = 0.745481; //1 CAD = 0.745481 EUR
   EUR_CAD = 1.34142; //1 EUR = 1.34142 CAD
   CAD_USD = 0.947215; //1 CAD = 0.947215 USD
   USD_CAD = 1.05573; //1 USD = 1.05573 CAD
   CAD_GBP = 0.609896; //1 CAD = 0.609896 GBP
   GBP_CAD = 1.63962; //1 GBP = 1.63962 CAD
      var rc;
      if(fromCurrency == toCurrency){
         rc = -1;
      } else {
         rc = (window[fromCurrency + '_' + toCurrency]);
      }
      return rc;
   }
};
CS.v3.CurrencyDisplay = Class.create(CS.v3.Base.prototype,{
   element: null, //amount textfield
   initialize: function(el){
      this.element = el;
      this.setVars();
      this.registerEvents();
   },
   setVars: function(){
      var HASH = new String('#');
      var EMPTY = new String('');
      //id selector only, label showing the currency symbol
      this.currencySymbolNode = $(this.element.readAttribute('data-cscurrencysymbolnode').replace(HASH,EMPTY));
      //id selector only, currency codes dropdown
      this.currencyCodesNode = $(this.element.readAttribute('data-cscurrencycodesnode').replace(HASH,EMPTY));
      //id selector only, message with yellow background showing conversion
      this.currencyConversionPanel = $(this.element.readAttribute('data-cscurrencyconversionpanel').replace(HASH,EMPTY));
      //actual link element of currency conversion
      this.currencyConversionLink = this.currencyConversionPanel.previous('.csIconCurrCalculator').firstDescendant();
      this.fromCurrencyNode = $(this.element.readAttribute('data-csfromcurrencynode').replace(HASH,EMPTY));
      this.toCurrencyNode = $(this.element.readAttribute('data-cstocurrencynode').replace(HASH,EMPTY));
      //make visible the currency symbol - as should be in JS version
      CS.v3.showFunction(this.currencySymbolNode);
      //make visible the launch currency convertor link - as should be in JS version
      CS.v3.showFunction(Element.up(this.currencyConversionPanel,'.csConversionContainer'));
      //currency code list should appear on left side - as should be in JS version
      Element.removeClassName(this.currencyCodesNode,'csNonJSPositioning');
      //amount field should appear on the right side - as should be in JS version
      Element.removeClassName(this.element,'csNonJSPosition-ShortField');
      this.changeCurrencyCode();
   },
   changeCurrencyCode: function(){
      var currencyCode = this.currencyCodesNode.value;
      CS.v3.CurrencyFormat.setCurrencyCode(this.element, currencyCode);
      var attrs = new CS.v3.getCurrencyFormatAttributes(this.element, currencyCode);
      this.currencySymbolNode.innerHTML = attrs.symbol;
      //accessibility - useful for screen readers such as JAWS and MAC VoiceOver
      this.currencySymbolNode.setAttribute("title",attrs.title);
   },
   registerEvents: function(){
      this.currencyCodesNode.observe('change',function(event){
         this.changeCurrencyCode();
         CS.v3.I18N.setValue(this.element,'');
      }.bind(this));
      this.currencyConversionLink.observe('focus',function(event){
         if(!this.element.value.empty()){
            this.showConversionMessage(event);
         }
      }.bind(this));
      this.currencyConversionLink.observe('mouseover',function(event){
         if(!this.element.value.empty()){
            this.showConversionMessage(event);
         }
      }.bind(this));
         Element.up(this.currencyConversionLink,'.csConversionContainer').observe('mouseout',function(e){
         var relatedTarget = $(e.relatedTarget || e.fromElement);
         var target = Event.element(e);
         if(!relatedTarget.descendantOf(Element.up(this.currencyConversionLink,'.csConversionContainer')))
            this.hideConversionMessage(e);
      }.bind(this));
      this.currencyConversionLink.observe('blur',function(event){
         this.hideConversionMessage();
      }.bind(this));
   },
   showConversionMessage: function(ev){
      if(CS.v3.I18N.isEmpty(this.element.value) || !CS.v3.Validators['csVal-isCurrency'].rule(this.element)){
         return;
      }
      var EMPTY = new String('');
      var attrs = new CS.v3.getCurrencyFormatAttributes(this.element);
      var amount = CS.v3.CurrencyFormat.getPureAmount(this.element.value, attrs);
      var formattedAmount = CS.v3.CurrencyFormat.getFormattedValue(amount, attrs);
      var CURRENCY_CODE_ATTR = new String('data-cscurrencycode');
      var fromCurrencyCode = this.fromCurrencyNode[this.fromCurrencyNode.selectedIndex].readAttribute(CURRENCY_CODE_ATTR);
      var toCurrencyCode = this.toCurrencyNode[this.toCurrencyNode.selectedIndex].readAttribute(CURRENCY_CODE_ATTR);
      var selectedCurrencyCode = this.currencyCodesNode.value;
      //only do conversions for different currencies
      var hasDifferent = false;
      var hintMsg1 = EMPTY;
      var hintMsg2 = EMPTY;
      var hintMsg3 = EMPTY;
      var hintMsg4 = EMPTY;
      var formattedAmount2 = EMPTY;
      var r;
      var attrs2;
      var exchangeRate = CS.v3.CurrencyExchangeRate.getExchangeRate(selectedCurrencyCode, fromCurrencyCode);
      if(exchangeRate != -1){
         //update the message panel with currency conversion
         if(exchangeRate){
            r = Math.round(parseFloat(amount) * parseFloat(exchangeRate)*1000)/1000;
            attrs2 = new CS.v3.getCurrencyFormatAttributes(this.element, fromCurrencyCode);
            formattedAmount2 = CS.v3.CurrencyFormat.getFormattedValue(r, attrs2);
         } else {
            formattedAmount2 = exchangeRate;
         }
         hintMsg1 = this.getConversionMessage(formattedAmount, selectedCurrencyCode, formattedAmount2, fromCurrencyCode);
         hintMsg2 = this.getConversionMessage('1', selectedCurrencyCode, exchangeRate, fromCurrencyCode);
         hasDifferent = true;
      }
      exchangeRate = CS.v3.CurrencyExchangeRate.getExchangeRate(selectedCurrencyCode, toCurrencyCode);
      if(fromCurrencyCode != toCurrencyCode && exchangeRate != -1){
         //update the message panel with currency conversion
         if(exchangeRate){
            r = Math.round(parseFloat(amount) * parseFloat(exchangeRate)*1000)/1000;
            attrs2 = new CS.v3.getCurrencyFormatAttributes(this.element, toCurrencyCode);
            formattedAmount2 = CS.v3.CurrencyFormat.getFormattedValue(r, attrs2);
         } else {
            formattedAmount2 = exchangeRate;
         }
         hintMsg3 = this.getConversionMessage(formattedAmount, selectedCurrencyCode, formattedAmount2, toCurrencyCode);
         hintMsg4 = this.getConversionMessage('1', selectedCurrencyCode, exchangeRate, toCurrencyCode);
         hasDifferent = true;
      }
      if(hasDifferent){
         var sb = new StringBuffer();
         sb.append(hintMsg1);
         sb.append(hintMsg3);
         sb.append(hintMsg2);
         sb.append(hintMsg4);
         Element.select(this.currencyConversionPanel,'.csHintMsg')[0].innerHTML = sb.toString(); sb=null;
         Element.removeClassName(this.currencyConversionPanel,'csCurrencyConversionToggle'); //shows the message
      }
   },
   hideConversionMessage: function(ev){
      Element.addClassName(this.currencyConversionPanel,'csCurrencyConversionToggle'); //hides the message
   },
   getConversionMessage: function(fromAmount, fromCurrency, toAmount, toCurrency){
      var sb = new StringBuffer();
      sb.append('<p>');
      sb.append(fromAmount);
      sb.append(' ');
      sb.append(fromCurrency);
      sb.append(' = ');
      sb.append(toAmount);
      sb.append(' ');
      sb.append(toCurrency);
      sb.append('</p>');
      var rc = sb.toString(); sb=null;
      return rc;
   }
});
/* Currency Format */
CS.v3.Components.CurrencyFormat = function(el,options){ new CS.v3.Config.CurrencyFormat(el,options); };
CS.v3.Config.CurrencyFormat = Class.create(CS.v3.Base.prototype, {
   initialize: function(el,options){
      this.allFormatCurr = Element.select(el,"input.csCurrencyFormat");
      if(options.bind==='true'){this.bind();}
   },
   bind: function(){
      var ar = this.allFormatCurr;
      var i = ar.length;
      var el;
      while(i--){
         el = ar[i];
         CS.v3.CurrencyFormat.setupCurrency(el);
      }
   }
});
CS.v3.getCurrencyFormatAttributes = Class.create(CS.v3.Base.prototype, {
   initialize: function(el, currencyCode){
      this.currency = currencyCode;
      if(CS.v3.I18N.isEmpty(this.currency)){
         this.currency = CS.v3.CurrencyFormat.getCurrencyCode(el);
         if(CS.v3.I18N.isEmpty(this.currency)){
            this.currency = 'default';
            this.log("No currency code defined, using '" + this.currency + "' instead.");
         }
      }
      var key = 'Currency-' + this.currency + '-pattern';
      var ar = CS.v3.I18N.findProperty(el, key);
      if(!ar){
         key = 'Currency-default-pattern';
         ar = CS.v3.I18N.findProperty(el,key);
         this.log("No pattern found for currency code '" + this.currency + "', using '" + key + "' instead.");
      }
      this.symbol = ar[0];
      this.thouStr = ar[1];
      this.deciStr = ar[2];
      this.groupStr = ar[3];
      this.nodStr = ar[4];
      this.title = ar[5];
      this.group = CS.v3.I18N.isEmpty(this.groupStr) ? 0 : parseInt(this.groupStr,10);
      this.nod = CS.v3.I18N.isEmpty(this.nodStr) ? 0 : parseInt(this.nodStr,10);
   },
   log: function(s,e){
      CS.v3.Logger.log("getCurrencyFormatAttributes:" + s,e);
   }
});
CS.v3.CurrencyFormat = {
   setupCurrency: function(el){
      var currencyCodeNodeId = Element.readAttribute(el,'data-cscurrencycodesnode');
      if(currencyCodeNodeId){
         var currencySelector = CS.v3.castToEl(currencyCodeNodeId.replace('#',''));
         this.setCurrencyCode(el, currencySelector.value);
      }
      this.reformat(el);
      Event.observe(el,'blur', function(event){ this.blurEv(event); }.bind(this));
   },
   setCurrencyCode: function(el, currencyCode){
      Element.writeAttribute(el,'data-cscurrencycode', currencyCode);
   },
   getCurrencyCode: function(el){
      return Element.readAttribute(el,'data-cscurrencycode');
   },
   blurEv: function(event){
      var el = $(Event.element(event));
      this.reformat(el);
   },
   reformat: function(el){
      if(CS.v3.I18N.isEmpty(el.value) || !CS.v3.Validators['csVal-isCurrency'].rule(el)){
         return;
      }
      var attrs = new CS.v3.getCurrencyFormatAttributes(el);
      var val = this.getPureAmount(el.value,attrs);
      var rc = this.getFormattedValue(val,attrs);
      CS.v3.I18N.setValue(el,rc);
   },
   getFormattedValue: function(value, attrs){
      var val = new String(value + '');
      var DECIMAL = '.';//TODO try String('.');
      var ar = CS.v3.I18N.split(val,DECIMAL);
      var integerVal = '';
      var decimalVal = '';
      //format integer
      if(attrs.group > 0){
         var remain = ar[0];
         while (remain.length > attrs.group){
            integerVal = attrs.thouStr + remain.substring(remain.length - attrs.group) + integerVal;
            remain = remain.substring(0, remain.length - attrs.group);
         }
         integerVal = remain + integerVal;
      } else {
         integerVal = ar[0];
      }
      // format decimal
      if(attrs.nod > 0){
         if(ar.length < 2){
            decimalVal = this.zeroPad('0', attrs.nod, true);
         } else {
            if(ar[1].length >= attrs.nod){
               decimalVal = ar[1].substring(0, attrs.nod);
            } else {
               decimalVal = this.zeroPad(ar[1], attrs.nod, true);
            }
         }
         decimalVal = attrs.deciStr + decimalVal;
      }
      return integerVal + decimalVal;
   },
   getPureAmount: function(value,attrs){
      var DECIMAL = new String('.');
      var val = CS.v3.I18N.trimString(value);
      if(!CS.v3.I18N.isEmpty(attrs.thouStr)){
         val = CS.v3.I18N.replaceString(val,attrs.thouStr,'');
      }
      if(!CS.v3.I18N.isEmpty(attrs.deciStr)){
         val = CS.v3.I18N.replaceString(val,attrs.deciStr,DECIMAL);
         var ar = CS.v3.I18N.split(val,DECIMAL);
         if(ar.length > 1){
            if(attrs.nod == 0){
               val = ar[0];
            } else if(attrs.nod < ar[1].length){
               val = ar[0] + DECIMAL + ar[1].substring(0, attrs.nod);
            }
         }
      }
      val = this.zeroTrim(val,DECIMAL);
      return val;
   },
   zeroPad: function(x,len,isRightPad){
   /* TODO - merge with CS.v3.I18N.zeroPad(...) */
      var rc = new String(x + '');
      var ZERO = new String('0');// use I18N?
      while(CS.v3.I18N.getLength(rc) < len){
         if(isRightPad){
            rc = new String(rc + ZERO);
         } else {
            rc = new String(ZERO + rc);
         }
      }
      return rc;
   },
   zeroTrim: function(x,deciStr){
      /* TODO - merge with CS.v3.I18N.zeroTrim(...) */
      var rc = '';
      var val = new String(x + '');
      if(val.length <= 1){
         rc = val;
      } else {
         var ZERO = new String('0');// use I18N?
         var i = 0;
         var c1;
         var c2;
         while(i < val.length - 1){
            c1 = CS.v3.I18N.getCharAt(val,i);
            c2 = CS.v3.I18N.getCharAt(val,i + 1);
            if(c1 == ZERO && c2 != deciStr){
               i++;
            } else {
               break;
            }
         }
         rc = val.substring(i);
      }
      return rc;
   },
   log: function(s,e){
      CS.v3.Logger.log("CurrencyFormat:" + s,e);
   }
};
/* form change */
CS.v3.Components.FormChange = function(el,options){ new CS.v3.Config.FormChange(el,options); };
CS.v3.Config.FormChange = Class.create(CS.v3.Base.prototype, {
   initialize: function(el,options){
      this.arAllForms = CS.v3.FormChange.findAllForms(el);
      if(options.bind==='true'){this.bind();}
   },
   bind: function(){
      var ar = this.arAllForms;
      if(ar){
         CS.v3.FormChange.addPageDecoration();
         var i = ar.length;
         var el;
         var b = false;
         while(i--){
            el = ar[i];
            CS.v3.FormChange.addFormDecoration(el);
         }
      }
   }
});
CS.v3.FormChange = {
   getClazz: function(){
      var rc = new String('CS.v3.FormChange');
      return rc;
   },
   getFormChangeFlag: function(){
      var rc = new String('csFormChange-init');
      return rc;
   },
   findAllForms: function(el){
      var rc = Element.select(el,"FORM.csFormChange");
      return rc;
   },
   addFormDecoration: function(el){
      var FLAG = this.getFormChangeFlag();
      var b = Element.hasClassName(el,FLAG);// only change once!
      if(!b){
         Element.addClassName(el,FLAG);
         Element.identify(el);// this is used later to identify the forms
         this.changeFormAttrs(el,0);
         // Buggy MSIE ( including 8) require that 'change' be attached to the fields, not the form!
         if(Prototype.Browser.IE){
            var ar = Form.getElements(el);
            if(ar){
               var i = ar.length;
               var obj;
               while(i--){
                  obj = ar[i];
                  Event.observe(obj,'change', function(event){ this.changeFormEv(event); }.bind(this));
               }
            }
         } else {
            Event.observe(el,'change', function(event){ this.changeFormEv(event); }.bind(this));
         }
         Event.observe(el,'reset', function(event){ this.resetFormEv(event); }.bind(this));
         //Event.observe(el,'submit', function(event){ this.submitFormEv(event); }.bind(this));
         Event.observe(el,'keydown', function(event){ this.keydownFormEv(event); }.bind(this));
      }
      //this.log('addFormDecoration:exit');
   },
   addPageDecoration: function(){
      var el = CS.v3.getBody();
      var FLAG = this.getFormChangeFlag();
      var b = Element.hasClassName(el,FLAG);// only change once!
      if(!b){
         Element.addClassName(el,FLAG);
         CS.v3.I18N.initialize(el);
         if(Prototype.Browser.IE){
            var ar = Element.select(el,'A');
            if(ar){
               var i = ar.length;
               var obj;
               while(i--){
                  obj = ar[i];
                  Event.observe(obj,'click', function(event){ this.clickPageEv(event); }.bind(this));
               }
            }
         } else {
            Event.observe(el,'click', function(event){ this.clickPageEv(event); }.bind(this));
         }
      }
   },
   isSubmitOverlayEv: function(event){
      var el = $(Event.element(event));
      var rc = false;
      var CLSOVERLAYSUBMIT = this.getFormChangedOverlaySubmitClass();
      var b = Element.hasClassName(el,CLSOVERLAYSUBMIT);
      if(b){
         rc = CS.v3.isInsideOverlay(el);
      }
      return rc;
   },
   handleSubmitOverlayEv: function(event){
      var el = $(Event.element(event));
      var overlayel = CS.v3.findOverlayWrapper(el);
      var btnid = CS.v3.I18N.getDataAttribute(overlayel,'data-csformbtnid','');
      var obj = CS.v3.castToEl(btnid);
      if(obj){
         var CSEXCEPT = this.getFormChangedExceptClass();
         Element.addClassName(obj,CSEXCEPT);// prevent FormChange from intercepting this again!
         var tag = obj.tagName;
         if(tag !== 'A'){
            CS.v3.fireOnclick(obj);
         }
      }
   },
   clickPageEv: function(event){
      var el = $(Event.element(event));
      var rc;
      var tag = el.tagName;
      switch(tag){
         case 'A':
            rc = this.clickPageLink(event,el);
            break;
         case 'INPUT':
            rc = this.clickPageInput(event,el);
            break;
         case 'BUTTON':
            rc = this.clickPageButton(event,el);
            break;
         default:
            // NOOP
            rc = false;
            break;
      }
   },
   clickPageLink: function(event,el){
      var rc = false;
      var isSubmitOverlay = this.isSubmitOverlayEv(event);
      if(isSubmitOverlay){
         this.handleSubmitOverlayEv(event);
      } else {
         var CSEXCEPT = this.getFormChangedExceptClass();
         var b = Element.hasClassName(el,CSEXCEPT);
         if(!b){
            var be = this.isExternalLink(el);
            if(be){
               rc = this.notifyChanges(event,el,false);
               /*
               if(rc == false){
                  var url = el.href;
                  window.location.href = url;
                  event.preventDefault(event);
               } else {
                  this.log('clickPageLink:rc=' + rc);
               }
               */
            }
         }
      }
      return rc;
   },
   isExternalLink: function(el){
      var url = CS.v3.UrlFunctions.getHref(el);
      var rc = true;
      var ba = url.startsWith('#');// is Anchor
      if(ba){
         rc = false;
      } else {
         var t = Element.readAttribute(el,'target');
         var bt = ((CS.v3.I18N.isEmpty(t)) || (t=='_self') );// is targeting this window
         if(!bt){
            rc = false;
         } else {
            if(Element.hasClassName(el,'csAjax')){
               rc = false;
            }
         }
      }
      return rc;
   },
   clickPageInput: function(event,el){
      var rc = false;
      var typ = el.type;
      if(typ==='submit' || typ==='button'){
         var isSubmitOverlay = this.isSubmitOverlayEv(event);
         if(isSubmitOverlay){
            this.handleSubmitOverlayEv(event);
         } else {
            rc = this.notifyChanges(event,el,true);
         }
      }
      return rc;
   },
   clickPageButton: function(event,el){
      var rc = false;
      var typ = el.type;
      if(typ==='submit' || typ==='button'){
         var isSubmitOverlay = this.isSubmitOverlayEv(event);
         if(isSubmitOverlay){
            this.handleSubmitOverlayEv(event);
         } else {
            rc = this.notifyChanges(event,el,true);
         }
      }
      return rc;
   },
   findParentForm: function(el){
      var rc = CS.v3.getParentElement(el,'FORM');
      return rc;
   },
   assertForm: function(el){
      var tag = el.tagName;
      var rc;
      if(tag==='FORM'){
         rc = el;
      } else {
         rc = this.findParentForm(el);
      }
      return rc;
   },
   changeFormEv: function(event){
      var el = $(Event.element(event));
      try{
         var CSEXCEPT = this.getFormChangedExceptClass();
         var b = Element.hasClassName(el,CSEXCEPT);
         if(!b){
            var frm = this.assertForm(el);
            var i = this.getCustomDataAttributeFormChanged(frm);
            var rc = i + 1;
            this.changeFormAttrs(frm,rc);
         }
      }
      catch(e){
         this.log('changeFormEv:',e);
      }
   },
   changeFormAttrs: function(el,num){
      try{
         var frm = this.assertForm(el);
         var CLS_FORMCHANGED = this.getFormChangedClass();
         var b = Element.hasClassName(frm,CLS_FORMCHANGED);
         var i = parseInt(num + '',10);
         if(i<1){
            if(b){
               Element.removeClassName(frm,CLS_FORMCHANGED);
            }
         } else {
            if(!b){
               Element.addClassName(frm,CLS_FORMCHANGED);
            }
         }
         this.setCustomDataAttributeFormChanged(frm,i);
      }
      catch(e){
         this.log('changeFormAttrs:',e);
      }
   },
   keydownFormEv: function(event){
      var el = $(Event.element(event));
      var key = event.keyCode;
      if(key===Event.KEY_RETURN){
         this.clickPageEv(event);// delegate to method used by 'click'
      }
   },
   resetFormEv: function(event){
      var el = $(Event.element(event));
      var frm = this.assertForm(el);
      this.changeFormAttrs(frm,'0');
   },
   submitFormEv: function(event){
      var el = $(Event.element(event));
      var rc = this.notifyChanges(event,el,true);
      return rc;
   },
   getFormChangedClass: function(){
      return new String('csFormChange-changed');// used only as a flag in the JS, could effect FORM CSS if desired.
   },
   getFormChangedExceptClass: function(){
      return new String('csFormChange-except');// used to flag fields to ignore changes
   },
   getFormChangedOverlaySubmitClass: function(){
      return new String('csOverlaySubmitParent');
   },
   getFormChangedPaginationOnly: function(){
      return new String('csFormChangePaginationNotify');// used to flag fields to ignore changes
   },
   isNotifyLink: function(el){
      var rc = false;
      var b = Element.hasClassName(el,'csFormChange-notify');// special links to notify
      if(b){
         rc = true;
      } else {
         var obj = CS.v3.getParentElementWithClass(el,'csFormChangeNotify');//Element.up(el,'P.csPageNumbers');// links inside pagination
         if(obj){
            rc = true;
         }
      }
      return rc;
   },
   notifyChanges: function(event,el,isForm){
      var rc = false;
      try{
         if(el){
            var isNotifyLink = this.isNotifyLink(el);
            var elid = Element.identify(el);
            var elfrmid;
            var frmEl;
            if(isForm){
               frmEl = this.assertForm(el);
               if(frmEl){
                  elfrmid = Element.identify(frmEl);
                  el = frmEl;
               }
            }
            var CLS_FORMCHANGED = this.getFormChangedClass();
            var CSPAGINATIONONLY = this.getFormChangedPaginationOnly();
            var body = CS.v3.getBody();
            var ar = this.findAllForms(body);
            if(ar){
               var i = ar.length;
               var frm;
               var myfrmid;
               var b;
               var isPaginationOnlyNotify;
               var isNotify = false;
               while(i--){
                  frm = ar[i];
                  myfrmid = Element.identify(frm);
                  b = Element.hasClassName(frm,CLS_FORMCHANGED);
                  if(b){
                     if(myfrmid!=elfrmid){
                        isPaginationOnlyNotify = Element.hasClassName(frm,CSPAGINATIONONLY);
                        if(isPaginationOnlyNotify){
                           if(isNotifyLink){
                              isNotify = true;
                              break;
                           }
                        } else {
                           // standard process
                           isNotify = true;
                           break;
                        }
                     }
                  }
               }
               if(isNotify){
                  var tag = el.tagName;
                  var myForm;
                  if(tag==='FORM'){
                     myForm = true;
                  } else {
                     myForm = false;
                  }
                  if(myForm){
                     url = '#';
                     //formid = objid;
                  } else {
                     url = CS.v3.UrlFunctions.getHref(el);
                     //formid = '';
                  }
                  this.showFormChangePopup(frm,url,elid);
                  Event.stop(event);
               }
            }
         }
      }
      catch(e){
         this.log('notify:',e);
      }
      return rc;
   },
   showFormChangePopup: function(frm,url,elid){
      try{
         var overlayid = new String('panelModal');// may be changed!
         var overlayel = CS.v3.castToEl(overlayid);
         if(overlayel){
            var overlayref = new String('#' + overlayid);
            // create a hidden link in the page
            var a = document.createElement('A');
            var body = CS.v3.getBody();
            body.appendChild(a);
            // manipulate the link
            var id = Element.identify(a);
            CS.v3.UrlFunctions.setHref(a,url);
            var CLSOVERLAYSUBMIT = this.getFormChangedOverlaySubmitClass();
            var CSEXCEPT = this.getFormChangedExceptClass();
            Element.addClassName(a,'csOverlayToggle');
            Element.addClassName(a,'csClick');
            Element.addClassName(a,'csHide');
            Element.addClassName(a,CSEXCEPT);// prevent FormChange from intercepting this!
            var CSNODESTOTOGGLE = 'data-csnodestotoggle';
            CS.v3.I18N.updateAttribute(a,CSNODESTOTOGGLE,overlayref);
            CS.v3.I18N.copyAttribute(frm,a,'data-cscontentfromnode','');//'.csMyInsertableContent'
            CS.v3.I18N.copyAttribute(frm,a,'data-csoverlayorigin','window');
            CS.v3.I18N.copyAttribute(frm,a,'data-cslightbox','true');
            CS.v3.I18N.copyAttribute(frm,a,'data-cssetpanelwidth','530');
            var sourcenode = CS.v3.I18N.getDataAttribute(frm,CSNODESTOTOGGLE,'');
            var b = CS.v3.I18N.isEmpty(sourcenode);
            if(b){// when empty, we will use our own overlay instead of one identified on the form.
               var panelHdr = this.getPopupHeaderText(frm);
               var panelMsg = this.getPopupMessageText(frm);
               var btnYes = this.getPopupButtonYesText(frm);
               var btnNo = this.getPopupButtonNoText(frm);
               var btnMsg = this.getPopupButtonMessageText(frm);
               var sb = new StringBuffer();
               sb.append('<p>');
               sb.append(panelMsg);
               sb.append('</p>');
               sb.append('<div class="csRow csButtons">');
               var bm = CS.v3.I18N.isEmpty(btnMsg);
               if(!bm){
                  sb.append('<p class="csButtonMsg">');
                  sb.append(btnMsg);
                  sb.append('</p>');
               }
               sb.append('<div class="csCol-1">');
               sb.append('<span class="csButtonWrap"><a class="csButton csClick csOverlayClose" href="#">');
               sb.append(btnNo);
               sb.append('</a></span>');
               sb.append('</div>');
               sb.append('<div class="csCol-2">');
               sb.append('<span class="csButtonWrap csAct"><a class="csButton csAct csClick csOverlayClose ');
               sb.append(CLSOVERLAYSUBMIT);
               sb.append('" href="#');
               //sb.append(url);
               //sb.append('" data-csformbtnid="');
               //sb.append(elid);
               //sb.append('" onclick="');
               //var CLAZZ = this.getClazz();
               //sb.append(CLAZZ);
               //sb.append('.confirmSubmitForm(event);">');
               sb.append('">');
               sb.append(btnYes);
               sb.append('</a></span>');
               sb.append('</div>');
               sb.append('</div>');
               var txt = sb.toString(); sb=null;
               var headEl = Element.down(overlayel,'SPAN.csPanelOverlayHeading');
               var contentEl = Element.down(overlayel,'DIV.csPanelScroll');
               CS.v3.updateFix(headEl,panelHdr);
               CS.v3.updateFix(contentEl,txt);
            }
            CS.v3.I18N.updateAttribute(overlayel,'data-csformbtnid',elid);
            CS.v3.I18N.updateAttribute(overlayel,'data-csformbtnurl',url);// mostly for debug now!
            var tmp = '.' + CLSOVERLAYSUBMIT;
            var ar = Element.select(overlayel,tmp);
            if(ar){
               var obj;
               var i = ar.length;
               while(i--){
                  obj = ar[i];
                  CS.v3.UrlFunctions.setHref(obj,url);
                  //CS.v3.I18N.updateAttribute(obj,'data-csformbtnurl',url);// mostly for debug now!
                  Element.addClassName(obj,CSEXCEPT);// prevent FormChange from intercepting this again!
               }
            }
            //Event.observe(overlayel,'click', function(event){ this.formChangeOverlayEv(event); }.bind(this));
            //Event.observe(overlayel,'keydown', function(event){ this.formChangeOverlayEv(event); }.bind(this));
            // fire the hidden link
            CS.v3.fireOnclick(a);
         } else {
            this.log('showFormChangePopup:overlay is missing');
         }
      }
      catch(e){
         this.log('showFormChangePopup:',e);
      }
   },
   confirmSubmitForm: function(event){
      var el = $(Event.element(event));
      var overlayel = CS.v3.findOverlayWrapper(el);
      var id = CS.v3.I18N.getDataAttribute(overlayel,'data-csformbtnid','');
      var b = CS.v3.I18N.isEmpty(id);
      if(!b){
         var frmBtnEl = CS.v3.castToEl(id);
         var tag = frmBtnEl.tagName;
         var body = CS.v3.getBody();
         var ar = this.findAllForms(body);
         if(ar){
            var i = ar.length;
            var obj;
            while(i--){
               // disable checks (if required)
               obj = ar[i];
               this.changeFormAttrs(obj,0);
            }
         }
         //CS.v3.submitForm(frmEl);
         CS.v3.fireOnclick(frmBtnEl);
      }
   },
   getCustomDataAttributeFormChanged: function(el){
      var def = '0';
      var str = CS.v3.I18N.getDataAttribute(el,'data-csformchanged',def);
      var rc = parseInt(str,10);
      return rc;
   },
   setCustomDataAttributeFormChanged: function(el,val){
      CS.v3.I18N.setDataAttribute(el,'data-csformchanged',val);
   },
   getPopupHeaderText: function(el){
      var def = CS.v3.I18N.findProperty(el,'formchange-header');
      var rc = CS.v3.I18N.getDataAttribute(el,'data-csformchange-header',def);
      return rc;
   },
   getPopupMessageText: function(el){
      var def = CS.v3.I18N.findProperty(el,'formchange-message');
      var rc = CS.v3.I18N.getDataAttribute(el,'data-csformchange-message',def);
      return rc;
   },
   getPopupButtonYesText: function(el){
      var def = CS.v3.I18N.findProperty(el,'formchange-button-yes');
      var rc = CS.v3.I18N.getDataAttribute(el,'data-csformchange-button-yes',def);
      return rc;
   },
   getPopupButtonNoText: function(el){
      var def = CS.v3.I18N.findProperty(el,'formchange-button-no');
      var rc = CS.v3.I18N.getDataAttribute(el,'data-csformchange-button-no',def);
      return rc;
   },
   getPopupButtonMessageText: function(el){
      var def = CS.v3.I18N.findProperty(el,'formchange-button-message');
      var rc = CS.v3.I18N.getDataAttribute(el,'data-csformchange-button-message',def);
      return rc;
   },
   log: function(s,e){
      CS.v3.Logger.log('FormChange:' + s,e);
   }
};
CS.v3.Components.VirtualKeypad = function(el,options){new CS.v3.Config.VirtualKeypad(el,options); };
CS.v3.Config.VirtualKeypad = Class.create(CS.v3.Base.prototype, {
   initialize: function(el,options){
      this.allKeypadInputFields = Element.select(el,"input.csKeypadInput");
      this.allKeypads = Element.select(el,"div.csKeypadContainer");
      if(options.bind==='true'){this.bind();}
   },
   bind: function(){
      var ar = this.allKeypadInputFields;
      var i;
      var el;
      if(ar){
         i = ar.length;
         while(i--){
            el = ar[i];
            CS.v3.VirtualKeypad.addDecorationForKeypadInputFields(el);
         }
      }
      ar = this.allKeypads;
      if(ar){
         i = ar.length;
         while(i--){
            el = ar[i];
            CS.v3.VirtualKeypad.addDecorationForKeypads(el);
         }
      }
   }
});
CS.v3.VirtualKeypad = {
   numberStyleArray: $w('csKey0 csKey1 csKey2 csKey3 csKey4 csKey5 csKey6 csKey7 csKey8 csKey9'),
   addDecorationForKeypadInputFields: function(el){
      var keypadCls = this.getKeypadCls(el);
      if(!CS.v3.I18N.isEmpty(keypadCls)){
         el.readOnly = true;
         Event.observe(el,'keydown', CS.v3.VirtualKeypad.keypadInputFieldEv.bind(this));
         Event.observe(el,'click', CS.v3.VirtualKeypad.keypadOpenEv.bind(this));
      }
   },
   addDecorationForKeypads: function(el){
      var keys = Element.select(el,'li a');
      if(keys){
         var i = keys.length;
         var key;
         var focusEv = CS.v3.VirtualKeypad.keyFocusEv.bind(this);
         var blurEv = CS.v3.VirtualKeypad.keyBlurEv.bind(this);
         while(i--){
            key = keys[i];
            Event.observe(key,'click', function(event){event.preventDefault();});
            Event.observe(key,'keydown', function(event){
               var keyCode = event.keyCode;
               if(keyCode === Event.KEY_RETURN || keyCode === Event.KEY_LEFT || keyCode === Event.KEY_RIGHT || keyCode === Event.KEY_UP || keyCode === Event.KEY_DOWN){
                  event.preventDefault();
               }
            });
            Event.observe(key,'focus', focusEv);
            Event.observe(key,'blur', blurEv);
         }
      }
      var mouseoverEv = CS.v3.VirtualKeypad.keyMouseoverEv.bind(this);
      var numKeys = Element.select(el,'.csNumKey');
      if(numKeys){
         var j = numKeys.length;
         var numKey;
         var numClickEv = CS.v3.VirtualKeypad.numKeyClickEv.bind(this);
         while(j--){
            numKey = numKeys[j];
            Event.observe(numKey,'click', numClickEv);
            Event.observe(numKey,'keydown', numClickEv);
            Event.observe(numKey,'mouseover', mouseoverEv);
         }
      }
      var clearKey = Element.down(el,'.csClearKey');
      if(clearKey){
         var clearClickEv = CS.v3.VirtualKeypad.clearKeyClickEv.bind(this);
         Event.observe(clearKey,'click', clearClickEv);
         Event.observe(clearKey,'keydown', clearClickEv);
         Event.observe(clearKey,'mouseover', mouseoverEv);
      }
      var enterKey = Element.down(el,'.csEnterKey');
      if(enterKey){
         var enterClickEv = CS.v3.VirtualKeypad.enterKeyClickEv.bind(this);
         Event.observe(enterKey,'click', enterClickEv);
         Event.observe(enterKey,'keydown', enterClickEv);
         Event.observe(enterKey,'mouseover', mouseoverEv);
      }
   },
   keypadInputFieldEv: function(event){
      var el = $(Event.element(event));
      var keyCode = event.keyCode;
      if((!event.shiftKey) && (keyCode === Event.KEY_TAB)){
         var keypadCls = this.getKeypadCls(el);
         var keypad = $$('.'+keypadCls)[0];
         var keypadInputId = Element.identify(el);
         var CSHIDE = 'csHide';// TODO try new String('csHide');
         if(!Element.hasClassName(keypad,CSHIDE) && this.getKeypadInputId(keypad) == keypadInputId){
            event.preventDefault();
            var target = Element.down(keypad,'a');
            CS.v3.focus(target);
         }
      } else if(keyCode === Event.KEY_RETURN){
         event.preventDefault();
         this.keypadOpenEv(event);
      }
   },
   keypadOpenEv: function(event){
      var el = $(Event.element(event));
      var keypadCls = this.getKeypadCls(el);
      var keypad = $$('.'+keypadCls)[0];
      var keypadInputId = Element.identify(el);
      var allKeypads = $$('div.csKeypadContainer');
      var CSHIDE = 'csHide';// TODO try new String('csHide');
      if(allKeypads){
         var j = allKeypads.length;
         var item;
         while(j--){
            item = allKeypads[j];
            if(!Element.hasClassName(item,keypadCls)){
               Element.addClassName(item,CSHIDE);
            }
         }
      }
      var isTheSameKeypadInputField = this.getKeypadInputId(keypad) == keypadInputId;
      if(!isTheSameKeypadInputField){
         this.setKeypadInputId(keypad, keypadInputId);
         Element.addClassName(keypad,CSHIDE);
      }
      if(Element.hasClassName(keypad,CSHIDE)){
         el.value='';
         var flag = this.getRandomKeyFlag(el);
         if(flag == 'true'){
            var randomKeyArray = this.getRandomArray(this.numberStyleArray.clone());
            var numKeys = Element.select(keypad,'.csNumKey');
            if(numKeys){
               var i = numKeys.length;
               var numKey;
               var numStyle;
               while(i--){
                  numKey = numKeys[i];
                  numStyle = this.getNumberStyle(numKey);
                  if(numStyle){
                     Element.removeClassName(numKey,numStyle);
                  }
                  numStyle = randomKeyArray[i];
                  Element.addClassName(numKey,numStyle);
                  this.autoGenPos(numKey);
               }
            }
         }
         this.moveKeypad(el, keypad,event);
         Element.removeClassName(keypad,CSHIDE);
      }
   },
   getNumberStyle: function(el){
      var rc;
      var clsAr = $w(el.className);
      var i = clsAr.length;
      var cls;
      while(i--){
         cls = clsAr[i];
         if(cls.startsWith('csKey')){
            rc = cls;
            break;
         }
      }
      return rc;
   },
   autoGenPos: function(el){
      var numberStyle = this.getNumberStyle(el);
      var number = numberStyle.gsub('csKey','');
      var basePosX;
      if(Element.hasClassName(el,'csClickedKey')){
         basePosX = -164;
      } else if(Element.hasClassName(el,'csOnKey')){
         basePosX = -123;
      } else {
         basePosX = -82;
      }
      var n = parseInt(number,10);
      var posX = basePosX - 123*n;
      var pos = posX + 'px 0';
      el.style.backgroundPosition = pos;
      return number;
   },
   keyFocusEv: function(event){
      var el = $(Event.element(event));
      var div = CS.v3.getParentElement(el,'div');
      if(Element.hasClassName(div,'csNumKey')){
         Element.addClassName(div,'csOnKey');
         this.autoGenPos(div);
      } else if(Element.hasClassName(div,'csClearKey')){
         Element.addClassName(div,'csClearKeyOn');
      } else if(Element.hasClassName(div,'csEnterKey')){
         Element.addClassName(div,'csEnterKeyOn');
      }
      el.hideFocus = true;
   },
   keyBlurEv: function(event){
      var el = $(Event.element(event));
      var div = CS.v3.getParentElement(el,'div');
      if(Element.hasClassName(div,'csNumKey')){
         Element.removeClassName(div,'csOnKey');
         this.autoGenPos(div);
      } else if(Element.hasClassName(div,'csClearKey')){
         Element.removeClassName(div,'csClearKeyOn');
      } else if(Element.hasClassName(div,'csEnterKey')){
         Element.removeClassName(div,'csEnterKeyOn');
      }
   },
   keyMouseoverEv: function(event){
      var el = $(Event.element(event));
      if(el.tagName!=='A'){
         el = Element.down(el,'a');
      }
      CS.v3.focus(el);
   },
   numKeyClickEv: function(event){
      var el = $(Event.element(event));
      if(el.tagName !== 'A'){
         el = Element.down(el,'a');
      }
      var keypad = Element.up(el,'div.csKeypadContainer');
      var typ = event.type;
      var keyCode = event.keyCode;
      if(typ==='click' || keyCode===Event.KEY_RETURN){
         var div = Element.up(el,'div.csNumKey');
         var clickedStyle = 'csClickedKey';// TODO try new String('csClickedKey');
         Element.addClassName(div,clickedStyle);
         var keypadInputField = CS.v3.castToEl(this.getKeypadInputId(keypad));
         var number = this.autoGenPos(div);
         keypadInputField.value = keypadInputField.value + number;
            setTimeout(function(){
            Element.removeClassName(div,clickedStyle);
            CS.v3.VirtualKeypad.autoGenPos(div);}, 200);
      } else {
         this.keydownEv(el, keyCode, keypad, event);
      }
   },
   clearKeyClickEv: function(event){
      var el = $(Event.element(event));
      if(el.tagName!=='A'){
         el = Element.down(el,'a');
      }
      var keypad = Element.up(el,'div.csKeypadContainer');
      var typ = event.type;
      var keyCode = event.keyCode;
      if(typ==='click' || keyCode===Event.KEY_RETURN){
         var div = Element.up(el,'div.csClearKey');
         div.style.backgroundPosition = '-41px 0';
         var keypadInputField = CS.v3.castToEl(this.getKeypadInputId(keypad));
         keypadInputField.value = '';
            setTimeout(function(){div.style.backgroundPosition = '0 0';}, 200);
      } else {
         this.keydownEv(el, keyCode, keypad, event);
      }
   },
   enterKeyClickEv: function(event){
      var el = $(Event.element(event));
      if(el.tagName !== 'A'){
         el = Element.down(el,'a');
      }
      var keypad = Element.up(el,'div.csKeypadContainer');
      var typ = event.type;
      var keyCode = event.keyCode;
      if(typ === 'click' || keyCode === Event.KEY_RETURN){
         var div = Element.up(el,'div.csEnterKey');
         div.style.backgroundPosition = '-41px 0';
         var keypadInputField = CS.v3.castToEl(this.getKeypadInputId(keypad));
         new CS.v3.getTextFieldError(keypadInputField,'realtime');
         CS.v3.focus(keypadInputField);
            setTimeout(function(){
            div.style.backgroundPosition = '0 0';
            Element.addClassName(keypad,'csHide');}, 100);
      } else {
         this.keydownEv(el, keyCode, keypad, event);
      }
   },
   keydownEv: function(el, keyCode, keypad, event){
      if(keyCode === Event.KEY_ESC){
         Element.addClassName(keypad,'csHide');
         var keypadInputField = CS.v3.castToEl(this.getKeypadInputId(keypad));
         CS.v3.focus(keypadInputField);
         event.preventDefault();
      } else {
         var liEl = CS.v3.getParentElement(el,'li');
         var prevSibs = liEl.previousSiblings();
         var nextSibs = liEl.nextSiblings();
         var obj;
         var target;
         switch(keyCode){
            case Event.KEY_LEFT:
               if(prevSibs.size() % 4 > 0){
                  obj = Element.previous(liEl);
                  target = Element.down(obj,'a');
                  CS.v3.focus(target);
               }
               break;
            case Event.KEY_RIGHT:
               if(nextSibs.size() % 4 > 0){
                  obj = Element.next(liEl);
                  target = Element.down(obj,'a');
                  CS.v3.focus(target);
               }
               break;
            case Event.KEY_UP:
               if(prevSibs.size() >= 4){
                  obj = liEl.previous(3);
                  target = Element.down(obj,'a');
                  CS.v3.focus(target);
               }
               break;
            case Event.KEY_DOWN:
               if(nextSibs.size() >= 4){
                  obj = liEl.next(3);
                  target = Element.down(obj,'a');
                  CS.v3.focus(target);
               }
               break;
            default:
               // NOOP
               break;
         }
      }
   },
   getKeypadCls: function(el){
      var rc = CS.v3.I18N.getDataAttribute(el,'data-cskeypad','');
      return rc;
   },
   getKeypadInputId: function(el){
      var rc = CS.v3.I18N.getDataAttribute(el,'data-cskeypadinputid','');
      return rc;
   },
   setKeypadInputId: function(el,val){
      CS.v3.I18N.setDataAttribute(el,'data-cskeypadinputid',val);
   },
   getRandomKeyFlag: function(el){
      var rc = CS.v3.I18N.getDataAttribute(el,'data-csrandomkeyflag','true');
      return rc;
   },
   getRandomArray: function(ar){
      if(ar){
         var i = ar.length;
         var randomNum;
         var temp;
         while(i--){
            randomNum = this.randomInt(i);
            temp = ar[i];
            ar[i] = ar[randomNum];
            ar[randomNum] = temp;
         }
      }
      return ar;
   },
   randomInt: function(n){
      return Math.round(Math.random() * n);
   },
   moveKeypad: function(el, keypad,event){
      if(keypad!=null){
         var elDimension=el.getDimensions();
         var keypadDimension=keypad.getDimensions();
         if(Element.hasClassName(keypad,'csBottomOpening')){
            var kHeight=keypadDimension.height-13;
            keypad.clonePosition(el,{setLeft:true,setTop:true,setWidth:false,setHeight:false,offsetLeft:-(elDimension.width/2-15),offsetTop:-kHeight});
         } else {
            keypad.clonePosition(el,{setLeft:true,setTop:true,setWidth:false,setHeight:false,offsetLeft:-(elDimension.width/2-15),offsetTop:elDimension.height-2});
         }
      }
   },
   log: function(s,e){
      CS.v3.Logger.log("VirtualKeypad:" + s,e);
   }
};
/* Theme Pack 3.2.1+ DO NOT modify */
CS.v3.Components.Flags = function(el,options){ new CS.v3.Config.Flags(el,options); };
CS.v3.Config.Flags = Class.create(CS.v3.Base.prototype,{
   initialize: function(el,options){
      this.allFlags = Element.select(el,"select[class*='csFlags']");// consider 'csSelectOne'
      if(options.bind==='true'){this.bind();}
   },
   bind: function(){
      var ar = this.allFlags;
      var i = ar.length;
      var el;
      while(i--){
         el = ar[i];
         CS.v3.Flags.setup(el);
      }
   }
});
CS.v3.Flags = {
   setup: function(el){
      var FLAG = new String('csFlags-init');
      var b=Element.hasClassName(el,FLAG);
      if(!b){
         Element.addClassName(el,FLAG);
         Event.observe(el,'change', function(event){ this.changedEv(event); }.bind(this));
         var selectedOption = this.findSelectedOption(el);
         if(selectedOption){
            var optClasses = Element.readAttribute(selectedOption,'class');
            Element.addClassName(el,optClasses);
            this.setCustomAttributeAppliedClasses(el,optClasses);
         }
      }
   },
   changedEv: function(event){
      var el = $(Event.element(event));
      this.updateEl(el);
      //event.preventDefault();
   },
   updateEl: function(el){
      var selectedOption = this.findSelectedOption(el);
      if(selectedOption!=null){
         var optClasses = Element.readAttribute(selectedOption,'class');
         var oldClasses = this.getCustomDataAttributeAppliedClasses(el,'');
         if(oldClasses==''){
            this.setCustomAttributeAppliedClasses(el,optClasses);
            oldClasses = optClasses;
         }
         if(oldClasses!=optClasses){
            Element.removeClassName(el,oldClasses);
            Element.addClassName(el,optClasses);
            this.setCustomAttributeAppliedClasses(el, optClasses);
            Element.removeClassName(el,'csSelected');//cleanup
         }
      }
   },
   findSelectedOption: function(el){
      var rc = null;
      var childs = Element.select(el,"option");
      if(childs){
         var selectedVal = CS.v3.I18N.getDropdownSelected(el,true);//el.options[el.selectedIndex].value;
         var selectedOption = null;
         var i = childs.length;
         var opt;
         var optionVal;
         while(i--){
            opt = childs[i];
            optionVal = CS.v3.I18N.getOptionSelected(opt,true);
            if(optionVal == selectedVal){
               rc = opt;
               break;
            }
         }
      }
      return rc;
   },
   getCustomDataAttributeAppliedClasses: function(el,def){
      var rc = CS.v3.I18N.getDataAttribute(el,'data-csflagsappliedclasses',def);
      return rc;
   },
   setCustomAttributeAppliedClasses: function(el,val){
      CS.v3.I18N.setDataAttribute(el,'data-csflagsappliedclasses',val);
   },
   log: function(s,e){
      CS.v3.Logger.log("Flags:" + s,e);
   }
};
CS.v3.I18N.initializeProperties = function(){
   // validation
   CS.v3.I18N.addProperty('val-error-header-count','','There is&nbsp;<span class="csCount"></span>&nbsp;error with the page you are trying to submit.');
   CS.v3.I18N.addProperty('val-errors-header-count','','There are&nbsp;<span class="csCount"></span>&nbsp;errors with the page you are trying to submit.');
   CS.v3.I18N.addProperty('val-required-hint','','tester');
   CS.v3.I18N.addProperty('val-required-error','','Required field(s).');
   CS.v3.I18N.addProperty('val-required-hint','','Required field(s).');
   CS.v3.I18N.addProperty('val-isNumber-error','','Enter a valid number in this field.');
   CS.v3.I18N.addProperty('val-isNumber-hint','','Enter a valid number in this field.');
   CS.v3.I18N.addProperty('val-isDigit-error','','Enter a valid number in this field.');
   CS.v3.I18N.addProperty('val-isDigit-hint','','Enter a valid number in this field.');
   CS.v3.I18N.addProperty('val-isAlpha-error','','Enter letters only (a-z) in this field.');
   CS.v3.I18N.addProperty('val-isAlpha-hint','','Enter letters only (a-z) in this field.');
   CS.v3.I18N.addProperty('val-isAlphaNum-error','','Enter only letters (a-z) or numbers (0-9) in this field. Spaces and other characters are not allowed.');
   CS.v3.I18N.addProperty('val-isAlphaNum-hint','','Enter only letters (a-z) or numbers (0-9) in this field. Spaces and other characters are not allowed.');
   CS.v3.I18N.addProperty('val-isLessThan-error','','The left field value <span class="value0"></span>  must be less than the right field value <span class="value1"></span>.');
   CS.v3.I18N.addProperty('val-isLessThan-hint','','The left field value <span class="value0"></span>  must be less than the right field value <span class="value1"></span>.');
   CS.v3.I18N.addProperty('val-isGreaterThan-error','','This left field value <span class="value0"></span>  must be greater than the right field value <span class="value1"></span>.');
   CS.v3.I18N.addProperty('val-isGreaterThan-hint','','This left field value <span class="value0"></span>  must be greater than the right field value <span class="value1"></span>.');
   CS.v3.I18N.addProperty('val-isMinLength-error','','This field does not meet the minimum length.');
   CS.v3.I18N.addProperty('val-isMinLength-hint','','This field does not meet the minimum length.');
   CS.v3.I18N.addProperty('val-isMaxLength-error','','This field exceeds the maximum length.');
   CS.v3.I18N.addProperty('val-isMaxLength-hint','','This field exceeds the maximum length.');
   // textarea
   CS.v3.I18N.addProperty('val-isEmail-error','','Enter a valid email address.');
   CS.v3.I18N.addProperty('val-isEmail-hint','','Enter a valid email address.');
   CS.v3.I18N.addProperty('val-isMod10Card-error','','Enter a valid card number.');
   CS.v3.I18N.addProperty('val-isMod10Card-hint','','Enter a valid card number.');
   CS.v3.I18N.addProperty('val-isWeakPin-error','','No 3 adjacent digits can be consecutive or the same.');
   CS.v3.I18N.addProperty('val-isWeakPin-hint','','No 3 adjacent digits can be consecutive or the same.');
   CS.v3.I18N.addProperty('val-isCalendarRange-error','','Enter a correctly formatted calendar date in this field, +/- one year permitted.');
   CS.v3.I18N.addProperty('val-isCalendarRange-hint','','Enter a correctly formatted calendar date in this field, +/- one year permitted.');
   CS.v3.I18N.addProperty('val-isCalendarSpaces-error','','Enter a correctly formatted calendar date in this field, spaces are not permitted.');
   CS.v3.I18N.addProperty('val-isCalendarSpaces-hint','','Enter a correctly formatted calendar date in this field, spaces are not permitted.');
   CS.v3.I18N.addProperty('textarea-messageMaxsize','','');//'Maximum ### Characters.';
   CS.v3.I18N.addProperty('textarea-messageRemain','','### characters remaining');
   CS.v3.I18N.addProperty('textarea-messageMask','','###');
   // format
   CS.v3.I18N.addProperty('format_thousand_separator','',',');
   CS.v3.I18N.addProperty('format_decimal_separator','','.');
   CS.v3.I18N.addProperty('format_zero','','0');
   CS.v3.I18N.addProperty('format_number_of_decimals','','3');
   CS.v3.I18N.addProperty('format_grouping','','3');
   CS.v3.I18N.addProperty('format_remove','',[',','.',' ','(',')','-','$']);
   /* future consideration is to read/derive these based on a CSS classname
   CS.v3.I18N.addProperty('format_amount','','###,###,###,###,###.##');
   CS.v3.I18N.addProperty('format_phone','','### (###)###-####');
   CS.v3.I18N.addProperty('format_postcode','','### ###');
   CS.v3.I18N.addProperty('format_zipcode','','#####-####');
   */
   //form change
   CS.v3.I18N.addProperty('formchange-header','','Confirm?');
   CS.v3.I18N.addProperty('formchange-message','','You have unsubmitted form data elsewhere on this page, do you wish to continue with this action?');
   CS.v3.I18N.addProperty('formchange-button-yes','','Yes');
   CS.v3.I18N.addProperty('formchange-button-no','','No');
   CS.v3.I18N.addProperty('formchange-button-message','','');
   // validation
   CS.v3.I18N.addProperty('val-isCurrency-error','','Enter currency in this field.');
   CS.v3.I18N.addProperty('val-isCurrency-hint','','Enter currency in this field.');
   // currency
   CS.v3.I18N.addProperty('Currency-default-pattern','',["$",",",".","3","2","United States Dollar"]);
   CS.v3.I18N.addProperty('Currency-USD-pattern','',["$",",",".","3","2","United States Dollar"]);
   CS.v3.I18N.addProperty('Currency-GBP-pattern','',["£",",",".","3","2","Great Britain (United Kingdom), Pounds"]);
   CS.v3.I18N.addProperty('Currency-EUR-pattern','',["€",",",".","3","2","Euro Member Countries, Euro"]);
   CS.v3.I18N.addProperty('Currency-CAD-pattern','',["$",",",".","3","2","Canadian Dollar"]);
   CS.v3.I18N.addProperty('Currency-JPY-pattern','',["¥",",",".","3","0","Japanese Yen"]);
};

/*** UKPWS additions ***/
CS.v3.UKPWS = {};
CS.v3.UKPWS.FormUtils = {};
/*
 * CS.v3.UKPWS.FormUtils.isHideShowVisible
 * Returns true if all hideShowTargets of the given element are visible, otherwise false.
 * If el is not in a hideShowTarget, this function will return true. 
 * Parameters:
 *    el             The element to test.
 */
CS.v3.UKPWS.FormUtils.isHideShowVisible = function(el) {
   var isVisible = true;
   var getParentHideShow = function (e) {
	   return e.up('div.csBox');
   }
   var parentHideShow = getParentHideShow(el);
   while (parentHideShow && isVisible) {
      if (parentHideShow.hasClassName('csHide')) {
         isVisible = false;
      } else {
        parentHideShow = getParentHideShow(parentHideShow);
      }            
   }
   return isVisible;
}