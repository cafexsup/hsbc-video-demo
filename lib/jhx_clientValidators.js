//highlight a field, typically in another color than white
function highlightField(obj) {
  if($(obj) != null){
	  $(obj).setStyle({backgroundColor: 'yellow'});
	}
}
//undo the highlighting of a field, typically put the backgroundcolor back to white
function resetFieldColor(obj) {
  if($(obj) != null)
	  $(obj).setStyle({backgroundColor: 'white'});
}
//showValidationError (for the moment doing only highlightField)
function showValidationError(field) {
		highlightField(field);
}
//hideValidationError ((for the moment doing only resetFieldColor)
function hideValidationError(field) {
		resetFieldColor(field);
}
//showValidationError simultaneously for 2 fields 
function showCompareValidationError(field1, field2) {
	showValidationError(field1);
	showValidationError(field2);
}
//hideValidationError simultaneously for 2 fields 
function hideCompareValidationError(field1, field2) {
	hideValidationError(field1);
	hideValidationError(field2);
}
//check whether required field has a non-empty value
function validateRequiredField(field) {
	var value = $F(field);
	//typing just 1 or more spaces is allowed
	//remark: the obligation to type something else than space should be defined in another function
    if(value.length == 0) {
		showValidationError(field);
		return false;
	} else {	
		hideValidationError(field);		
		return true;
	}
}
//check whether field is an email value
function validateEmail(field) {	
  var value = $F(field);
  if(value == "") {
		hideValidationError(field);
		return true;
  }  
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value)){
		hideValidationError(field);	
		return (true)
  } else {
		showValidationError(field);
		return false;
  }
}
//check whether field is a cardnumber value
function validateCardNumber(field) {
  var i, n, c, r, t;
  var value = $F(field);
  if(value == "") {
		hideValidationError(field);
		return true;
  }  
  // First, reverse the string and remove any non-numeric characters.
  r = "";
  for (i = 0; i < value.length; i++) {
    c = parseInt(value.charAt(i), 10);
    if (c >= 0 && c <= 9)
    {
      r = c + r;
    }
  }
  // Check for a bad string.
  if (r.length <= 1) {
		showValidationError(field);
		return false;
  }
  // Now run through each single digit to create a new string. Even digits
  // are multiplied by two, odd digits are left alone.
  t = "";
  for (i = 0; i < r.length; i++) {
    c = parseInt(r.charAt(i), 10);
    if (i % 2 != 0){
      c *= 2;
    }
    t = t + c;
  }
  // Finally, add up all the single digits in this string.
  n = 0;
  for (i = 0; i < t.length; i++) {
    c = parseInt(t.charAt(i), 10);
    n = n + c;
  }
  // If the resulting sum is an even multiple of ten (but not zero), the
  // card number is good.
  if (n != 0 && n % 10 == 0) {
		hideValidationError(field);
		return (true)
  } else {
		showValidationError(field);
		return false;
  }
}

function validateLengthInterval(field,minValue,maxValue) {
  validateRangeField(field,minValue,maxValue);
}
//check whether the length of the value in a field is between the minValue and maxValue (minValue <= field <= maxValue)
function validateRangeField(field,minValue,maxValue) {
	var valueText = $F(field);
	var value = valueText.length
	var numberOfChecks = 0;
	var numberOfFailures = 0;
  
  if(minValue != null) {
    numberOfChecks++;
		if(value < minValue) {
		  numberOfFailures++;
		} 
	} 
  if(maxValue != null) {
    numberOfChecks++;
		if(value > maxValue) {
			numberOfFailures++;
		} 
	} 
  if(numberOfChecks == 0) {
    hideValidationError(field);
    return true;
  } else {
    if (numberOfFailures == 0) {
      hideValidationError(field);
      return true;
    } else {
      showValidationError(field);
      return false;
    }
  }
}
//check whether field1 operator field2? the operator is a parameter ('eq' or 'not' standing resp. for == or !=)
function validateCompareFields(field1,field2,operator) {
	if(operator == 'eq')
		return validateEquality(field1,field2);
	else if(operator == 'notEq')
		return validateNotEquals(field1,field2);
	
}



//check whether field 1 == field2
function validateEquality(field1,field2) {
	var value1 = $F(field1);
	var value2 = $F(field2);
	
	if(value1 == value2) {
		hideCompareValidationError(field1, field2);
		return true;
	}
	else {
		showCompareValidationError(field1, field2);
		return false;
	}
}
//check whether field1 differs from field2
function validateNotEquals(field1,field2) {
	var value1 = $F(field1);
	var value2 = $F(field2);
	
	if(value1 != value2) {
		hideCompareValidationError(field1, field2);
		return true;
	}
	else {
		showCompareValidationError(field1, field2);
		return false;
	}
}
//check whether a field matches a regex
function validateRegExp(field,pattern) {
	var value = $F(field);
	var re = new RegExp(pattern);	
	
	if(value == "") {
		hideValidationError(field);
		return true;
	}
	
	if(value.match(re)) {
		hideValidationError(field);		
		
		return false;
	}
	else {
		showValidationError(field);
		return true;
	}
}
//check whether a field has a specific length
function validateLengthExactly(field, exactValue) {
	var value = $F(field);
	if(value.length != exactValue) {
		showValidationError(field);
		return false;
	} else {
		hideValidationError(field);
		return true;
	}
}
/*check whether a field equals a StringToCompare
without generating validationErrors*/
function testEqual(field,StringToCompare){
	var value = $F(field);
	if(value == StringToCompare) {
		return true;
	}
	else {
		return false;
	}
}

/* Force Maxlength on textareas */
function restrictTextAreaLength(field,maxlength) {
	var obj = $(field);
	var value = obj.value;
    if (value.length > maxlength) {
    	obj.value=obj.value.substring(0,maxlength);
    	return false;
    }
    return true;
}




function blockKey(o,w){

  o.value = o.value.replace(w,'');
}




