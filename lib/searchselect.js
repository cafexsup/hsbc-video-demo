/* Theme Pack 3.2.1+ DO NOT modify */
CS.v3.Components.SearchNSelect = function(el,options){ new CS.v3.Config.SearchNSelect(el,options); };
CS.v3.Config.SearchNSelect = Class.create(CS.v3.Base.prototype, {
	initialize: function(el,options){
		this.arSearchNSelect = CS.v3.SearchNSelect.findSearchNSelect(el,true);
		if(options.bind==='true'){this.bind();}
	},
	bind: function(){
		var ar = this.arSearchNSelect;
		var i = ar.length;
		var el;
		while(i--){
			el = ar[i];
			CS.v3.SearchNSelect.addDecoration(el);
		}
	}
});
CS.v3.SearchNSelect = {
	findSearchNSelect: function(el,isIncludeList){
		var rc;
		//this.log('findSearchNSelect:el=' + el);
		if(isIncludeList){
			rc = Element.select(el,["SELECT[class*='csSearchNSelect']","UL[class*='csSearchNSelect']","INPUT[class*='csSearchNSelect']"]);
		} else {
			rc = Element.select(el,["SELECT[class*='csSearchNSelect']","INPUT[class*='csSearchNSelect']"]);
		}
		//this.log('findSearchNSelect:rc=' + rc);
		return rc;
	},
	getClazz: function(){
		var rc = new String('CS.v3.SearchNSelect');
		return rc;
	},
	addDecoration: function(el){
		try{
			var FLAG = new String('csSearchNSelect-init');
			var b = Element.hasClassName(el,FLAG);// only change once!
			if(!b){
				var inputid = Element.identify(el);// NOTE: this is used in cases where we want to assign an ID to an element (if it's not already assigned).
				var isSupported = false;
				var wrapEl = this.findDivWrapper(el);
				this.setCustomDataAttributeInputId(wrapEl,inputid);
				CS.v3.I18N.initialize(wrapEl);
				CS.v3.I18N.initialize(el);
				this.tryExternalEventsHook(el,null);
				this.copyAttributesFromInputToWrapper(el,wrapEl);
				var isAttachKeyEvent = false;
				var ar;
				var tag = el.tagName;
				switch(tag){
					case 'SELECT':
						this.forceSingle(el);
						ar = this.getArrayFromInput(el);
						this.insertContextualMenu(wrapEl,el,ar);
						isSupported = true;
						break;
					case 'INPUT':
						CS.v3.I18N.updateAttribute(el,'autocomplete','off');
						isAttachKeyEvent = true;
						ar = this.getArrayFromInput(el);
						this.insertContextualMenu(wrapEl,el,ar);
						isSupported = true;
						break;
					case 'UL':
						//NOTE: this is the 'pro' scenario where we can add content to the bottom (as links)
						//NOTE: we will massage the data in advance here to prevent problems when we copy it later!
						ar = Element.select(el,'A');
						if(ar){
							var i = ar.length;
							var obj;
							while(i--){
								obj = ar[i];
								Element.addClassName(obj,'csNavItem');
								CS.v3.I18N.updateAttribute(obj,'tabindex','-1');
							}
						}
						isSupported = true;
						break;
					case 'TEXTAREA':
						isSupported = false;// NOTE:  we MIGHT consider supporting this in the future
						break;
					default:
						// NOOP
						break;
				}
				if(isSupported){
					Element.addClassName(el,FLAG);
					Element.hide(el);
					//Event.observe(wrapEl,'click', function(event){ this.clickedWrapEv(event); }.bind(this));
					Event.observe(el,'click', function(event){ this.clickedInputEv(event); }.bind(this));
					Event.observe(el,'change', function(event){ this.changedInputEv(event); }.bind(this));
					if(isAttachKeyEvent){
						Event.observe(el,'keydown', function(event){ this.keydownSearchEv(event); }.bind(this));
					}
					var frm = CS.v3.getParentElement(el,'FORM');
					if(frm){
						if(!Element.hasClassName(frm,FLAG)){
							Element.addClassName(frm,FLAG);
							Event.observe(frm,'reset', function(event){ this.resetFormSearchNSelectEv(event); }.bind(this));
						}
					}
				}
			}
		}
		catch(e){
			this.log('addDecoration:',e);
		}
	},
	forceSingle:function(selEl){
		// force to be single (not MULTIPLE!) dropdowns
		CS.v3.I18N.updateAttribute(selEl,'size','1');
		CS.v3.I18N.updateAttribute(selEl,'multiple','');
	},
	findDivWrapper: function(el){
		var rc = Element.up(el,'DIV.csSearchNSelect-wrapper');
		return rc;
	},
	findTarget: function(wrapEl){
		var rc;
		var ar = Element.select(wrapEl,'UL.csSearchNSelect-target');
		if(ar){
			var i = ar.length;
			if(i>0){
				rc = ar[0];// we use only the first!
			}
		}
		return rc;
	},
	updateSearchIcon: function(searchEl){
		var el = this.findSearchIcon(searchEl);
		if(el){
			var val = CS.v3.I18N.getValue(searchEl,true);
			var CSICONCLEAR = this.getIconClearClass();
			var b = CS.v3.I18N.isEmpty(val);
			var b2 = Element.hasClassName(el,CSICONCLEAR);
			if(b){
				if(b2){
					Element.removeClassName(el,CSICONCLEAR);
				}
			} else {
				if(!b2){
					Element.addClassName(el,CSICONCLEAR);
				}
			}
		}
	},
	findSearchIcon: function(el){
		var rc;
		var wrapEl = this.findDivWrapper(el);
		if(wrapEl){
			var ar = Element.select(wrapEl,'DIV.csSearchNSelectIcon');
			if(ar){
				var i = ar.length;
				if(i>0){
					rc = ar[0];// we use only the first!
				}
			}
		}
		return rc;
	},
	findSearchContent: function(wrapEl){
		var rc;
		if(wrapEl){
			var ar = Element.select(wrapEl,'DIV.csSearchNSelect-content');
			if(ar){
				var i = ar.length;
				if(i>0){
					rc = ar[0];// we use only the first!
				}
			}
		}
		return rc;
	},
	findSearchInput: function(el){
		var rc;
		if(el){
			var isSupported = false;
			var tag = el.tagName;
			if(tag==='INPUT'){
				var b = CS.v3.supportedInputTypeEl(el);
				if(b){
					isSupported = true;
				}
			}
			if(isSupported){
				rc = el; // sent in the input!
			} else {
				var wrapEl = this.findDivWrapper(el);
				if(wrapEl){
					var ar = Element.select(wrapEl,'INPUT.csSearchNSelect-search');
					if(ar){
						var i = ar.length;
						if(i>0){
							rc = ar[0];// we use only the first!
						}
					}
				}
			}
		}
		return rc;
	},
	parseTextInput: function(inputEl){
		var rc = [];
		var wrapEl = this.findDivWrapper(inputEl);
		var delim = this.getCustomDataAttributeLineDelimiter(wrapEl);
		var txt = CS.v3.I18N.getValue(inputEl,false) + delim;
		var val = this.getLabelDisplay(txt,delim,0);
		var b = CS.v3.I18N.isEmpty(val);
		if(!b){
			this.setCustomDataAttributeDataSelected(wrapEl,val);
			this.setCustomDataAttributeDataSelected(inputEl,val);
		}
		// TODO get data
		return rc;
	},
	parseAnchorlist: function(wrapEl,inputVal){
		var rc = [];
		var arUl = Element.select(wrapEl,'UL.csSearchNSelect');
		if(arUl){
			var i = arUl.length;
			var b = CS.v3.I18N.isEmpty(inputVal);
			var j;
			var ulEl;
			var arLi;
			var objLi;
			var objUl;
			var val;
			var mask;
			var clean;
			while(i--){
				objUl = arUl[i];
				Element.hide(objUl);
				arLi = Element.select(objUl,'LI');
				j = arLi.length;
				while(j--){
					objLi = arLi[j];
					val = objLi.innerHTML;//CS.v3.I18N.getValue(objLi,false);
					// NOTE: we MAY want to check the  visibility before adding the element
					mask = this.getCustomDataAttributeMessageMask(objLi);
					if(b){
						clean = val;
					} else {
						clean = CS.v3.I18N.replaceString(val,mask,inputVal);
					}
					if(clean.indexOf(mask,0)===-1){
						rc.push(clean);
					}
				}
			}
		}
		return rc;
	},
	parseDropdown: function(selEl){
		// NOTE: this will read the OPTIONs from the dropdown and add them to the JS array
		//var selEl = $(selEl);
		var delim = this.getCustomDataAttributeLineDelimiter(selEl);//wrapEl
		var ar = Element.select(selEl,'OPTION');
		var i = ar.length;
		var rc = [];
		var obj;
		var lbl;
		var url;
		var str;
		var HASH = this.getDefaultUrl();
		while(i--){
			obj = ar[i];
			// we exclude 'empty value' and 'disabled' options from the SELECT
			if(!this.getDisabledVal(obj)){
				lbl = CS.v3.I18N.getValue(obj,false);
				//val = CS.v3.I18N.getValue(obj,true);
				//blbl = CS.v3.I18N.isEmpty(lbl);
				//if(!blbl){
					//bval = CS.v3.I18N.isEmpty(val);
					//if(!bval){
						url = this.getCustomDataAttributeUrl(obj,HASH);
						str = url + delim + lbl;
						rc.push(str);
					//}
				//}
			}
		}
		var wrapEl = this.findDivWrapper(selEl);
		var txt = CS.v3.I18N.getValue(selEl,false) + delim;
		var val = this.getLabelDisplay(txt,delim,0);
		var b = CS.v3.I18N.isEmpty(val);
		if(!b){
			this.setCustomDataAttributeDataSelected(wrapEl,val);
		}
		var nam = Element.identify(selEl);// NOTE: this MAY have to be cleaned if portal makes them incompatible
		this.saveValues(selEl,rc);
		this.setCustomDataAttributeDataSource(wrapEl,nam);
		return rc;
	},
	getDisabledVal: function(el){
		var rc = false;
		if(el){
			rc = el.disabled;
		}
		return rc;
	},
	findFormInputFromWrapper: function(wrapEl){
		var inputId = this.getCustomDataAttributeInputId(wrapEl);
		var rc = CS.v3.castToEl(inputId);
		return rc;
	},
	updateContextualMenu: function(el){
		var wrapEl = this.findDivWrapper(el);
		var labelVal = this.getCustomDataAttributeValue(el);
		var delim = this.getCustomDataAttributeLineDelimiter(wrapEl);
		var labelStr = this.getLabelDisplay(labelVal,delim,1);
		var inputEl = this.findFormInputFromWrapper(wrapEl);
		var myval = this.stripLabelUrl(labelVal,delim);
		var tag;
		if(inputEl){
			tag = inputEl.tagName;
			CS.v3.I18N.setValue(inputEl,myval);
		}
		var b = CS.v3.I18N.isEmpty(labelVal);
		if(!b){
			this.setCustomDataAttributeDataSelected(wrapEl,labelVal);
		}
		switch(tag){
			case 'INPUT':
				CS.v3.I18N.setValue(inputEl,myval);
				var inp = this.findSearchInput(el);
				CS.v3.I18N.setValue(inp,myval);
				break;
			case 'SELECT':
				var xar = Element.select(wrapEl,'A.csNavToggle');
				var i = xar.length;
				if(i>0){
					var obj = xar[0];
					CS.v3.updateFix(obj,labelStr);
				}
				break;
			default:
				// NOOP
				break;
		}
		//CS.v3.fireOnclick(inputEl);// helps trigger validation!
	},
	keydownSearchEv: function(event){
		var key = event.keyCode;
		switch(key){
			case Event.KEY_ESC:
				break;
			case Event.KEY_TAB:
				break;
			case Event.KEY_LEFT:
				break;
			case Event.KEY_RIGHT:
				break;
			case Event.KEY_UP:
				break;
			case Event.KEY_DOWN:
				break;
			case Event.KEY_BACKSPACE:
				this.doDeleteKeyEv(event);
				break;
			case Event.KEY_DELETE:
				this.doDeleteKeyEv(event);
				break;
			default:
				this.doAppendKeyEv(event);
				break;
		}
	},
	doDeleteKeyEv: function(event){
		var el = $(Event.element(event));
		var str = CS.v3.I18N.getValueUnmasked(el,true);
		if(str){
			var size = str.length;
			if(size>0){
				var mysize = size-1;
				var myval;
				if(mysize>0){
					myval = CS.v3.I18N.truncateString(str,mysize);
				} else {
					myval = new String('');
				}
				CS.v3.I18N.setValue(el,myval);
			}
		}
		this.doRefreshKeyEv(event);
		event.preventDefault(event);
	},
	doAppendKeyEv: function(event){
		var el = $(Event.element(event));
		var c = event.keyCode || event.charCode;
		var s = CS.v3.I18N.getStringFromChar(el,c);
		var b = CS.v3.I18N.isAlphanumeric(el,s);
		if(b){
			var oldval = CS.v3.I18N.getValueUnmasked(el,true);
			var val = oldval + s;
			CS.v3.I18N.setValue(el,val);
			this.doRefreshKeyEv(event);
			var m = new CS.v3.MenuToggle(el,event);
			CS.v3.MenuVis.effect(m,event);
			event.preventDefault(event);
		}
	},
	doRefreshKeyEv: function(event){
		var el = $(Event.element(event));
		this.updateSearchIcon(el);
		this.findResults(event);
	},
	focusedSearchEv: function(event){
		var el = $(Event.element(event));
		this.clearPlaceholder(el);
	},
	clickedSearchEv: function(event){
		var el = $(Event.element(event));
		this.clearPlaceholder(el);
		Event.stop(event);
	},
	clearPlaceholder: function(el){
		var val = CS.v3.I18N.getValue(el,true);
		var mask = CS.v3.I18N.getPlaceholder(el);
		var b = CS.v3.I18N.isEmptyOrBeginsWithMask(val,mask);
		if(b){
			CS.v3.I18N.setValue(el,'');
		}
	},
	clickedIconEv: function(event){
		var el = $(Event.element(event));
		var searchEl = this.findSearchInput(el);
		if(searchEl){
			CS.v3.I18N.setValue(searchEl,'');
			searchEl.removeAttribute('value');
			this.updateSearchIcon(searchEl);
			Event.stop(event);
		}
		this.findResults(event);
	},
	clickedItemEv: function(event){
		var el = $(Event.element(event));
		var HASH = this.getDefaultUrl();
		var url;
		var tag = el.tagName;
		switch(tag){
			case 'A':
				this.clickedLink(el);
				url = CS.v3.UrlFunctions.getHref(el);
				break;
			case 'DIV':
				var a = CS.v3.getParentElement(el,'A');
				if(a){
					this.clickedLink(a);
					url = HASH;// NOTE: this should be for non-link clicks
				}
				break;
			default:
				// NOOP
				break;
		}
		var b = CS.v3.I18N.isEmpty(url);
		if(!b){
			if(url==HASH){
				event.preventDefault(event);
			}
		}
	},
	clickedLink: function(el){
		var val = this.getCustomDataAttributeValue(el);
		var b = CS.v3.I18N.isEmpty(val);
		if(!b){
			this.updateContextualMenu(el);
		}
	},
	clickedInputEv: function(event){
		var el = $(Event.element(event));
		try{
			var tag = el.tagName;
			var id = Element.identify(el);
			if(tag==='INPUT'){
				this.findResults(event);
			}
		}
		catch(e){
			this.log('clickedInputEv:',e);
		}
	},
	resetFormSearchNSelectEv: function(event){
		var el = $(Event.element(event));//frm
		//this.tryExternalEventsHook(el,event);// NOTE: frm!
		var ar = this.findSearchNSelect(el,true);
		this.log('resetFormSearchNSelectEv');
		// TODO for future
	},
	changedInputEv: function(event){
		var el = $(Event.element(event));
		var tag = el.tagName;
		if(tag==='INPUT'){
			this.findResults(event);
		}
	},
	assembleKey: function(nam){
		var rc = 'SearchNSelectValue_' + nam;
		return rc;
	},
	addValues: function(nam,locale,val){
		// NOTE: move to CS.v3.PersistState?
		var key = this.assembleKey(nam);
		CS.v3.I18N.addProperty(key,locale,val);
	},
	saveValues:function(el,val){
		// NOTE: move to CS.v3.PersistState?
		var nam = Element.identify(el);
		var locale = CS.v3.I18N.getLocale(el);
		this.addValues(nam,locale,val);
	},
	loadValues:function(el,nam){
		// NOTE: move to CS.v3.PersistState?
		var rc;
		if(nam){
			var key = this.assembleKey(nam);
			rc = CS.v3.I18N.findProperty(el,key);
		}
		return rc;
	},
	tryAjaxFirst: function(el,nam,val,isStartsWith){
		var wrapEl = this.findDivWrapper(el);
		var url = this.getCustomDataAttributeDataAjax(wrapEl);// for ajax
		var b = CS.v3.I18N.isEmpty(url);
		if(!b){
			var localkey = 'ajax_' + nam + '[' + val + ']';
			this.log('tryAjaxFirst:localkey=' + localkey);
			// TODO look in our local cache
			// TODO ajax request
			var myurl = CS.v3.UrlFunctions.urlAppenderCacheBuster(url);//append attribute to make it 'unique'
			new Ajax.Request(myurl, {
				method: 'get',
				onSuccess: function(transport){
					CS.v3.SearchNSelect.log('AJAX:success:el=' + el);
				},
				onFailure: function(){
					CS.v3.SearchNSelect.log('AJAX:fail:el=' + el);
				}
			});
			// TODO store in our local cache
		}
	},
	findResults: function(event){
		var rc;
		try{
			var el = $(Event.element(event));
			var bb = this.isUsingSingleDiv(el);
			var wrapEl;
			if(bb){
				wrapEl = this.findSingleSearchContainer(el);
			} else {
				wrapEl = this.findDivWrapper(el);
			}
			var inputEl = this.findFormInputFromWrapper(wrapEl);
			this.tryExternalEventsHook(inputEl,event);
			var targetEl = this.findTarget(wrapEl);
			var myval = CS.v3.I18N.getValue(el,true);//getValueUnmasked
			var val = CS.v3.I18N.trimString(myval);
			var minStr = this.getCustomDataAttributeMinSize(el);
			var min = parseInt(minStr,10);
			var sz = CS.v3.I18N.getLength(val);
			if(sz<min){
				rc = [];
				this.showResults(el,targetEl,rc,val);
			} else {
				var nam = this.getCustomDataAttributeDataSource(wrapEl);
				var delim = this.getCustomDataAttributeLineDelimiter(wrapEl);
				var isStartsWith = this.isStartsWith(inputEl);
				//this.tryAjaxFirst(el,nam,val,isStartsWith);
				if(targetEl){
					var ar = this.loadValues(wrapEl,nam);
					if(ar){
						var b = CS.v3.I18N.isEmpty(val);
						if(b){
							rc = ar;
						} else {
							var val_lc = CS.v3.I18N.toLower(val);
							var i = ar.length;
							var obj;
							rc = [];
							var j;
							var str;
							var del;
							while(i--){
								obj = ar[i];
								obj_lc = CS.v3.I18N.toLower(obj);// NOTE: compare lowercase versions (case-insensitive)
								del = obj_lc.indexOf(delim);
								j = (obj_lc.indexOf(val_lc,del) - del);
								if(isStartsWith){
									if(j==1){
										rc.push(obj);
									}
								} else {
									if(j>0){
										rc.push(obj);
									}
								}
							}
						}
					}
					this.showResults(el,targetEl,rc,val);
				}
			}
		}catch(e){
			this.log('findResults:',e);
		}
	},
	createSearchField: function(val,bFirst,txtPlaceholder){
		var CLAZZ = this.getClazz();
		var CSS_NAV_ITEM1 = new String('csNavItem-1');
		var sb = new StringBuffer();
		sb.append('<input type="text"');
		sb.append(' class="');
		sb.append(CSS_NAV_ITEM1);
		if(bFirst){
			sb.append(' csNavFirstItem');
		}
		var bv = CS.v3.I18N.isEmpty(val);
		if(bv){
			sb.append(' csPlaceholder');
		}
		sb.append(' csTextField csSearchNSelect-search" tabindex="-1" autocomplete="off"');
		sb.append(' data-csplaceholder="');
		sb.append(txtPlaceholder);
		sb.append('"');
		sb.append(' onclick="');//tabindex="-1"
		sb.append(CLAZZ);
		sb.append('.clickedSearchEv(event);"');
		sb.append('" onkeydown="');
		sb.append(CLAZZ);
		sb.append('.keydownSearchEv(event);"');
		sb.append('" value="');
		if(bv){
			sb.append(txtPlaceholder);
		} else {
			sb.append(val);
		}
		sb.append('" /><div class="csIcon csSearchNSelectIcon ');
		if(!bv){
			var CSICONCLEAR = this.getIconClearClass();
			sb.append(CSICONCLEAR);
		}
		sb.append('" onclick="');
		sb.append(CLAZZ);
		sb.append('.clickedIconEv(event);"');
		sb.append('>&nbsp;</div>');
		var rc = sb.toString(); sb=null;
		return rc;
	},
	showResults: function(el,targetEl,ar,val){
		try{
			var tid;
			if(targetEl){
				tid = Element.identify(targetEl);
			}
			var wrapEl = this.findDivWrapper(targetEl);
			if(targetEl){
				var obj;
				var myTargetEl = targetEl;
				var CLAZZ = this.getClazz();
				var CSS_NAV_FIRST = new String(' csNavFirst');
				var CSS_NAV_LAST = new String(' csNavLast');
				var CSS_NAV_ITEM1 = new String('csNavItem-1');
				var labelVal = this.getCustomDataAttributeDataSelected(wrapEl);
				var ar_size = -1;
				if(ar){
					ar_size = ar.length;
				}
				var arpro = this.parseAnchorlist(wrapEl,val);
				var arpro_size = -1;
				if(arpro){
					arpro_size = arpro.length;
				}
				var delim = this.getCustomDataAttributeLineDelimiter(wrapEl);
				// TODO this currently combines the filtering with the display of the results, it should be two separate functions to better allow the error display flows
				var bFirst = true;
				var bFirst2 = true;
				var bulEl = Element.hasClassName(targetEl,'csSearchNSelect-target');
				var sb = new StringBuffer();
				if(bulEl){
					var ar2 = Element.select(targetEl,'LI');
					if(ar2){
						var i2 = ar2.length;
						var obj2;
						var i3;
						var ar3;
						var obj3;
						var bFoundInput;
						while(i2--){
							obj2 = $(ar2[i2]);
							bFoundInput = false;
							if(obj2){
								ar3 = Element.select(obj2,'INPUT');
								if(ar3){
									i3 = ar3.length;
									while(i3--){
										obj3 = $(ar3[i3]);
										bFoundInput = true;
									}
								}
							}
							if(bFoundInput){
								bFirst = false;
							} else {
								//CS.v3.hideFunction(obj);
								Element.remove(obj2);// remove from DOM!
							}
						}
					}
				} else {
					sb.append('<ul class="csSearchNSelect-target">');
					var xEl = this.findFormInputFromWrapper(wrapEl);
					var tag;
					if(xEl){
						tag = xEl.tagName;
					}
					var isInsertSearch = false;
					switch(tag){
						case 'INPUT':
							//isInsertSearch = true;
							break;
						case 'SELECT':
							isInsertSearch = true;
							break;
						default:
							//NOOP
							break;
					}
					if(isInsertSearch){
						var txtPlaceholder = CS.v3.I18N.getPlaceholder(el);/* wrapEl */
						var searchField = this.createSearchField(val,bFirst,txtPlaceholder);
						if(!CS.v3.I18N.isEmpty(searchField)){
							sb.append('<li class="');
							sb.append(CSS_NAV_ITEM1);
							if(bFirst){
								sb.append(CSS_NAV_FIRST);
								bFirst = false;
							}
							sb.append('">');
							sb.append(searchField);
							sb.append('</li>');
						}
					}
				}
				var i;
				if(ar){
					var str;
					i = ar_size;
					if(i<1){
						var minStr = this.getCustomDataAttributeMinSize(el);
						var min = parseInt(minStr,10);
						var sz = CS.v3.I18N.getLength(val);
						var nonemsg;
						if(sz<min){
							nonemsg = this.getCustomDataAttributeMinMessage(el);
						} else {
							nonemsg = this.getCustomDataAttributeNoneMessage(el);
						}
						sb.append('<li class="');
						sb.append(CSS_NAV_ITEM1);
						if(bFirst){
							sb.append(CSS_NAV_FIRST);
							bFirst = false;
						}
						sb.append(CSS_NAV_LAST);
						sb.append('">');
						str = this.appendValues(delim,nonemsg);
						sb.append( this.decorateResults(el,str,delim,val,false,'',bFirst2,true) ); bFirst2 = false;
						sb.append('</li>');
					} else {
						var maxStr = this.getCustomDataAttributeMaxResults(el);
						var max = parseInt(maxStr,10);
						var j;
						if((max>0) && (i>max)){
							var maxmsg = this.getCustomDataAttributeMaxMessage(el);
							sb.append('<li class="');
							sb.append(CSS_NAV_ITEM1);
							if(bFirst){
								sb.append(CSS_NAV_FIRST);
								bFirst = false;
							}
							sb.append(CSS_NAV_LAST);
							sb.append('">');
							str = this.appendValues(delim,maxmsg);
							sb.append( this.decorateResults(el,str,delim,val,false,'',bFirst2,false) ); bFirst2 = false;
							sb.append('</li>');
						} else {
							var txt;
							var sar;
							var sbparms;
							var parms;
							while(i--){
								txt = ar[i];
								sb.append('<li class="');
								sb.append(CSS_NAV_ITEM1);
								if(bFirst){
									sb.append(CSS_NAV_FIRST);
									bFirst = false;
								}
								if((i==0) && (arpro_size <1)){
									sb.append(CSS_NAV_LAST);
								}
								if(obj==labelVal){
									sb.append(' csNavOn');
								}
								sb.append('">');
								str = txt + delim;
								sbparms = new StringBuffer();
								sbparms.append(' data-cssearchnselect-value="');
								sbparms.append(txt);
								sbparms.append('" onclick="');
								sbparms.append(CLAZZ);
								sbparms.append('.clickedItemEv(event);"');
								parms = sbparms.toString(); sbparms = null;
								sb.append( this.decorateResults(el,str,delim,val,true,parms,bFirst2,false) ); bFirst2 = false;
								sb.append('</li>');
							}
						}
					}
				}
				if(arpro){
					i = arpro_size;
					if(i>0){
						if(!bFirst){
							// add delimiter only when there are no items above
							sb.append('<li class="');
							sb.append(CSS_NAV_ITEM1);
							sb.append(' csMenuRule"></li>');
						}
					}
					while(i--){
						obj = arpro[i];
						sb.append('<li class="');
						sb.append(CSS_NAV_ITEM1);
						if(bFirst){
							sb.append(CSS_NAV_FIRST);
							bFirst = false;
						}
						if(i==0){
							sb.append(CSS_NAV_LAST);
						}
						sb.append('">');
						sb.append(obj);
						sb.append('</li>');
					}
				}
				if(!bulEl){
					sb.append('</ul>');
				}
				str = sb.toString(); sb=null;
				Element.insert(myTargetEl,{bottom:str});
			}
		}catch(e){
			this.log('showResults:',e);
		}
	},
	appendValues: function(delim,txt){
		var url = this.getDefaultUrl();
		var sb = new StringBuffer();
		sb.append(url);
		sb.append(delim);
		sb.append(txt);
		sb.append(delim);
		var rc = sb.toString(); sb=null;
		return rc;
	},
	decorateResults: function(el,str,delim,val,bDisplay,parms,bFirst2,bLast){
		//this.log('decorateResults:init');
		var rc;
		try{
			var CSS_NAV_ITEM1 = new String('csNavItem-1');
			var HASH = this.getDefaultUrl();
			var CLAZZ = this.getClazz();
			//var bDisplay = true;
			var url;
			var ar = CS.v3.I18N.split(str,delim);
			var i = ar.length;
			if(i>0){
				url = ar[0];
				ar.splice(0,1); // remove the first item
			}
			var sb = new StringBuffer();
			ar.reverse();
			sb.append('<a class="');
			if(bFirst2){
				sb.append('csNavFirstItem ');
			}
			if(bLast){
				sb.append('csNavLastItem ');
			}
			sb.append(CSS_NAV_ITEM1);
			sb.append('" tabindex="-1" href="');
			sb.append(HASH);
			sb.append('"');
			sb.append(parms);
			sb.append('>');
			var rows = ar.length;
			i = rows;
			var obj;
			var j;
			var hilite;
			var x;
			var val_lc = CS.v3.I18N.toLower(val);
			var obj_lc;
			var val_size;
			var obj_size;
			var strPre;
			var strMid;
			var strPost;
			var b2;
			while(i--){
				obj = ar[i];
				b = CS.v3.I18N.isEmpty(obj);
				if(!b){
					if(bDisplay){
						x = rows-i;
						sb.append('<div class="csSearchNSelect-row');
						sb.append(x);
						if(url!=HASH){
							sb.append(' title="');
							sb.append(url);
							sb.append('" onclick="');
							sb.append(CLAZZ);
							sb.append('.launchUrl(\'');
							sb.append(url);
							sb.append('\');" style="text-decoration:underline;"');// TODO, use a class instead!
						}
						sb.append('">');
						if(x==1){// only hilite the top row
							obj_lc = CS.v3.I18N.toLower(obj);
							j = obj_lc.indexOf(val_lc);
							if(j>-1){
								val_size = val.length;
								obj_size = obj.length;
								strPre = obj.substr(0,j);
								strMid = obj.substr(j,val_size);
								strPost = obj.substr(j+val_size,obj_size);
								sb.append(strPre);
								b2 = CS.v3.I18N.isEmpty(strMid);
								if(!b2){
									sb.append('<em>');
									sb.append(strMid);
									sb.append('</em>');
								}
								sb.append(strPost);
							} else {
								sb.append(obj);// not j
							}
						} else {
							sb.append(obj);// not x==1
						}
					} else {
						sb.append(obj);// default (non-pretty)
					}
					if(bDisplay){
						sb.append('</div>');
					} else {
						sb.append('<br />');
					}
				}
			}
			sb.append('</a>');
			rc = sb.toString(); sb=null;
		}
		catch(e){
			this.log('decorateResults:',e);
		}
		return rc;
	},
	getCustomDataAttributeValue: function(el){
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cssearchnselect-value','');
		return rc;
	},
	getCustomDataAttributeDataAjax: function(el){
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cssearchnselect-dataajax','');
		return rc;
	},
	setCustomDataAttributeDataAjax: function(el,val){
		CS.v3.I18N.setDataAttribute(el,'data-cssearchnselect-dataajax',val);
	},
	getCustomDataAttributeInputId: function(el){
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cssearchnselect-inputid','');
		return rc;
	},
	setCustomDataAttributeInputId: function(el,val){
		CS.v3.I18N.setDataAttribute(el,'data-cssearchnselect-inputid',val);
	},
	getCustomDataAttributeUrl: function(el,def){
		var rc = CS.v3.I18N.getDataAttribute(el,'data-csurl',def);
		return rc;
	},
	getDefaultUrl: function(){
		return new String('#');
	},
	setCustomDataAttributeUrl: function(el,val){
		CS.v3.I18N.setDataAttribute(el,'data-csurl',val);
	},
	getCustomDataAttributeDataSource: function(el){
		var def = CS.v3.I18N.findProperty(el,'SearchNSelect-datasource');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cssearchnselect-datasource',def);
		return rc;
	},
	setCustomDataAttributeDataSource: function(el,val){
		CS.v3.I18N.setDataAttribute(el,'data-cssearchnselect-datasource',val);
	},
	getCustomDataAttributeMaxResults: function(el){
		var def = CS.v3.I18N.findProperty(el,'SearchNSelect-maxresults');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cssearchnselect-maxresults',def);
		return rc;
	},
	getCustomDataAttributeMaxMessage: function(el){
		var def = CS.v3.I18N.findProperty(el,'SearchNSelect-maxmessage');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cssearchnselect-maxmessage',def);
		return rc;
	},
	getCustomDataAttributeNoneMessage: function(el){
		var def = CS.v3.I18N.findProperty(el,'SearchNSelect-nonemessage');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cssearchnselect-nonemessage',def);
		return rc;
	},
	getCustomDataAttributeMinSize: function(el){
		var def = CS.v3.I18N.findProperty(el,'SearchNSelect-minsize');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cssearchnselect-minsize',def);
		return rc;
	},
	getCustomDataAttributeMinMessage: function(el){
		var def = CS.v3.I18N.findProperty(el,'SearchNSelect-minmessage');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cssearchnselect-minmessage',def);
		return rc;
	},
	getCustomDataAttributeDataSelected: function(el){
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cssearchnselect-selected','');
		return rc;
	},
	setCustomDataAttributeDataSelected: function(el,val){
		CS.v3.I18N.setDataAttribute(el,'data-cssearchnselect-selected',val);
	},
	setCustomDataAttributeLineDelimiter: function(el,val){
		CS.v3.I18N.setDataAttribute(el,'data-cssearchnselect-linedelimiter',val);
	},
	getCustomDataAttributeLineDelimiter: function(el){
		var def = CS.v3.I18N.findProperty(el,'SearchNSelect-linedelimiter');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cssearchnselect-linedelimiter',def);
		return rc;
	},
	getCustomAttributeEventsHook: function(el){
		var def = CS.v3.I18N.findProperty(el,'SearchNSelect-eventshook');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cssearchnselect-eventshook',def);
		return rc;
	},
	getCustomDataAttributeMessageMask: function(el){
		var def = CS.v3.I18N.findProperty(el,'SearchNSelect-messageMask');
		var rc = CS.v3.I18N.getDataAttribute(el,'data-cssearchnselect-messageMask',def);
		return rc;
	},
	copyAttributesFromInputToWrapper: function(el,wrapEl){
		try{
			var datasource = this.getCustomDataAttributeDataSource(el);
			var b = CS.v3.I18N.isEmpty(datasource);
			if(!b){
				this.setCustomDataAttributeDataSource(wrapEl,datasource);
			}
			var url = this.getCustomDataAttributeDataAjax(el);
			b = CS.v3.I18N.isEmpty(url);
			if(!b){
				this.setCustomDataAttributeDataAjax(wrapEl,url);
			}
			var delim = this.getCustomDataAttributeLineDelimiter(el);
			b = CS.v3.I18N.isEmpty(delim);
			if(!b){
				this.setCustomDataAttributeLineDelimiter(wrapEl,delim);
			}
			var txt = CS.v3.I18N.getValue(el,false) + delim;
			var val = this.getLabelDisplay(txt,delim,0);
			b = CS.v3.I18N.isEmpty(val);
			if(!b){
				this.setCustomDataAttributeDataSelected(wrapEl,val);
			}
			var str = CS.v3.I18N.getPlaceholder(el);
			b = CS.v3.I18N.isEmpty(str);
			if(!b){
				CS.v3.I18N.setPlaceholder(wrapEl,str);
			}
			var myid = Element.identify(el);
			this.setCustomDataAttributeInputId(wrapEl,myid);
		}
		catch(e){
			this.log('copyAttributesFromInputToWrapper:',e);
		}
	},
	focusSearchNSelectEv: function(event){
		var el = $(Event.element(event));
		if(el){
			var cls = this.getHolderClass();
			var b = Element.hasClassName(el,cls);
			if(b){
				this.showSearchNSelect(el);
				Event.stop(event);//Event.preventDefault(event);
			}
		}
	},
	getHolderClass: function(){
		return new String('csSearchNSelect-holder');
	},
	getIconClearClass: function(){
		return new String('csSearchNSelectIcon-clear');
	},
	showSearchNSelect: function(el){
		//this.log('showSearchNSelect:init:el=' + el);
		var bb = this.isUsingSingleDiv(el);
		if(bb){
			var x = Element.identify(el);
			var val = 'TODO';
			var wrapEl = this.findDivWrapper(el);
			var inputid = this.getCustomDataAttributeInputId(wrapEl);
			var contentid = this.createContentId(inputid,bb);//'csSearchNSelectContent';
			var labelStr;
			var tag = el.tagName;
			switch(tag){
				case 'INPUT':
					labelStr = CS.v3.I18N.getValue(el,true);
					break;
				default:
					labelStr = el.innerHTML;
					break;
			}
			var str = this.createContentAll(el,labelStr,contentid,'');
			var targetEl = CS.v3.castToEl(contentid);
			//var targetEl = this.findSearchContent(wrapEl);
			var overlay = this.findSingleSearchContainer(el);
			CS.v3.updateFix(overlay,str);
			var elinput = CS.v3.castToEl(inputid);
			var ar = this.getArrayFromInput(elinput);
			this.showResults(wrapEl,targetEl,ar,val);
			this.showEl(el);
		}
		//this.log('showSearchNSelect:exit');
	},
	getArrayFromInput: function(el){
		var rc;
		if(el){
			var tag = el.tagName;
			switch(tag){
				case 'SELECT':
					rc = this.parseDropdown(el);
					break;
				case 'INPUT':
					rc = this.parseTextInput(el);
					break;
				default:
					// NOOP
					break;
			}
		}
		return rc;
	},
	createContentId: function(prefix,suffix){
		var rc = prefix + '_div_' + suffix;
		return rc;
	},
	insertContextualMenu: function(wrapEl,elinput,ar){
		try{
			var rc;
			var myid = Element.identify(wrapEl);
			var targetEl;
			this.copyAttributesFromInputToWrapper(elinput,wrapEl);
			var labelVal = this.getCustomDataAttributeDataSelected(wrapEl);
			var delim = this.getCustomDataAttributeLineDelimiter(wrapEl);
			var labelStr;
			var inputTag = elinput.tagName;
			var inputid = Element.identify(elinput);
			if(inputTag==='SELECT'){
				labelStr = this.getLabelDisplay(labelVal,delim,0);
			} else {
				var txtPlaceholder = CS.v3.I18N.getPlaceholder(elinput);/* wrapEl */
				labelStr = this.createSearchField(labelVal,false,txtPlaceholder);
			}
			var contentid;
			var sb = new StringBuffer();
			var bb = this.isUsingSingleDiv(elinput);// either attribute should work here
			if(bb){
				var txt = CS.v3.I18N.getValue(elinput,false) + delim;
				var val = this.getLabelDisplay(txt,delim,0);
				var cls = this.getHolderClass();
				sb.append('<div class="csNav-x csMenu-x csSearchNSelect">');
				sb.append('<ul class="csNavList"><li class="csNavToggle-0 csNavFirst csMenuContextual"><div class="csNavWrap">');
				var sb2 = new StringBuffer();
				sb2.append('<a href="#" class="csNavToggle">');
				sb2.append('<div class="csNav-x csMenu-x csSearchNSelect ');
				sb2.append(cls);
				sb2.append('">');
				switch(inputTag){
					case 'SELECT':
						sb2.append(labelVal);
						break;
					case 'INPUT':
						sb2.append('<input type="text" class="csTextField ');
						sb2.append(cls);
						sb2.append('" value="' + labelVal + '" />');
						break;
					default:
						// NOOP
						break;
				}
				sb2.append('</div>');
				sb2.append('</a>');
				var str2 = sb2.toString(); sb2 = null;
				sb.append(str2);
				sb.append('</div>');
				var htm = this.createContentPanel(contentid,'TEST');
				sb.append(htm);
				// test end
				sb.append('</li></ul>');
				var FLAG = new String('csSearchNSelect-initevt');
				var b = Element.hasClassName(wrapEl,FLAG);
				if(!b){
					Element.addClassName(wrapEl,FLAG);
					Event.observe(wrapEl,'click', function(event){ this.focusSearchNSelectEv(event); }.bind(this));
					//Event.observe(wrapEl,'focus', function(event){ this.focusSearchNSelectEv(event); }.bind(this));
				}
				//var tmp = sb.toString(); sb=null;
			} else {
				contentid = this.createContentId(inputid,bb);
				var tmp = this.createContentAll(elinput,labelStr,contentid,'');
				sb.append(tmp);
			}
			var str = sb.toString(); sb=null;
			Element.insert(wrapEl,{bottom: str});
			//if(contentid){
			//	targetEl = CS.v3.castToEl(contentid);
			//}
			targetEl = this.findSearchContent(wrapEl);
			this.showResults(wrapEl,targetEl,ar,'');
		}
		catch(e){
			this.log('insertContextualMenu:',e);
		}
		return rc;
	},
	createContentAll: function(elinput,labelStr,contentid,content){
		var sb = new StringBuffer();
		sb.append('<div class="csNav-x csMenu-x csClick csAccordian csSearchNSelect">');
		var bb = this.isUsingSingleDiv(elinput);// NOTE: either attribute should work here!
		if(!bb){
			sb.append('<div class="csPanelCover"></div>');
		}
		var contentLabel = this.createContentLabel(labelStr);
		var htm = this.createContentList(contentLabel,contentid,content);
		sb.append(htm);
		sb.append('</div>');
		var rc = sb.toString(); sb=null;
		return rc;
	},
	createContentLabel: function(labelStr){
		var sb = new StringBuffer();
		sb.append('<a data-csoverlaycontextnode="div.csFields" data-alignoverlay="" class="csNavToggle" href="#">');
		sb.append(labelStr);
		sb.append('</a>');
		var rc = sb.toString(); sb=null;
		return rc;
	},
	createContentList: function(contentLabel,contentid,content){
		var sb = new StringBuffer();
		sb.append('<ul class="csNavList"><li class="csNavToggle-0 csNavFirst csMenuContextual"><div class="csNavWrap">');
		sb.append(contentLabel);
		sb.append('</div>');
		var htm = this.createContentPanel(contentid,content);
		sb.append(htm);
		sb.append('</li></ul>');
		var rc = sb.toString(); sb=null;
		return rc;
	},
	createContentPanel: function(contentid,content){
		var sb = new StringBuffer();
		sb.append('<div class="csNavContent"><div class="csPanel"><div class="csSearchPanelTopLeft"><div class="csSearchPanelTopRight"><div class="csPanelContent">');
		var htm = this.createContentBody(contentid,content);
		sb.append(htm);
		sb.append('</div></div></div><div class="csSearchPanelBottomLeft"><div class="csSearchPanelBottomRight"></div></div></div></div>');
		var rc = sb.toString(); sb=null;
		return rc;
	},
	createContentBody: function(contentid,content){
		var sb = new StringBuffer();
		sb.append('<div');
		var b = CS.v3.I18N.isEmpty(contentid);
		if(!b){
			sb.append(' id="');
			sb.append(contentid);
			sb.append('"');
		}
		sb.append(' class="csPanelScroll csSearchNSelect-content">');
		sb.append(content);
		sb.append('</div>');
		var rc = sb.toString(); sb=null;
		return rc;
	},
	getLabelDisplay: function(str,delim,idx){
		var rc;
		var i = str.indexOf(delim);
		if(i>-1){
			var ar = CS.v3.I18N.split(str,delim);
			var j = ar.length;
			if(j>idx){
				rc = ar[idx];
			} else {
				rc = str;
			}
		} else {
			rc = str;
		}
		return rc;
	},
	stripLabelUrl: function(str,delim){
		var rc;
		var i = str.indexOf(delim);
		if(i>-1){
			rc = str.substring(i+1);//to end
		}
		return rc;
	},
	isStartsWith: function(el){
		var rc = true;
		if(el){
			var b = Element.hasClassName(el,'csSearchNSelect-contains');
			if(b){
				rc = false;
			}
		}
		return rc;
	},
	isEvents: function(el){
		var rc = false;
		if(el){
			var b = Element.hasClassName(el,'csSearchNSelect-events');
			if(b){
				rc = true;
			}
		}
		return rc;
	},
	isUsingSingleDiv: function(el){
		/*NOTE: 'el' is passed, but not currently used */
		var rc = false;
		var obj = this.findSingleSearchContainer(el);
		if(obj){
			rc = true;
		}
		return rc;
	},
	findSingleSearchContainer: function(el){
		/*NOTE: 'el' is passed, but not currently used */
		//var rc = CS.v3.ContainerSearchNSelect;
		//var rc = $('div#csSearchNSelect');
		var rc;
		return rc;
	},
	launchUrl: function(url){
		/* NOTE: the return code 'could' be used to control/close the window later */
		var rc = window.open(url,'_new');
		return rc;
	},
	tryExternalEventsHook: function(inputEl,event){
		// WARNING: a 'null' event will be passed for 'load'
		var isEvents = this.isEvents(inputEl);
		if(isEvents){
			try{
				var hook = this.getCustomAttributeEventsHook(inputEl);
				var action = CS.v3.I18N.findProperty(inputEl,hook);
				if(action instanceof Function){
					action(inputEl,event);
				} else {
					var s = 'action not a Function was:[' + action + '])';
					this.log('tryExternalEventsHook:s=' + s);
				}
			}
			catch(e){
				this.log('tryExternalEventsHook:',e);
			}
		}
	},
	showEl: function(el){
		//this.log('showEl:el=' + el);
		var event=null;
		var obj = this.findDivWrapper(el);
		el.wrap = obj;
		el.target = el;
		var overlay = this.findSingleSearchContainer(el);
		var ar = Element.select(overlay,'.csPanelScroll');
		if(ar){
			var targetDiv = ar[0];
			var scrollEl = Element.up(el,'div.csPanelScroll');
			var htm = scrollEl.innerHTML;
			targetDiv.innerHTML = htm;
			CS.v3.SearchNSelectVis.show(el,event);
		}
	},
	hideEl: function(el){
		//this.log('hideEl:el=' + el);
		var event=null;
		var obj = this.findDivWrapper(el);
		el.wrap = obj;
		CS.v3.SearchNSelectVis.hide(el,event);
	},
	log: function(s,e){
		CS.v3.Logger.log('SearchNSelect:' + s,e);
	}
};
CS.v3.SearchNSelectVis = {
	show: function(m){
		var event = null;
		//Element.update(CS.v3.contextualMenuScroll,obj.content);
		var obj = new CS.v3.OverlaysObj(m,event);
		var overlay = CS.v3.ContainerSearchNSelect;
		if(overlay){
			overlay.style.left = (obj.left - obj.containerWrapLeft) + 'px';
			overlay.style.top = (obj.top) + 'px';// + obj.height + 'px';
			//CS.v3.calendarActive = obj.target;
			overlay.style.display = 'block';
			//if(CS.v3.Base.prototype.coverIE){
				CS.v3.OverlayIE6IFrame.show(overlay,obj);
			//}
		}
	},
	hide: function(){
		var overlay = CS.v3.ContainerSearchNSelect;
		if(overlay){
			overlay.style.display = 'none';
		}
	}
};
setTimeout(function(){
	CS.v3.I18N.addProperty('SearchNSelect-datasource','','');
	CS.v3.I18N.addProperty('SearchNSelect-minsize','','0');//'2'
	CS.v3.I18N.addProperty('SearchNSelect-minmessage','','You must enter at least {2} characters to perform a search.');
	CS.v3.I18N.addProperty('SearchNSelect-maxresults','','10');
	CS.v3.I18N.addProperty('SearchNSelect-linedelimiter','','|');
	CS.v3.I18N.addProperty('SearchNSelect-maxmessage','','Too many results (10+), please refine your search criteria.');
	CS.v3.I18N.addProperty('SearchNSelect-nonemessage','','No results match your search criteria.');
	CS.v3.I18N.addProperty('SearchNSelect-messageMask','','###');
	CS.v3.I18N.addProperty('SearchNSelect-eventshook','',
	function(inputEl,event){
/* BEGIN EXAMPLE
		//WARNING: 'null' event passed for 'load'
		var el;
		var typ;
		var key;
		if(event!=null){
			el = $(Event.element(event));
			typ = event.type;
			key = event.keyCode;
		}
		var s = 'default hook triggered:[typ=' + typ + '|key=' + key + '|el=' + el + '|inputEl=' + inputEl + ']';
		CS.v3.Logger.log('SearchNSelect:' + s);
END EXAMPLE */
	});
});
