OHUKSessionTimer = Class.create( {
	timerID : null,
	intervalID : null,
	initialize : function(params) {
		this.params = {
			expires : 720,
			popupTitle : 'WarningAlert',
			popupOptions : 'left=400,top=200,height=600,width=400',
			popupURL : '',
			keepAliveURL : '',
			logoffFormID : '',
			logoffLinkID : '',
			logoffImplicitLinkID : '',
			exitProcessingFormID : '',
			refreshURL : ''
		};
		Object.extend(this.params, params || {});
		this.startTimer();
	},
	startTimer : function() {
      this.timerID = setTimeout(this.timeOutPopup.bind(this), 1000*this.params.expires);
	},
	timeOutPopup : function() {
		var url = this.params.popupURL + "?_ts=" + (new Date()).valueOf();
		window.open(url, this.params.popupTitle, this.params.popupOptions);
	},
	/*If YES is clicked. */
	continueSession : function(cwindow) {
		clearTimeout(this.timerID);
		clearInterval(this.intervalID);

		if (typeof cwindow != 'undefined') {
			cwindow.close();
		}
		this.startTimer();

		/* PIB URL. */
		if (this.params.keepAliveURL) {
			new Ajax.Request(this.params.keepAliveURL, {
				requestHeaders : {
					"If-Modified-Since" : "Sat, 1 Jan 2000 00:00:00 GMT"
				},
				onException : function() {
					window.location.reload(true);
				}
			});
		}
		/*  To Keep MTP Alive.*/
		new Ajax.Request(this.params.refreshURL, {
			requestHeaders : {
				"If-Modified-Since" : "Sat, 1 Jan 2000 00:00:00 GMT"
			},
			onException : function() {
				window.location.reload(true);
			}
		});
	},

	/*This function will get invoked when user clicks on No button to terminate the session*/
	timedOut : function(cwindow,submitButtonElem) {		
		clearInterval(this.intervalID);
		/*JEP URL*/
		if ($(this.params.logoffLinkID)) {			
			/*If we are going to JEP then close the popup*/
			cwindow.close();
			var element = $(this.params.logoffLinkID);
			if ((Prototype.Browser.IE) && element.click) {
				element.click();
			} else {
				CS.v3.fireOnclick(element);
			}
		}
		/*Added for defect 16714 start */
		/*If We can not go to JEP, then try exit processing and logoff*/
		else if (submitButtonElem) {
		
			var element = submitButtonElem;
			if ((Prototype.Browser.IE) && element.click) {
				element.click();
			} else {
				CS.v3.fireOnclick(element);
			}

		}		
		/*Added for defect 16714 end */
	},
	/*This function is called when user do nothing and will call exit processing and logoff. */
	implicitTimedOut : function(cwindow,submitButtonel) {
		
		clearInterval(this.intervalID);

		/*Added for defect 16714 start */
		if (submitButtonel) {

			var element = submitButtonel;

			if ((Prototype.Browser.IE) && element.click) {

				element.click();
			} else {

				CS.v3.fireOnclick(element);
			}

		}
	},

	startCountdown : function(cwindow, el,submitButtonel) {
		this.intervalID = setInterval( function() {
			if (!cwindow.closed) {
				cwindow.focus();
				var count = el.value;
				el.value = --count;
				if (count <= 0) {
					this.implicitTimedOut(cwindow,submitButtonel);
				}
			} else { /*This situation should not appear.*/
				this.continueSession()
			}
		}.bind(this), 1000);
	},

	/*Added for defect 16714 start */
	/*This function is called from exitProcessing.jsp. The JSP is called after exit procssing. The function should either do IDV logoff for customer or URL log off for agent*/
	exitLogOff : function(cwindow) {		
		cwindow.close();
		if ($(this.params.logoffFormID)) {
			$(this.params.logoffFormID).submit();
		} else if ($(this.params.logoffImplicitLinkID)) {
			var element = $(this.params.logoffImplicitLinkID);

			if ((Prototype.Browser.IE) && element.click) {

				element.click();
			} else {

				CS.v3.fireOnclick(element);
			}
		}

	}
/*Added for defect 16714 end */

});
