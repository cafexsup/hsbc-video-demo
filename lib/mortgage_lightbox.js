(function($){

$(document).ready(function() {

	$('[class*=triggerModalDetails]').unbind('click').bind('click', function(e) {
		e.preventDefault();
		var id = $(this).attr('class').replace('overlayLaunchLink triggerModalDetails','');
		var l = $('#lightviewDetails' + parseInt(id,10))
		l.show();
		l.find('.lightview').css('background', 'white').find('h3').css('color', 'black');
		l.find('.lightview a').css('color', '#db0011');
		l.css({top: '50%', left:'50%', 'margin-left': '-310px', 'width': 'auto', height: 'auto', position: 'fixed', 'z-index': '10000'});
		l.find('.lightview .column').css('width', 'auto').css('padding', '0px');
		l.css('margin-top', '-' + (l.height()/2) + 'px');
		l.find('.lightview .close a').unbind('click').bind('click', function(e){
			e.preventDefault();
			$(this).parents('.jqmWindow').hide();
			$('.jqmOverlay').hide();
		});
		$('.jqmOverlay').show();
	});

	$('body .contactRow .question').prepend('<div class="jqmOverlay" style="display: none; background-color: #4e4e4e; height: 100%; width: 100%; position: fixed; left: 0px; top: 0px; z-index: 40; opacity: 0.4; filter: alpha(opacity=40);"></div>');
	$('.jqmOverlay').click(function() {
		$('.jqmWindow').hide();
		$(this).hide();
	});

	$('.jqmOverlay, .jqmWindow').appendTo($('#top')).appendTo($('.mtpExtContent').first());

	});

})(jQuery);